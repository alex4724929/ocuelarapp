# frozen_string_literal: true

Rails.application.routes.draw do
  scope :api do
    scope :v1 do
      use_doorkeeper do
        skip_controllers :applications, :authorized_applications
      end
    end
    scope :v2 do
      use_doorkeeper do
        skip_controllers :applications, :authorized_applications
      end
    end
  end

  namespace :api do
    namespace :v1 do
      resources :reset, only: [:index]
      resources :reset2, only: [:index]

      namespace :doctor do
        resources :drop do
          resources :drop_info, path: :info
          resources :drop_prescription, path: :prescription
        end
        resources :patient
        resources :doctor
        resources :admin
        resources :ocuelar
        resources :message, only: [:index, :show, :create]
        get "message/:id/last", to: "message#last_message"
      end

      namespace :patient do
        resources :daily
        resources :monthly
        resources :notification, only: [:index]
        namespace :doctor do
          resources :message, only: [:index, :create]
          get "message/last", to: "message#last_message"
        end
        namespace :message do
          resources :doctor
        end
        namespace :drop do
          resources :info, only: [:index]
        end
        namespace :friend do
          scope :request do
            get "send", to: "request#index_send"
            post "send", to: "request#create_send"
            get "receive", to: "request#index_receive"
            patch "receive/:id", to: "request#update_receive"
          end
          resources :list, only: [:index]
          resources :message
          get "message/:id/last", to: "message#last_message"
          # resources :messages
        end
      end

      namespace :device do
        resources :online, only: [:create]
        resources :drops, only: [:create]
      end

      resources :user, only: [:show, :update]
    end

    namespace :v2 do
      namespace :doctor do
        scope :ble do
          post "pair/:patient_id", to: "ble#pair"
          post "unpair/:patient_id", to: "ble#unpair"
        end
      end
      namespace :device do
        resources :drops, only: [:create]
      end
    end
  end
end
