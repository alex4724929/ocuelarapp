# frozen_string_literal: true

# config valid for current version and patch releases of Capistrano
lock "~> 3.17.2"

set :application, "ocuelar"
set :repo_url, "git@gitlab.com:hy_ong/ocuelar.git"
set :branch, "main"
set :repo_tree, "server"
set :deploy_to, "/home/deploy/#{fetch(:application)}"

append :linked_files, "config/master.key"
append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "tmp/webpacker", "vendor", "storage"
