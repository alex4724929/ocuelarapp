# frozen_string_literal: true

require_relative "boot"

require "rails/all"

Bundler.require(*Rails.groups)

module Ocuelar
  class Application < Rails::Application
    config.load_defaults(7.0)

    # config.time_zone = "Central Time (US & Canada)"
    config.time_zone = "Pacific Time (US & Canada)"
    config.beginning_of_week = :sunday

    config.api_only = true

    config.autoload_paths << Rails.root.join("lib")
    config.after_initialize do
      require "oauth_token_response"
    end

    config.active_job.queue_adapter = :sidekiq
  end
end
