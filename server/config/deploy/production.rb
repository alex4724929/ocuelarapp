# frozen_string_literal: true

set :stage, :production
set :rails_env, :production

server "96.126.102.87", user: "deploy", roles: ["app", "db", "web"]
