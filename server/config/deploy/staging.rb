# frozen_string_literal: true

set :stage, :staging
set :rails_env, :staging

server "172.105.193.121", user: "deploy", roles: ["app", "db", "web"]
