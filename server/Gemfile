# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.2.2"

# Bundle edge Rails instead: gem "rails", github: "rails/rails", branch: "main"
gem "rails", "~> 7.0", ">= 7.0.6"

# Use postgresql as the database for Active Record
gem "pg", "~> 1.5", ">= 1.5.3"

# Use the Puma web server [https://github.com/puma/puma]
gem "puma", "~> 6.3"

# Build JSON APIs with ease [https://github.com/rails/jbuilder]
gem "jbuilder", "~> 2.11", ">= 2.11.5"

# Use Redis adapter to run Action Cable in production
gem "redis", "~> 5.0", ">= 5.0.6"

# Use Kredis to get higher-level data types in Redis [https://github.com/rails/kredis]
gem "kredis", "~> 1.3", ">= 1.3.0.1"

# Use Active Model has_secure_password [https://guides.rubyonrails.org/active_model_basics.html#securepassword]
gem "bcrypt", "~> 3.1", ">= 3.1.18"

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# Reduces boot times through caching; required in config/boot.rb
gem "bootsnap", "~> 1.16", require: false

# Use Active Storage variants [https://guides.rubyonrails.org/active_storage_overview.html#transforming-images]
gem "image_processing", "~> 1.12", ">= 1.12.2"

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem "rack-cors", "~> 2.0", ">= 2.0.1"

gem "devise", "~> 4.9", ">= 4.9.2"
gem "doorkeeper", "~> 5.6", ">= 5.6.6"
gem "sidekiq", "~> 7.1", ">= 7.1.2"
gem "sidekiq-cron", "~> 1.10", ">= 1.10.1"
gem "sidekiq-scheduler", "~> 5.0", ">= 5.0.3"

gem "capistrano", "~> 3.17", ">= 3.17.2"
gem "capistrano-passenger", "~> 0.2.1"
gem "capistrano-rails", "~> 1.6", ">= 1.6.3"
gem "capistrano-rbenv", "2.2.0"
gem "capistrano-sidekiq", "2.3.0"

group :development, :test do
  gem "debug", "~> 1.7", ">= 1.7.2", platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  gem "rubocop-performance", "~> 1.18", require: false
  gem "rubocop-rails", "~> 2.20", ">= 2.20.2", require: false
  gem "rubocop-shopify", "~> 2.14", require: false
end
