# frozen_string_literal: true

module OauthTokenResponse
  def body
    role_dict = {
      "doctor" => "2ZB83E2Z5DH6R4YA637LDC2U6PGVL63P",
      "patient" => "W4ABOIB4K1UR4SBP53N6V5RQTSCIGCM6",
      "admin" => "ELKYKXRG5TAZPXCADSSPT65KEH27XY5U",
    }

    additional_data = {}

    if @token["scopes"].include?("doctor")
      additional_data = {
        "role" => role_dict[Doctor.find_by(id: @token["resource_owner_id"]).role],
      }
    elsif @token["scopes"].include?("patient")
      patient = Patient.find_by(id: @token["resource_owner_id"])
      additional_data = {
        "role" => role_dict["patient"],
        "control_group" => patient.control_group,
        "devices" => patient.ocuelars,
      }
    end
    super.merge(additional_data)
  end
end
