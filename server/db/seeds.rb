# frozen_string_literal: true

# check doorkeeper application
doctor_api = Doorkeeper::Application.find_or_create_by(name: "doctor_api") do |app|
  app.redirect_uri = ""
  app.scopes = "doctor"
end

patient_api = Doorkeeper::Application.find_or_create_by(name: "patient_api") do |app|
  app.redirect_uri = ""
  app.scopes = "patient"
end

# check drops
drop_f = Drop.find_or_create_by(name: "flourometholone")
drop_v = Drop.find_or_create_by(name: "vigamox")
drop_k = Drop.find_or_create_by(name: "ketorolac")

# check & create admin
admin = Doctor.find_or_create_by(username: "admin") do |doctor|
  doctor.password = "123456"
  doctor.password_confirmation = "123456"
  doctor.role = "admin"
end

# check & create doctor
doctor = Doctor.find_or_create_by(username: "doctor") do |doctor|
  doctor.password = "123456"
  doctor.password_confirmation = "123456"
  doctor.role = "doctor"
end

# check & create patients
p1 = doctor.patients.find_or_create_by(username: "8330") do |patient|
  patient.af_uid = "8330"
  patient.password = "123456"
  patient.password_confirmation = "123456"
end
p1.ocuelars.find_or_create_by(ocuelar_id: "F8330_AJ3P") do |ocuelar|
  ocuelar.ocuelar_type = "flourometholone"
end
p1.ocuelars.find_or_create_by(ocuelar_id: "V8330_JE9P") do |ocuelar|
  ocuelar.ocuelar_type = "vigamox"
end
p1.ocuelars.find_or_create_by(ocuelar_id: "K8330_3JD9") do |ocuelar|
  ocuelar.ocuelar_type = "ketorolac"
end

p2 = doctor.patients.find_or_create_by(username: "8331") do |patient|
  patient.af_uid = "8331"
  patient.password = "123456"
  patient.password_confirmation = "123456"
end
p2.ocuelars.find_or_create_by(ocuelar_id: "F8331_AJ4F") do |ocuelar|
  ocuelar.ocuelar_type = "flourometholone"
end
p2.ocuelars.find_or_create_by(ocuelar_id: "V8331_F4L2") do |ocuelar|
  ocuelar.ocuelar_type = "vigamox"
end
p2.ocuelars.find_or_create_by(ocuelar_id: "K8331_0S83") do |ocuelar|
  ocuelar.ocuelar_type = "ketorolac"
end

# create prescriptions
(1..63).each do |i|
  pre_f = drop_f.prescriptions.find_by(days: i)
  pre_v = drop_v.prescriptions.find_by(days: i)
  pre_k = drop_k.prescriptions.find_by(days: i)

  if i >= 1 && i <= 3 # days 1-3
    drop_f.prescriptions.create!(days: i, count_1: 1, count_4: 1) if pre_f.nil?
    drop_v.prescriptions.create!(days: i, count_1: 1, count_2: 1, count_3: 1, count_4: 1) if pre_v.nil?
    drop_k.prescriptions.create!(days: i, count_1: 1, count_2: 1, count_3: 1, count_4: 1) if pre_k.nil?
  elsif i >= 4 && i <= 6 # days 4-6
    drop_f.prescriptions.create!(days: i, count_1: 1, count_4: 1) if pre_f.nil?
    drop_v.prescriptions.create!(days: i, count_1: 1, count_2: 1, count_3: 1, count_4: 1) if pre_v.nil?
    drop_k.prescriptions.create!(days: i) if pre_k.nil?
  elsif i == 7 # day 7
    drop_f.prescriptions.create!(days: i, count_1: 1, count_2: 1, count_3: 1, count_4: 1) if pre_f.nil?
    drop_v.prescriptions.create!(days: i, count_1: 1, count_2: 1, count_3: 1, count_4: 1) if pre_v.nil?
    drop_k.prescriptions.create!(days: i) if pre_k.nil?
  elsif i >= 8 && i <= 21 # weeks 2-3
    drop_f.prescriptions.create!(days: i, count_1: 1, count_2: 1, count_3: 1, count_4: 1) if pre_f.nil?
    drop_v.prescriptions.create!(days: i) if pre_v.nil?
    drop_k.prescriptions.create!(days: i) if pre_k.nil?
  elsif i >= 22 && i <= 35 # weeks 4-5
    drop_f.prescriptions.create!(days: i, count_1: 1, count_2: 1, count_3: 1) if pre_f.nil?
    drop_v.prescriptions.create!(days: i) if pre_v.nil?
    drop_k.prescriptions.create!(days: i) if pre_k.nil?
  elsif i >= 36 && i <= 49 # weeks 6-7
    drop_f.prescriptions.create!(days: i, count_1: 1, count_4: 1) if pre_f.nil?
    drop_v.prescriptions.create!(days: i) if pre_v.nil?
    drop_k.prescriptions.create!(days: i) if pre_k.nil?
  elsif i >= 50 && i <= 63 # weeks 8-9
    drop_f.prescriptions.create!(days: i, count_1: 1) if pre_f.nil?
    drop_v.prescriptions.create!(days: i) if pre_v.nil?
    drop_k.prescriptions.create!(days: i) if pre_k.nil?
  end
end
