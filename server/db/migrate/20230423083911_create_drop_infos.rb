# frozen_string_literal: true

class CreateDropInfos < ActiveRecord::Migration[7.0]
  def change
    create_table(:drop_infos) do |t|
      t.belongs_to(:drop, null: false, foreign_key: true)

      t.integer(:info_type, null: false, default: 0, comment: "內容類別")
      t.integer(:order, null: false, comment: "順序")
      t.text(:content, null: false, default: "", comment: "內容")

      t.timestamps

      t.index([:drop_id, :order], unique: true)
    end
  end
end
