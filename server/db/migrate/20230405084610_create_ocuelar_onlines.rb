# frozen_string_literal: true

class CreateOcuelarOnlines < ActiveRecord::Migration[7.0]
  def change
    create_table(:ocuelar_onlines) do |t|
      t.belongs_to(:ocuelar_device, null: false, foreign_key: true)

      t.timestamps
    end
  end
end
