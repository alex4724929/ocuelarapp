# frozen_string_literal: true

class CreateDrops < ActiveRecord::Migration[7.0]
  def change
    create_table(:drops) do |t|
      t.string(:name, null: false, comment: "藥品名稱")

      t.timestamps

      t.index(:name, unique: true)
    end
  end
end
