# frozen_string_literal: true

class CreateOcuelarDrops < ActiveRecord::Migration[7.0]
  def change
    create_table(:ocuelar_drops) do |t|
      t.belongs_to(:ocuelar_device, null: false, foreign_key: true)

      t.integer(:schedule, null: false, default: 0)
      t.integer(:timestamps, null: false, array: true, default: [])

      t.timestamps
    end
  end
end
