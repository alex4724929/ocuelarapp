# frozen_string_literal: true

class CreateDoorkeeperTables < ActiveRecord::Migration[7.0]
  def change
    create_table(:oauth_applications) do |t|
      t.string(:name, null: false)
      t.string(:uid, null: false)
      t.string(:secret, null: false)

      # Remove `null: false` if you are planning to use grant flows
      # that doesn't require redirect URI to be used during authorization
      # like Client Credentials flow or Resource Owner Password.
      t.text(:redirect_uri)
      t.string(:scopes, null: false, default: "")
      t.boolean(:confidential, null: false, default: true)
      t.timestamps(null: false)
    end

    add_index(:oauth_applications, :uid, unique: true)

    create_table(:oauth_access_tokens) do |t|
      t.references(:resource_owner, index: true)

      # Remove `null: false` if you are planning to use Password
      # Credentials Grant flow that doesn't require an application.
      t.references(:application, null: false)

      t.string(:token, null: false)

      t.string(:refresh_token)
      t.integer(:expires_in)
      t.string(:scopes)
      t.datetime(:created_at, null: false)
      t.datetime(:revoked_at)

      t.string(:previous_refresh_token, null: false, default: "")
    end

    add_index(:oauth_access_tokens, :token, unique: true)

    # See https://github.com/doorkeeper-gem/doorkeeper/issues/1592
    if ActiveRecord::Base.connection.adapter_name == "SQLServer"
      execute(<<~SQL.squish)
        CREATE UNIQUE NONCLUSTERED INDEX index_oauth_access_tokens_on_refresh_token ON oauth_access_tokens(refresh_token)
        WHERE refresh_token IS NOT NULL
      SQL
    else
      add_index(:oauth_access_tokens, :refresh_token, unique: true)
    end

    add_foreign_key(
      :oauth_access_tokens,
      :oauth_applications,
      column: :application_id,
    )

    # Uncomment below to ensure a valid reference to the resource owner's table
    # add_foreign_key :oauth_access_grants, <model>, column: :resource_owner_id
    # add_foreign_key :oauth_access_tokens, <model>, column: :resource_owner_id
  end
end
