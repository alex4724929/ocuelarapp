# frozen_string_literal: true

class DeviseCreatePatients < ActiveRecord::Migration[7.0]
  def change
    create_table(:patients) do |t|
      t.belongs_to(:doctor, null: false, foreign_key: true)

      ## Database authenticatable
      t.string(:username, null: false, default: "")
      t.string(:encrypted_password, null: false, default: "")

      ## Recoverable
      t.string(:reset_password_token)
      t.datetime(:reset_password_sent_at)

      ## Rememberable
      t.datetime(:remember_created_at)

      t.string(:af_uid, null: false)
      t.integer(:control_group, null: false, default: 0)
      t.integer(:post_op_phase, null: false, default: 0)
      t.datetime(:medication_start_at, null: false, default: Time.zone.now)
      t.float(:adherence, null: false, default: 0.0)

      t.boolean(:active, null: false, default: true)

      t.timestamps(null: false)
    end

    add_index(:patients, :username, unique: true)
    add_index(:patients, :af_uid, unique: true)
    add_index(:patients, :reset_password_token, unique: true)
  end
end
