# frozen_string_literal: true

class CreateFriendRequests < ActiveRecord::Migration[7.0]
  def change
    create_table(:friend_requests) do |t|
      t.belongs_to(:sender, null: false, foreign_key: { to_table: :patients })
      t.belongs_to(:receiver, null: false, foreign_key: { to_table: :patients })

      t.integer(:status, null: false, default: 0)
      t.datetime(:response_at, null: true)

      t.timestamps
    end
  end
end
