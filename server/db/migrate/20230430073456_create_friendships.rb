# frozen_string_literal: true

class CreateFriendships < ActiveRecord::Migration[7.0]
  def change
    create_table(:friendships) do |t|
      t.belongs_to(:patient, null: false)
      t.belongs_to(:friend, null: false)

      t.timestamps
    end
  end
end
