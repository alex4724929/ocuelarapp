# frozen_string_literal: true

class UpdateOcuelarDevice < ActiveRecord::Migration[7.0]
  def change
    # set id can null and remove unique
    change_column_null(:ocuelar_devices, :ocuelar_id, true)
  end
end
