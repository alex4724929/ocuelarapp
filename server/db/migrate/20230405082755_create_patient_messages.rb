# frozen_string_literal: true

class CreatePatientMessages < ActiveRecord::Migration[7.0]
  def change
    create_table(:patient_messages) do |t|
      t.belongs_to(:patient, null: false, foreign_key: true)

      t.integer(:message_type, null: false, default: 0)
      t.boolean(:is_from_doctor, null: false, default: false)
      t.integer(:status, null: false, default: 0)

      t.text(:content, null: false)

      t.timestamps
    end
  end
end
