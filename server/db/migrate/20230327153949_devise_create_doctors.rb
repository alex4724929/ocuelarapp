# frozen_string_literal: true

class DeviseCreateDoctors < ActiveRecord::Migration[7.0]
  def change
    create_table(:doctors) do |t|
      ## Database authenticatable
      t.string(:username, null: false, default: "")
      t.string(:encrypted_password, null: false, default: "")

      ## Recoverable
      t.string(:reset_password_token)
      t.datetime(:reset_password_sent_at)

      ## Rememberable
      t.datetime(:remember_created_at)

      t.integer(:role, null: false, default: 0)
      t.boolean(:active, null: false, default: true)

      t.timestamps(null: false)
    end

    add_index(:doctors, :username, unique: true)
    add_index(:doctors, :reset_password_token, unique: true)
  end
end
