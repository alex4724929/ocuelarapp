# frozen_string_literal: true

class AddCountToOcuelarDrop < ActiveRecord::Migration[7.0]
  def change
    add_column(:ocuelar_drops, :count, :integer, default: 0)
  end
end
