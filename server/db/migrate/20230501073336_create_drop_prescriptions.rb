# frozen_string_literal: true

class CreateDropPrescriptions < ActiveRecord::Migration[7.0]
  def change
    create_table(:drop_prescriptions) do |t|
      t.belongs_to(:drop, null: false, foreign_key: true)

      t.integer(:days, null: false)

      t.integer(:start_hour_1, null: false, default: 0)
      t.integer(:count_1, null: false, default: 0)

      t.integer(:start_hour_2, null: false, default: 11)
      t.integer(:count_2, null: false, default: 0)

      t.integer(:start_hour_3, null: false, default: 15)
      t.integer(:count_3, null: false, default: 0)

      t.integer(:start_hour_4, null: false, default: 19)
      t.integer(:count_4, null: false, default: 0)

      t.timestamps

      t.index([:days, :drop_id], unique: true)
    end
  end
end
