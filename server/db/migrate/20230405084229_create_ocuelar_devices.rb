# frozen_string_literal: true

class CreateOcuelarDevices < ActiveRecord::Migration[7.0]
  def change
    create_table(:ocuelar_devices) do |t|
      t.belongs_to(:patient, null: false, foreign_key: true)

      t.string(:ocuelar_id, null: false, unique: true)
      t.integer(:ocuelar_type, null: false, default: 0)

      t.timestamps
    end
  end
end
