# frozen_string_literal: true

class CreateFriendMessages < ActiveRecord::Migration[7.0]
  def change
    create_table(:friend_messages) do |t|
      t.belongs_to(:friendship, null: false, foreign_key: true)

      t.integer(:patient_id, null: false, default: false)

      t.integer(:message_type, null: false, default: 0)
      t.integer(:status, null: false, default: 0)

      t.text(:content, null: false)

      t.timestamps
    end
  end
end
