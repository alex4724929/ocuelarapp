# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_07_05_154351) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "doctors", force: :cascade do |t|
    t.string "username", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "role", default: 0, null: false
    t.boolean "active", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["reset_password_token"], name: "index_doctors_on_reset_password_token", unique: true
    t.index ["username"], name: "index_doctors_on_username", unique: true
  end

  create_table "drop_infos", force: :cascade do |t|
    t.bigint "drop_id", null: false
    t.integer "info_type", default: 0, null: false, comment: "內容類別"
    t.integer "order", null: false, comment: "順序"
    t.text "content", default: "", null: false, comment: "內容"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["drop_id", "order"], name: "index_drop_infos_on_drop_id_and_order", unique: true
    t.index ["drop_id"], name: "index_drop_infos_on_drop_id"
  end

  create_table "drop_prescriptions", force: :cascade do |t|
    t.bigint "drop_id", null: false
    t.integer "days", null: false
    t.integer "start_hour_1", default: 0, null: false
    t.integer "count_1", default: 0, null: false
    t.integer "start_hour_2", default: 11, null: false
    t.integer "count_2", default: 0, null: false
    t.integer "start_hour_3", default: 15, null: false
    t.integer "count_3", default: 0, null: false
    t.integer "start_hour_4", default: 19, null: false
    t.integer "count_4", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["days", "drop_id"], name: "index_drop_prescriptions_on_days_and_drop_id", unique: true
    t.index ["drop_id"], name: "index_drop_prescriptions_on_drop_id"
  end

  create_table "drops", force: :cascade do |t|
    t.string "name", null: false, comment: "藥品名稱"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_drops_on_name", unique: true
  end

  create_table "friend_messages", force: :cascade do |t|
    t.bigint "friendship_id", null: false
    t.integer "patient_id", default: 0, null: false
    t.integer "message_type", default: 0, null: false
    t.integer "status", default: 0, null: false
    t.text "content", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["friendship_id"], name: "index_friend_messages_on_friendship_id"
  end

  create_table "friend_requests", force: :cascade do |t|
    t.bigint "sender_id", null: false
    t.bigint "receiver_id", null: false
    t.integer "status", default: 0, null: false
    t.datetime "response_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["receiver_id"], name: "index_friend_requests_on_receiver_id"
    t.index ["sender_id"], name: "index_friend_requests_on_sender_id"
  end

  create_table "friendships", force: :cascade do |t|
    t.bigint "patient_id", null: false
    t.bigint "friend_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["friend_id"], name: "index_friendships_on_friend_id"
    t.index ["patient_id"], name: "index_friendships_on_patient_id"
  end

  create_table "oauth_access_tokens", force: :cascade do |t|
    t.bigint "resource_owner_id"
    t.bigint "application_id", null: false
    t.string "token", null: false
    t.string "refresh_token"
    t.integer "expires_in"
    t.string "scopes"
    t.datetime "created_at", null: false
    t.datetime "revoked_at"
    t.string "previous_refresh_token", default: "", null: false
    t.index ["application_id"], name: "index_oauth_access_tokens_on_application_id"
    t.index ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true
    t.index ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id"
    t.index ["token"], name: "index_oauth_access_tokens_on_token", unique: true
  end

  create_table "oauth_applications", force: :cascade do |t|
    t.string "name", null: false
    t.string "uid", null: false
    t.string "secret", null: false
    t.text "redirect_uri"
    t.string "scopes", default: "", null: false
    t.boolean "confidential", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["uid"], name: "index_oauth_applications_on_uid", unique: true
  end

  create_table "ocuelar_devices", force: :cascade do |t|
    t.bigint "patient_id", null: false
    t.string "ocuelar_id"
    t.integer "ocuelar_type", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["patient_id"], name: "index_ocuelar_devices_on_patient_id"
  end

  create_table "ocuelar_drops", force: :cascade do |t|
    t.bigint "ocuelar_device_id", null: false
    t.integer "schedule", default: 0, null: false
    t.integer "timestamps", default: [], null: false, array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "count", default: 0
    t.index ["ocuelar_device_id"], name: "index_ocuelar_drops_on_ocuelar_device_id"
  end

  create_table "ocuelar_onlines", force: :cascade do |t|
    t.bigint "ocuelar_device_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ocuelar_device_id"], name: "index_ocuelar_onlines_on_ocuelar_device_id"
  end

  create_table "patient_messages", force: :cascade do |t|
    t.bigint "patient_id", null: false
    t.integer "message_type", default: 0, null: false
    t.boolean "is_from_doctor", default: false, null: false
    t.integer "status", default: 0, null: false
    t.text "content", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["patient_id"], name: "index_patient_messages_on_patient_id"
  end

  create_table "patients", force: :cascade do |t|
    t.bigint "doctor_id", null: false
    t.string "username", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "af_uid", null: false
    t.integer "control_group", default: 0, null: false
    t.integer "post_op_phase", default: 0, null: false
    t.datetime "medication_start_at", default: "2023-07-25 16:17:44", null: false
    t.float "adherence", default: 0.0, null: false
    t.boolean "active", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["af_uid"], name: "index_patients_on_af_uid", unique: true
    t.index ["doctor_id"], name: "index_patients_on_doctor_id"
    t.index ["reset_password_token"], name: "index_patients_on_reset_password_token", unique: true
    t.index ["username"], name: "index_patients_on_username", unique: true
  end

  add_foreign_key "drop_infos", "drops"
  add_foreign_key "drop_prescriptions", "drops"
  add_foreign_key "friend_messages", "friendships"
  add_foreign_key "friend_requests", "patients", column: "receiver_id"
  add_foreign_key "friend_requests", "patients", column: "sender_id"
  add_foreign_key "oauth_access_tokens", "oauth_applications", column: "application_id"
  add_foreign_key "ocuelar_devices", "patients"
  add_foreign_key "ocuelar_drops", "ocuelar_devices"
  add_foreign_key "ocuelar_onlines", "ocuelar_devices"
  add_foreign_key "patient_messages", "patients"
  add_foreign_key "patients", "doctors"
end
