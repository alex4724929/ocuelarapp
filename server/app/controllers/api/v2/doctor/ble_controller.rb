# frozen_string_literal: true

class Api::V2::Doctor::BleController < Api::V2::DoctorController
  def pair
  end

  def unpair
  end
end
