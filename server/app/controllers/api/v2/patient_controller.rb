# frozen_string_literal: true

class Api::V2::PatientController < ApplicationController
  before_action -> { doorkeeper_authorize!(:patient) }
  before_action -> { current_patient }

  private

  def current_patient
    @current_patient ||= Patient.find_by!(id: doorkeeper_token[:resource_owner_id])
  rescue ActiveRecord::RecordNotFound
    render(json: { error: "Patient not found" }, status: :not_found)
  end
end
