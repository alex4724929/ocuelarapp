# frozen_string_literal: true

class Api::V2::DoctorController < ApplicationController
  before_action -> { doorkeeper_authorize!(:doctor) }
  before_action -> { current_doctor }

  private

  def current_doctor
    @current_doctor ||= Doctor.find_by!(id: doorkeeper_token[:resource_owner_id])
  rescue ActiveRecord::RecordNotFound
    render(json: { error: "Doctor not found" }, status: :not_found)
  end
end
