# frozen_string_literal: true

class Api::V2::Device::DropsController < Api::V2::DeviceController
  def create
    drop_at = Time.zone.at(params[:timestamp])
    prescription = Drop.find_by(name: @current_device.ocuelar_type)
      .prescriptions.find_by(days: @current_device.patient.days_from_start(drop_at))

    if prescription.nil?
      render(json: { message: "invalid" }, status: :created)
      return
    end

    range, time_start, time_end, required = calculate_range(prescription, drop_at)
    dropped = @current_device.drops.where(created_at: time_start..time_end).count

    drops_hash = {}
    if dropped < required
      drops_hash[:schedule] = range
      drops_hash[:count] = params[:data]
      drops_hash[:created_at] = drop_at
      @current_device.drops.create!(drops_hash)
      @current_device.patient.update_adherence
      render(json: { message: "valid" }, status: :created)
    else
      drops_hash[:count] = params[:data]
      drops_hash[:created_at] = drop_at
      @current_device.drops.create!(drops_hash)
      @current_device.patient.update_adherence
      render(json: { message: "invalid" }, status: :created)
    end
  end

  private

  def drops_params
    params.permit(:count)
  end

  def calculate_range(prescription, drop_at)
    schedules = [
      [prescription.start_hour_1, prescription.start_hour_2, prescription.count_1],
      [prescription.start_hour_2, prescription.start_hour_3, prescription.count_2],
      [prescription.start_hour_3, prescription.start_hour_4, prescription.count_3],
      [prescription.start_hour_4, 24, prescription.count_4],
    ]
    range = nil
    time_schedule_start = nil
    time_schedule_end = nil
    required_drops = nil
    schedules.each do |start_hour, end_hour, drops|
      next if drop_at.hour < start_hour || drop_at.hour >= end_hour

      range = schedules.index([start_hour, end_hour, drops]) + 1
      required_drops = drops
      time_schedule_start = Time.zone.parse("#{drop_at.strftime("%Y-%m-%d")} #{start_hour}:00:00")
      time_schedule_end = Time.zone.parse("#{drop_at.strftime("%Y-%m-%d")} #{end_hour - 1}:59:59")
      break
    end
    [range, time_schedule_start, time_schedule_end, required_drops]
  end
end
