# frozen_string_literal: true

class Api::V1::Reset2Controller < ApplicationController
  def index
    patient = Patient.find_by(af_uid: "8330")
    patient.update(medication_start_at: Time.zone.now.beginning_of_day, adherence: 80.0)
    patient.ocuelars.each do |ocuelar|
      ocuelar.drops.destroy_all
      ocuelar.onlines.destroy_all
    end

    patient.update_adherence
    patient.messages.destroy_all

    render(json: { message: "Reset success" }, status: :ok)
  end
end
