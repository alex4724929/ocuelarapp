# frozen_string_literal: true

class Api::V1::Device::OnlineController < Api::V1::DeviceController
  def create
    online = @current_device.onlines.create!
    render(json: online.as_json(only: :created_at), status: :created)
  end
end
