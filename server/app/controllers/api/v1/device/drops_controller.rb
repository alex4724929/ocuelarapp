# frozen_string_literal: true

class Api::V1::Device::DropsController < Api::V1::DeviceController
  def create
    today_prescription = Drop.find_by(name: @current_device.ocuelar_type)
      .prescriptions.find_by(days: @current_device.patient.days_from_start)
    range, time_schedule_start, time_schedule_end, required_drops = calculate_range(today_prescription)

    dropped = @current_device.drops.where(created_at: time_schedule_start..time_schedule_end).count

    if dropped < required_drops
      drops_hash = drops_params.to_h
      drops_hash[:schedule] = range
      @current_device.drops.create!(drops_hash)
      @current_device.patient.update_adherence
      render(json: { message: "valid" }, status: :created)
    else
      @current_device.drops.create!(drops_params)
      @current_device.patient.update_adherence
      render(json: { message: "invalid" }, status: :created)
    end
  end

  private

  def drops_params
    params.permit(:count)
  end

  def calculate_range(prescription)
    now_time = Time.zone.now
    schedules = [
      [prescription.start_hour_1, prescription.start_hour_2, prescription.count_1],
      [prescription.start_hour_2, prescription.start_hour_3, prescription.count_2],
      [prescription.start_hour_3, prescription.start_hour_4, prescription.count_3],
      [prescription.start_hour_4, 24, prescription.count_4],
    ]
    range = nil
    time_schedule_start = nil
    time_schedule_end = nil
    required_drops = nil
    schedules.each do |start_hour, end_hour, drops|
      next if now_time.hour < start_hour || now_time.hour >= end_hour

      range = schedules.index([start_hour, end_hour, drops]) + 1
      required_drops = drops
      time_schedule_start = Time.zone.parse("#{now_time.strftime("%Y-%m-%d")} #{start_hour}:00:00")
      time_schedule_end = Time.zone.parse("#{now_time.strftime("%Y-%m-%d")} #{end_hour - 1}:59:59")
      break
    end
    [range, time_schedule_start, time_schedule_end, required_drops]
  end
end
