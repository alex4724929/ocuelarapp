# frozen_string_literal: true

class Api::V1::DeviceController < ApplicationController
  before_action -> { auth_api_key }
  before_action -> { current_device }

  private

  def current_device
    @current_device = OcuelarDevice.find_by!(ocuelar_id: params[:ocuelar_id])
  rescue ActiveRecord::RecordNotFound
    render(json: { error: "Device not found" }, status: :not_found)
  end

  def auth_api_key
    if params[:api_key] == Rails.application.credentials.api_key
      true
    else
      render(json: { error: "Unauthorized" }, status: :unauthorized)
    end
  end
end
