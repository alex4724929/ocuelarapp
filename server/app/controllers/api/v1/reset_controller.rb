# frozen_string_literal: true

class Api::V1::ResetController < ApplicationController
  def index
    patient = Patient.find_by(af_uid: "8330")
    patient.update(medication_start_at: Time.zone.now.beginning_of_day, adherence: 80.0)
    patient.ocuelars.each do |ocuelar|
      ocuelar.drops.destroy_all
      ocuelar.onlines.destroy_all
    end

    time_schedule_1 = Time.zone.now.change(hour: 8, min: 0, sec: 0)
    time_schedule_2 = Time.zone.now.change(hour: 12, min: 0, sec: 0)
    time_schedule_3 = Time.zone.now.change(hour: 16, min: 0, sec: 0)
    time_schedule_4 = Time.zone.now.change(hour: 20, min: 0, sec: 0)

    device_1 = patient.ocuelars.find_by(ocuelar_type: 1)
    device_1.onlines.create!(created_at: time_schedule_1)
    device_1.drops.create!(schedule: 1, created_at: time_schedule_1)
    device_1.onlines.create!(created_at: time_schedule_2)
    device_1.drops.create!(schedule: 2, created_at: time_schedule_2)
    device_1.onlines.create!(created_at: time_schedule_3)
    device_1.drops.create!(schedule: 3, created_at: time_schedule_3)
    device_1.onlines.create!(created_at: time_schedule_4)
    device_1.drops.create!(schedule: 4, created_at: time_schedule_4)

    device_2 = patient.ocuelars.find_by(ocuelar_type: 2)
    device_2.onlines.create!(created_at: time_schedule_1)
    device_2.drops.create!(schedule: 1, created_at: time_schedule_1)
    device_2.onlines.create!(created_at: time_schedule_2)
    device_2.drops.create!(schedule: 2, created_at: time_schedule_2)
    device_2.onlines.create!(created_at: time_schedule_3)
    device_2.drops.create!(schedule: 3, created_at: time_schedule_3)
    device_2.onlines.create!(created_at: time_schedule_4)
    device_2.drops.create!(schedule: 4, created_at: time_schedule_4)

    patient.update_adherence
    patient.messages.destroy_all

    render(json: { message: "Reset success" }, status: :ok)
  end
end
