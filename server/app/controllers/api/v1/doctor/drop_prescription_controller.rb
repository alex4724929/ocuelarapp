# frozen_string_literal: true

class Api::V1::Doctor::DropPrescriptionController < Api::V1::DoctorController
  before_action -> { set_drop }, only: [:index, :show, :create, :update]

  def index
    render(json: @current_drop.prescriptions.order(days: :asc))
  end

  def show
    render(json: @current_drop.prescriptions.find(params[:id]))
  end

  def create
  end

  def update
    @current_drop.prescriptions.find(params[:id]).update(drop_prescription_params)
    render(json: { message: "Saved successfully" })
  end

  private

  def set_drop
    @current_drop = Drop.find(params[:drop_id])
  end

  def drop_prescription_params
    params.permit(
      :start_hour_1,
      :start_hour_2,
      :start_hour_3,
      :start_hour_4,
      :count_1,
      :count_2,
      :count_3,
      :count_4,
    )
  end
end
