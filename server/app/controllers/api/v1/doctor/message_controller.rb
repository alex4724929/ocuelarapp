# frozen_string_literal: true

class Api::V1::Doctor::MessageController < Api::V1::DoctorController
  before_action -> { set_patient }, only: [:show, :create, :last_message]

  def index
    patients = @current_doctor.patients.all
    render(json: patients.as_json(
      only: [:id, :af_uid],
      include: { last_message: { only: [:created_at, :content] } },
    ))
  end

  def show
    condition = if params[:message_before_id].present?
      {
        id: ..params[:message_before_id].to_i - 1,
      }.compact.delete_if { |_, v| v.blank? }
    else
      {}
    end

    messages = @patient.messages.where(condition).order(created_at: :asc).last(20)

    render(json: {
      messages: messages.as_json(only: [:id, :content, :created_at, :is_from_doctor]),
      first_message_id: @patient.messages.first&.id,
    })
  end

  def create
    message = @patient.messages.create!(content: params[:message], is_from_doctor: true)
    render(json: message)
  end

  def last_message
    message = @patient.messages.last
    render(json: message, status: :ok)
  end

  private

  def set_patient
    @patient = Patient.find(params[:id])
  end
end
