# frozen_string_literal: true

class Api::V1::Doctor::DoctorController < Api::V1::DoctorController
  before_action -> { set_prescription }, only: [:show, :update, :destroy]

  def index
    conditions = {
      role: "doctor",
      active: true,
    }.compact.delete_if { |_, v| v.blank? }

    doctors = Doctor.where(conditions)
    render(json: doctors.as_json)
  end

  def show
    render(json: @doctor.as_json(except: [:id, :role, :active, :created_at, :updated_at]))
  end

  def create
    params_hash = doctor_params.to_h
    params_hash[:role] = "doctor"
    doctor = Doctor.new(params_hash)
    if doctor.save
      render(json: doctor, status: :created)
    else
      render(json: doctor.errors.full_messages, status: :unprocessable_entity)
    end
  end

  def update
    if @doctor.update(doctor_params)
      render(json: @doctor)
    else
      render(json: { error: @doctor.errors }, status: :unprocessable_entity)
    end
  end

  def destroy
    @doctor.destroy
    head(:no_content)
  end

  private

  def set_doctor
    @doctor = Doctor.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render(json: { error: "Doctor not found" }, status: :not_found)
  end

  def doctor_params
    params.permit(:username, :password, :password_confirmation, :active)
  end
end
