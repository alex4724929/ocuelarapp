# frozen_string_literal: true

class Api::V1::Doctor::AdminController < Api::V1::DoctorController
  before_action -> { set_prescription }, only: [:show, :update, :destroy]

  def index
    conditions = {
      role: "admin",
      active: true,
    }.compact.delete_if { |_, v| v.blank? }

    admins = Doctor.where(conditions)
    render(json: admins.as_json)
  end

  def show
    render(json: @admin.as_json(except: [:id, :role, :active, :created_at, :updated_at]))
  end

  def create
    params_hash = admin_params.to_h
    params_hash[:role] = "admin"
    admin = Doctor.new(params_hash)
    if admin.save
      render(json: admin, status: :created)
    else
      render(json: admin.errors.full_messages, status: :unprocessable_entity)
    end
  end

  def update
    if @admin.update(admin_params)
      render(json: @admin)
    else
      render(json: { error: @admin.errors }, status: :unprocessable_entity)
    end
  end

  def destroy
    @admin.destroy
    head(:no_content)
  end

  private

  def set_doctor
    @admin = Doctor.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render(json: { error: "Doctor not found" }, status: :not_found)
  end

  def admin_params
    params.permit(:username, :password, :password_confirmation, :active)
  end
end
