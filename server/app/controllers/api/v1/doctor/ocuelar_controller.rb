# frozen_string_literal: true

class Api::V1::Doctor::OcuelarController < Api::V1::DoctorController
  before_action -> { set_ocuelar }, only: [:show, :update, :destroy]

  def index
    conditions = {
      ocuelar_id: params[:ocuelar_id],
      ocuelar_type: params[:ocuelar_type],
    }.compact.delete_if { |_, v| v.blank? }

    conditions_pat = {
      "patient.af_uid": params[:af_uid],
    }.compact.delete_if { |_, v| v.blank? }

    ocuelars = OcuelarDevice.where(conditions).joins(:patient).where(conditions_pat).order(created_at: :asc)
    render(json: ocuelars.as_json(
      only: [:id, :ocuelar_id, :ocuelar_type],
      include: {
        patient: { only: [:af_uid] },
        last_drops: { only: [:created_at] },
        last_online: { only: [:created_at] },
      },
    ))
  end

  def show
    if params[:type] == "drops"
      render(json: @ocuelar.as_json(
        only: [:id, :ocuelar_id, :ocuelar_type],
        include: {
          drops_20: { only: [:created_at, :count] },
        },
      ))
    elsif params[:type] == "online"
      render(json: @ocuelar.as_json(
        only: [:id, :ocuelar_id, :ocuelar_type],
        include: {
          onlines_20: { only: [:created_at] },
        },
      ))
    end
  end

  def create
  end

  def update
  end

  def destroy
  end

  private

  def set_ocuelar
    @ocuelar = OcuelarDevice.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render(json: { error: "Ocuelar Device not found" }, status: :not_found)
  end
end
