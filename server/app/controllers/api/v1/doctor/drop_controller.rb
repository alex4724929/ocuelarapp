# frozen_string_literal: true

class Api::V1::Doctor::DropController < Api::V1::DoctorController
  def index
    render(json: Drop.all, only: [:id, :name])
  end

  def show
  end

  def create
  end

  def update
  end
end
