# frozen_string_literal: true

class Api::V1::Doctor::DropInfoController < Api::V1::DoctorController
  before_action -> { set_drop }, only: [:index, :show, :create, :update]

  def index
    render(json: {
      id: @current_drop.id,
      info: @current_drop.info.all.order(:order).as_json(only: [:id, :info_type, :order, :content]),
    })
  end

  def show
  end

  def create
    @current_drop.info.destroy_all
    params[:data].each do |info|
      @current_drop.info.create!(info_type: info[:info_type], order: info[:order], content: info[:content])
    end
    render(json: { message: "Saved successfully" })
  end

  def update
  end

  private

  def set_drop
    @current_drop = Drop.find(params[:drop_id])
  end
end
