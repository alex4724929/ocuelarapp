# frozen_string_literal: true

class Api::V1::Doctor::PatientController < Api::V1::DoctorController
  before_action -> { set_patient }, only: [:show, :update, :destroy]

  def index
    conditions = {
      af_uid: params[:af_uid],
      # ocuelar_id: params[:ocuelar_id],r
      control_group: params[:control_group],
      post_op_phase: params[:post_op_phase],
      active: true,
    }.compact.delete_if { |_, v| v.blank? }

    patients = @current_doctor.patients.where(conditions)
    render(json: patients.as_json(
      except: [:doctor_id, :created_at, :updated_at],
      include: { ocuelars: { only: [:ocuelar_id] } },
    ))
  end

  def show
    render(json: @patient.as_json(
      except: [:doctor_id, :created_at, :updated_at],
      include: { ocuelars: { only: [:id, :ocuelar_id, :ocuelar_type] } },
    ))
  end

  def create
    patient = @current_doctor.patients.new(patient_params)
    if patient.save
      params[:ocuelars].each do |ocuelar|
        patient.ocuelars.create(
          ocuelar_id: ocuelar[:ocuelar_id],
          ocuelar_type: ocuelar[:ocuelar_type],
        )
      end
      render(json: patient, status: :created)
    else
      render(json: patient.errors.full_messages, status: :unprocessable_entity)
    end
  end

  def update
    if @patient.update(patient_params)
      if params[:ocuelars].present?
        params[:ocuelars].each do |ocuelar|
          device = @patient.ocuelars.find(ocuelar[:id])
          device.update!(ocuelar_id: ocuelar[:ocuelar_id], ocuelar_type: ocuelar[:ocuelar_type])
        end
      end
      render(json: @patient)
    else
      render(json: { error: @patient.errors }, status: :unprocessable_entity)
    end
  end

  def destroy
    @patient.destroy
    head(:no_content)
  end

  private

  def set_patient
    @patient = Patient.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render(json: { error: "Patient not found" }, status: :not_found)
  end

  def patient_params
    params.permit(
      :username,
      :password,
      :password_confirmation,
      :active,
      :af_uid,
      :control_group,
      :post_op_phase,
      :medication_start_at,
    )
  end
end
