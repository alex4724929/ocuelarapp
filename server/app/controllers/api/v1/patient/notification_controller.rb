# frozen_string_literal: true

class Api::V1::Patient::NotificationController < Api::V1::PatientController
  def index
    notifications = []
    DropPrescription.includes(:drop).all.order(days: :desc).each do |prescription|
      today = @current_patient.medication_start_at + (prescription.days - 1).days
      notifications.push({
        title: "Drops Remember",
        body: "You have to take #{prescription.drop.name} now",
        time: today + prescription.start_hour_1.hours,
      }) if prescription.count_1 > 0
      notifications.push({
        title: "Drops Remember",
        body: "You have to take #{prescription.drop.name} now",
        time: today + prescription.start_hour_2.hours,
      }) if prescription.count_2 > 0
      notifications.push({
        title: "Drops Remember",
        body: "You have to take #{prescription.drop.name} now",
        time: today + prescription.start_hour_3.hours,
      }) if prescription.count_3 > 0
      notifications.push({
        title: "Drops Remember",
        body: "You have to take #{prescription.drop.name} now",
        time: today + prescription.start_hour_4.hours,
      }) if prescription.count_4 > 0
    end

    render(json: { notifications: notifications }, status: :ok)
  end
end
