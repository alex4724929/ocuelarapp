# frozen_string_literal: true

class Api::V1::Patient::Friend::MessageController < Api::V1::PatientController
  def index
    friendship = @current_patient.friendships.includes([:friend, :last_message])
    result = friendship.map do |f|
      {
        id: f.id,
        friend_name: (f.friend.id == @current_patient.id ? f.patient : f.friend).af_uid,
        last_message: f.last_message&.content,
      }
    end
    render(json: result.sort_by { |f| f[:friend_name] })
  end

  def show
    condition = if params[:message_before_id].present?
      {
        id: ..params[:message_before_id].to_i - 1,
      }.compact.delete_if { |_, v| v.blank? }
    else
      {}
    end

    friendship = @current_patient.friendships.find(params[:id])
    render(json: {
      my_id: @current_patient.id,
      friend_name: friendship.friend.af_uid,
      messages: friendship.messages.where(condition).order(created_at: :asc).last(20),
      first_message_id: friendship.messages.first&.id,
    })
  end

  def last_message
    friendship = @current_patient.friendships.find(params[:id])
    render(json: friendship.messages.last, status: :ok)
  end

  def create
    friendship = @current_patient.friendships.find(params[:friendship_id])
    params_h = message_params.to_h
    params_h[:patient_id] = @current_patient.id
    friendship.messages.create!(params_h)
    render(json: { status: :ok })
  end

  private

  def message_params
    params.permit(:content)
  end
end
