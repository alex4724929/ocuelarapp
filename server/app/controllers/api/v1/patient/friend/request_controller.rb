# frozen_string_literal: true

class Api::V1::Patient::Friend::RequestController < Api::V1::PatientController
  def index_send
    requests = @current_patient.sent_friend_requests.where(status: :pending)
    render(json: requests, status: :ok)
  end

  def create_send
    # check old requests
    friend = Patient.find_by(af_uid: send_request_params[:receiver_id])

    if friend.nil?
      render(json: { errors: ["Could not found this patient"] }, status: :bad_request)
      return
    end

    if @current_patient.sent_friend_requests.find_by(receiver_id: friend.id, status: :pending).nil?
      request = @current_patient.sent_friend_requests.new(receiver_id: friend.id)
      if request.save
        render(json: request, status: :ok)
      else
        render(json: { errors: request.errors.full_messages }, status: :bad_request)
      end
    else
      render(json: { errors: ["Friend request already sent"] }, status: :bad_request)
    end
  end

  def index_receive
    requests = @current_patient.received_friend_requests.where(status: :pending)
    render(
      json: requests.as_json(
        include: { sender: { only: [:af_uid] } },
        only: [:id, :created_at],
      ),
      status: :ok,
    )
  end

  def update_receive
    request = @current_patient.received_friend_requests.find(params[:id])
    params = receive_request_params.to_h
    params[:response_at] = Time.zone.now
    if request.update(params)
      # if params[:status] == "accepted"
      #   @current_patient.friendships.create(friend: request.sender)
      # end
      render(json: request, status: :ok)
    else
      render(json: { errors: request.errors.full_messages }, status: :bad_request)
    end
  end

  private

  def send_request_params
    params.permit(:receiver_id)
  end

  def receive_request_params
    params.permit(:status)
  end
end
