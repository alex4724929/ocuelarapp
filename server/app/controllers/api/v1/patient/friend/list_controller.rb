# frozen_string_literal: true

class Api::V1::Patient::Friend::ListController < Api::V1::PatientController
  def index
    friendship = @current_patient.friendships.includes([:friend])

    result = friendship.map do |f|
      {
        id: f.id,
        friend_name: (f.friend.id == @current_patient.id ? f.patient : f.friend).af_uid,
      }
    end

    render(json: result.sort_by { |f| f[:friend_name] })
  end
end
