# frozen_string_literal: true

class Api::V1::Patient::DailyController < Api::V1::PatientController
  def index
    pre_f = Drop.find_by(id: 1).prescriptions.find_by(days: @current_patient.days_from_start)
    pre_v = Drop.find_by(id: 2).prescriptions.find_by(days: @current_patient.days_from_start)
    pre_k = Drop.find_by(id: 3).prescriptions.find_by(days: @current_patient.days_from_start)
    expect_f = pre_f.count_1&.+ pre_f.count_2&.+ pre_f.count_3&.+ pre_f.count_4
    expect_v = pre_v.count_1&.+ pre_v.count_2&.+ pre_v.count_3&.+ pre_v.count_4
    expect_k = pre_k.count_1&.+ pre_k.count_2&.+ pre_k.count_3&.+ pre_k.count_4
    actual_f = @current_patient.ocuelars.find_by(ocuelar_type: 0).drops.where(
      created_at: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day, schedule: 1..4,
    ).count
    actual_v = @current_patient.ocuelars.find_by(ocuelar_type: 1).drops.where(
      created_at: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day, schedule: 1..4,
    ).count
    actual_k = @current_patient.ocuelars.find_by(ocuelar_type: 2).drops.where(
      created_at: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day, schedule: 1..4,
    ).count

    render(
      json: {
        total_adherence: @current_patient.adherence,
        days_from_start: @current_patient.days_from_start,
        flourometholone_expected: expect_f.nil? ? 0 : expect_f,
        flourometholone_actual: actual_f.nil? ? 0 : actual_f,
        vigamox_expected: expect_v.nil? ? 0 : expect_v,
        vigamox_actual: actual_v.nil? ? 0 : actual_v,
        ketorolac_expected: expect_k.nil? ? 0 : expect_k,
        ketorolac_actual: actual_k.nil? ? 0 : actual_k,
        weekly: generate_weekly, # 0 = haven't, 1 = not full, 2 = success, 3 = today empty
      },
      status: :ok,
    )
  end

  def show
  end

  def create
  end

  def update
  end

  def destroy
  end

  private

  def generate_weekly
    weekly = []
    today_days = Time.zone.now.strftime("%u").to_i % 7

    p(today_days)

    7.times do |i|
      weekly.push({
        text: Date::ABBR_DAYNAMES[i].upcase,
        status: i > today_days ? 0 : get_status(Time.zone.now - (today_days - i).day),
      })
    end
    weekly
  end

  def get_status(date)
    if @current_patient.days_from_start(date) <= 0
      return 0
    end

    pre_f = Drop.find_by(id: 1).prescriptions.find_by(days: @current_patient.days_from_start(date))
    pre_v = Drop.find_by(id: 2).prescriptions.find_by(days: @current_patient.days_from_start(date))
    pre_k = Drop.find_by(id: 3).prescriptions.find_by(days: @current_patient.days_from_start(date))

    expect_f = pre_f.count_1&.+ pre_f.count_2&.+ pre_f.count_3&.+ pre_f.count_4
    expect_v = pre_v.count_1&.+ pre_v.count_2&.+ pre_v.count_3&.+ pre_v.count_4
    expect_k = pre_k.count_1&.+ pre_k.count_2&.+ pre_k.count_3&.+ pre_k.count_4

    actual_f = @current_patient.ocuelars.find_by(ocuelar_type: 0).drops.where(
      created_at: date.beginning_of_day..date.end_of_day, schedule: 1..4,
    ).count
    actual_v = @current_patient.ocuelars.find_by(ocuelar_type: 1).drops.where(
      created_at: date.beginning_of_day..date.end_of_day, schedule: 1..4,
    ).count
    actual_k = @current_patient.ocuelars.find_by(ocuelar_type: 2).drops.where(
      created_at: date.beginning_of_day..date.end_of_day, schedule: 1..4,
    ).count

    if expect_f == actual_f && expect_v == actual_v && expect_k == actual_k
      2
    elsif date.to_date == Time.zone.now.to_date
      3
    else
      1
    end
  end
end
