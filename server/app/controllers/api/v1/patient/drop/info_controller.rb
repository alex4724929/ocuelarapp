# frozen_string_literal: true

class Api::V1::Patient::Drop::InfoController < Api::V1::PatientController
  before_action -> { set_drop }, only: [:index]

  def index
    render(json: {
      name: @current_drop.name,
      info: @current_drop.info.all.order(:order).as_json(only: [:id, :info_type, :order, :content]),
    })
  end

  private

  def set_drop
    @current_drop = Drop.find_by(name: params[:name])
  end
end
