# frozen_string_literal: true

class Api::V1::Patient::Doctor::MessageController < Api::V1::PatientController
  def index
    condition = if params[:message_before_id].present?
      {
        id: ..params[:message_before_id].to_i - 1,
      }.compact.delete_if { |_, v| v.blank? }
    else
      {}
    end

    messages = @current_patient.messages.where(condition).order(created_at: :asc).last(20)

    render(json: {
      messages: messages.as_json(only: [:id, :content, :created_at, :is_from_doctor]),
      first_message_id: @current_patient.messages.first&.id,
    })
  end

  def create
    message = @current_patient.messages.create!(message_params)
    render(
      json: message,
      status: :ok,
      except: [:id, :user_id, :status, :updated_at],
    )
  end

  def last_message
    message = @current_patient.messages.last
    render(json: message, status: :ok)
  end

  private

  def message_params
    params.permit(:content)
  end
end
