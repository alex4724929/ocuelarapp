# frozen_string_literal: true

class Api::V1::UserController < ApplicationController
  before_action -> { doorkeeper_authorize!(:public) }
  before_action -> { current_user }

  def show
    render(json: @current_user)
  end

  def update
    # @current_user.update(user_params)
    # render json: @current_user
  end

  private

  def current_user
    if doorkeeper_token[:scopes] == "doctor"
      @current_user ||= Doctor.find_by(id: doorkeeper_token[:resource_owner_id])
    elsif doorkeeper_token[:scopes] == "patient"
      @current_user ||= Patient.find_by(id: doorkeeper_token[:resource_owner_id])
    end
  end
end
