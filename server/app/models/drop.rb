# frozen_string_literal: true

class Drop < ApplicationRecord
  has_many :info, dependent: :destroy, class_name: "DropInfo", inverse_of: :drop
  has_many :prescriptions, dependent: :destroy, class_name: "DropPrescription", inverse_of: :drop

  validates :name, presence: true
end
