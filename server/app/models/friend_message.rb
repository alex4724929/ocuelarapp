# frozen_string_literal: true

class FriendMessage < ApplicationRecord
  belongs_to :friendship, inverse_of: :messages

  enum message_type: {
    text: 0,
    image: 1,
    audio: 2,
    video: 3,
    file: 4,
    location: 5,
    contact: 6,
    sticker: 7,
  }

  enum status: { sending: 0, sent: 1, delivered: 2, read: 3 }
end
