# frozen_string_literal: true

class DropPrescription < ApplicationRecord
  belongs_to :drop, inverse_of: :prescriptions

  validates :drop_id, presence: true

  validates :days, presence: true, uniqueness: { scope: :drop_id }
  validates :days, presence: true, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 365 }

  validates :start_hour_1, presence: true, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 23 }
  validates :count_1, presence: true, numericality: { greater_than_or_equal_to: 0 }

  validates :start_hour_2, presence: true, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 23 }
  validates :count_2, presence: true, numericality: { greater_than_or_equal_to: 0 }

  validates :start_hour_3, presence: true, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 23 }
  validates :count_3, presence: true, numericality: { greater_than_or_equal_to: 0 }

  validates :start_hour_4, presence: true, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 23 }
  validates :count_4, presence: true, numericality: { greater_than_or_equal_to: 0 }
end
