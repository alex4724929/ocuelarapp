# frozen_string_literal: true

class PatientMessage < ApplicationRecord
  belongs_to :patient, inverse_of: :messages
end
