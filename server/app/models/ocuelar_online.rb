# frozen_string_literal: true

class OcuelarOnline < ApplicationRecord
  belongs_to :ocuelar_device, inverse_of: :onlines
end
