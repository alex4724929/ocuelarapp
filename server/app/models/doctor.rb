# frozen_string_literal: true

class Doctor < ApplicationRecord
  devise :database_authenticatable,
    :registerable,
    :recoverable,
    :rememberable,
    :validatable,
    authentication_keys: [:username]

  has_many :patients, dependent: :destroy

  enum role: { admin: 0, doctor: 1 }

  def email_required?
    false
  end

  def email_changed?
    false
  end

  def will_save_change_to_email?
    false
  end

  class << self
    def authenticate(username, password)
      doctor = Doctor.find_for_authentication(username: username, active: true)
      doctor&.valid_password?(password) ? doctor : nil
    end
  end
end
