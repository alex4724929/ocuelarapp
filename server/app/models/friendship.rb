# frozen_string_literal: true

class Friendship < ApplicationRecord
  belongs_to :patient, class_name: "Patient"
  belongs_to :friend, class_name: "Patient"

  has_many :messages, class_name: "FriendMessage", inverse_of: :friendship, dependent: :destroy

  has_one :last_message,
    -> { order(created_at: :desc) },
    class_name: "FriendMessage",
    inverse_of: :friendship,
    dependent: :destroy
end
