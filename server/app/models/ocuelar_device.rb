# frozen_string_literal: true

class OcuelarDevice < ApplicationRecord
  belongs_to :patient, inverse_of: :ocuelars

  has_many :drops, dependent: :destroy, class_name: "OcuelarDrop", inverse_of: :ocuelar_device
  has_many :onlines, dependent: :destroy, class_name: "OcuelarOnline", inverse_of: :ocuelar_device

  has_one :last_online,
    -> { order(created_at: :desc) },
    dependent: :destroy,
    class_name: "OcuelarOnline",
    inverse_of: :ocuelar_device

  has_one :last_drops,
    -> { order(created_at: :desc) },
    dependent: :destroy,
    class_name: "OcuelarDrop",
    inverse_of: :ocuelar_device

  has_many :drops_20,
    -> { order(created_at: :desc).limit(20) },
    dependent: :destroy,
    class_name: "OcuelarDrop",
    inverse_of: :ocuelar_device

  has_many :onlines_20,
    -> { order(created_at: :desc).limit(20) },
    dependent: :destroy,
    class_name: "OcuelarOnline",
    inverse_of: :ocuelar_device

  validates :ocuelar_type, presence: true

  enum ocuelar_type: { flourometholone: 0, vigamox: 1, ketorolac: 2 }
end
