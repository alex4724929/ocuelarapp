# frozen_string_literal: true

class DropInfo < ApplicationRecord
  belongs_to :drop, inverse_of: :info

  enum info_type: { title: 0, text: 1 }

  validates :info_type, presence: true, inclusion: { in: DropInfo.info_types.keys }
  validates :order, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :content, presence: true
end
