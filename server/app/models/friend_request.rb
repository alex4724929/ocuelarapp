# frozen_string_literal: true

class FriendRequest < ApplicationRecord
  belongs_to :sender, class_name: "Patient", inverse_of: :sent_friend_requests
  belongs_to :receiver, class_name: "Patient", inverse_of: :received_friend_requests

  enum status: { pending: 0, accepted: 1, rejected: 2 }, _prefix: true

  after_update_commit :add_friendship, if: :status_accepted?

  private

  def add_friendship
    Friendship.create(patient: sender, friend: receiver)
  end
end
