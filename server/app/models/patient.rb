# frozen_string_literal: true

class Patient < ApplicationRecord
  devise :database_authenticatable,
    :registerable,
    :recoverable,
    :rememberable,
    :validatable,
    authentication_keys: [:username]

  belongs_to :doctor, class_name: "Doctor", inverse_of: :patients
  has_many :ocuelars, -> { order(id: :asc) }, dependent: :destroy, class_name: "OcuelarDevice", inverse_of: :patient
  has_many :messages, -> { order(id: :asc) }, dependent: :destroy, class_name: "PatientMessage", inverse_of: :patient

  has_many :sent_friend_requests,
    class_name: "FriendRequest",
    foreign_key: :sender_id,
    dependent: :destroy,
    inverse_of: :sender

  has_many :received_friend_requests,
    class_name: "FriendRequest",
    foreign_key: :receiver_id,
    dependent: :destroy,
    inverse_of: :receiver

  has_one :last_message,
    -> { order(created_at: :desc) },
    class_name: "PatientMessage",
    dependent: :destroy,
    inverse_of: :patient

  has_one :device_flourometholone,
    -> { where(ocuelar_type: "flourometholone") },
    dependent: :destroy,
    class_name: "OcuelarDevice",
    inverse_of: :patient

  has_one :device_vigamox,
    -> { where(ocuelar_type: "vigamox") },
    dependent: :destroy,
    class_name: "OcuelarDevice",
    inverse_of: :patient

  has_one :device_ketorolac,
    -> { where(ocuelar_type: "ketorolac") },
    dependent: :destroy,
    class_name: "OcuelarDevice",
    inverse_of: :patient

  enum control_group: { control: 0, smartcap: 1, smartcap_gamification: 2 }
  enum post_op_phase: {
    week_1: 0,
    week_2: 1,
    week_3: 2,
    week_4: 3,
    week_5: 4,
    week_6: 5,
    week_7: 6,
    week_8: 7,
    week_9: 8,
  }

  validates :username, presence: true, uniqueness: true
  validates :af_uid, presence: true, uniqueness: true
  validates :control_group, presence: true, inclusion: { in: control_groups.keys }
  validates :post_op_phase, presence: true, inclusion: { in: post_op_phases.keys }

  after_update_commit :update_post_op_phase, if: :saved_change_to_medication_start_at?

  # has_many :friendships, ->(patient) { where('patient_id = :id OR friend_id = :id', id: patient.id) }
  # has_many :friends, through: :friendships

  def friendships
    Friendship.where("patient_id = ? OR friend_id = ?", id, id)
  end

  def friends
    friendships.map { |f| f.patient_id == id ? f.friend : f.patient }
  end

  def update_adherence
    numerator = device_flourometholone.drops.where(schedule: 1..4).count +
      device_vigamox.drops.where(schedule: 1..4).count +
      device_ketorolac.drops.where(schedule: 1..4).count
    denominator = DropPrescription.where(days: 1..days_from_start).sum(:count_1) +
      DropPrescription.where(days: 1..days_from_start).sum(:count_2) +
      DropPrescription.where(days: 1..days_from_start).sum(:count_3) +
      DropPrescription.where(days: 1..days_from_start).sum(:count_4)

    update(adherence: (numerator.to_f / denominator.to_f) * 100)
  end

  def update_post_op_phase
    today = Time.zone.now.beginning_of_day
    post_op_day = ((today - medication_start_at) / 1.week).to_i
    update!(post_op_phase: post_op_day)
  end

  def days_from_start(date = nil)
    if date.nil?
      (Time.zone.now.to_date - medication_start_at.to_date).to_i + 1
    else
      (date.to_date - medication_start_at.to_date).to_i + 1
    end
  end

  def email_required?
    false
  end

  def email_changed?
    false
  end

  def will_save_change_to_email?
    false
  end

  class << self
    def authenticate(username, password)
      patient = Patient.find_for_authentication(username: username, active: true)
      patient&.valid_password?(password) ? patient : nil
    end
  end
end
