# frozen_string_literal: true

class OcuelarDrop < ApplicationRecord
  belongs_to :ocuelar_device, inverse_of: :drops
end
