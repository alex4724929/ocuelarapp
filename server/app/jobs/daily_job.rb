# frozen_string_literal: true

class DailyJob < ApplicationJob
  queue_as :default

  def perform(*_args)
    Patient.all.each do |patient|
      # post op day = weeks since start_date
      today = Time.zone.now.beginning_of_day
      post_op_day = ((today - patient.medication_start_at) / 1.week).to_i
      patient.update!(post_op_phase: post_op_day)
    end
  end
end
