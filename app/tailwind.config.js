/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [
      require("daisyui")
  ],
  daisyui: {
    themes: [
      {
        ocuelar: {
          "color-scheme": "light",
          "primary": "#FFCA67",
          "primary-content": "#261E0F",
          "secondary": "#00B5B8",
          "secondary-content": "#FFFFFF",
          "accent": "#5260ff",
          "accent-content": "#FFFFFF",
          "base-100": "#f4f5f8",
          "base-content": "#000000",
          "--rounded-box": "0rem",
          "--rounded-btn": "0rem",
          "--rounded-badge": "0rem",
          "--animation-btn": "0",
          "--animation-input": "0",
          "--btn-focus-scale": "1",
        }
      }
    ]
  }
}

