import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import zh from './zh.json';
import en from './en.json';
import ko from './ko.json';
const resources = {
  en: {
    translation: en,
  },
  ko: {
    translation: ko,
  },
  zh: {
    translation: zh,
  },
};

i18n.use(initReactI18next).init({
  resources,
  lng: 'en', // Default language
  interpolation: {
    escapeValue: false, // React already escapes values
  },
});

export default i18n;
