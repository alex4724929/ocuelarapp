import {ActivityIndicator, Pressable, Text, ViewStyle} from 'react-native';
import {Color} from '../theme';
import React from 'react';
interface DefaultButtonProps {
  onPress: () => void;
  title: string;
  active?: boolean;
  style: ViewStyle;
  isLoading?: boolean;
}
const DefaultButton = ({
  onPress,
  title,
  active = true,
  style,
  isLoading,
}: DefaultButtonProps) => {
  return (
    <Pressable
      onPress={onPress}
      style={{
        backgroundColor: active ? Color.main : 'white',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 4,
        marginTop: 10,
        borderColor: active ? Color.main : Color.text_gray,
        borderWidth: 1,
        ...style,
      }}>
      {isLoading ? (
        <ActivityIndicator color={'white'} size={'small'} />
      ) : (
        <Text
          style={{
            color: active ? 'white' : 'black',
            fontWeight: 'bold',
            textAlign: 'center',
          }}>
          {title}
        </Text>
      )}
    </Pressable>
  );
};

export default DefaultButton;
