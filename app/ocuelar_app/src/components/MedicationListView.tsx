import React, {useContext, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  Dimensions,
  ImageBackground,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Color} from '../theme';
import {rescaleHeight, rescaleWidth} from '../lib/utils/rescale';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {httpClient} from '../lib/network/httpClient';
import axios from 'axios';
import {BLETest} from '../BLETest';
import {PeripheralsContext} from '../../App';
import {useTranslation} from 'react-i18next';

const {width, height} = Dimensions.get('screen');
export const MedicationListView = () => {
  const {t} = useTranslation();
  const [deviceList, setDeviceList] = useState<any[]>([]);
  const {
    peripherals,
    isSyncingData,
    currentMedication,
    setCurrentMedication,
    isScanning,
  } = useContext(PeripheralsContext);

  useEffect(() => {
    getMedicationList();
  }, []);

  const getMedicationList = async () => {
    try {
      const response = await httpClient.get('/api/my-device');
      axios.defaults.headers.common.Authorization = `Bearer ${response.data.token}`;
      console.log('response.data)', response.data);
      setDeviceList(response.data);
    } catch (error) {
      Alert.alert(
        'failed to /api/my-device',
        error?.response?.data?.message ?? 'unknown error',
      );
    }
  };

  return (
    <View style={{flex: 1}}>
      <View style={{alignItems: 'center', marginTop: 44, flex: 1}}>
        <Text
          style={{
            color: Color.text,
            textAlign: 'center',
            fontSize: 32,
            fontWeight: '400',
          }}>
          {t('welcome!')}
        </Text>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 8,
          }}>
          <Text
            style={{
              color: Color.text_gray,
              fontSize: 14,
              maxWidth: rescaleWidth(248),
            }}>
            {t('device_use_guide')}
          </Text>
        </View>
        {isScanning && <ActivityIndicator size={'large'} color={Color.main} />}

        {deviceList.map((item, index) => {
          const peripheral = peripherals.get(item.display_udid);
          const isSelected = peripheral && peripheral.id === currentMedication;
          return <ListItem item={item} key={index} />;
        })}
        {__DEV__ &&
          [fake1, fake2, fake3].map((item, index) => {
            const peripheral = peripherals.get(item.display_udid);
            const isSelected =
              peripheral && peripheral.id === currentMedication;
            return <ListItem item={item} key={index} />;
          })}
      </View>
      <ImageBackground
        style={{
          width,
          height: rescaleHeight(168),
          zIndex: -1,
        }}
        source={require('../assets/bgFooter.png')}>
        <View
          style={{
            width,
            height: rescaleHeight(168),
            alignItems: 'center',
            justifyContent: 'center',
            bottom: 200,
          }}></View>
      </ImageBackground>
    </View>
  );
};
const ListItem = ({item}: {item: any}) => {
  const [isConnected, setIsConnected] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(false);
  const {togglePeripheralConnection} = BLETest();
  const {peripherals, connectedDevices, setConnectedDevices} =
    useContext(PeripheralsContext);

  const onPress = React.useCallback(() => {
    if (__DEV__) {
      let p = peripherals.get(item.id || '');
      p && togglePeripheralConnection(p);
      togglePeripheralConnection(item);
    } else {
      let p = peripherals.get(item.display_udid || '');
      p && togglePeripheralConnection(p);
      togglePeripheralConnection(item);
    }
  }, []);

  useEffect(() => {
    const index = __DEV__
      ? connectedDevices.indexOf(item.id)
      : connectedDevices.indexOf(item.display_udid);
    if (index != -1) {
      setIsConnected(true);
    }
  }, [connectedDevices]);

  return (
    <View
      style={{
        borderRightWidth: 1,
        borderLeftWidth: 1,
        borderTopWidth: 1,
        borderColor: '#ccc',
        marginTop: 12,
        marginBottom: 10,
      }}>
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          width: rescaleWidth(270),
          paddingVertical: 8,
          paddingHorizontal: 12,
          borderBottomWidth: 1,
          borderColor: '#ccc',
          // backgroundColor: isSelected ? Color.main : 'transparent',
        }}
        onPress={onPress}>
        <>
          {/* <FontAwesome
            name={'bluetooth-b'}
            size={15}
            color={isConnected ? 'green' : '#d5d5d5'}
          /> */}
          <Text
            style={{
              fontSize: 21,
              color: isConnected ? 'green' : '#d5d5d5',
              fontWeight: '400',
              marginLeft: 8,
              flex: 1,
            }}>
            {item?.label || 'unKnown'}
          </Text>
          {isLoading && <ActivityIndicator />}
        </>
      </TouchableOpacity>
    </View>
  );
};

const fake1 = {
  config: null,
  display_udid: 'Ocuelar-9B3BBB',
  id: '0ac09d06-cf1f-5d81-615b-caa0731f07df',
  label: 'fake001',
  last_online_time: null,
  owner: 'ef5173c5-f135-4791-b7ed-add195a67cc1',
};
const fake2 = {
  config: null,
  display_udid: 'Ocuelar-9B3BBB',
  id: 'e9c30e52-60ef-ddfa-2e30-db38ed5ab473',
  label: 'fake002',
  last_online_time: null,
  owner: 'ef5173c5-f135-4791-b7ed-add195a67cc1',
};
const fake3 = {
  config: null,
  display_udid: 'Ocuelar-9B3BBB',
  id: 'c5a9120c-4db1-465b-a079-b68299033e03',
  label: 'fake003',
  last_online_time: null,
  owner: 'ef5173c5-f135-4791-b7ed-add195a67cc1',
};
