import {Calendar} from 'react-native-calendars';
import {Pressable, Text, TouchableWithoutFeedback, View} from 'react-native';
import moment from 'moment/moment';
import Entypo from 'react-native-vector-icons/Entypo';
import * as Progress from 'react-native-progress';
import {Color} from '../theme';
import {rescaleHeight, rescaleWidth} from '../lib/utils/rescale';
import {MedicationsComp} from './MedicationsComp';
import React, {useEffect, useState} from 'react';
import {httpClient} from '../lib/network/httpClient';
import {useMMKVString} from 'react-native-mmkv';
import Share from 'react-native-share';

export const MonthlyView = () => {
  const [date, setDate] = useState(moment().format('YYYY-MM-DD'));
  const [data, setData] = useState({});
  useEffect(() => {
    (async () => {
      try {
        setData({});
        const {data} = await httpClient.post('/api/record-by-date', {
          start: moment(date).startOf('day').unix(),
          end: moment(date).endOf('day').unix(),
        });
        setData(data);
        console.log(data);
      } catch (error) {}
    })();
  }, [date]);
  const [user] = useMMKVString('user.user');
  return (
    <View style={{flex: 1}}>
      <Calendar
        renderHeader={() => {
          return (
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                marginVertical: 24,
                justifyContent: 'space-between',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: '#383838',
                    fontWeight: 'bold',
                    fontSize: 15,
                    marginRight: 3,
                  }}>
                  {moment(date).format('MMMM')}
                </Text>
                <Text
                  style={{
                    color: '#383838',
                    fontWeight: 'bold',
                    fontSize: 15,
                  }}>
                  {moment(date).format('YYYY')}
                </Text>
              </View>
              <View
                style={{flexDirection: 'row', position: 'absolute', right: 10}}>
                <Pressable
                  onPress={() => {
                    setDate(
                      moment(date).subtract(1, 'month').format('YYYY-MM-DD'),
                    );
                  }}
                  style={{
                    width: 21,
                    height: 21,
                    borderRadius: 10.5,
                    backgroundColor: 'white',
                    marginRight: 12,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Entypo name="chevron-left" size={20} color="black" />
                </Pressable>
                <Pressable
                  onPress={() => {
                    setDate(moment(date).add(1, 'month').format('YYYY-MM-DD'));
                  }}
                  style={{
                    width: 21,
                    height: 21,
                    borderRadius: 10.5,
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Entypo name="chevron-right" size={20} color="black" />
                </Pressable>
              </View>
            </View>
          );
        }}
        markedDates={{
          [date]: {selected: true},
        }}
        initialDate={date}
        hideArrows
        theme={{
          todayTextColor: 'gray',
          textSectionTitleColor: '#34485e',
          textDayHeaderFontSize: 14,
          textDayHeaderFontWeight: 'bold',
          textDayFontWeight: '600',
          textInactiveColor: '#383838',
          textDisabledColor: 'white',
          textDayStyle: {
            color: 'black',
          },
          stylesheet: {
            calendar: {
              main: {
                color: '#ffffff',
              },
            },
          },
          calendarBackground: '#0bcbbc',
          selectedDayBackgroundColor: '#FFFFFF',
          selectedDayTextColor: 'black',
        }}
        date={date}
        style={{paddingBottom: 20}}
        onDayPress={day => {
          setDate(day.dateString);
        }}
      />
      <View style={{alignItems: 'center', marginTop: 12, flex: 1}}>
        <View style={{flexDirection: 'row', flex: 1}}>
          <View style={{flex: 1, alignItems: 'center', marginTop: 44}}>
            <Progress.Circle
              color={Color.main}
              size={rescaleWidth(111)}
              progress={0.85}
              textStyle={{fontWeight: '200', color: Color.text, fontSize: 43}}
              borderColor={'transparent'}
              showsText={true}
              indeterminate={false}
            />
            <TouchableWithoutFeedback
              onPress={() => {
                const userData = JSON.parse(user!);
                Share.open({
                  message: `https://ocuelar-portal-web.vercel.app/invite?inviterId=${userData.id}`,
                })
                  .then(res => {
                    console.log(res);
                  })
                  .catch(err => {
                    err && console.log(err);
                  });
              }}>
              <View
                style={{
                  marginTop: 36,
                }}>
                <View
                  style={{
                    backgroundColor: Color.main,
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: rescaleHeight(64),
                    width: rescaleHeight(64),
                    borderRadius: 150,
                  }}>
                  <Entypo
                    name="message"
                    size={rescaleWidth(48)}
                    color="white"
                  />
                </View>
                <Text
                  style={{
                    alignSelf: 'center',
                    marginTop: 8,
                    fontWeight: '600',
                  }}>
                  Invite a friend
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View style={{flex: 1, marginTop: 12}}>
            <Text style={{color: Color.text, fontSize: 10, fontWeight: 'bold'}}>
              LEGEND:
            </Text>
            {data.devices &&
              data.devices.map((item, index) => {
                return (
                  <MedicationsComp
                    data={data}
                    key={index}
                    name={item.label}
                    times={item.times}
                  />
                );
              })}
          </View>
        </View>
      </View>
    </View>
  );
};
