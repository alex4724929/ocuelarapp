import React from 'react';
import {CalendarProvider, WeekCalendar} from 'react-native-calendars/src';
import {View, Text, ImageBackground, Dimensions} from 'react-native';
import {MedicationsComp} from './MedicationsComp';
import {Color} from '../theme';
import moment from 'moment';
import {rescaleHeight, rescaleWidth} from '../lib/utils/rescale';
import Entypo from 'react-native-vector-icons/Entypo';
import {useDevicesContext} from '../context/DevicesContext';

const {width, height} = Dimensions.get('screen');
export const WeeklyView = () => {
  const {devices} = useDevicesContext();
  const [date, setDate] = React.useState(moment());
  return (
    <CalendarProvider
      date={date.format('YYYY-MM-DD')}
      style={{backgroundColor: 'white'}}>
      <View style={{flex: 1}}>
        <WeekCalendar
          firstDay={1}
          onDayPress={day => setDate(moment(day.dateString))}
        />
        <View style={{alignItems: 'center', marginTop: 44, flex: 1}}>
          <View>
            <Text
              style={{
                color: Color.main,
                fontSize: 32,
                fontWeight: '300',
              }}>
              {date.format('DD MMM YYYY, ddd').toString()}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginTop: 8,
              }}>
              <Text style={{color: Color.text_gray, fontSize: 10}}>
                POST OPERATION:
              </Text>
              <Text style={{color: Color.text, fontSize: 10}}>
                WEEK 1, DAY 6
              </Text>
            </View>
          </View>
          <View>
            {devices.map(item => {
              return (
                <MedicationsComp key={item.id} name={item.label} times={0} />
              );
            })}
          </View>
        </View>
        <ImageBackground source={require('../assets/bgFooter.png')}>
          <View
            style={{
              width,
              height: rescaleHeight(168),
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'row',
            }}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <View
                style={{
                  backgroundColor: 'white',
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: rescaleHeight(64),
                  width: rescaleHeight(64),
                  borderRadius: 150,
                }}>
                <Entypo
                  name="message"
                  size={rescaleWidth(48)}
                  color="#727272"
                />
              </View>
              <Text
                style={{alignSelf: 'center', marginTop: 8, fontWeight: '400'}}>
                Messages
              </Text>
            </View>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Text
                style={{color: Color.text, fontSize: 57, fontWeight: '200'}}>
                100%
              </Text>
              <Text style={{fontSize: 10, fontWeight: '400'}}>
                TOTAL ADHERENCE
              </Text>
              <Text style={{fontSize: 10, fontWeight: '400'}}>
                TO MEDICATION
              </Text>
            </View>
          </View>
        </ImageBackground>
      </View>
    </CalendarProvider>
  );
};
