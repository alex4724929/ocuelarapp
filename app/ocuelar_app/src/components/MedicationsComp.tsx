import {Text, View} from 'react-native';
import {Color} from '../theme';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import React from 'react';

export const MedicationsComp = ({
  name,
  times,
  data,
}: {
  name: string;
  times: number;
  data: {
    [key: string]: [];
  };
}) => {
  return (
    <View style={{marginTop: 24}}>
      <Text style={{color: Color.text, fontSize: 21}}>{name}</Text>
      <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 8}}>
        <Text style={{fontSize: 10, color: Color.text}}>{`${times}X`}</Text>
        <Text style={{fontSize: 10, color: '#999999', marginLeft: 2}}>
          DAILY
        </Text>
        <Drop activeTimes={data?.[name]?.length ?? 0} totalTimes={times} />
      </View>
    </View>
  );
};

const Drop = ({
  totalTimes = 0,
  activeTimes = 0,
}: {
  totalTimes: number;
  activeTimes: number;
}) => {
  console.log(totalTimes, activeTimes, [...Array(totalTimes)]);
  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      {[...Array(parseInt(totalTimes))].map((_, index) => {
        return (
          <MaterialIcons
            name={'water-drop'}
            key={`default-${index}`}
            size={17}
            color={'grey'}
          />
        );
      })}
      {[...Array(parseInt(parseInt(activeTimes) ?? 0))].map((_, index) => {
        return (
          <MaterialIcons
            name={'water-drop'}
            key={`default-${index}`}
            size={17}
            style={{position: 'absolute', left: 17 * index}}
            color={'green'}
          />
        );
      })}
    </View>
  );
};
