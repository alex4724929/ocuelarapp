import {
  DrawerContentScrollView,
  DrawerItem,
  DrawerItemList,
} from '@react-navigation/drawer';
import {Alert} from 'react-native';
import {storage} from '../lib/mmkv';
import {useTranslation} from 'react-i18next';

function CustomDrawerContent(props) {
  const {t} = useTranslation();
  return (
    <DrawerContentScrollView {...props}>
      <DrawerItemList {...props} />
      <DrawerItem
        label={t('change_language')}
        onPress={() => {
          storage.delete('lang');
        }}
      />
      <DrawerItem
        label={t('logout')}
        onPress={() =>
          Alert.alert('Login Out', 'Are you sure?', [
            {
              text: 'Cancel',
              onPress: () => {},
            },
            {
              text: 'Ok',
              onPress: () => {
                storage.clearAll();
              },
            },
          ])
        }
      />
    </DrawerContentScrollView>
  );
}

export default CustomDrawerContent;
