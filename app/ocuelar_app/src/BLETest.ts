import BleManager, {
  BleDisconnectPeripheralEvent,
  BleManagerDidUpdateValueForCharacteristicEvent,
  BleScanCallbackType,
  BleScanMatchMode,
  BleScanMode,
  Peripheral,
} from 'react-native-ble-manager';
import { useContext } from 'react';
import { Alert, NativeEventEmitter, NativeModules, PermissionsAndroid, Platform, } from 'react-native';
import moment from 'moment';
import { PeripheralsContext } from '../App';
import { httpClient } from './lib/network/httpClient';

declare module 'react-native-ble-manager' {
  // enrich local contract with custom state properties needed by App.tsx
  interface Peripheral {
    connected?: boolean;
    connecting?: boolean;
  }
}

const SECONDS_TO_SCAN_FOR = 7;
const SERVICE_UUIDS: string[] = [];
const ALLOW_DUPLICATES = true;
export const BleManagerModule = NativeModules.BleManager;
export const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

export const BLETest = () => {
  const {
    peripherals,
    setPeripherals,
    connectedDevices,
    setConnectedDevices,
    isScanning,
    setIsScanning,
    setCurrentMedication,
    setIsSyncingData,
  } = useContext(PeripheralsContext);

  const addOrUpdatePeripheral = (
    name: string | undefined,
    updatedPeripheral: Peripheral,
  ) => {
    // new Map() enables changing the reference & refreshing UI.
    // TOFIX not efficient.
    if (__DEV__) {
      setPeripherals((map: any) => new Map(map.set(updatedPeripheral.id, updatedPeripheral)));
    } else {
      setPeripherals((map: any) => new Map(map.set(name, updatedPeripheral)));
    }
  };

  const startScan = () => {
    if (!isScanning) {
      // reset found peripherals before scan
      setPeripherals(new Map<Peripheral['name'], Peripheral>());

      try {
        console.debug('[startScan] starting scan...');
        setIsScanning(true);
        BleManager.scan(SERVICE_UUIDS, SECONDS_TO_SCAN_FOR, ALLOW_DUPLICATES, {
          matchMode: BleScanMatchMode.Sticky,
          scanMode: BleScanMode.LowLatency,
          callbackType: BleScanCallbackType.AllMatches,
        })
          .then(() => {
            console.debug('[startScan] scan promise returned successfully.');
          })
          .catch(err => {
            console.error('[startScan] ble scan returned in error', err);
          });
      } catch (error) {
        console.error('[startScan] ble scan error thrown', error);
      }
    }
  };

  const handleStopScan = () => {
    setIsScanning(false);
    console.debug('[handleStopScan] scan is stopped.');
  };

  const handleDisconnectedPeripheral = (
    event: BleDisconnectPeripheralEvent,
  ) => {
    let peripheral = peripherals.get(event.peripheral);
    if (peripheral) {
      console.debug(
        `[handleDisconnectedPeripheral][${peripheral.id}] previously connected peripheral is disconnected.`,
        event.peripheral,
      );
      addOrUpdatePeripheral(peripheral.name, { ...peripheral, connected: false });
    }
    console.debug(
      `[handleDisconnectedPeripheral][${event.peripheral}] disconnected.`,
    );
  };

  const handleUpdateValueForCharacteristic = (
    data: BleManagerDidUpdateValueForCharacteristicEvent,
  ) => {
    console.debug(
      `[handleUpdateValueForCharacteristic] received data from '${data.peripheral}' with characteristic='${data.characteristic}' and value='${data.value}'`,
    );
  };

  const handleDiscoverPeripheral = (peripheral: Peripheral) => {
    if (peripheral.name && peripheral.name.includes('Ocuelar')) {
      console.debug(
        '[handleDiscoverPeripheral] new BLE peripheral=',
        peripheral,
      );
      addOrUpdatePeripheral(peripheral.name, peripheral);
    }
  };

  const togglePeripheralConnection = async (peripheral: Peripheral) => {
    if (peripheral && peripheral.connected) {
      try {
        setCurrentMedication('');
        await BleManager.disconnect(peripheral.id);

        /**
         * delete device from list
         */
        const index = connectedDevices.indexOf(peripheral.name || '')
        if (index != -1) {
          let arr = [...connectedDevices]
          arr.splice(index, 1)
          setConnectedDevices(arr)
        }
      } catch (error) {
        console.error(
          `[togglePeripheralConnection][${peripheral.id}] error when trying to disconnect device.`,
          error,
        );
      }
    } else {
      await connectPeripheral(peripheral);
    }
  };

  const retrieveConnected = async () => {
    try {
      const connectedPeripherals = await BleManager.getConnectedPeripherals();
      if (connectedPeripherals.length === 0) {
        console.warn('[retrieveConnected] No connected peripherals found.');
        return;
      }

      console.debug(
        '[retrieveConnected] connectedPeripherals',
        connectedPeripherals,
      );

      for (var i = 0; i < connectedPeripherals.length; i++) {
        var peripheral = connectedPeripherals[i];
        addOrUpdatePeripheral(peripheral.name, {
          ...peripheral,
          connected: true,
        });
      }
    } catch (error) {
      console.error(
        '[retrieveConnected] unable to retrieve connected peripherals.',
        error,
      );
    }
  };

  const getRegularArr = (_buffer: any) => {
    const arr = new Uint8Array(_buffer);
    return Array.from(arr);
  };

  const dropRecords = async (records: any, device_id: string) => {
    httpClient
      .post('/api/drop_record', {
        records,
        device_id,
      })
      .catch(error => {
        Alert.alert(
          'failed to /api/drop_record',
          error?.response?.data?.message ?? 'unknown error',
        );
      });
  };

  const syncDevice = async (peripheralData: any, peripheral: any) => {
    const uuid =
      peripheralData.services && peripheralData.services.length > 0
        ? peripheralData.services[0].uuid
        : '';
    const currentTimestamp = moment().unix().toString();
    await BleManager.write(
      peripheral.id,
      uuid,
      '2A08',
      getRegularArr(new TextEncoder().encode(currentTimestamp).buffer),
    );

    let all_records: any[] = [];
    // await BleManager.write(peripheral.id, uuid, "3AB4", getRegularArr(new TextEncoder().encode('-1').buffer))
    // await BleManager.write(peripheral.id, uuid, "3AB4", getRegularArr(new TextEncoder().encode('-2').buffer))
    for (let i = 0; i < 25; i++) {
      // await BleManager.write(peripheral.id, uuid, "3AB4", getRegularArr(new TextEncoder().encode('-2').buffer))
      await BleManager.write(
        peripheral.id,
        uuid,
        '3AB4',
        getRegularArr(new TextEncoder().encode(i.toString()).buffer),
      );
      const res = await BleManager.read(peripheral.id, uuid, '3AB4');
      const decoded = new TextDecoder().decode(new Uint8Array(res));
      const records = JSON.parse(decoded);
      let result = [];

      for (const record of records) {
        if (!__DEV__) {
          if (record.timestamp === 0) return;
        }

        const data = {
          ocuelar_id: peripheralData.name,
          timestamp: record.timestamp,
          data: record.data,
        };

        result.push(data);
      }
      all_records = all_records.concat(result);
      //await BleManager.write(peripheralData.id, uuid, "3AB4", getRegularArr(new TextEncoder().encode('-1').buffer))
    }

    Alert.alert('JSON_records', JSON.stringify(all_records));
    await BleManager.write(
      peripheral.id,
      uuid,
      '3AB4',
      getRegularArr(new TextEncoder().encode('-1').buffer),
    );

    /**
     * test no disconnect
     */
    //await BleManager.disconnect(peripheralData.id);
    setIsSyncingData(false);
    dropRecords(all_records, peripheralData['services'][0]['uuid']);
  };

  const connectPeripheral = async (peripheral: Peripheral) => {
    try {
      if (peripheral) {
        setIsSyncingData(true);
        addOrUpdatePeripheral(peripheral.name, {
          ...peripheral,
          connecting: true,
        });

        await BleManager.connect(peripheral.id);
        console.debug(`[connectPeripheral][${peripheral.id}] connected.`);

        /**
         * update connected devices
         */
        if (__DEV__) {
          const arr = [...connectedDevices]
          arr.push(peripheral.id)
          setConnectedDevices(arr)

          addOrUpdatePeripheral(peripheral.name, {
            ...peripheral,
            connecting: false,
            connected: true,
          });
        } else {
          const arr = [...connectedDevices]
          arr.push(peripheral.name)
          setConnectedDevices(arr)

          addOrUpdatePeripheral(peripheral.name, {
            ...peripheral,
            connecting: false,
            connected: true,
          });
        }

        // before retrieving services, it is often a good idea to let bonding & connection finish properly
        await sleep(900);

        /* Test read current RSSI value, retrieve services first */
        const peripheralData = await BleManager.retrieveServices(peripheral.id);
        console.debug(
          `[connectPeripheral][${peripheral.id}] retrieved peripheral services`,
          peripheralData,
        );

        const rssi = await BleManager.readRSSI(peripheral.id);
        console.debug(
          `[connectPeripheral][${peripheral.id}] retrieved current RSSI value: ${rssi}.`,
        );

        if (peripheralData.characteristics) {
          for (let characteristic of peripheralData.characteristics) {
            if (characteristic.descriptors) {
              for (let descriptor of characteristic.descriptors) {
                try {
                  let data = await BleManager.readDescriptor(
                    peripheral.id,
                    characteristic.service,
                    characteristic.characteristic,
                    descriptor.uuid,
                  );
                  console.debug(
                    `[connectPeripheral][${peripheral.id}] descriptor read as:`,
                    data,
                  );
                } catch (error) {
                  console.error(
                    `[connectPeripheral][${peripheral.id}] failed to retrieve descriptor ${descriptor} for characteristic ${characteristic}:`,
                    error,
                  );
                }
              }
            }
          }
        }

        let p = peripherals.get(peripheral.name || '');
        if (p) {
          addOrUpdatePeripheral(peripheral.name, { ...peripheral, rssi });
        }

        syncDevice(peripheralData, peripheral).catch(async err => {
          await BleManager.disconnect(peripheralData.id);
          setCurrentMedication('');
          setIsSyncingData(false);
          console.log('syncDevice error', err);
        });
      }
    } catch (error) {
      console.error(
        `[connectPeripheral][${peripheral.id}] connectPeripheral error`,
        error,
      );
    }
  };

  function sleep(ms: number) {
    return new Promise<void>(resolve => setTimeout(resolve, ms));
  }

  const handleAndroidPermissions = () => {
    if (Platform.OS === 'android' && Platform.Version >= 31) {
      PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.BLUETOOTH_SCAN,
        PermissionsAndroid.PERMISSIONS.BLUETOOTH_CONNECT,
      ]).then(result => {
        if (result) {
          console.debug(
            '[handleAndroidPermissions] User accepts runtime permissions android 12+',
          );
        } else {
          console.error(
            '[handleAndroidPermissions] User refuses runtime permissions android 12+',
          );
        }
      });
    } else if (Platform.OS === 'android' && Platform.Version >= 23) {
      PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      ).then(checkResult => {
        if (checkResult) {
          console.debug(
            '[handleAndroidPermissions] runtime permission Android <12 already OK',
          );
        } else {
          PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          ).then(requestResult => {
            if (requestResult) {
              console.debug(
                '[handleAndroidPermissions] User accepts runtime permission android <12',
              );
            } else {
              console.error(
                '[handleAndroidPermissions] User refuses runtime permission android <12',
              );
            }
          });
        }
      });
    }
  };

  return {
    syncDevice,
    connectPeripheral,
    sleep,
    handleAndroidPermissions,
    retrieveConnected,
    togglePeripheralConnection,
    handleDiscoverPeripheral,
    handleUpdateValueForCharacteristic,
    handleDisconnectedPeripheral,
    handleStopScan,
    startScan,
    addOrUpdatePeripheral,
  };
}


// const t = {
//     "advertising": {
//       "isConnectable": 1,
//       "kCBAdvDataRxPrimaryPHY": 129,
//       "kCBAdvDataRxSecondaryPHY": 0,
//       "kCBAdvDataTimestamp": 713008426.411468,
//       "localName": "Ocuelar-9B3123",
//       "txPowerLevel": 9
//     },
//     "characteristics": [
//       {
//         "characteristic": "3ab4",
//         "isNotifying": false,
//         "properties": [Object],
//         "service": "0000180d-0000-1000-8000-00805f9b3123"
//       }, {
//         "characteristic": "2a19",
//         "isNotifying": false,
//         "properties": [Object],
//         "service": "0000180d-0000-1000-8000-00805f9b3123"
//       }, {
//         "characteristic": "2a08",
//         "isNotifying": false,
//         "properties": [Object],
//         "service": "0000180d-0000-1000-8000-00805f9b3123"
//       }, {
//         "characteristic": "3000",
//         "isNotifying": false,
//         "properties": [Object],
//         "service": "0000180d-0000-1000-8000-00805f9b3123"
//       }, {
//         "characteristic": "9000",
//         "isNotifying": false,
//         "properties": [Object],
//         "service": "0000180d-0000-1000-8000-00805f9b3123"
//       }],
//     "id": "f1b5dbef-c841-4bc9-2b83-1dcc487da57e",
//     "name": "Ocuelar-9B3123",
//     "rssi": -58,
//     "services": [{ "uuid": "0000180d-0000-1000-8000-00805f9b3123" }]
//   }
