import {useNavigation} from '@react-navigation/native';
import {Image, Pressable, ScrollView, StyleSheet, Text} from 'react-native';
import {View} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
const StoreProductListPage = () => {
  return (
    <ScrollView style={{flex: 1, paddingHorizontal: 17}}>
      <Text
        style={{
          color: '#666666',
          textAlign: 'center',
          marginTop: 21,
          fontWeight: 'bold',
        }}>
        Coin Bank Total
      </Text>
      <Text
        style={{
          color: '#272727',
          fontSize: 48,
          textAlign: 'center',
        }}>
        23456
      </Text>
      <Card />
      <Card />
      <Card />
    </ScrollView>
  );
};
const TriangleCorner = ({style}) => {
  return <View style={[styles.triangleCorner, style]} />;
};
const styles = StyleSheet.create({
  triangleCorner: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderRightWidth: 100,
    borderTopWidth: 100,
    borderRightColor: 'transparent',
    borderTopColor: '#34cfc3',
  },
  triangleCornerBottomRight: {
    transform: [{rotate: '180deg'}],
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
});
const Card = () => {
  const {navigate} = useNavigation();
  return (
    <Pressable
      onPress={() => {
        navigate('ProductDetailPage');
      }}
      style={{
        backgroundColor: 'white',
        paddingVertical: 12,
        paddingHorizontal: 13,
        borderRadius: 6,
        flexDirection: 'row',
        overflow: 'hidden',
        marginBottom: 16,
      }}>
      <Image style={{width: 150, height: 120, backgroundColor: 'gray'}} />
      <View
        style={{
          marginLeft: 13,
        }}>
        <Text style={{fontWeight: 'bold', fontSize: 18}}>$5 Target</Text>
        <Text style={{}}>300 Coins</Text>
      </View>
      <TriangleCorner
        style={[styles.triangleCornerBottomRight]}></TriangleCorner>
      <Entypo
        style={{position: 'absolute', bottom: 10, right: 10, zIndex: 99}}
        name="plus"
        size={40}
        color="black"
      />
    </Pressable>
  );
};
export default StoreProductListPage;
