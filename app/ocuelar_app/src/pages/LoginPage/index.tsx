import {FormProvider, useController, useForm} from 'react-hook-form';
import {
  ActivityIndicator,
  Alert,
  Image,
  Pressable,
  ScrollView,
  Text,
  TextInput,
  TextInputProps,
  View,
  useWindowDimensions,
} from 'react-native';
import React from 'react';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import * as yup from 'yup';
import {yupResolver} from '@hookform/resolvers/yup';
import {Color} from '../../theme';
import {httpClient} from '../../lib/network/httpClient';
import axios from 'axios';
import {useMMKVString} from 'react-native-mmkv';
import {useTranslation} from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import UpdatePasswordPage from '../UpdatePasswordPage';
import {SetupPage} from '../DataPage';
import CameraScanPage from '../CameraScan';
interface IFormInput {
  account: string;
  password: string;
}
const schema = yup
  .object({
    account: yup.string().required(),
    password: yup.string().required(),
  })
  .required();
const IntoPage = () => {
  const [step, setStep] = React.useState(0);
  const [token, setToken] = useMMKVString('user.token');
  const [chatToken, setChatToken] = useMMKVString('user.chatToken');
  const [user, setUser] = useMMKVString('user.user');
  const [tempUserData, setTempUserData] = React.useState(null);
  if (step === 0)
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'white',
        }}>
        <Text style={{fontSize: 32, color: '#727272', marginBottom: 20}}>
          Welcome!
        </Text>
        <Counter
          onPress={() => setStep(1)}
          text={
            'Enter the unique patient number given to you and provide a password.'
          }
          active
          label={1}
        />
        <Counter
          onPress={() => setStep(1)}
          text={
            'Scan QR Code on the device, provide WIFI name and password, and assign medication. Make sure to charge devices before pairing.'
          }
          label={2}
        />
        <Counter
          onPress={() => setStep(1)}
          text={'Set your drop schedule.'}
          label={3}
        />
      </View>
    );
  if (step === 1)
    return (
      <UpdatePasswordPage
        onSucess={data => {
          setStep(2);
          setTempUserData(data);
        }}
      />
    );
  if (step === 2) {
    return (
      <View style={{marginTop: 100, flex: 1}}>
        <SetupPage
          onPress={() => {
            setStep(3);
          }}
        />
      </View>
    );
  }
  return (
    <CameraScanPage
      onSuccess={() => {
        const {token, chat_token, ...userData} = tempUserData;
        setToken(token);
        setChatToken(chat_token);
        setUser(JSON.stringify(userData));
      }}
    />
  );
};
const Counter = ({active, label, text, onPress}) => {
  return (
    <Pressable
      onPress={onPress}
      style={{
        flexDirection: 'row',
        width: '80%',
        alignItems: 'center',
        marginBottom: 20,
      }}>
      <View
        style={{
          width: 63,
          height: 63,
          borderRadius: 63 / 2,
          backgroundColor: active ? '#0bcbbc' : '#f1f1f1',
          justifyContent: 'center',
          alignItems: 'center',
          borderColor: active ? '#0bcbbc' : '#f1f1f1',
        }}>
        <Text
          style={{
            color: active ? 'white' : '#999999',
            fontSize: 32,
          }}>
          {label}
        </Text>
      </View>
      <Text style={{marginLeft: 10, color: '#999999', maxWidth: '70%'}}>
        {text}
      </Text>
      <AntDesign name="arrowright" size={24} color="black" />
    </Pressable>
  );
};
const LoginPage = () => {
  const methods = useForm<IFormInput>({
    resolver: yupResolver(schema),
    mode: 'all',
    defaultValues: {
      account: '',
      password: '',
    },
  });
  const [token, setToken] = useMMKVString('user.token');
  const [chatToken, setChatToken] = useMMKVString('user.chatToken');
  const [user, setUser] = useMMKVString('user.user');
  const {top} = useSafeAreaInsets();
  const {height} = useWindowDimensions();
  const {t} = useTranslation();
  const [step, setStep] = React.useState(0);
  const onSubmit = async (data: IFormInput) => {
    try {
      const response = await httpClient.post('/api/login', {
        account: data.account,
        password: data.password,
      });
      axios.defaults.headers.common.Authorization = `Bearer ${response.data.token}`;
      const {token, chat_token, ...userData} = response.data;
      console.log(response.data);
      setToken(token);
      setChatToken(chat_token);
      setUser(JSON.stringify(userData));
    } catch (error) {
      Alert.alert(
        'failed to login',
        error?.response?.data?.message ?? 'unknown error',
      );
    }
  };
  console.log(t('username'));
  if (step === 0) {
    return <IntoPage />;
  }
  return (
    <FormProvider {...methods}>
      <KeyboardAwareScrollView style={{flex: 1, backgroundColor: '#308a7d4b'}}>
        <View
          style={{
            flex: 1,
            //   justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: 20,
            paddingTop: top + height * 0.1,
          }}>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
            }}>
            <Image
              style={{
                width: 60,
                height: 60,
                marginRight: 5,
                marginBottom: 10,
              }}
              source={require('../../assets/logo.png')}
            />
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 18,
                marginBottom: 50,
                color: '#2ca58d',
                fontFamily: 'Barlow',
              }}>
              {t('login_to_ocuelar')}
            </Text>
          </View>
          <View
            style={{
              backgroundColor: 'white',
              paddingTop: 20,
              paddingHorizontal: 10,
              width: '100%',
              borderRadius: 6,
            }}>
            <ControlledInput
              textContentType="username"
              name={'account'}
              title={'account'}
            />
            <ControlledInput
              secureTextEntry
              textContentType="password"
              name={'password'}
              title={'password'}
            />
            <Pressable
              disabled={methods.formState.isSubmitting}
              onPress={methods.handleSubmit(onSubmit)}
              style={{
                backgroundColor: '#7ed3c2',
                height: 40,
                borderRadius: 6,
                justifyContent: 'center',
                alignItems: 'center',
                marginVertical: 20,
              }}>
              {methods.formState.isSubmitting ? (
                <ActivityIndicator color={'white'} />
              ) : (
                <Text
                  style={{color: 'white', fontWeight: 'bold', fontSize: 16}}>
                  {t('login')}
                </Text>
              )}
            </Pressable>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </FormProvider>
  );
};
interface ControlledInputProps extends TextInputProps {
  title: string;
  name: string;
}
export const ControlledInput = ({
  title,
  name,
  ...props
}: ControlledInputProps) => {
  const {
    field,
    fieldState: {error},
  } = useController({
    name,
  });
  const {t} = useTranslation();
  return (
    <View style={{marginBottom: 10}}>
      <Text
        style={{
          marginBottom: 2,
          fontWeight: 'bold',
          color: '#808080',
          fontFamily: 'barlow',
        }}>
        {t(title)}
      </Text>
      <View
        style={{
          width: '100%',
          height: 35,
          borderBottomWidth: 1,
          borderBottomColor: error ? Color.error : '#d3d3d3',
        }}>
        <TextInput
          autoCapitalize="none"
          autoCorrect={false}
          keyboardType="ascii-capable"
          textContentType="username"
          style={{width: '100%', height: '100%'}}
          onChangeText={field.onChange}
          onBlur={field.onBlur}
          value={field.value}
          {...props}
        />
      </View>
      <Text style={{color: Color.error, margin: 3, fontFamily: 'barlow'}}>
        {error?.message}
      </Text>
    </View>
  );
};
export default LoginPage;
