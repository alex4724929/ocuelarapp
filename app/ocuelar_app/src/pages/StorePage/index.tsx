import {useNavigation} from '@react-navigation/native';
import {Image, Pressable, ScrollView, Text} from 'react-native';
import {StyleSheet, View, useWindowDimensions} from 'react-native';

const StorePage = () => {
  const {navigate} = useNavigation();
  const {width} = useWindowDimensions();
  return (
    <ScrollView style={{flex: 1, backgroundColor: 'white'}}>
      <View
        style={{
          width: width,
          height: width,
          borderBottomLeftRadius: width / 2,
          borderBottomRightRadius: width / 2,
          backgroundColor: '#ffca67',
          transform: [{scaleX: 2}],
          marginTop: -width / 3,
        }}></View>
      <View
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%',
          height: '100%',
        }}>
        <Text style={{marginLeft: 14.5, fontWeight: 'bold', marginBottom: 14}}>
          FEATURED
        </Text>
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          <Card title={'Target'} point={300} />
          <Card title={'Amazon'} point={300} />
          <Card title={'Visa'} point={300} />
        </ScrollView>
      </View>
      <Text
        style={{
          color: '#666666',
          textAlign: 'center',
          marginTop: 21,
          fontWeight: 'bold',
        }}>
        Coin Bank Total
      </Text>
      <Text
        style={{
          color: '#272727',
          fontSize: 48,
          textAlign: 'center',
        }}>
        23456
      </Text>
      <Progress />
      <Pressable
        onPress={() => {
          navigate('StoreProductListPage');
        }}
        style={{
          backgroundColor: '#34cfc3',
          borderRadius: 30,
          justifyContent: 'center',
          alignItems: 'center',
          width: 182,
          height: 54,
          alignSelf: 'center',
        }}>
        <Text style={{fontWeight: 'bold', fontSize: 18}}>Shop</Text>
      </Pressable>
    </ScrollView>
  );
};
const Progress = () => {
  const {width} = useWindowDimensions();
  return (
    <Image
      style={{width: width, height: (width / 1162) * 196}}
      source={require('./Progress.png')}
    />
  );
};
const Card = ({title, point}) => {
  return (
    <View
      style={{
        marginRight: 5,
      }}>
      <Image
        source={{uri: ''}}
        style={{
          width: 150,
          height: 120,
          backgroundColor: 'gray',
        }}
      />
      <Text
        style={{
          fontSize: 21,
          fontWeight: 'bold',
        }}>
        {title}
      </Text>
      <Text
        style={{
          fontSize: 16,
          fontWeight: '600',
        }}>
        {point} Coins
      </Text>
    </View>
  );
};
export default StorePage;
