import {useIsFocused, useNavigation} from '@react-navigation/native';
import React from 'react';
import {FlatList, Pressable, Text, View} from 'react-native';
import Orientation from 'react-native-orientation-locker';

const GameList = () => {
  const {navigate} = useNavigation();
  const isFocused = useIsFocused();
  React.useEffect(() => {
    if (isFocused) {
      Orientation.lockToPortrait();
    }
  }, [isFocused]);
  return (
    <FlatList
      data={['blackjack21', 'candyMatchSaga', 'tronix', 'sudoku']}
      keyExtractor={item => item}
      renderItem={({item, index}) => {
        return (
          <Pressable
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              paddingVertical: 10,
              paddingHorizontal: 10,
            }}
            onPress={() => {
              if (
                item === 'blackjack21' ||
                item === 'candyMatchSaga' ||
                item === 'tronix'
              ) {
                Orientation.lockToLandscape();
              }
              navigate('GameWebView', {gameName: item, port: 8080 + index * 2});
            }}>
            <Text>{item}</Text>
          </Pressable>
        );
      }}
    />
  );
};
export default GameList;
