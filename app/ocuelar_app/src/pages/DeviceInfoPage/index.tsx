import React from 'react';
import {
  ActivityIndicator,
  Alert,
  Image,
  ScrollView,
  Text,
  View,
} from 'react-native';
import {httpClient} from '../../lib/network/httpClient';
import {useNavigation, useRoute} from '@react-navigation/native';
import {useMMKVString} from 'react-native-mmkv';
import DefaultButton from '../../components/DefaultButton';
// import {InAppBrowser} from 'react-native-inappbrowser-reborn';
import WifiManager from 'react-native-wifi-reborn';
import {useTranslation} from 'react-i18next';
interface Response {
  display_udid: string;
  end_date: any;
  id: string;
  label: any;
  last_online_time: any;
  owner: string;
  ssid: any;
  start_date: any;
  times: any;
}

const DeviceInfoPage = () => {
  const {params} = useRoute();
  const [isLoad, setIsLoad] = React.useState(true);
  const [response, setResponse] = React.useState<Response>();
  const [user] = useMMKVString('user.user');
  const navigation = useNavigation();
  const {t} = useTranslation();
  React.useEffect(() => {
    (async () => {
      try {
        const {data} = await httpClient.post('/api/devices/getInfo', {
          deviceId: params?.deviceId,
        });
        setResponse(data);
        console.log(data);
      } catch (error) {
      } finally {
        setIsLoad(false);
      }
    })();
  }, [params?.deviceId]);
  const isSameUser = React.useMemo(() => {
    return JSON.parse(user ?? '{}').id === response?.owner;
  }, [response?.owner]);
  const failCount = React.useRef(0);

  const [isConnecting, setIsConnecting] = React.useState(false);

  const connectWifi = () => {
    setIsConnecting(true);
    WifiManager.connectToSSID(params?.ssid)
      .then(() => {
        console.log('Connected successfully!');
        navigation.navigate('Home');
        // InAppBrowser.open('http://192.168.1.4');
        setIsConnecting(false);
        params?.onSucess?.();
      })
      .catch(e => {
        console.log(failCount.current);
        if (failCount.current < 5) {
          setTimeout(() => {
            failCount.current = failCount.current + 1;
            connectWifi();
          }, 2000);
        } else {
          setIsConnecting(false);
          Alert.alert('Connection failed!', JSON.stringify(e));
        }
      });
  };
  const bindeDevice = async () => {
    try {
      await httpClient.post('/api/devices/binding', {
        deviceId: params.deviceId,
      });
      connectWifi();
    } catch (error) {}
  };
  if (isLoad) {
    return <ActivityIndicator color={'black'} size={'large'} />;
  }
  if (!response) {
    return <Text>Device not found</Text>;
  }
  return (
    <ScrollView style={{flex: 1}}>
      <View style={{flex: 1, alignItems: 'center', paddingTop: 100}}>
        <Image
          style={{
            width: 120,
            height: 120,
            marginRight: 5,
            marginBottom: 10,
          }}
          source={require('../../assets/logo.png')}
        />
        <Text style={{fontWeight: 'bold', fontSize: 20, marginTop: 20}}>
          {response?.display_udid}
        </Text>
        {response?.owner && (
          <Text style={{marginTop: 10, marginBottom: 50}}>
            {isSameUser ? t('ask_wifi_update') : t('reset_change_user')}
          </Text>
        )}
        {isSameUser && (
          <DefaultButton onPress={connectWifi} title={t('update_wifi')} />
        )}
        {!isSameUser && response.owner ? (
          <DefaultButton
            onPress={bindeDevice}
            title={t('reset_and_pair_device')}
          />
        ) : response.owner ? null : (
          <DefaultButton onPress={bindeDevice} title={t('pair_device')} />
        )}
      </View>
    </ScrollView>
  );
};
export default DeviceInfoPage;
