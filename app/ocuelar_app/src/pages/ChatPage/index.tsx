import React from 'react';
import {Button, SafeAreaView, StyleSheet, View} from 'react-native';
import {
  Channel,
  Chat,
  ImageUploadPreview,
  OverlayProvider,
  AutoCompleteInput,
  useMessageInputContext,
  FileUploadPreview,
  ChannelList,
  MessageList,
  MessageInput,
} from 'stream-chat-react-native';

import {StreamChat} from 'stream-chat';
import {useMMKVString} from 'react-native-mmkv';
import {
  useSafeAreaFrame,
  useSafeAreaInsets,
} from 'react-native-safe-area-context';
import {useMessagingContext} from '../../context/MessagingContex';
const client = StreamChat.getInstance('g7m74vwxmjkg');
const CustomInput = props => {
  const {sendMessage, text, toggleAttachmentPicker, openCommandsPicker} =
    useMessageInputContext();

  return (
    <View style={styles.fullWidth}>
      <ImageUploadPreview />
      <FileUploadPreview />
      <View style={[styles.fullWidth, styles.inputContainer]}>
        <AutoCompleteInput />
      </View>
      <View style={[styles.fullWidth, styles.row]}>
        <Button title="Attach" onPress={toggleAttachmentPicker} />
        <Button title="Commands" onPress={openCommandsPicker} />
        <Button title="Send" onPress={sendMessage} disabled={!text} />
      </View>
    </View>
  );
};

const ChatPage = () => {
  const {channel} = useMessagingContext();
  const {bottom} = useSafeAreaInsets();
  return (
    <Channel
      channel={channel}
      //   keyboardVerticalOffset={headerHeight}
      //   Message={CustomMessageComponent}
    >
      <View style={{flex: 1, paddingBottom: bottom}}>
        <MessageList />
        <MessageInput />
      </View>
    </Channel>
  );
};
export default ChatPage;
const styles = StyleSheet.create({
  flex: {flex: 1},
  fullWidth: {
    width: '100%',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  inputContainer: {
    height: 40,
  },
});
