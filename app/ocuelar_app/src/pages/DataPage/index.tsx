import {MonthlyView} from '../../components/MonthlyView';
import React from 'react';
import {useScheduleStore} from '../../store/scheduleStore';
import {Image, Pressable, ScrollView, Text, View} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Controller,
  FormProvider,
  useFieldArray,
  useForm,
} from 'react-hook-form';
import moment from 'moment';
import {storage} from '../../lib/mmkv';
import {useTranslation} from 'react-i18next';
const DataPage = () => {
  const dateRanges = useScheduleStore(state => state.dateRange);
  React.useEffect(() => {
    storage.delete('dateRange');
  }, []);
  if (dateRanges.length === 0) {
    return <SetupPage />;
  } else {
    return (
      <View style={{flex: 1}}>
        <MonthlyView />
      </View>
    );
  }
};
export interface IFormInput {
  startDay: Date;
  endDay: Date;
  details: Date[];
}
export const SetupPage = ({onPress}) => {
  const methods = useForm<IFormInput>({
    defaultValues: {
      details: [],
      startDay: new Date(),
      endDay: new Date(),
    },
  });
  const {t} = useTranslation();
  const addDateRange = useScheduleStore(state => state.addDateRange);
  const [step, setStep] = React.useState(0);
  const [type, setType] = React.useState<'default' | 'myown'>('default');
  return (
    <ScrollView style={{flex: 1, backgroundColor: 'white'}}>
      <FormProvider {...methods}>
        <View style={{flex: 1}}>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
            }}>
            <Image
              style={{
                width: 60,
                height: 60,
                marginRight: 5,
                marginBottom: 10,
              }}
              source={require('../../assets/logo.png')}
            />
            <Text
              style={{
                fontWeight: '500',
                fontSize: 18,
                marginBottom: 50,
                fontFamily: 'Barlow',
              }}>
              {t('setup_schedule')}
            </Text>
          </View>
          <View style={{marginHorizontal: 42}}>
            <CheckBox
              onChange={() => {
                setType('default');
              }}
              checked={type === 'default'}
              label={t('default_setup')}
            />
          </View>

          {step === 0 && type === 'default' && (
            <View style={{marginLeft: 30}}>
              <Text style={{marginLeft: 40, marginRight: 10}}>
                {t('setup_guide')}
              </Text>
              <DateBlock />
            </View>
          )}
          <View style={{marginHorizontal: 42, marginTop: 20}}>
            <CheckBox
              onChange={() => {
                setType('myown');
              }}
              checked={type === 'myown'}
              label={t('default_setup_my_own')}
            />
          </View>
          {type === 'myown' && (
            <View>
              <FieldArray control={methods.control} />
            </View>
          )}

          <Pressable
            onPress={() => {
              console.log('pressed');
              if (onPress) {
                onPress();
                return;
              }
              if (step === 0) {
                setStep(1);
              } else {
                methods.handleSubmit(addDateRange)();
              }
            }}
            style={{
              backgroundColor: '#00b5b8',
              justifyContent: 'center',
              alignItems: 'center',
              width: 200,
              paddingVertical: 17,
              alignSelf: 'center',
              borderRadius: 4,
              marginTop: 50,
            }}>
            <Text style={{fontSize: 20, color: 'white', fontWeight: 'bold'}}>
              {t('save')}
            </Text>
          </Pressable>
        </View>
      </FormProvider>
    </ScrollView>
  );
};
const DateBlock = () => {
  const {t} = useTranslation();
  return (
    <View style={{flexDirection: 'row'}}>
      <Controller
        name="startDay"
        render={({field: {onChange, value}}) => {
          return (
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'flex-start',
                marginHorizontal: 32,
                marginTop: 20,
              }}>
              <Text style={{marginLeft: 10, marginBottom: 0}}>
                {t('start_of_day')}
              </Text>
              <DateTimePicker
                mode="time"
                value={value}
                onChange={(e, date) => {
                  onChange(date);
                }}
                value={new Date()}
              />
            </View>
          );
        }}
      />

      <Controller
        name="endDay"
        render={({field: {onChange, value}}) => {
          return (
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'flex-start',
                marginHorizontal: 32,
                marginTop: 20,
              }}>
              <Text style={{marginLeft: 10, marginBottom: 0}}>
                {t('end_of_day')}
              </Text>
              <DateTimePicker
                mode="time"
                onChange={(e, date) => {
                  onChange(date);
                }}
                value={value}
              />
            </View>
          );
        }}
      />
    </View>
  );
};
const CheckBox = ({label, checked, onChange}: any) => {
  return (
    <Pressable
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginBottom: 5,
      }}
      onPress={onChange}>
      <View
        style={{
          borderColor: 'rgba(215,215,215,1)',
          borderWidth: 1,
          borderRadius: 10,
          width: 20,
          height: 20,
          backgroundColor: checked ? '#979797' : 'white',
          marginRight: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        {checked && (
          <View
            style={{
              backgroundColor: '#d8d8d8',
              width: 8,
              height: 8,
              borderRadius: 4,
            }}
          />
        )}
      </View>
      <Text
        style={{
          fontWeight: '500',
          fontSize: 18,

          fontFamily: 'Barlow',
        }}>
        {label}
      </Text>
    </Pressable>
  );
};
function FieldArray({control}: {control: any}) {
  const {fields, append, prepend, remove, swap, move, insert} = useFieldArray({
    control, // control props comes from useForm (optional: if you are using FormContext)
    name: 'details', // unique name for your Field Array
  });
  const {t} = useTranslation();
  return (
    <View style={{flex: 1, marginHorizontal: 42, alignItems: 'flex-start'}}>
      <Pressable
        onPress={() => {
          append({time: moment().startOf('day').toDate()});
        }}
        style={{flexDirection: 'row', alignItems: 'center', marginTop: 30}}>
        <Ionicons
          style={{
            color: '#00b5b8',
          }}
          name="add-circle"
          size={24}
          color="black"
        />
        <Text>{t('add_time')}</Text>
      </Pressable>
      <Text style={{marginTop: 20}}>
        {fields.length}x/{t('day')}
      </Text>
      {fields.map((field, index) => (
        <Controller
          key={index + 'time'}
          name={`details.${index}.time`}
          render={({field: {onChange, value}}) => {
            return (
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginTop: 10,
                }}>
                <View
                  style={{
                    width: 150,
                    flexDirection: 'row',
                  }}>
                  <DateTimePicker
                    onChange={(e, selectedDate) => {
                      onChange(selectedDate);
                    }}
                    mode="time"
                    value={value ?? new Date()}
                  />
                </View>
                <Pressable
                  onPress={() => {
                    remove(index);
                  }}>
                  <Ionicons name="remove-circle" size={24} color="red" />
                </Pressable>
              </View>
            );
          }}
        />
      ))}
    </View>
  );
}
export default DataPage;
