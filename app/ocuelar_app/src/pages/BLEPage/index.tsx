import {
  ActivityIndicator,
  Dimensions,
  FlatList,
  ImageBackground,
  RefreshControl,
  SafeAreaView,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {Color} from '../../theme';
import {rescaleHeight, rescaleWidth} from '../../lib/utils/rescale';
import Entypo from 'react-native-vector-icons/Entypo';
import React, {useContext, useEffect} from 'react';
import {DrawerActions, useNavigation} from '@react-navigation/native';
import {BLETest} from '../../BLETest';
import {Peripheral} from 'react-native-ble-manager';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {PeripheralsContext} from '../../../App';

const {width, height} = Dimensions.get('screen');
const BLEPage = () => {
  const navigation = useNavigation();
  const {peripherals, setPeripherals, isScanning, isSyncingData} =
    useContext(PeripheralsContext);
  // const {togglePeripheralConnection, startScan} = BLETest();

  const renderHeader = () => {
    return (
      <SafeAreaView>
        <View style={{marginVertical: 12, alignItems: 'center'}}>
          <TouchableWithoutFeedback
            onPress={() => navigation.dispatch(DrawerActions.openDrawer)}>
            <View style={{position: 'absolute', left: 12}}>
              <Entypo name={'menu'} size={20} />
            </View>
          </TouchableWithoutFeedback>
          <Text style={{fontSize: 17, fontWeight: 'bold'}}>Pair Bluetooth</Text>
        </View>
      </SafeAreaView>
    );
  };

  const renderItem = ({item}: {item: Peripheral}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          !isSyncingData && togglePeripheralConnection(item);
        }}>
        <View
          style={{
            borderTopWidth: 1,
            borderBottomWidth: 1,
            borderColor: '#e3e3e3',
            flexDirection: 'row',
            alignItems: 'center',
            paddingLeft: 10,
            paddingRight: 10,
            paddingTop: 8,
            paddingBottom: 8,
            marginHorizontal: 8,
            borderRadius: 20,
            marginBottom: 8,
            width: rescaleWidth(270),
          }}>
          <FontAwesome5 name={'signal'} color={Color.main} size={18} />
          <Text
            style={{
              fontSize: 16,
              fontWeight: '300',
              color: Color.text,
              textAlign: 'center',
              padding: 10,
            }}>
            {/* completeLocalName (item.name) & shortAdvertisingName (advertising.localName) may not always be the same */}
            {item.name}
            {/*{item.connecting && ' - Connecting...'}*/}
          </Text>
          {isSyncingData && (
            <ActivityIndicator
              style={{flex: 1, justifyContent: 'flex-end'}}
              size={'small'}
              color={Color.main}
            />
          )}
          {/*<Text style={styles.rssi}>RSSI: {item.rssi}</Text>*/}
          {/*<Text style={styles.peripheralId}>{item.id}</Text>*/}
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={{flex: 1}}>
      {renderHeader()}
      <View style={{alignItems: 'center', marginTop: 44, flex: 1}}>
        <Text
          style={{
            color: Color.text,
            textAlign: 'center',
            fontSize: 18,
            fontWeight: '600',
          }}>
          Device found!
        </Text>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 8,
          }}>
          <Text style={{color: Color.text_gray, fontSize: 14}}>
            Click bluetooth device below
          </Text>
          <Text style={{color: Color.text_gray, fontSize: 14}}>
            to pair with your Vigamox
          </Text>
        </View>
        <FlatList
          data={Array.from(peripherals.values())}
          style={{flex: 1, minHeight: 300, marginTop: 24}}
          renderItem={renderItem}
          keyExtractor={item => item.id}
          refreshControl={
            <RefreshControl
              refreshing={isScanning}
              onRefresh={() => {
                startScan();
              }}
            />
          }
        />
      </View>
      {isScanning && (
        <View
          style={{
            width,
            height,
            position: 'absolute',
            zIndex: 99,
            backgroundColor: 'rgba(0,0,0, 0.3)',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <ActivityIndicator size={'large'} color={Color.main} />
        </View>
      )}
      <ImageBackground
        style={{
          width,
          height: rescaleHeight(168),
          zIndex: -1,
        }}
        source={require('../../assets/bgFooter.png')}>
        <View
          style={{
            width,
            height: rescaleHeight(168),
            alignItems: 'center',
            justifyContent: 'center',
            bottom: 200,
          }}></View>
      </ImageBackground>
    </View>
  );
};

export default BLEPage;
