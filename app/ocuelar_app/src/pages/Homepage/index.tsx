import React, {useState} from 'react';

import {
  ActivityIndicator,
  Alert,
  ImageBackground,
  Pressable,
  ScrollView,
  Text,
  View,
  useWindowDimensions,
} from 'react-native';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {useTranslation} from 'react-i18next';
import {rescaleHeight, rescaleWidth} from '../../lib/utils/rescale';
import {Color} from '../../theme';
import {Device, useDevicesContext} from '../../context/DevicesContext';
import Entypo from 'react-native-vector-icons/Entypo';
import {httpClient} from '../../lib/network/httpClient';
import {getDevices} from '../../store/devicesStore';
const Homepage = () => {
  const {t} = useTranslation();
  const navigation = useNavigation();
  const {width, height} = useWindowDimensions();
  const {devices} = useDevicesContext();
  const focus = useIsFocused();
  React.useEffect(() => {
    if (focus) {
      getDevices();
    }
  }, [focus]);

  return (
    <View style={{flex: 1}}>
      <View style={{flex: 1}}>
        <View style={{alignItems: 'center', marginTop: 44, flex: 1}}>
          <Text
            style={{
              color: Color.text,
              textAlign: 'center',
              fontSize: 32,
              fontWeight: '400',
            }}>
            {t('welcome!')}
          </Text>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 8,
            }}>
            <Text
              style={{
                color: Color.text_gray,
                fontSize: 14,
                paddingHorizontal: 10,
              }}>
              {t('device_use_guide')}
            </Text>
            <ScrollView
              style={{
                flex: 1,
                width: width - 20,
                marginHorizontal: 10,
                marginTop: 20,
                paddingHorizontal: 10,
              }}>
              <Text
                style={{
                  textAlign: 'left',
                  fontWeight: '700',
                  marginBottom: 20,
                }}>
                {t('paired_devices')}
              </Text>
              {devices.map(item => {
                return <DeviceItem key={item.id} item={item} />;
              })}
            </ScrollView>
          </View>
        </View>
        <ImageBackground
          style={{
            width,
            height: rescaleHeight(168),
            zIndex: -1,
          }}
          source={require('../../assets/bgFooter.png')}>
          <View
            style={{
              width,
              height: rescaleHeight(168),
              alignItems: 'center',
              justifyContent: 'center',
              bottom: 0,
            }}>
            <Pressable
              onPress={() => {
                navigation.navigate('CameraScan');
              }}
              style={{
                backgroundColor: 'black',
                paddingHorizontal: 20,
                paddingVertical: 10,
                borderRadius: 4,
                marginTop: 10,
              }}>
              <Text style={{color: 'white', fontWeight: 'bold', fontSize: 20}}>
                {t('pair_devices')}
              </Text>
            </Pressable>
          </View>
        </ImageBackground>
      </View>
    </View>
  );
};
const DeviceItem = ({item}: {item: Device}) => {
  const [isLoading, setIsLoading] = useState(false);
  const unPairDevice = async () => {
    setIsLoading(true);
    try {
      const {} = await httpClient.post('/api/devices/unPairUser', {
        deviceId: item.id,
      });
      getDevices();
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };
  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 20,
      }}>
      <Entypo name="check" size={24} color={Color.main} />
      <Text style={{fontWeight: '600', fontSize: 20, marginRight: 10}}>
        {item.display_udid}
      </Text>
      <Pressable
        onPress={() => {
          Alert.alert('Unpair Device', 'Are you sure to unpair this device?', [
            {
              text: 'Cancel',
              onPress: () => {},
              style: 'cancel',
            },
            {
              text: 'OK',
              onPress: () => {
                unPairDevice();
              },
            },
          ]);
        }}>
        {isLoading ? (
          <ActivityIndicator size="small" color="black" />
        ) : (
          <AntDesign name="delete" size={20} color="black" />
        )}
      </Pressable>
    </View>
  );
};
export default Homepage;
