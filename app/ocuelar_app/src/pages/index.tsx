import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import Homepage from './Homepage';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import LoginPage from './LoginPage';
import React from 'react';
import {useMMKVString} from 'react-native-mmkv';
import CustomDrawerContent from '../components/CustomDrawerContent';
import messaging from '@react-native-firebase/messaging';
import ChatPage from './ChatPage';
import DataPage from './DataPage';
import {httpClient} from '../lib/network/httpClient';
import GameList from './GameList';
import GameWebView from './Blackjack21';
import ChannelListPage from './ChannelListPage';
import MessagingContexProvider from '../context/MessagingContex';
import RealtimeDataPage from './RealtimeDataPage';
import StorePage from './StorePage';
import StoreProductListPage from './StoreProductListPage';
import ProductDetailPage from './ProductDetailPage';
import {WeeklyView} from '../components/WeeklyView';
import DevicesContextProvider from '../context/DevicesContext';
import {useTranslation} from 'react-i18next';
import CameraScanPage from './CameraScan';
import DeviceInfoPage from './DeviceInfoPage';
import {Platform} from 'react-native';

const Drawer = createDrawerNavigator();
const Stack = createNativeStackNavigator();
export default function RootNavigator() {
  const [token] = useMMKVString('user.token');
  const {t} = useTranslation();
  React.useEffect(() => {
    // Assume a message-notification contains a "type" property in the data payload of the screen to open
    (async () => {
      const authStatus = await messaging().requestPermission();
      const enabled =
        authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
        authStatus === messaging.AuthorizationStatus.PROVISIONAL;

      if (enabled) {
        console.log('Authorization status:', authStatus);
      }
      messaging().onNotificationOpenedApp(remoteMessage => {
        console.log(
          'Notification caused app to open from background state:',
          remoteMessage.notification,
        );
      });

      // Check whether an initial notification is available
      messaging()
        .getInitialNotification()
        .then(remoteMessage => {
          if (remoteMessage) {
            console.log(
              'Notification caused app to open from quit state:',
              remoteMessage.notification,
            );
          }
        });
    })();
  }, []);
  React.useEffect(() => {
    (async () => {
      if (token) {
        try {
          const data = await messaging().getToken();
          httpClient.post('/api/notification', {
            notificationToken: data,
          });
        } catch (error) {}
      }
    })();
  }, [token]);

  return (
    <NavigationContainer>
      {!token ? (
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}>
          <Stack.Screen name="LoginPage" component={LoginPage} />
          <Stack.Screen
            name="CameraScan"
            options={{
              title: t('my_devices'),
              headerShown: false,
            }}
            component={CameraScanPage}
          />
          <Stack.Screen
            name="DeviceInfoPage"
            options={{
              title: t('device_info'),
              headerShown: false,
            }}
            component={DeviceInfoPage}
          />
        </Stack.Navigator>
      ) : (
        <DevicesContextProvider>
          <Drawer.Navigator
            initialRouteName="Home"
            drawerContent={props => <CustomDrawerContent {...props} />}>
            <Drawer.Screen
              name="Home"
              options={{
                title: t('my_devices'),
              }}
              component={HomePageStack}
            />
            <Drawer.Screen
              name="Data"
              options={{
                title: t('my_schedule'),
                headerShown: true,
              }}
              component={React.memo(DataPage)}
            />
            {/* <Drawer.Screen
              name="GameList"
              options={{
                title: t('game_list'),
                headerShown: false,
              }}
              component={GameStack}
            /> */}
            <Drawer.Screen
              name="RealtimeDataPage"
              options={{
                title: t('real_time_data'),
              }}
              component={RealtimeDataPage}
            />
            {/* <Drawer.Screen
              name="StorePage"
              options={{
                headerShown: false,
                title: t('store'),
              }}
              component={StoreStack}
            /> */}
            {/* <Drawer.Screen
            name="BLE"
            options={{
              title: 'Pair Bluetooth',
              headerShown: false,
            }}
            component={BLEPage}
          /> */}
            <Drawer.Screen
              name="Chat"
              options={{
                headerShown: false,
                // title: 'Ocuelar',
                title: t('chat'),
              }}
              component={ChatRoomStack}
            />
            {/* <Drawer.Screen name="Notifications" component={NotificationsScreen} /> */}
          </Drawer.Navigator>
        </DevicesContextProvider>
      )}
    </NavigationContainer>
  );
}
const GameStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="GameList" component={GameList} />
      {Platform.OS === 'ios' && (
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name="GameWebView"
          component={GameWebView}
        />
      )}
    </Stack.Navigator>
  );
};
const StoreStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{
          title: 'Rewards',
          headerLargeTitleShadowVisible: false,
          headerStyle: {
            backgroundColor: '#ffca67',
          },
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
        name="StorePage"
        component={StorePage}
      />
      <Stack.Screen
        options={{
          headerLargeTitleShadowVisible: false,
          title: 'Rewards',
          headerStyle: {
            backgroundColor: '#ffca67',
          },
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
        name="StoreProductListPage"
        component={StoreProductListPage}
      />
      <Stack.Screen
        options={{
          title: 'Item Details',
          headerLargeTitleShadowVisible: false,
          headerStyle: {
            backgroundColor: '#ffca67',
          },
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
        name="ProductDetailPage"
        component={ProductDetailPage}
      />
    </Stack.Navigator>
  );
};
const ChatRoomStack = () => {
  return (
    <MessagingContexProvider>
      <Stack.Navigator screenOptions={{}}>
        <Stack.Screen
          options={{
            title: 'Channel List',
          }}
          name="ChannelListPage"
          component={ChannelListPage}
        />
        <Stack.Screen
          options={{
            title: 'Chat Room',
          }}
          name="ChatPage"
          component={ChatPage}
        />
      </Stack.Navigator>
    </MessagingContexProvider>
  );
};

const HomePageStack = () => {
  const {t} = useTranslation();
  return (
    <Stack.Navigator screenOptions={{}}>
      <Stack.Screen
        name="Home"
        options={{
          title: t('my_devices'),
          headerShown: false,
        }}
        component={Homepage}
      />
      <Stack.Screen
        name="CameraScan"
        options={{
          title: t('my_devices'),
          headerShown: false,
        }}
        component={CameraScanPage}
      />
      <Stack.Screen
        name="DeviceInfoPage"
        options={{
          title: t('device_info'),
          headerShown: false,
        }}
        component={DeviceInfoPage}
      />
    </Stack.Navigator>
  );
};
