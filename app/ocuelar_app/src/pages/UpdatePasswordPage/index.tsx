import {
  ActivityIndicator,
  Alert,
  Pressable,
  ScrollView,
  Text,
  TextInput,
  TextInputProps,
  View,
} from 'react-native';
import React from 'react';
import {FormProvider, useController, useForm} from 'react-hook-form';
import {useTranslation} from 'react-i18next';
import {Color} from '../../theme';
import {httpClient} from '../../lib/network/httpClient';
interface IFormInput {
  username: string;
  password: string;
  confirmPassword: string;
}
const UpdatePasswordPage = ({onSucess}) => {
  const methods = useForm<IFormInput>({
    mode: 'all',
  });
  const {t} = useTranslation();
  const onSubmit = (data: IFormInput) => {
    if (data.password !== data.confirmPassword) {
      Alert.alert('Password not match');
      return;
    }
    httpClient
      .post('/api/users/update-password', {
        user_name: data.username,
        password: data.password,
      })
      .then(res => {
        console.log(res.data);
        onSucess(res.data);
      })
      .catch(err => {
        console.log(err);
        Alert.alert('Error', 'User not found');
      });
  };
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <FormProvider {...methods}>
        <ScrollView style={{flex: 1, marginTop: 100}}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'white',
            }}>
            <ControlledInput title="Enter Patient Number" name="username" />
            <ControlledInput title="Enter password" name="password" />
            <Text style={{width: '80%', marginBottom: 50, marginTop: -10}}>
              At least 12 characters long but 14 or more is better. A
              combination of uppercase letters, lowercase letters, numbers, and
              symbols.
            </Text>
            <ControlledInput title="Confirm password" name="confirmPassword" />
            <Pressable
              disabled={methods.formState.isSubmitting}
              onPress={methods.handleSubmit(onSubmit)}
              style={{
                backgroundColor: '#7ed3c2',
                height: 40,
                borderRadius: 6,
                justifyContent: 'center',
                alignItems: 'center',
                marginVertical: 20,
                width: 100,
              }}>
              {methods.formState.isSubmitting ? (
                <ActivityIndicator color={'white'} />
              ) : (
                <Text
                  style={{color: 'white', fontWeight: 'bold', fontSize: 16}}>
                  Submit
                </Text>
              )}
            </Pressable>
          </View>
        </ScrollView>
      </FormProvider>
    </View>
  );
};
interface ControlledInputProps extends TextInputProps {
  title: string;
  name: string;
}
const ControlledInput = ({title, name, ...props}: ControlledInputProps) => {
  const {
    field,
    fieldState: {error},
  } = useController({
    name,
  });
  const {t} = useTranslation();
  return (
    <View style={{width: '80%'}}>
      <Text
        style={{
          marginBottom: 2,
          fontWeight: 'bold',
          color: '#808080',
          fontFamily: 'barlow',
        }}>
        {t(title)}
      </Text>
      <View
        style={{
          width: '100%',
          height: 35,
          borderBottomWidth: 1,
          borderBottomColor: error ? Color.error : '#d3d3d3',
        }}>
        <TextInput
          autoCapitalize="none"
          autoCorrect={false}
          keyboardType="ascii-capable"
          textContentType="username"
          style={{
            width: '100%',
            height: '100%',
            borderWidth: 1,
            borderColor: '#e3e3e3',
          }}
          onChangeText={field.onChange}
          onBlur={field.onBlur}
          value={field.value}
          {...props}
        />
      </View>
      <Text style={{color: Color.error, margin: 3, fontFamily: 'barlow'}}>
        {error?.message}
      </Text>
    </View>
  );
};
export default UpdatePasswordPage;
