import {Dimensions, Pressable, View} from 'react-native';
import {Camera, useCodeScanner} from 'react-native-vision-camera';
import React from 'react';
import {Text} from 'react-native';
import {useTranslation} from 'react-i18next';
import {Color} from '../../theme';
import {useIsFocused, useNavigation} from '@react-navigation/native';
const CameraScanPage = ({onSuccess}) => {
  const {navigate, goBack} = useNavigation();
  const {t} = useTranslation();
  const devices = Camera.getAvailableCameraDevices();
  const device = devices.find(d => d.position === 'back');
  //   const device = useCameraDevice('back');
  const codeScanner = useCodeScanner({
    codeTypes: ['qr', 'ean-13'],
    onCodeScanned: codes => {
      const item = codes[0].value;
      console.log(item);
      if (
        item?.startsWith('https://ocuelar-portal-web.vercel.app/') &&
        item?.includes('device_id') &&
        item?.includes('ssid')
      ) {
        if (onSuccess) {
          navigate('DeviceInfoPage', {
            deviceId: item?.split('device_id=')[1].split('&')[0],
            ssid: item?.split('ssid=')[1].split('&')[0],
          });
        } else {
          navigate('DeviceInfoPage', {
            deviceId: item?.split('device_id=')[1].split('&')[0],
            ssid: item?.split('ssid=')[1].split('&')[0],
            onSucess: onSuccess,
          });
        }
      }
    },
  });
  const isFocus = useIsFocused();

  if (!isFocus) {
    return null;
  }
  return (
    <View style={{flex: 1}}>
      <Camera
        style={{
          flex: 1,
          width: Dimensions.get('window').width,
          height: Dimensions.get('window').height,
        }}
        isActive={true}
        device={device}
        codeScanner={codeScanner}
      />
      <Pressable
        onPress={() => {
          goBack();
          // navigate('Homepage')
        }}
        style={{
          position: 'absolute',
          bottom: 100,
          backgroundColor: Color.error,
          paddingHorizontal: 20,
          paddingVertical: 10,
          alignSelf: 'center',
          borderRadius: 4,
        }}>
        <Text style={{color: 'white'}}>{t('cancel')}</Text>
      </Pressable>
    </View>
  );
};

export default CameraScanPage;
