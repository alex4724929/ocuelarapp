import {FlatList, ScrollView, View} from 'react-native';
import React, {useState} from 'react';
import {httpClient} from '../../lib/network/httpClient';
import {Text} from 'react-native';
import moment from 'moment';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import PagerView from 'react-native-pager-view';
import {useDevicesContext} from '../../context/DevicesContext';
import {useIsFocused} from '@react-navigation/native';
import DefaultButton from '../../components/DefaultButton';
import {useTranslation} from 'react-i18next';
const RealtimeData = ({id}: {id: string}) => {
  const [records, setRecords] = useState([]);
  const getRecord = React.useCallback(async () => {
    const {data} = await httpClient.get(
      `/api/devices/getTestData?device_id=${id}`,
    );
    setRecords(data);
  }, [id]);
  React.useEffect(() => {
    let timer = setInterval(() => {
      getRecord();
    }, 1000);
    return () => clearInterval(timer);
  }, [getRecord]);
  return (
    <FlatList
      style={{flex: 1, backgroundColor: 'white'}}
      data={records}
      ListEmptyComponent={() => {
        return (
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{color: 'black'}}>No Data</Text>
          </View>
        );
      }}
      renderItem={({item}) => {
        return (
          <View
            style={{
              flexDirection: 'row',
              paddingVertical: 3,
              paddingHorizontal: 5,
            }}>
            <Text style={{flex: 1}}>
              {moment.unix(item.timestamp).format('YYYY/MM/DD HH:mm:ss')}
            </Text>
            <View style={{flex: 1}}>
              {Array.from(Array(parseInt(item.count)).keys()).map(item => {
                return (
                  <MaterialIcons
                    key={`default-${item}`}
                    name={'water-drop'}
                    size={17}
                    color={'green'}
                  />
                );
              })}
            </View>
          </View>
        );
      }}
    />
  );
};
const RealtimeDataPage = () => {
  const {devices} = useDevicesContext();
  const [currentPosition, setCurrentPosition] = useState(0);
  const isFocused = useIsFocused();
  const pagerRef = React.useRef<PagerView>(null);
  const {t} = useTranslation();
  const [isLoading, setIsLoading] = useState(false);
  const restData = async id => {
    setIsLoading(true);
    try {
      await httpClient.post('/api/devices/resetRecord', {
        device_id: id,
      });
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <View style={{flexDirection: 'row', height: 50}}>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          style={{paddingLeft: 10, height: 50, flexDirection: 'row'}}>
          {devices.map((item, index) => {
            return (
              <DefaultButton
                key={item.id}
                style={{marginRight: 10}}
                active={currentPosition === index}
                title={item.display_udid}
                onPress={() => {
                  pagerRef.current?.setPage(index);
                }}
              />
            );
          })}
        </ScrollView>
      </View>
      <PagerView
        onPageSelected={event => {
          setCurrentPosition(event.nativeEvent.position);
        }}
        ref={pagerRef}
        style={{flex: 1, backgroundColor: 'white'}}
        initialPage={0}>
        {devices.map((item, index) => {
          return (
            <View style={{paddingHorizontal: 10}} key={item.id}>
              <View
                style={{
                  backgroundColor: '#f2f2f2',
                  marginVertical: 10,
                  borderRadius: 4,
                  padding: 5,
                }}>
                <Text>label:{item.label ?? '_'}</Text>
                <Text>device Id: {item.id}</Text>
                <DefaultButton
                  onPress={() => restData(item.id)}
                  style={{}}
                  isLoading={isLoading}
                  title={t('reset_data')}
                />
              </View>
              {isFocused && currentPosition === index && (
                <RealtimeData id={item.id} />
              )}
            </View>
          );
        })}
      </PagerView>
    </View>
  );
};
export default RealtimeDataPage;
