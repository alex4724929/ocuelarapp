import {WebView} from 'react-native-webview';
import React from 'react';
import StaticServer from 'react-native-static-server';
import RNFS from 'react-native-fs';
import {useWindowDimensions} from 'react-native';
import Orientation from 'react-native-orientation-locker';
import {useIsFocused, useRoute} from '@react-navigation/native';

const GameWebView = () => {
  const {params} = useRoute();
  const [url, setUrl] = React.useState('');
  const {width, height} = useWindowDimensions();
  const [reloading, setReloading] = React.useState(false);

  React.useEffect(() => {
    let path = RNFS.MainBundlePath + '/' + params?.gameName;
    let server = new StaticServer(params?.port, path);
    server.start().then(url => {
      setUrl(url);
      console.log('Serving at URL', url);
    });
    return () => {
      server.stop();
    };
  }, []);
  React.useEffect(() => {
    setReloading(true);
    setTimeout(() => {
      setReloading(false);
    }, 100);
  }, [width, height]);
  if (reloading) return null;
  return <WebView source={{uri: url}} style={{width, height}} />;
};

export default GameWebView;
