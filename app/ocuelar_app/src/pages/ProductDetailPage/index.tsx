import {Pressable, Text} from 'react-native';
import {Image, View, useWindowDimensions} from 'react-native';

const ProductDetailPage = () => {
  const {width} = useWindowDimensions();
  return (
    <View style={{flex: 1}}>
      <Image style={{width, height: 180, backgroundColor: 'grey'}} />
      <View style={{flexDirection: 'row', padding: 19}}>
        <View
          style={{
            width: 56,
            height: 56,
            borderRadius: 8,
            backgroundColor: 'white',
            marginRight: 14,
          }}></View>
        <View style={{flexDirection: 'column'}}>
          <Text style={{fontWeight: 'bold', fontSize: 18}}>
            $5 Target Gift Card
          </Text>
          <Text style={{}}>300 Coins</Text>
        </View>
      </View>
      <Text style={{paddingHorizontal: 19, marginBottom: 31}}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eget
        ultrices velit. Sed sodales, est ac faucibus pretium, eros ligula
        posuere dui, sit amet porta magna velit eget arcu. Integer ac diam at
        diam pulvinar laoreet. Quisque a risus eget magna facilisis
        sollicitudin. Praesent et nunc semper, auctor mi rhoncus, rutrum ligula.
        Duis cursus convallis ante. Praesent finibus lorem nisl, eget viverra
        nisl pharetra consectetur. Integer dictum eu sem ut ornare. Etiam eget
        volutpat turpis. Nulla facilisis non elit ut eleifend
      </Text>
      <View
        style={{
          width: width,
          height: width,
          borderTopLeftRadius: width / 2,
          borderTopRightRadius: width / 2,
          backgroundColor: 'white',
          transform: [{scaleX: 2}],
        }}
      />
      <Pressable
        style={{
          backgroundColor: '#34cfc3',
          borderRadius: 30,
          justifyContent: 'center',
          alignItems: 'center',
          width: 182,
          height: 54,
          alignSelf: 'center',
          bottom: 100,
          position: 'absolute',
        }}>
        <Text style={{fontWeight: 'bold', fontSize: 18}}>Add to Cart</Text>
      </Pressable>
    </View>
  );
};
export default ProductDetailPage;
