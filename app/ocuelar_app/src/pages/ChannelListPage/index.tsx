import {ChannelList} from 'stream-chat-react-native';
import React from 'react';
import {View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {
  StreamChatGenerics,
  useMessagingContext,
} from '../../context/MessagingContex';
import {Channel as ChannelType, ChannelSort, StreamChat} from 'stream-chat';
const sort: ChannelSort<StreamChatGenerics> = {last_message_at: -1};
const options = {
  presence: true,
  state: true,
  watch: true,
  limit: 30,
};

const ChannelListPage = ({}) => {
  const {navigate} = useNavigation();
  const {setChannel} = useMessagingContext();
  return (
    <View style={{height: '100%'}}>
      <ChannelList
        onSelect={channel => {
          setChannel(channel);
          navigate('ChatPage');
        }}
        options={options}
        sort={sort}
      />
    </View>
  );
};
export default ChannelListPage;
