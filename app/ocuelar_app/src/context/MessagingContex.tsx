import React, {ReactNode} from 'react';
import {useMMKVString} from 'react-native-mmkv';
import {Channel as ChannelType, ChannelSort, StreamChat} from 'stream-chat';
import {
  Chat,
  OverlayProvider,
  ThreadContextValue,
} from 'stream-chat-react-native';
type LocalAttachmentType = Record<string, unknown>;
type LocalChannelType = Record<string, unknown>;
type LocalCommandType = string;
type LocalEventType = Record<string, unknown>;
type LocalMessageType = Record<string, unknown>;
type LocalReactionType = Record<string, unknown>;
type LocalUserType = Record<string, unknown>;

export type StreamChatGenerics = {
  attachmentType: LocalAttachmentType;
  channelType: LocalChannelType;
  commandType: LocalCommandType;
  eventType: LocalEventType;
  messageType: LocalMessageType;
  reactionType: LocalReactionType;
  userType: LocalUserType;
};
const MessagingContext = React.createContext(
  {} as {
    channel: ChannelType<StreamChatGenerics> | undefined;
    setChannel: React.Dispatch<
      React.SetStateAction<ChannelType<StreamChatGenerics> | undefined>
    >;
    setThread: React.Dispatch<
      React.SetStateAction<
        ThreadContextValue<StreamChatGenerics>['thread'] | undefined
      >
    >;
    thread: ThreadContextValue<StreamChatGenerics>['thread'] | undefined;
  },
);
const client = StreamChat.getInstance('g7m74vwxmjkg');
export const useMessagingContext = () => {
  const context = React.useContext(MessagingContext);
  return context;
};
const MessagingContexProvider = ({children}: {children: ReactNode}) => {
  const [user] = useMMKVString('user.user');
  const [chatToken] = useMMKVString('user.chatToken');
  const [channel, setChannel] =
    React.useState<ChannelType<StreamChatGenerics>>();
  const [clientReady, setClientReady] = React.useState(false);
  const [thread, setThread] =
    React.useState<ThreadContextValue<StreamChatGenerics>['thread']>();
  console.log(user);
  React.useEffect(() => {
    (async () => {
      if (user) {
        const userData = JSON.parse(user);

        await client.connectUser(
          {
            id: userData.id,
            name: `${userData.firstName} ${userData.lastName}`,
            image: '',
          },
          chatToken,
        );
        setClientReady(true);
      }
    })();
  }, [user, chatToken]);
  return (
    <MessagingContext.Provider value={{channel, setChannel, thread, setThread}}>
      <OverlayProvider>
        <Chat client={client}>{children}</Chat>
      </OverlayProvider>
    </MessagingContext.Provider>
  );
};

export default MessagingContexProvider;
