import React, {ReactNode} from 'react';
import {getDevices, useDeviceStore} from '../store/devicesStore';

export type Device = {
  owner: string;
  label: string;
  display_udid: string;
  id: string;
  last_online_time: string;
  times: number;
  start_date: string;
  end_date: string;
};
const DevicesContext = React.createContext(
  {} as {
    devices: Device[];
    getDevices: () => void;
  },
);

const DevicesContextProvider = ({children}: {children: ReactNode}) => {
  const devices = useDeviceStore(state => state.devices);
  React.useEffect(() => {
    getDevices();
  }, []);
  React.useEffect(() => {
    let timer = setInterval(() => {
      getDevices();
    }, 30000);
    return () => clearInterval(timer);
  }, []);
  return (
    <DevicesContext.Provider
      value={{
        devices: devices,
      }}>
      {children}
    </DevicesContext.Provider>
  );
};

export const useDevicesContext = () => {
  const context = React.useContext(DevicesContext);
  return context;
};
export default DevicesContextProvider;
