export enum Color {
  error = '#cc0000',
  text = '#333333',
  main = '#0bcbbc',
  text_gray = '#999999',
}
