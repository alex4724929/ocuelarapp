import moment from 'moment';
import React from 'react';
import {Alert} from 'react-native';
import BleManager, {Peripheral} from 'react-native-ble-manager';
const useConnectDevice = (peripheral: Peripheral, serviceUUID: string) => {
  const [isConnected, setIsConnected] = React.useState(false);
  const [recordHistory, setRecordHistory] = React.useState([]);
  const syncDeviceTimeStamp = React.useCallback(async () => {
    const currentTimestamp = moment().unix().toString();
    await BleManager.write(
      peripheral.id,
      serviceUUID,
      '2A08',
      getRegularArr(new TextEncoder().encode(currentTimestamp).buffer),
    );
  }, [peripheral.id, serviceUUID]);
  const connect = React.useCallback(async () => {
    try {
      await BleManager.connect(peripheral.id);
      setIsConnected(true);
      syncDeviceTimeStamp();
    } catch (error) {
      Alert.alert(
        'Error',
        `Failed to connect to device id:${peripheral.id} name ${peripheral.name} `,
      );
    }
  }, [peripheral.id, peripheral.name, syncDeviceTimeStamp]);

  const cleanData = React.useCallback(async () => {
    await BleManager.write(
      peripheral.id,
      serviceUUID,
      '3AB4',
      getRegularArr(new TextEncoder().encode('-1').buffer),
    );
  }, [peripheral.id, serviceUUID]);
  const readData = React.useCallback(async () => {
    let recordsTemp = [];

    //將25頁資料讀出來
    for (let i = 0; i < 25; i++) {
      await BleManager.write(
        peripheral.id,
        serviceUUID,
        '3AB4',
        getRegularArr(new TextEncoder().encode(i.toString()).buffer),
      );
      const res = await BleManager.read(peripheral.id, serviceUUID, '3AB4');
      const decoded = new TextDecoder().decode(new Uint8Array(res));
      const records = JSON.parse(decoded);
      let result = [];

      for (const record of records) {
        const data = {
          ocuelar_id: peripheral.name,
          timestamp: record.timestamp,
          data: record.data,
        };

        result.push(data);
      }
      //合併資料
      recordsTemp = recordsTemp.concat(result);
    }
    //存回歷史紀錄
    setRecordHistory(data => {
      let newData = [...data];
      newData.push(recordsTemp);
      return newData;
    });
  }, [peripheral.id, peripheral.name, serviceUUID]);
  return {
    connect,
    syncDeviceTimeStamp,
    isConnected,
    cleanData,
    readData,
    recordHistory,
  };
};
const getRegularArr = (_buffer: any) => {
  const arr = new Uint8Array(_buffer);
  return Array.from(arr);
};
export default useConnectDevice;
