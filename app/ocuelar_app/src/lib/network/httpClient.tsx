import axios from 'axios';
import {storage} from '../mmkv';

export const httpClient = axios.create({
  baseURL: 'https://ocuelar-portal-web.vercel.app',
});

httpClient.interceptors.request.use(config => {
  config.headers['Content-Type'] = 'application/json';
  config.headers.Authorization = `Bearer ${storage.getString('user.token')}`;
  return config;
});
