import { Dimensions } from "react-native";
const {width, height} = Dimensions.get('screen');

export const rescaleWidth = (_width: number) => {
    return (_width / 375) * width;
};

export const rescaleHeight = (_height: number) => {
    return (_height / 667) * height;
};