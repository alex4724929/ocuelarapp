import {create} from 'zustand';
import {storage} from '../lib/mmkv';
import {IFormInput} from '../pages/DataPage';
interface ScheduleStore {
  schedule: [];
  setSchedule: (schedule: any) => void;
  dateRange: {}[];
  addDateRange: (dateRange: any) => void;
}
export const useScheduleStore = create<ScheduleStore>((set, get) => ({
  schedule: [],
  setSchedule: (schedule: any) => {
    set({schedule});
  },
  dateRange: storage.getString('dateRange')
    ? JSON.parse(storage.getString('dateRange')!)
    : [],
  addDateRange: (dateRange: IFormInput) => {
    storage.set('dateRange', JSON.stringify([...get().dateRange, dateRange]));
    set({dateRange: [...get().dateRange, dateRange]});
  },
}));
