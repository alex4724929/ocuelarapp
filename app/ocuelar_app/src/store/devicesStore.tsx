import {create} from 'zustand';
import {Device} from '../context/DevicesContext';
import {httpClient} from '../lib/network/httpClient';
interface ScheduleStore {
  devices: Device[];
}
export const useDeviceStore = create<ScheduleStore>((set, get) => ({
  devices: [],
}));
export const getDevices = async () => {
  const {data} = await httpClient.get<Device[]>('/api/my-device');
  useDeviceStore.setState({devices: data});
};
