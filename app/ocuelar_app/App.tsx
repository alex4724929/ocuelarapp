import React, {createContext, useState} from 'react';
import RootNavigator from './src/pages';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import Reactotron from 'reactotron-react-native';
import {Peripheral} from 'react-native-ble-manager';
import {useMMKVString} from 'react-native-mmkv';
import ReactNativeModal from 'react-native-modal';
import {Pressable, View} from 'react-native';
import {Text} from 'react-native';
import codePush from 'react-native-code-push';
import i18n from './src/i18n/index';
if (__DEV__) {
  Reactotron.configure() // AsyncStorage would either come from `react-native` or `@react-native-community/async-storage` depending on where you get it from // controls connection & communication settings
    .useReactNative() // add all built-in react native plugins
    .connect(); // let's connect!
}

export const PeripheralsContext = createContext({
  peripherals: new Map<Peripheral['id'], Peripheral>(),
  setPeripherals: (items: any) => {},
  currentMedication: '',
  setCurrentMedication: (deviceName: string) => {},
  isScanning: false,
  setIsScanning: (status: boolean) => {},
  isSyncingData: false,
  setIsSyncingData: (status: boolean) => {},
  connectedDevices: [] as any[],
  setConnectedDevices: (device: any) => {},
});

const PeripheralsProvider = (props: any) => {
  const [peripherals, setPeripherals] = useState<any>(
    new Map<Peripheral['id'], Peripheral>(),
  );
  const [currentMedication, setCurrentMedication] = useState('');
  const [isScanning, setIsScanning] = useState(false);
  const [isSyncingData, setIsSyncingData] = useState(false);
  const [connectedDevices, setConnectedDevices] = useState<any[]>([]);

  return (
    <PeripheralsContext.Provider
      value={{
        peripherals,
        setPeripherals,
        currentMedication,
        setCurrentMedication,
        isScanning,
        setIsScanning,
        isSyncingData,
        setIsSyncingData,
        connectedDevices,
        setConnectedDevices,
      }}>
      {props.children}
    </PeripheralsContext.Provider>
  );
};
function App(): JSX.Element {
  const [lang, setLang] = useMMKVString('lang');
  React.useEffect(() => {
    if (lang) {
      i18n.changeLanguage(lang);
      console.log('lang', lang);
    }
  }, [lang]);
  return (
    <GestureHandlerRootView style={{flex: 1}}>
      <PeripheralsProvider>
        <RootNavigator />
      </PeripheralsProvider>
      <ReactNativeModal style={{margin: 0}} isVisible={!lang}>
        <View
          style={{
            flex: 1,
            backgroundColor: '#f8f8f8',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Pressable
            onPress={() => {
              setLang('en');
            }}
            style={{
              backgroundColor: 'white',
              width: '80%',
              paddingVertical: 10,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 1,
              borderColor: '#ccc',
              marginBottom: 10,
              borderRadius: 4,
            }}>
            <Text>English</Text>
          </Pressable>
          <Pressable
            onPress={() => {
              setLang('zh');
            }}
            style={{
              backgroundColor: 'white',
              width: '80%',
              paddingVertical: 10,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 1,
              borderColor: '#ccc',
              marginBottom: 10,
              borderRadius: 4,
            }}>
            <Text>中文</Text>
          </Pressable>
          <Pressable
            onPress={() => {
              setLang('ko');
            }}
            style={{
              backgroundColor: 'white',
              width: '80%',
              paddingVertical: 10,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 1,
              borderColor: '#ccc',
              marginBottom: 10,
              borderRadius: 4,
            }}>
            <Text>한국어</Text>
          </Pressable>
        </View>
      </ReactNativeModal>
    </GestureHandlerRootView>
  );
}

export default codePush(App);
