import React, {useRef} from "react"
import {AxiosResponse} from "axios"
import {IonContent, IonPage, useIonViewWillEnter} from "@ionic/react"
import {StatusBar, Style} from "@capacitor/status-bar"

import useToken from "../../hooks/useToken"
import {login, refresh} from "../../providers/oauth.provider"

import Logo from "../../assets/images/logo.png"
import {NavigationBar} from "@hugotomazi/capacitor-navigation-bar"

const statusBarColor: string = "#FFCA67"
const navigationBarColor: string = "#FFCA67"
const statusBarStyle: Style = Style.Light

const Page: React.FC = () => {
    useIonViewWillEnter(() => {
        StatusBar.setBackgroundColor({color: statusBarColor}).then()
        StatusBar.setStyle({style: statusBarStyle}).then()
        NavigationBar.setColor({color: navigationBarColor, darkButtons: true}).then()
    })

    const {token, setToken} = useToken()

    const refUsername: React.RefObject<HTMLInputElement> = useRef<HTMLInputElement>(null)
    const refPassword: React.RefObject<HTMLInputElement> = useRef<HTMLInputElement>(null)
    const refBtnLogin: React.RefObject<HTMLButtonElement> = useRef<HTMLButtonElement>(null)
    const [errorMessage, setErrorMessage] = React.useState("")

    useIonViewWillEnter(() => {
        if (token) {
            refresh(token.refresh_token).then((response: AxiosResponse) => {
                setToken(response.data)
                document.location.replace("/drops/daily")
            }).catch(error => {
                setToken(null)
                console.error(error)
                if (error.response.data.error === "invalid_grant") {
                    setErrorMessage("Incorrect Username or Password")
                }
            })
        }
    })

    function submitLogin(event: React.FormEvent<HTMLFormElement>): void {
        event.preventDefault()

        const username: string = refUsername?.current?.value || ""
        const password: string = refPassword?.current?.value || ""
        refBtnLogin.current?.setAttribute("disabled", "disabled")

        login(username, password).then((response: AxiosResponse) => {
            setToken(response.data)
            document.location.replace("/drops/daily")
        }).catch(error => {
            console.error(error)
            if (error.response.data.error === "invalid_grant") {
                setErrorMessage("Incorrect Username or Password")
            }
            setToken(null)
        }).finally(() => {
            refBtnLogin.current?.removeAttribute("disabled")
        })
    }

    return (
        <IonPage>
            <IonContent fullscreen>
                <div className={"min-h-screen flex flex-col justify-end items-center bg-primary p-4"}>
                    <form className={"card max-w-sm w-full mx-auto py-12 px-10 bg-white rounded-xl"} onSubmit={submitLogin}>
                        <div className={"flex gap-2 items-center"}>
                            <img src={Logo} alt={"Logo"} width={40}/>
                            <div className={"text-2xl font-bold text-slate-600"}>Ocuelar</div>
                        </div>
                        <div className={"text-3xl font-bold my-4"}>Sign in</div>
                        <div className={"flex flex-col gap-4"}>
                            <input ref={refUsername} type="text" placeholder="Username" required
                                   className="input input-bordered w-full rounded-full"/>
                            <input ref={refPassword} type="password" placeholder="Password" required
                                   className="input input-bordered w-full rounded-full"/>
                        </div>
                        <div className={"flex justify-between"}>
                            <div className={"text-center text-error"}>{errorMessage}</div>
                        </div>
                        <div className={"flex justify-end mt-4"}>
                            <button ref={refBtnLogin} type={"submit"} className={"btn btn-primary rounded-full w-full"}>
                                Login
                            </button>
                        </div>
                    </form>
                </div>
            </IonContent>
        </IonPage>
    )
}

export default Page