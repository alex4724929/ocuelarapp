import React from "react"
import {
    IonButtons,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonRefresher,
    IonRefresherContent,
    IonTitle,
    IonToolbar,
    RefresherEventDetail,
    useIonViewWillEnter
} from "@ionic/react"
import {StatusBar, Style} from "@capacitor/status-bar"
import blackjack from "../../assets/images/games/blackjack.jpg"

import "./index.css"
import {IoExtensionPuzzle} from "react-icons/io5"
import {RiMoneyDollarCircleFill} from "react-icons/ri"
import {useHistory} from "react-router-dom"
import {NavigationBar} from "@hugotomazi/capacitor-navigation-bar"

const statusBarColor: string = "#FFCA67"
const navigationBarColor: string = "#FFFFFF"
const statusBarStyle: Style = Style.Light

const Page: React.FC = () => {
    useIonViewWillEnter(() => {
        StatusBar.setBackgroundColor({color: statusBarColor}).then()
        StatusBar.setStyle({style: statusBarStyle}).then()
        NavigationBar.setColor({color: navigationBarColor, darkButtons: true}).then()
    })

    const history = useHistory()

    useIonViewWillEnter(() => {
        StatusBar.setBackgroundColor({color: "#FFCA67"}).then()
        StatusBar.setStyle({style: Style.Light}).then()
    })

    function reload(event: CustomEvent<RefresherEventDetail>) {
        event.detail.complete()
    }

    return (
        <IonPage>
            <IonHeader className="ion-no-border">
                <IonToolbar color={"warning"}>
                    <IonButtons slot="start">
                        <IonMenuButton/>
                    </IonButtons>
                    <IonTitle>Rewards</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen>
                <IonRefresher slot="fixed" onIonRefresh={reload}>
                    <IonRefresherContent/>
                </IonRefresher>

                <div className={"bg-primary pb-14 top-area"}>
                    <div className={"font-black px-4"}>FEATURED</div>
                    <div className={"overflow-x-scroll overflow-y-hidden grid grid-flow-col gap-4 px-4"}>
                        <div className={"w-48"}>
                            <img className={"object-cover h-40 w-48"} src={blackjack} alt={"blackjack"}/>
                            <div className={"text-3xl font-black"}>Target</div>
                            <div className={"text-xl font-semibold"}>300 Coins</div>
                        </div>
                        <div className={"w-48"}>
                            <img className={"object-cover h-40 w-48"} src={blackjack} alt={"blackjack"}/>
                            <div className={"text-3xl font-black"}>Amazon</div>
                            <div className={"text-xl font-semibold"}>300 Coins</div>
                        </div>
                        <div className={"w-48"}>
                            <img className={"object-cover h-40 w-48"} src={blackjack} alt={"blackjack"}/>
                            <div className={"text-3xl font-black"}>Visa</div>
                            <div className={"text-xl font-semibold"}>300 Coins</div>
                        </div>
                    </div>
                </div>

                <div className={"w-full text-center"}>
                    <div className={"py-10"}>
                        <div className={"font-black text-xl text-gray-600"}>COIN BANK TOTAL</div>
                        <div className={"font-semibold text-5xl"}>123456</div>
                    </div>
                    <div className={"w-full py-2"}>
                        <div className={"flex gap-2 items-center justify-center"}>
                            <div className={"flex gap-2 w-28"}>
                                <div className={"font-bold text-xl"}>Games</div>
                                <IoExtensionPuzzle size={24}/>
                            </div>
                            <progress className={"progress progress-secondary rounded-full w-32 outline outline-secondary"} value={50} max={100}/>
                            <div className={"flex gap-2 w-28"}>
                                <RiMoneyDollarCircleFill size={28}/>
                                <div className={"font-bold text-xl"}>300</div>
                            </div>
                        </div>
                        <div className={"w-full text-center"}>
                            <span className={"px-0.5 font-bold text-xl"}>150</span>
                            <span className={"px-0.5 font-bold text-xl text-gray-600"}>Coins</span>
                        </div>
                    </div>
                    <div className={"flex flex-col items-center w-full gap-4 py-8"}>
                        <button className={"btn btn-lg btn-secondary w-44 rounded-full"}
                                onClick={() => history.replace("/reward/shop")}>SHOP
                        </button>
                        <button className={"btn btn-lg bg-gray-700 w-44 rounded-full"}
                                onClick={() => history.replace("/reward/wallet")}>VIEW WALLET</button>
                    </div>
                </div>
            </IonContent>
        </IonPage>
    )
}

export default Page
