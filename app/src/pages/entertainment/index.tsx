import React from "react"
import {useHistory} from "react-router-dom"

import {
    IonButtons,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonRefresher,
    IonRefresherContent,
    IonTitle,
    IonToolbar,
    RefresherEventDetail, useIonViewWillEnter
} from "@ionic/react"
import WeekStatus from "../../components/WeekStatus"
import {IoCart, IoExtensionPuzzle} from "react-icons/io5"

import "./index.css"

import blackjack from "../../assets/images/games/blackjack.jpg"
import {StatusBar, Style} from "@capacitor/status-bar"
import {NavigationBar} from "@hugotomazi/capacitor-navigation-bar"

const statusBarColor: string = "#FFFFFF"
const navigationBarColor: string = "#FFCA67"
const statusBarStyle: Style = Style.Light

const Page: React.FC = () => {
    useIonViewWillEnter(() => {
        StatusBar.setBackgroundColor({color: statusBarColor}).then()
        StatusBar.setStyle({style: statusBarStyle}).then()
        NavigationBar.setColor({color: navigationBarColor, darkButtons: true}).then()
    })

    const history = useHistory()
    const [weekly, setWeekly] = React.useState([
        {text: "SUN", status: 0},
        {text: "MON", status: 0},
        {text: "TUE", status: 0},
        {text: "WED", status: 0},
        {text: "THU", status: 0},
        {text: "FRI", status: 0},
        {text: "SAT", status: 0}
    ])

    function reload(event: CustomEvent<RefresherEventDetail>) {
        event.detail.complete()
    }

    return (
        <IonPage>
            <IonHeader className="ion-no-border">
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton/>
                    </IonButtons>
                    <IonTitle>Ocuelar</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen>
                <IonRefresher slot="fixed" onIonRefresh={reload}>
                    <IonRefresherContent/>
                </IonRefresher>
                <WeekStatus weekly={weekly}/>

                <div className={"bg-gray-100 overflow-x-scroll overflow-y-hidden grid grid-flow-col p-4 gap-4"}>
                    <div className={"w-80"}>
                        <img className={"object-cover h-44 w-80"} src={blackjack} alt={"blackjack"}/>
                    </div>
                    <div className={"w-80"}>
                        <img className={"object-cover h-44 w-80"} src={blackjack} alt={"blackjack"}/>
                    </div>
                    <div className={"w-80"}>
                        <img className={"object-cover h-44 w-80"} src={blackjack} alt={"blackjack"}/>
                    </div>
                </div>

                <div className={"flex justify-between mt-10 items-center"}>
                    <div>
                        <button className={"btn bg-gray-200 rounded-r-full h-14 w-20 text-gray-900"}
                                onClick={() => history.replace("/game")}>
                            <IoExtensionPuzzle size={28}/>
                        </button>
                    </div>
                    <div>
                        <div className={"w-full text-center font-semibold text-gray-600"}>COINS BANK TOTAL</div>
                        <div className={"w-full text-center font-bold text-3xl"}>123456</div>
                    </div>
                    <div>
                        <button className={"btn bg-gray-200 rounded-l-full h-14 w-20 text-gray-900"}
                                onClick={() => history.replace("/reward")}>
                            <IoCart size={28}/>
                        </button>
                    </div>
                </div>

                <div className={"fixed bottom-0 bg-primary h-40 w-full bottom-area flex items-center"}>
                    <div className={"overflow-x-scroll overflow-y-hidden grid grid-flow-col p-4 gap-4"}>
                        <div className={"w-20"}>
                            <img className={"rounded-full object-cover h-20 w-20"} src={blackjack} alt={"blackjack"}/>
                            <div className={"w-full text-center font-bold mt-2"}>Rick</div>
                        </div>
                        <div className={"w-20"}>
                            <img className={"rounded-full object-cover h-20 w-20"} src={blackjack} alt={"blackjack"}/>
                            <div className={"w-full text-center font-bold mt-2"}>Rick</div>
                        </div>
                        <div className={"w-20"}>
                            <img className={"rounded-full object-cover h-20 w-20"} src={blackjack} alt={"blackjack"}/>
                            <div className={"w-full text-center font-bold mt-2"}>Rick</div>
                        </div>
                        <div className={"w-20"}>
                            <img className={"rounded-full object-cover h-20 w-20"} src={blackjack} alt={"blackjack"}/>
                            <div className={"w-full text-center font-bold mt-2"}>Rick</div>
                        </div>
                        <div className={"w-20"}>
                            <img className={"rounded-full object-cover h-20 w-20"} src={blackjack} alt={"blackjack"}/>
                            <div className={"w-full text-center font-bold mt-2"}>Rick</div>
                        </div>
                        <div className={"w-20"}>
                            <img className={"rounded-full object-cover h-20 w-20"} src={blackjack} alt={"blackjack"}/>
                            <div className={"w-full text-center font-bold mt-2"}>Rick</div>
                        </div>
                        <div className={"w-20"}>
                            <img className={"rounded-full object-cover h-20 w-20"} src={blackjack} alt={"blackjack"}/>
                            <div className={"w-full text-center font-bold mt-2"}>Rick</div>
                        </div>
                        <div className={"w-20"}>
                            <img className={"rounded-full object-cover h-20 w-20"} src={blackjack} alt={"blackjack"}/>
                            <div className={"w-full text-center font-bold mt-2"}>Rick</div>
                        </div>
                    </div>
                </div>
            </IonContent>
        </IonPage>
    )
}

export default Page
