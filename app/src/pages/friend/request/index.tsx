import React from "react"
import {useIonViewWillEnter} from "@ionic/react"
import PatientProfile from "../../../assets/images/profile.png"
import {friend} from "../../../providers/cloud.provider"
import moment from "moment-timezone"

const Page: React.FC = () => {
    const [requests, setRequests] = React.useState([])

    useIonViewWillEnter(() => {
        loadRequest()
    })

    function loadRequest() {
        friend.getReceivedRequest().then(response => {
            setRequests(response.data)
        }).catch(error => {
            console.error(error)
        })
    }

    function replyResponse(id: number, response: string) {
        friend.responseRequest(id, response).catch(error => {
            console.error(error)
        }).finally(() => {
            loadRequest()
        })
    }

    const NoRequest = () => {
        if (requests.length === 0)
            return (
                <div className={"w-full text-center text-gray-500"}>No new requests</div>
            )
        else return (
            <div></div>
        )
    }

    return (
        <div className={"p-2 bg-white min-h-full"}>
            <NoRequest/>
            {requests.map((request: FriendRequest) => (
                <div key={request.id} className={"p-2 bg-gray-100 flex items-center gap-2"}>
                    <div className={"w-10 rounded-full"}>
                        <img src={PatientProfile} alt={"profile"}/>
                    </div>
                    <div className={"flex-1"}>
                        <div className={"font-bold text-xl"}>{request.sender.af_uid}</div>
                        <div className={"text-gray-400"}>
                            {moment(request.created_at).format("YYYY-MM-DD hh:mm:ss A")}
                        </div>
                    </div>
                    <div className={"flex flex-col gap-2"}>
                        <button className={"btn btn-sm btn-primary"}
                                onClick={() => replyResponse(request.id, "accepted")}>
                            Accept
                        </button>
                        <button className={"btn btn-sm btn-error btn-outline"}
                                onClick={() => replyResponse(request.id, "rejected")}>
                            Reject
                        </button>
                    </div>
                </div>
            ))}
        </div>
    )
}

export default Page

type FriendRequest = {
    id: number,
    created_at: string,
    sender: {
        af_uid: string
    }
}
