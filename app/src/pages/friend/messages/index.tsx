import React from "react"
import {friendMessage} from "../../../providers/cloud.provider"
import PatientProfile from "../../../assets/images/profile.png"
import {useIonViewWillEnter} from "@ionic/react"
import {useHistory} from "react-router-dom"

const Page: React.FC = () => {
    const history = useHistory()
    const [messages, setMessages] = React.useState<MessageProps[]>([])

    useIonViewWillEnter(() => {
        loadMessage()
    })

    function loadMessage() {
        friendMessage.index().then(response => {
            setMessages(response.data)
        }).catch(error => {
            console.error(error)
        })
    }

    const FriendDivider = (props: FriendDividerProps) => {
        if (props.currentIndex !== 0) return <div className={"divider m-0 p-0"}></div>
        return <div></div>
    }

    return (
        <div className={"p-2 bg-white min-h-full"}>
            <div className={"text-gray-500"}>Messages ({messages.length})</div>
            <div className={"mt-2"}>
                {messages.map((message: MessageProps, index: number) => (
                    <div key={message.id}>
                        <FriendDivider currentIndex={index}/>
                        <div className={"py-0 flex items-center gap-2"}
                             onClick={() => history.replace(`/friend/message?friendship_id=${message.id}`)}>
                            <div className={"w-10 rounded-full"}>
                                <img src={PatientProfile} alt={"profile"}/>
                            </div>
                            <div className={"flex-1"}>
                                <div className={"text-xl font-semibold"}>{message.friend_name}</div>
                                <div className={"text-md text-gray-500"}>{message.last_message || "Let's chat now!"}</div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    )
}

export default Page

interface MessageProps {
    id: number,
    friend_name: string,
    last_message: string
}

interface FriendDividerProps {
    currentIndex: number
}