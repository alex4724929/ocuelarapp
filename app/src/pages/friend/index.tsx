import React from "react"
import {
    IonButton,
    IonButtons,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar,
    useIonViewWillEnter
} from "@ionic/react"
import {StatusBar, Style} from "@capacitor/status-bar"
import {FaUserFriends} from "react-icons/fa"
import {BsFillChatDotsFill, BsFillChatFill} from "react-icons/bs"
import {Redirect, Route, Switch, useHistory, useLocation} from "react-router-dom"

import FriendList from "./list"
import FriendAdd from "./add"
import FriendRequest from "./request"
import FriendMessage from "./message"
import FriendMessages from "./messages"
import {NavigationBar} from "@hugotomazi/capacitor-navigation-bar"

const statusBarColor: string = "#FFFFFF"
const navigationBarColor: string = "#F9FAFB"
const statusBarStyle: Style = Style.Light

const Page: React.FC = () => {
    useIonViewWillEnter(() => {
        StatusBar.setBackgroundColor({color: statusBarColor}).then()
        StatusBar.setStyle({style: statusBarStyle}).then()
        NavigationBar.setColor({color: navigationBarColor, darkButtons: true}).then()
    })

    const history = useHistory()
    const location = useLocation()

    const btmBtnStyle = (path: string) => {
        if (location.pathname.includes(path)) {
            return "text-secondary"
        } else {
            return "text-black"
        }
    }

    const title = () => {
        if (location.pathname === "/friend/list") return "Friends"
        else if (location.pathname === "/friend/add") return "Friends"
        else if (location.pathname === "/friend/message") return "Chat"
        else if (location.pathname === "/friend/request") return "Requests"
        else if (location.pathname === "/friend/message") return "Chat"
        else if (location.pathname === "/friend/messages") return "Chat"
        return ""
    }

    const TopRightButton = () => {
        if (location.pathname === "/friend/list") {
            return (
                <IonButtons slot="primary">
                    <IonButton onClick={() => history.replace("/friend/add")}>Add</IonButton>
                </IonButtons>
            )
        } else if (location.pathname === "/friend/add") {
            return (
                <IonButtons slot="primary">
                    <IonButton onClick={() => history.replace("/friend/list")}>Back</IonButton>
                </IonButtons>
            )
        } else {
            return (
                <div></div>
            )
        }
    }

    return (
        <IonPage>
            <IonHeader className="ion-no-border">
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton/>
                    </IonButtons>
                    <IonTitle>{title()}</IonTitle>
                    <TopRightButton/>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen>
                <Switch>
                    <Route path={"/friend/list"}><FriendList/></Route>
                    <Route path={"/friend/add"}><FriendAdd/></Route>
                    <Route path={"/friend/request"}><FriendRequest/></Route>
                    <Route path={"/friend/message"}><FriendMessage/></Route>
                    <Route path={"/friend/messages"}><FriendMessages/></Route>
                    <Route path={"/friend/*"}><Redirect to={"/friend/list"}/></Route>
                </Switch>
                <div className="btm-nav btm-nav-md h-16 bg-gray-50">
                    <button className={btmBtnStyle("/friend/list")}
                            onClick={() => history.replace("/friend/list")}>
                        <FaUserFriends size={24}/>
                        <span className={"btm-nav-label text-black"}>Friends</span>
                    </button>
                    <button className={btmBtnStyle("/friend/message")}
                            onClick={() => history.replace("/friend/messages")}>
                        <BsFillChatFill size={18}/>
                        <span className={"btm-nav-label text-black"}>Chat</span>
                    </button>
                    <button className={btmBtnStyle("/friend/request")}
                            onClick={() => history.replace("/friend/request")}>
                        <BsFillChatDotsFill size={18}/>
                        <span className={"btm-nav-label text-black"}>Requests</span>
                    </button>
                </div>
            </IonContent>
        </IonPage>
    )
}

export default Page
