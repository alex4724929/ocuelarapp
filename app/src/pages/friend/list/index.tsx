import React from "react"
import {useIonViewWillEnter} from "@ionic/react"
import {friend} from "../../../providers/cloud.provider"
import PatientProfile from "../../../assets/images/profile.png"
import {useHistory} from "react-router-dom"

function compareFriendship(a: FriendshipProps, b: FriendshipProps) {
    if (a.friend_name < b.friend_name) return -1
    if (a.friend_name > b.friend_name) return 1
    return 0
}

const Page: React.FC = () => {
    const history = useHistory()
    const [friendships, setFriendships] = React.useState([])

    useIonViewWillEnter(() => {
        loadFriend()
    })

    function loadFriend() {
        friend.getList().then(response => {
            setFriendships(response.data.sort(compareFriendship))
        }).catch(error => {
            console.error(error)
        })
    }

    const FriendDivider = (props: FriendDividerProps) => {
        if (props.currentIndex !== 0) return <div className={"divider m-0 p-0"}></div>
        return <div></div>
    }

    return (
        <div className={"p-2 bg-white min-h-full"}>
            <div className={"text-gray-500"}>Friends ({friendships.length})</div>
            <div className={"mt-2"}>
                {friendships.map((friendship: FriendshipProps, index: number) => (
                    <div key={friendship.id}>
                        <FriendDivider currentIndex={index}/>
                        <div className={"py-0 flex items-center gap-2"}
                             onClick={() => history.replace(`/friend/message?friendship_id=${friendship.id}`)}>
                            <div className={"w-10 rounded-full"}>
                                <img src={PatientProfile} alt={"profile"}/>
                            </div>
                            <div className={"flex-1"}>
                                <div className={"font-bold text-xl"}>{friendship.friend_name}</div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    )
}

export default Page

interface FriendshipProps {
    id: number,
    friend_name: string
}

interface FriendDividerProps {
    currentIndex: number
}
