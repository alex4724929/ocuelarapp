import axios from "axios"

axios.defaults.baseURL = import.meta.env.VITE_SERVER_URL

export function login(username: string, password: string) {
    const data = {
        username: username,
        password: password,
        scope: "patient",
        grant_type: "password",
        client_id: import.meta.env.VITE_OAUTH_ID,
        client_secret: import.meta.env.VITE_OAUTH_SECRET
    }
    return axios.post(`${import.meta.env.VITE_SERVER_URL}/api/v1/oauth/token`, data)
}

export function revoke(token: string) {
    const data = {
        token: token,
        client_id: import.meta.env.VITE_OAUTH_ID,
        client_secret: import.meta.env.VITE_OAUTH_SECRET
    }
    return axios.post(`${import.meta.env.VITE_SERVER_URL}/api/v1/oauth/revoke`, data)
}

export function refresh(refreshToken: string) {
    const data = {
        refresh_token: refreshToken,
        grant_type: "refresh_token",
        client_id: import.meta.env.VITE_OAUTH_ID,
        client_secret: import.meta.env.VITE_OAUTH_SECRET
    }
    return axios.post(`${import.meta.env.VITE_SERVER_URL}/api/v1/oauth/token`, data)
}
