import {axiosInstance} from "../utils/axios.util"

export const friend = {
    getList: () => axiosInstance.get("/api/v1/patient/friend/list"),
    getSentRequest: () => axiosInstance.get("/api/v1/patient/friend/request/send"),
    getReceivedRequest: () => axiosInstance.get("/api/v1/patient/friend/request/receive"),
    sendRequest: (data: FormData) => axiosInstance.post("/api/v1/patient/friend/request/send", data),
    responseRequest: (requestId: number, status: string) => axiosInstance.patch(`/api/v1/patient/friend/request/receive/${requestId}`, {status: status}),
}

export const friendMessage = {
    index: () => axiosInstance.get(`/api/v1/patient/friend/message`),
    show: (friendshipId: number, messageBeforeId: string) => axiosInstance.get(`/api/v1/patient/friend/message/${friendshipId}?message_before_id=${messageBeforeId}`),
    send: (data: {}) => axiosInstance.post(`/api/v1/patient/friend/message`, data),
    last: (friendshipId: number) => axiosInstance.get(`/api/v1/patient/friend/message/${friendshipId}/last`),
}

export const daily = {
    index: () => axiosInstance.get("/api/v1/patient/daily"),
}

export const monthly = {
    index: (date: string) => axiosInstance.get("/api/v1/patient/monthly?date=" + date),
}

export const doctorMessage = {
    index: (messageBeforeId: string) => axiosInstance.get("/api/v1/patient/message/doctor?message_before_id=" + messageBeforeId),
    create: (message: string) => axiosInstance.post("/api/v1/patient/message/doctor", {content: message}),
    last: () => axiosInstance.get("/api/v1/patient/doctor/message/last")
}

export const drop = {
    info: {
        index: (params?: any) => axiosInstance.get("/api/v1/patient/drop/info", {params: params}),
    }
}

export const notification = {
    index: () => axiosInstance.get("/api/v1/patient/notification"),
}