import React from "react"
import {useLocation} from "react-router-dom"

import {
    IonContent,
    IonFooter,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonListHeader,
    IonMenu,
    IonMenuToggle
} from "@ionic/react"
import {
    calendarNumberOutline,
    calendarNumberSharp,
    calendarOutline,
    calendarSharp, gameControllerOutline, gameControllerSharp, happyOutline, happySharp,
    logOutOutline,
    logOutSharp,
    mailOutline,
    mailSharp,
    peopleOutline,
    peopleSharp, ribbonOutline, ribbonSharp
} from "ionicons/icons"

import "./Menu.css"
import {revoke} from "../providers/oauth.provider"
import useToken from "../hooks/useToken"

const medicinePages: AppPage[] = [
    {title: "Daily View", url: "/drops/daily", iosIcon: calendarNumberOutline, mdIcon: calendarNumberSharp},
    {title: "Monthly View", url: "/drops/monthly", iosIcon: calendarOutline, mdIcon: calendarSharp},
    {title: "Doctor Message", url: "/message/doctor", iosIcon: mailOutline, mdIcon: mailSharp},
    {title: "Friends", url: "/friend/list", iosIcon: peopleOutline, mdIcon: peopleSharp}
]

const entertainmentPages: AppPage[] = [
    {title: "Entertainment", url: "/entertainment", iosIcon: happyOutline, mdIcon: happySharp},
    {title: "Games", url: "/game", iosIcon: gameControllerOutline, mdIcon: gameControllerSharp},
    {title: "Rewards", url: "/reward", iosIcon: ribbonOutline, mdIcon: ribbonSharp},
    {title: "Test Page", url: "/test", iosIcon: ribbonOutline, mdIcon: ribbonSharp},
    {title: "Blackjack21", url: "/game/blackjack21", iosIcon: gameControllerOutline, mdIcon: gameControllerSharp},
    {title: "Candy Match Saga", url: "/game/candyMatchSaga", iosIcon: gameControllerOutline, mdIcon: gameControllerSharp},
    {title: "Sudoku", url: "/game/sudoku", iosIcon: gameControllerOutline, mdIcon: gameControllerSharp},
    {title: "Tronix", url: "/game/tronix", iosIcon: gameControllerOutline, mdIcon: gameControllerSharp}
]

const Menu: React.FC = () => {
    const location = useLocation()
    const {token, setToken} = useToken()

    function logout() {
        revoke(token.access_token).then(() => {
            setToken(null)
            document.location.replace("/login")
        }).catch(error => {
            console.error(error)
        })
    }

    return (
        <IonMenu contentId="main" type="overlay">
            <IonContent>
                <IonList id="inbox-list">
                    <IonListHeader>Ocuelar</IonListHeader>
                    {medicinePages.map((appPage, index) => {
                        return (
                            <IonMenuToggle key={index} autoHide={false}>
                                <IonItem className={location.pathname === appPage.url ? "selected" : ""}
                                         routerLink={appPage.url} routerDirection="none" lines="none" detail={false}>
                                    <IonIcon aria-hidden="true" slot="start" ios={appPage.iosIcon} md={appPage.mdIcon}/>
                                    <IonLabel>{appPage.title}</IonLabel>
                                </IonItem>
                            </IonMenuToggle>
                        )
                    })}
                </IonList>
                <IonList id="inbox-list">
                    <IonListHeader>Games & Rewards</IonListHeader>
                    {entertainmentPages.map((appPage, index) => {
                        return (
                            <IonMenuToggle key={index} autoHide={false}>
                                <IonItem className={location.pathname === appPage.url ? "selected" : ""}
                                         routerLink={appPage.url} routerDirection="none" lines="none" detail={false}>
                                    <IonIcon aria-hidden="true" slot="start" ios={appPage.iosIcon} md={appPage.mdIcon}/>
                                    <IonLabel>{appPage.title}</IonLabel>
                                </IonItem>
                            </IonMenuToggle>
                        )
                    })}
                </IonList>
            </IonContent>
            <IonFooter>
                <IonMenuToggle autoHide={false}>
                    <IonItem routerDirection="none" lines="none" detail={false} onClick={logout}>
                        <IonIcon aria-hidden="true" slot="start" ios={logOutOutline} md={logOutSharp}/>
                        <IonLabel>Logout</IonLabel>
                    </IonItem>
                </IonMenuToggle>
            </IonFooter>
        </IonMenu>
    )
}

export default Menu

interface AppPage {
    url: string;
    iosIcon: string;
    mdIcon: string;
    title: string;
}