import axios from "axios"
import {refresh} from "../providers/oauth.provider"

export const axiosInstance = axios.create({baseURL: import.meta.env.VITE_SERVER_URL})

axiosInstance.interceptors.request.use(
    (config) => {
        const token = JSON.parse(localStorage.getItem("token") || "{}")?.access_token
        config.headers["Authorization"] = `Bearer ${token}`
        config.headers["Accept"] = "application/json"
        return config
    },
    error => {
        return Promise.reject(error)
    }
)

axiosInstance.interceptors.response.use(
    (response) => {
        return response
    }, async (error) => {
        const originalRequest = error.config
        if (error.response.status === 401) {
            const refreshToken = JSON.parse(localStorage.getItem("token") || "{}")?.refresh_token
            const refreshResult = await refresh(refreshToken)
            if (refreshResult.status === 200) {
                localStorage.setItem("token", JSON.stringify(refreshResult.data))
            } else {
                localStorage.removeItem("token")
            }
            return axiosInstance(originalRequest)
        }
        return Promise.reject(error)
    }
)
