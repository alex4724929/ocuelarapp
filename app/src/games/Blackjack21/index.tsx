import React from "react"
import {IonContent, IonPage, useIonViewWillEnter} from "@ionic/react"
import {StatusBar, Style} from "@capacitor/status-bar"

const Page: React.FC = () => {
    useIonViewWillEnter(() => {
        StatusBar.setBackgroundColor({color: "#000000"}).then()
        StatusBar.setStyle({style: Style.Dark}).then()
    })

    return (
        <IonPage>
            <IonContent fullscreen>
                <div className={"w-full h-full bg-black overflow-hidden"}>
                    <div className={"rotate-90 -mt-[50vh] origin-bottom-left w-[100vh] h-[100vw]"}>
                        <iframe className={"w-full h-full"} allowFullScreen={true}
                                src={"/games/blackjack21/index.html"}></iframe>
                    </div>
                </div>
            </IonContent>
        </IonPage>
    )
}

export default Page
