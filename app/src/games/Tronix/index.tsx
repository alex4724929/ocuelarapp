import React from "react"
import {IonContent, IonPage, useIonViewWillEnter} from "@ionic/react"

import "./index.css"
import {StatusBar, Style} from "@capacitor/status-bar"

const Page: React.FC = () => {

    useIonViewWillEnter(() => {
        StatusBar.setBackgroundColor({color: "#191916"}).then()
        StatusBar.setStyle({style: Style.Dark}).then()
    })

    return (
        <IonPage>
            <IonContent fullscreen>
                <div className={"w-full h-full flex items-center my-bg overflow-hidden"}>
                    <iframe className={"w-[100%] h-[75%] bg-black"} allowFullScreen={true} src={"/games/tronix/index.html"}></iframe>
                </div>
            </IonContent>
        </IonPage>
    )
}

export default Page
