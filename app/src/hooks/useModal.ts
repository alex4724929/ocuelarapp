import * as React from "react"

export default function (onClose?: ModalCallbacks["onClose"], onCancel?: ModalCallbacks["onCancel"]): ModelHookResult {
    const [isOpen, setIsOpen] = React.useState(false)
    const [params, setParams] = React.useState({})

    const open = (params: {}) => {
        setParams(params)
        setIsOpen(true)
    }

    const close = (result: {}) => {
        setIsOpen(false)
        onClose?.(result)
    }

    const cancel = () => {
        setIsOpen(false)
        onCancel?.()
    }

    return [isOpen, params, open, close, cancel]
}

type ModalCallbacks = {
    onClose?: (result?: any) => void;
    onCancel?: () => void;
}

type ModelHookResult = [
    isOpen: boolean,
    params: {},
    open: (params: {}) => void,
    close: (result: {}) => void,
    cancel: () => void
]
