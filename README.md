# Ocuelar

### app
##### Environment
- Node.js 18
- Ionic Framework 7
- Android Studio
- Xcode

#### Setup
`npm install` for install the packages.

#### Run live-reload on Android device
`ionic capacitor run android -l --external` in terminal.

#### Build Android Installer (APK)
1. `ionic cap sync` for copy and update plugins.
2. `ionic cap open android` for run Android Studio.
3. In Android Studio toolbar, `Build > Generate Signed Bundle / APK` to build a signed apk.

### web
##### Environment
- Node.js 18

#### Setup
`npm install` for install the packages.

#### Debuging
`npm run dev` for devlopment

#### Build project (for production)
`npm run build:production` for compile to production code.

### server
