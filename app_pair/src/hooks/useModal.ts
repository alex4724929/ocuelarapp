import {Ref, ref} from "vue"

export default function (onClose?: (result?: {}) => void, onCancel?: () => void) {
    const show: Ref<boolean> = ref(false)
    const param: Ref<{}> = ref({})

    function open(paramsValue?: {}) {
        show.value = true
        param.value = paramsValue || {}
    }

    function close(result?: {}) {
        show.value = false
        onClose?.(result)
    }

    function cancel() {
        show.value = false
        onCancel?.()
    }

    return [show, param, open, close, cancel] as const
}
