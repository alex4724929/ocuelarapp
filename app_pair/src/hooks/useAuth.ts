import {ref} from "vue"

export default function () {
    const getAuth = () => {
        return JSON.parse(localStorage.getItem("auth") || "{}") as AuthProps | null
    }

    const auth = ref(getAuth())

    function saveAuth(input: AuthProps | null) {
        if (input === null) localStorage.removeItem("auth")
        else localStorage.setItem("auth", JSON.stringify(input))
        auth.value = input
    }

    return {setAuth: saveAuth, auth} as const
}

interface AuthProps {
    access_token: string,
    created_at: number,
    expires_in: number,
    refresh_token: string,
    role: string,
    scope: string,
    token_type: string,
}
