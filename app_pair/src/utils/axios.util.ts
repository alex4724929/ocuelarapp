import axios from "axios"
import {oauth} from "@/providers"

export const axiosPublic = axios.create({baseURL: import.meta.env.VITE_SERVER_URL})
export const axiosPrivate = axios.create({baseURL: import.meta.env.VITE_SERVER_URL})

axiosPrivate.interceptors.request.use(
    (config) => {
        config.headers["Authorization"] = `Bearer ${JSON.parse(localStorage.getItem("token") || "")["access_token"]}`
        config.headers["Accept"] = "application/json"
        return config
    },
    error => {
        return Promise.reject(error)
    }
)

axiosPrivate.interceptors.response.use(
    (response) => {
        return response
    }, async (error) => {
        const originalRequest = error.config
        if (error.response.status === 401) {
            const refreshResult = await oauth.refresh(JSON.parse(localStorage.getItem("token") || ""))
            if (refreshResult.status === 200) {
                localStorage.setItem("token", JSON.stringify(refreshResult.data))
            } else {
                localStorage.removeItem("token")
            }
            return axiosPrivate(originalRequest)
        }
        return Promise.reject(error)
    }
)
