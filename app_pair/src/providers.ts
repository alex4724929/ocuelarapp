import axios from "axios"
import {axiosPublic} from "@/utils/axios.util"

export const oauth = {
    login: (username: string, password: string) => {
        const data = {
            grant_type: "password",
            scope: "doctor",
            client_id: import.meta.env.VITE_OAUTH_ID,
            client_secret: import.meta.env.VITE_OAUTH_SECRET,
            username: username,
            password: password
        }
        return axiosPublic.post("/api/v1/oauth/token", data)
    },
    logout: (token: string) => {
        const data = {
            token: token,
            client_id: import.meta.env.VITE_OAUTH_ID,
            client_secret: import.meta.env.VITE_OAUTH_SECRET
        }
        return axiosPublic.post("/api/v1/oauth/revoke", data)
    },
    refresh: (token: string) => {
        const data = {
            refresh_token: token,
            grant_type: "refresh_token",
            client_id: import.meta.env.VITE_OAUTH_ID,
            client_secret: import.meta.env.VITE_OAUTH_SECRET
        }
        return axiosPublic.post("/api/v1/oauth/token", data)
    }
}
