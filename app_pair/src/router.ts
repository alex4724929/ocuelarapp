import {createRouter, createWebHistory} from "@ionic/vue-router"
import {RouteRecordRaw} from "vue-router"
import useAuth from "@/hooks/useAuth"

const routes: Array<RouteRecordRaw> = [
    {path: "/", redirect: "/login"},
    {path: "/login", name: "Login", component: () => import("./views/LoginPage.vue")},
    {path: "/ble/scan", name: "ble.scan", component: () => import("./views/BleScanPage.vue"), meta: {requiresAuth: true}},
    {path: "/message/:id", component: () => import("./views/ViewMessagePage.vue")}
]

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes
})

router.beforeEach((to, from, next) => {
    const {auth} = useAuth()

    if (to?.meta?.requiresAuth && !auth.value?.access_token) next({name: "Login"})
    else next()
})

export default router
