/** @type {import("tailwindcss").Config} */
export default {
    content: [
        "./index.html",
        "./src/**/*.{js,ts,jsx,tsx}",
    ],
    theme: {
        extend: {}
    },
    plugins: [
        require("daisyui")
    ],
    daisyui: {
        themes: [
            {
                ocuelar: {
                    "color-scheme": "light",
                    "primary": "#00B5B8",
                    "primary-content": "#FFFFFF",
                    "secondary": "#7B9696",
                    "secondary-content": "#FFFFFF",
                    "accent": "#7E92B1",
                    "accent-content": "#FFFFFF",
                    "neutral": "#8E9191",
                    "neutral-content": "#edf2f7",
                    "base-100": "#f0f2f5",
                    "base-content": "#181a2a",
                    "--rounded-box": "0.25rem",
                    "--rounded-btn": ".125rem",
                    "--rounded-badge": ".125rem",
                    "--animation-btn": "0",
                    "--animation-input": "0",
                    "--btn-focus-scale": "1",
                }
            }
        ]
    }
}

