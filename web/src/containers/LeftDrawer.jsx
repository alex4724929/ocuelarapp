import React from "react"
import {Link, NavLink, useLocation} from "react-router-dom"

import routes from "../routes/drawer"
import SidebarSubmenu from "../components/DrawerSubmenu"

import Logo from "../assets/images/logo.png"
import useToken from "../hooks/useToken.js"
import {roleDict} from "../utils/role.util.js"
import {logout} from "../providers/oauth.provider.js"
import {CgProfile} from "react-icons/cg"
import {MdLogout} from "react-icons/md"

export default function LeftDrawer() {
    const location = useLocation()
    const {token, setToken} = useToken()

    function btnLogout() {
        logout(token).then(() => {
            setToken(null)
            document.location.replace("/login")
        })
    }

    return (
        <div className={"drawer-side"}>
            <label htmlFor={"drawer-left"} className={"drawer-overlay"}></label>
            <ul className="menu menu-compact w-60 bg-slate-800 text-white p-1">
                <li className={"font-semibold text-xl"}>
                    <Link className={"hover:bg-slate-700 bg-slate-800 rounded-0"} to={"/app/patient"}>
                        <img className="mask mask-squircle w-10" src={Logo} alt="logo"/>
                        <div className={"text-xl font-semibold"}>Ocuelar<span
                            className={"badge badge-sm bg-white text-slate-800 rounded-full mx-2"}>Cloud</span></div>
                    </Link>
                </li>

                {routes[roleDict[token.role]].map((route, key) => {
                    return (
                        <li key={key}>
                            {
                                route.submenu ?
                                    <SidebarSubmenu {...route}/> :
                                    (<NavLink
                                        end
                                        to={route.path}
                                        className={({isActive}) => `${
                                            isActive ?
                                                "font-semibold bg-slate-700 text-white" :
                                                "font-normal hover:text-white hover:bg-slate-700"
                                        }`}>
                                        <div className={"flex gap-4 items-center"}>{route.icon} {route.name}</div>
                                        {location.pathname === route.path ? (<span
                                            className="absolute inset-y-0 left-0 w-1 rounded-tr-md rounded-br-md bg-primary "
                                            aria-hidden="true"></span>) : null
                                        }
                                    </NavLink>)
                            }
                        </li>
                    )
                })}
                {/*<li className={"absolute bottom-11 w-full pr-2"}>*/}
                {/*    <NavLink to={"/app/profile"} className={({isActive}) => `${*/}
                {/*        isActive ?*/}
                {/*            "font-semibold bg-slate-700 text-white" :*/}
                {/*            "font-normal hover:text-white hover:bg-slate-700"*/}
                {/*    }`}>*/}
                {/*        <div className={"flex gap-4 items-center"}><CgProfile size={16}/>My Profile</div>*/}
                {/*        {location.pathname === "/app/profile" ? (<span*/}
                {/*            className="absolute inset-y-0 left-0 w-1 rounded-tr-md rounded-br-md bg-primary "*/}
                {/*            aria-hidden="true"></span>) : null*/}
                {/*        }*/}
                {/*    </NavLink>*/}
                {/*</li>*/}
                <li className={"absolute bottom-1 w-full pr-2"}>
                    <button className={"btn btn-sm text-xs btn-error w-full text-black"} onClick={btnLogout}><MdLogout
                        size={16}/> Logout
                    </button>
                </li>
            </ul>
        </div>
    )
}
