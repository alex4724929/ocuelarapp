import React, {Suspense} from "react"
import {createBrowserRouter, createRoutesFromElements, Navigate, Route, RouterProvider, Routes} from "react-router-dom"

import routes from "../routes/route"

import Header from "./Header"
import SuspenseContent from "../components/SuspenseContent"
import PrivateRoute from "../components/PrivateRoute.jsx"
import useToken from "../hooks/useToken.js"

function PageContent() {
    const {token} = useToken()

    return (
        <div className="drawer-content flex flex-col">
            <Header/>
            <main className="flex-1 overflow-y-auto px-6 bg-base-100 flex flex-col">
                <Suspense fallback={<SuspenseContent/>}>
                    <Routes>
                        {routes.map((route, key) =>
                            <Route key={key} exact={true} path={route.path} element={
                                <PrivateRoute token={token} redirect={"/app"} role={route.role}
                                              element={<route.component/>}/>}/>
                        )}
                        <Route path="*" element={<Navigate to={"/app/patient"}/>}/>
                    </Routes>
                </Suspense>
            </main>
        </div>
    )
}

export default PageContent
