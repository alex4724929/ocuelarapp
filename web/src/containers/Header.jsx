import React from 'react';
import {Link} from "react-router-dom"

import ProfilePicture from "../assets/images/profile.png"
import useToken from "../hooks/useToken"
import {logout} from "../providers/oauth.provider"
import {MdMenu} from "react-icons/md"

export default function Header() {
    const {token, setToken} = useToken()

    function btnLogout() {
        logout(token).then(() => {
            setToken(null)
            document.location.replace("/login")
        })
    }

    return (
        <div className={"navbar bg-white z-10 shadow-md flex justify-between md:hidden"}>
            <div>
                <div className={"flex items-center lg:hidden"}>
                    <label htmlFor={"drawer-left"} className={"btn btn-ghost btn-circle drawer-button"}>
                        <MdMenu size={24}/>
                    </label>
                    <div className={"text-xl font-semibold"}>Ocuelar Cloud</div>
                </div>
            </div>

            <div className={"order-last"}>
                <div className={"dropdown dropdown-end ml-4"}>
                    <label tabIndex={0} className={"btn btn-ghost btn-circle avatar"}>
                        <div className={"w-10 rounded-full"}>
                            <img src={ProfilePicture} alt={"profile"}/>
                        </div>
                    </label>
                    <ul tabIndex={0} className={"menu menu-compact dropdown-content mt-3 p-2 shadow bg-white w-52"}>
                        <li className={"justify-between"}>
                            <a href={"/app/profile"}>Profile</a>
                        </li>
                        <div className="divider m-0"></div>
                        <li><a onClick={btnLogout}>Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
    )
}
