import React from "react"
import ReactDOM from "react-dom/client"
import moment from "moment-timezone"

import App from "./App"

import "./i18n"
import "./main.css"

moment.tz.setDefault("US/Pacific")

ReactDOM.createRoot(document.getElementById("root")).render(<App/>)
