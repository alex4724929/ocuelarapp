import * as React from "react";

export default function Alert(props) {
    const { isOpen, style, message } = props;

    const css = (isOpen, style) => {
        if (isOpen) return `alert shadow-lg z-50 absolute right-4 bottom-4 max-w-xs ${style}`;
        else return `alert shadow-lg z-50 absolute right-4 bottom-4 max-w-xs hidden`
    }

    return (
        <div>
            <div className={"alert-info alert-warning alert-error alert-success hidden"}/>
            <div className={css(isOpen, style)}>
                <div><span>{message}</span></div>
            </div>
        </div>
    )
}
