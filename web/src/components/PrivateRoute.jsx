import React from "react";
import { Navigate } from "react-router-dom";
import {roleDict} from "../utils/role.util.js"

export default function PrivateRoute({token, redirect, element, role = []}) {
    if (!token) return <Navigate to={redirect} replace/>;
    if (role.includes(roleDict[token.role])) return element;
    return <Navigate to={redirect} replace/>;
}
