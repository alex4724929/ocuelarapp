import {useState} from "react"

export default function useAlert() {
    const [isOpen, setIsOpen] = useState(false)
    const [style, setStyle] = useState(AlertStyle.Normal)
    const [message, setMessage] = useState("")

    const showAlert = (style, message, timeout = 3000) => {
        setIsOpen(true)
        setMessage(message)
        setStyle(style)

        setInterval(() => {
            setIsOpen(false)
            setMessage("")
            setStyle(AlertStyle.Normal)
        }, timeout)
    }

    return [isOpen, style, message, showAlert]
}

export const AlertStyle = {
    Normal: "",
    Info: 'alert-info',
    Success: 'alert-success',
    Warning: 'alert-warning',
    Error: 'alert-error'
}
