import * as React from "react";

export default function useModal(callbackClose = () => {}, callbackCancel = () => {}) {
    const [isOpen, setIsOpen] = React.useState(false);
    const [params, setParams] = React.useState({});

    const open = (params) => {
        setIsOpen(true);
        setParams(params)
    }

    const close = (result) => {
        setIsOpen(false);
        callbackClose(result)
    }

    const cancel = () => {
        setIsOpen(false);
        callbackCancel()
    }

    return [isOpen, params, open, close, cancel];
}
