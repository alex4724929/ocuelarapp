import {BrowserRouter, Navigate, Route, Routes} from "react-router-dom"
import React, {lazy, Suspense, useEffect} from "react"

import useToken from "./hooks/useToken"
import PrivateRoute from "./components/PrivateRoute.jsx"

const Login = lazy(() => import("./pages/login/index.jsx"))
const Main = lazy(() => import("./pages/app/index.jsx"))

import "nprogress/nprogress.css"
import NProgress from "nprogress"

NProgress.configure({ showSpinner: false });

const LazyLoad = () => {
    useEffect(() => {
        NProgress.start();
        return () => {
            NProgress.done();
        };
    });
    return '';
};

function App() {
    const {token, setToken} = useToken()

    return (
        <BrowserRouter>
            <Suspense fallback={<LazyLoad />}>
                <Routes>
                    <Route path={"/"} element={<Navigate to="/login" replace/>}/>
                    <Route path={"login"} element={<Login setToken={setToken} token={token}/>}/>
                    <Route path={"app/*"} element={
                        <PrivateRoute token={token} redirect={"/login"} role={["admin", "doctor"]} element={
                            <Main token={token} setToken={setToken}/>
                        }/>
                    }/>
                    <Route path={"*"} element={<Navigate to="/login" replace/>}/>
                </Routes>
            </Suspense>
        </BrowserRouter>
    )
}

export default App
