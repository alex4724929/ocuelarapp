import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";

// This function can be marked `async` if using `await` inside
export function middleware(request: NextRequest) {
  if (request.nextUrl.pathname === "/invite") {
    return NextResponse.next();
  }
  if (request.nextUrl.pathname === "/login") {
    if (request.cookies.get("token")) {
      return NextResponse.redirect(new URL("/users", request.url));
    } else {
      return NextResponse.next();
    }
  }
  if (request.nextUrl.pathname === "/") {
    if (request.cookies.get("token")) {
      return NextResponse.redirect(new URL("/users", request.url));
    } else {
      return NextResponse.redirect(new URL("/login", request.url));
    }
  }
  if (request.cookies.get("token")) {
    return NextResponse.next();
  } else {
    return NextResponse.redirect(new URL("/login", request.url));
  }
}
export const config = {
  matcher: [
    /*
     * Match all request paths except for the ones starting with:
     * - api (API routes)
     * - _next/static (static files)
     * - _next/image (image optimization files)
     * - favicon.ico (favicon file)
     */
    "/((?!api|_next/static|_next/image|favicon.ico).*)",
  ],
};
