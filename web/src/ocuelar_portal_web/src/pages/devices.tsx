import CreateDeviceModal from "@/components/CreateDeviceModal";
import { AddIcon, TriangleDownIcon, TriangleUpIcon } from "@chakra-ui/icons";
import {
  Box,
  Button,
  Table,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
  chakra,
  useDisclosure,
} from "@chakra-ui/react";
import { Stack } from "@mui/material";
import { devices, users } from "@prisma/client";
import {
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  getSortedRowModel,
  useReactTable,
} from "@tanstack/react-table";
import axios from "axios";
import React from "react";
interface ColumnData extends devices {
  users: users;
}
const columnHelper = createColumnHelper<ColumnData>();

const EditButton = ({ info, users, reloadDevices }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <>
      <Button onClick={onOpen}>Edit</Button>
      <CreateDeviceModal
        users={users}
        reloadDevices={reloadDevices}
        mode="edit"
        defaultValue={{
          id: info.row.original.id,
          label: info.row.original?.label,
          owner: info.row.original?.users?.id,
        }}
        isOpen={isOpen}
        onClose={onClose}
      />
    </>
  );
};
const Devices = () => {
  const [data, setData] = React.useState<ColumnData[]>([]);
  const [users, setUsers] = React.useState<users[]>([]);
  const getDevices = React.useCallback(async () => {
    try {
      const { data } = await axios.get("/api/devices");
      setData(data);
      const {
        data: { users },
      } = await axios.get("/api/users");
      setUsers(users);
    } catch (error) {
      console.log(error);
    }
  }, []);
  const columns = React.useMemo(
    () => [
      columnHelper.accessor("display_udid", {
        cell: (info) => info.getValue(),
        header: "Device Name",
      }),

      columnHelper.accessor("id", {
        cell: (info) => info.getValue(),
        header: "Device ID",
      }),
      columnHelper.accessor("label", {
        cell: (info) => info.getValue(),
        header: "Label",
      }),
      columnHelper.accessor("users.user_name", {
        cell: (info) => info.getValue(),
        header: "Owner",
      }),
      columnHelper.accessor("id", {
        cell: (info) => (
          <EditButton reloadDevices={getDevices} users={users} info={info} />
        ),
        header: "Edit",
      }),
    ],
    [getDevices, users]
  );
  React.useEffect(() => {
    getDevices();
  }, [getDevices]);
  const table = useReactTable({
    columns,
    data,
    getCoreRowModel: getCoreRowModel(),
    getSortedRowModel: getSortedRowModel(),
    state: {},
  });
  const { isOpen, onClose, onOpen } = useDisclosure();
  return (
    <Stack position={"relative"} px={"20px"}>
      <Box
        onClick={onOpen}
        alignSelf={"flex-end"}
        cursor={"pointer"}
        width={50}
        bgColor={"teal.400"}
        justifyContent={"center"}
        alignItems={"center"}
        height={50}
        borderRadius={3}
        display={"flex"}
        mb={10}
      >
        <AddIcon />
      </Box>
      <Stack overflow={"scroll"} maxHeight={"80vh"} position={"relative"}>
        <Table borderRadius={3}>
          <Thead
            bgColor={"teal.500"}
            position={"sticky"}
            maxH={"80vh"}
            top={"0px"}
          >
            {table.getHeaderGroups().map((headerGroup, idx) => (
              <Tr borderRadius={3} key={headerGroup.id + idx}>
                {headerGroup.headers.map((header, idx2) => {
                  // see https://tanstack.com/table/v8/docs/api/core/column-def#meta to type this correctly
                  const meta: any = header.column.columnDef.meta;
                  return (
                    <Th
                      key={header.id + idx2}
                      onClick={header.column.getToggleSortingHandler()}
                      isNumeric={meta?.isNumeric}
                      align="left"
                      pl={2}
                      py={3}
                    >
                      {flexRender(
                        header.column.columnDef.header,
                        header.getContext()
                      )}

                      <chakra.span pl="4">
                        {header.column.getIsSorted() ? (
                          header.column.getIsSorted() === "desc" ? (
                            <TriangleDownIcon aria-label="sorted descending" />
                          ) : (
                            <TriangleUpIcon aria-label="sorted ascending" />
                          )
                        ) : null}
                      </chakra.span>
                    </Th>
                  );
                })}
              </Tr>
            ))}
          </Thead>
          <Tbody bgColor={"blackAlpha.500"}>
            {table.getRowModel().rows.map((row, idx) => (
              <Tr key={row.id + idx} py={3}>
                {row.getVisibleCells().map((cell, idx2) => {
                  // see https://tanstack.com/table/v8/docs/api/core/column-def#meta to type this correctly
                  const meta: any = cell.column.columnDef.meta;
                  return (
                    <Td
                      pl={2}
                      py={3}
                      key={cell.id + idx2}
                      isNumeric={meta?.isNumeric}
                    >
                      {flexRender(
                        cell.column.columnDef.cell,
                        cell.getContext()
                      )}
                    </Td>
                  );
                })}
              </Tr>
            ))}
          </Tbody>
        </Table>
      </Stack>
      <CreateDeviceModal
        users={users}
        reloadDevices={getDevices}
        onClose={onClose}
        isOpen={isOpen}
      />
    </Stack>
  );
};
export default Devices;
