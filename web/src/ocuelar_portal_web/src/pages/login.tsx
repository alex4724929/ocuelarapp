import {
  Box,
  FormControl,
  FormHelperText,
  InputLabel,
  OutlinedInput,
  Stack,
  TextField,
  useMediaQuery,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";
import { Controller, FormProvider, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";
import { useCookies } from "react-cookie";
import { useRouter } from "next/router";
import { useToast } from "@chakra-ui/react";
import { Button, Flex, Input, Typography, Image, message } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
interface IFormInput {
  account: string;
  password: string;
}
const schema = yup
  .object({
    account: yup.string().required(),
    password: yup.string().required(),
  })
  .required();
const Login = () => {
  const [messageApi, contextHolder] = message.useMessage();
  const router = useRouter();
  const [cookies, setCookie, removeCookie] = useCookies();
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down("md"));
  const toast = useToast();
  const methods = useForm<IFormInput>({
    resolver: yupResolver(schema),
    mode: "all",
  });
  const onSubmit = async (data: IFormInput) => {
    try {
      const response = await axios.post("/api/login", {
        account: data.account,
        password: data.password,
      });
      axios.defaults.headers.common.Authorization = `Bearer ${response.data.token}`;
      setCookie("token", response.data.token);
      router.push("/");
    } catch (error) {
      messageApi.open({
        type: "error",
        content: `failed to login. ${
          error?.response?.data?.message ?? "unknown error"
        }`,
      });
    }
  };
  return (
    <FormProvider {...methods}>
      <div
        style={{
          backgroundImage:
            "url('https://mdn.alipayobjects.com/yuyan_qk0oxh/afts/img/V-_oS6r-i7wAAAAAAAAAAAAAFl94AQBr')",
          height: "100vh",
          backgroundSize: "100% 100%",
          display: "flex",
          backgroundColor: "white",
        }}
      >
        <Stack
          width={"100%"}
          height={"100vh"}
          paddingTop={20}
          alignItems={"center"}
        >
          <Flex justify="center">
            <Image
              preview={false}
              src={"/favicon.ico"}
              width={"50px"}
              height={"50px"}
              alt={"logo"}
            />
            <Typography.Title
              level={2}
              style={{ margin: 0, marginTop: 5, marginLeft: 5 }}
            >
              Ocuelar
            </Typography.Title>
          </Flex>

          <Stack
            justifySelf={"center"}
            borderRadius={1}
            direction={"column"}
            alignItems={"center"}
            justifyContent={"center"}
            pb={3}
            maxWidth={400}
            pl={2}
            pr={2}
            width={"100%"}
            alignSelf={"center"}
          >
            <Typography.Title level={4} style={{ marginTop: 10 }}>
              Hi, Welcome Back
            </Typography.Title>
            <Typography.Title level={5} style={{ marginTop: 0 }}>
              Enter your credentials to continue
            </Typography.Title>
            <Controller
              name="account"
              render={({ field, fieldState: { error, isTouched } }) => {
                return (
                  <Input
                    status={error ? "error" : undefined}
                    size="large"
                    placeholder="account"
                    onBlur={field.onBlur}
                    onChange={field.onChange}
                    value={field.value}
                    prefix={<UserOutlined />}
                  />
                  // <FormControl
                  //   fullWidth
                  //   error={!!error}
                  //   sx={{ ...theme.typography.customInput }}
                  // >
                  //   <InputLabel htmlFor="outlined-adornment-account-login">
                  //     Account
                  //   </InputLabel>
                  //   <OutlinedInput
                  //     id="outlined-adornment-account-login"
                  //     type="text"
                  //     value={field.value}
                  //     name="account"
                  //     onBlur={field.onBlur}
                  //     onChange={field.onChange}
                  //   />
                  //   {isTouched && error && (
                  //     <FormHelperText
                  //       error
                  //       id="standard-weight-helper-text-account-login"
                  //     >
                  //       {error.message}
                  //     </FormHelperText>
                  //   )}
                  // </FormControl>
                );
              }}
            />
            <Controller
              name="password"
              render={({ field, fieldState: { error, isTouched } }) => {
                return (
                  <Input
                    status={error ? "error" : undefined}
                    size="large"
                    placeholder="password"
                    onBlur={field.onBlur}
                    onChange={field.onChange}
                    value={field.value}
                    style={{ marginTop: 10 }}
                    prefix={<LockOutlined />}
                  />
                  // <FormControl
                  //   fullWidth
                  //   error={!!error}
                  //   sx={{ ...theme.typography.customInput }}
                  // >
                  //   <InputLabel htmlFor="outlined-adornment-password-login">
                  //     Password
                  //   </InputLabel>
                  //   <OutlinedInput
                  //     id="outlined-adornment-password-login"
                  //     type="password"
                  //     value={field.value}
                  //     name="password"
                  //     onBlur={field.onBlur}
                  //     onChange={field.onChange}
                  //   />
                  //   {isTouched && error && (
                  //     <FormHelperText
                  //       error
                  //       id="standard-weight-helper-text-password-login"
                  //     >
                  //       {error.message}
                  //     </FormHelperText>
                  //   )}
                  // </FormControl>
                );
              }}
            />
            {contextHolder}
            <Button
              onClick={methods.handleSubmit(onSubmit)}
              style={{ width: "100%", marginTop: 10 }}
              disabled={
                methods.formState.isLoading ||
                !methods.formState.isValid ||
                methods.formState.isSubmitting
              }
              type="primary"
            >
              Sign In
            </Button>
          </Stack>
        </Stack>
      </div>
    </FormProvider>
  );
};

export default Login;
