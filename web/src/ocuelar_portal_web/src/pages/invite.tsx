import { HStack, VStack } from "@chakra-ui/react";
import {
  Box,
  Button,
  FormControl,
  FormHelperText,
  InputLabel,
  OutlinedInput,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { Controller, FormProvider, useForm } from "react-hook-form";
import { useTheme } from "@mui/material/styles";
import { useRouter } from "next/router";
import axios from "axios";
interface FormValues {
  username: string;
  password: string;
  name: string;
  first_name: string;
  last_name: string;
}
const Invite = () => {
  const methods = useForm<FormValues>();
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down("md"));
  const onSubmit = async (values: FormValues) => {
    // console.log(query.inviterId);
    // return;
    try {
      const { data } = await axios.post("/api/invite", {
        username: values.username,
        password: values.password,
        first_name: values.first_name,
        last_name: values.last_name,
        inviterId: query.inviterId,
      });
      alert("done");
      methods.reset();
    } catch (error) {}
  };
  const { query } = useRouter();
  return (
    <FormProvider {...methods}>
      <VStack bgColor={"#091f3c"} h={"100vh"} pt={20}>
        <VStack w={"40%"}>
          <Typography
            variant="caption"
            fontSize="16px"
            textAlign={matchDownSM ? "center" : "inherit"}
            mb={3}
          >
            Register your account
          </Typography>
          <Controller
            name="username"
            render={({ field, fieldState: { error, isTouched } }) => {
              return (
                <FormControl
                  fullWidth
                  error={!!error}
                  sx={{ ...theme.typography.customInput }}
                >
                  <InputLabel htmlFor="outlined-adornment-account-login">
                    Username
                  </InputLabel>
                  <OutlinedInput
                    id="outlined-adornment-account-login"
                    type="text"
                    value={field.value}
                    name="Username"
                    onBlur={field.onBlur}
                    onChange={field.onChange}
                  />
                  {isTouched && error && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text-account-login"
                    >
                      {error.message}
                    </FormHelperText>
                  )}
                </FormControl>
              );
            }}
          />
          <Controller
            name="password"
            render={({ field, fieldState: { error, isTouched } }) => {
              return (
                <FormControl
                  fullWidth
                  error={!!error}
                  sx={{ ...theme.typography.customInput }}
                >
                  <InputLabel htmlFor="outlined-adornment-password-login">
                    Password
                  </InputLabel>
                  <OutlinedInput
                    id="outlined-adornment-password-login"
                    type="password"
                    value={field.value}
                    name="password"
                    onBlur={field.onBlur}
                    onChange={field.onChange}
                  />
                  {isTouched && error && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text-password-login"
                    >
                      {error.message}
                    </FormHelperText>
                  )}
                </FormControl>
              );
            }}
          />
          <HStack w={"100%"}>
            <Controller
              name="first_name"
              render={({ field, fieldState: { error, isTouched } }) => {
                return (
                  <FormControl
                    fullWidth
                    error={!!error}
                    sx={{ ...theme.typography.customInput }}
                  >
                    <InputLabel htmlFor="outlined-adornment-account-login">
                      First Name
                    </InputLabel>
                    <OutlinedInput
                      id="outlined-adornment-account-login"
                      type="text"
                      value={field.value}
                      name="first_name"
                      onBlur={field.onBlur}
                      onChange={field.onChange}
                    />
                    {isTouched && error && (
                      <FormHelperText
                        error
                        id="standard-weight-helper-text-account-login"
                      >
                        {error.message}
                      </FormHelperText>
                    )}
                  </FormControl>
                );
              }}
            />
            <Controller
              name="last_name"
              render={({ field, fieldState: { error, isTouched } }) => {
                return (
                  <FormControl
                    fullWidth
                    error={!!error}
                    sx={{ ...theme.typography.customInput }}
                  >
                    <InputLabel htmlFor="outlined-adornment-account-login">
                      Last Name
                    </InputLabel>
                    <OutlinedInput
                      id="outlined-adornment-account-login"
                      type="text"
                      value={field.value}
                      name="last_name"
                      onBlur={field.onBlur}
                      onChange={field.onChange}
                    />
                    {isTouched && error && (
                      <FormHelperText
                        error
                        id="standard-weight-helper-text-account-login"
                      >
                        {error.message}
                      </FormHelperText>
                    )}
                  </FormControl>
                );
              }}
            />
          </HStack>
          <Box
            sx={{ mt: 2 }}
            bgcolor={"#2ca58d"}
            width={"100%"}
            borderRadius={1}
          >
            <Button
              onClick={methods.handleSubmit(onSubmit)}
              color="secondary"
              disabled={
                methods.formState.isLoading ||
                !methods.formState.isValid ||
                methods.formState.isSubmitting
              }
              fullWidth
              size="large"
              type="submit"
              variant="contained"
            >
              Sign In
            </Button>
          </Box>
        </VStack>
      </VStack>
    </FormProvider>
  );
};
export default Invite;
