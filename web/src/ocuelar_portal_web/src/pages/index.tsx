import Image from "next/image";
import { Inter } from "next/font/google";
import { Box, Stack } from "@mui/material";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <Box m={0} width={"100%"} height={"100vh"} bgcolor={"#1A202C"}>
      Home Page
    </Box>
  );
}
