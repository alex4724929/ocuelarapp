import { UserTable } from "@/components/UserTable";
import { exclude } from "@/lib/exclude";
import { AddIcon } from "@chakra-ui/icons";
import {
  Box,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Tag,
  useDisclosure,
  useModal,
} from "@chakra-ui/react";
import { Stack } from "@mui/material";
import { PrismaClient, users } from "@prisma/client";
import { createColumnHelper } from "@tanstack/react-table";
import axios from "axios";
import { GetServerSideProps } from "next";
import React from "react";
import { FaUserDoctor } from "react-icons/fa6";
import { RiAdminFill } from "react-icons/ri";
import { MdSick } from "react-icons/md";
import { Button, Flex, Select } from "antd";
import CreateAdminModal from "@/components/CreateAdminModal";
import CreatePatientModal from "@/components/CreatePatientModal";
import { useForm } from "react-hook-form";
import { UserAddOutlined } from "@ant-design/icons";
const options = [
  { value: "all", label: "All" },
  { value: "admin", label: "Admin" },
  { value: "doctor", label: "Doctor" },
  { value: "patient", label: "Patient" },
];
const columnHelper = createColumnHelper<users>();

const EditButton = ({ info, reloadUsers }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const defaultValue = info.row.original;
  const methods = useForm({
    defaultValues: {
      ...defaultValue,
      roles: defaultValue?.roles?.map((role) => ({ label: role, value: role })),
      devices: defaultValue?.devices?.map((device) => ({
        label: `${device.display_udid} (${device.label})`,
        value: device.id,
      })),
    },
  });
  React.useEffect(() => {
    methods.reset({
      ...defaultValue,
      roles: defaultValue?.roles?.map((role) => ({ label: role, value: role })),
      devices: defaultValue?.devices?.map((device) => ({
        label: `${device.display_udid} (${device.label})`,
        value: device.id,
        start_date: device.start_date,
        end_date: device.end_date,
        times: device.times,
      })),
    });
  }, [defaultValue, methods]);
  return (
    <>
      <Button onClick={onOpen}>Edit</Button>
      <CreatePatientModal
        methods={methods}
        defaultValue={info.row.original}
        mode={"edit"}
        isOpen={isOpen}
        onClose={onClose}
        reloadUsers={reloadUsers}
      />
    </>
  );
};
const Users = ({ defaultUsers = [] }: { defaultUsers: users[] }) => {
  const [users, setUsers] = React.useState<users[]>(defaultUsers);
  const [selectedOption, setSelectedOption] = React.useState<{
    label: string;
    value: string;
  }>(options[0]);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const reloadUsers = React.useCallback(async () => {
    const { data } = await axios.get("/api/users");
    setUsers(data.users);
  }, []);
  const columns = React.useMemo(
    () => [
      columnHelper.accessor("user_name", {
        cell: (info) => info.getValue(),
        header: "User Name",
      }),

      columnHelper.accessor("first_name", {
        cell: (info) => info.getValue(),
        header: "First name",
      }),
      columnHelper.accessor("last_name", {
        cell: (info) => info.getValue(),
        header: "Last name",
      }),
      columnHelper.accessor("phone", {
        cell: (info) => info.getValue(),
        header: "Phone",
      }),
      columnHelper.accessor("email", {
        cell: (info) => info.getValue(),
        header: "Email",
      }),

      columnHelper.accessor("roles", {
        cell: (info) => <Tag>{info.getValue()}</Tag>,
        header: "Roles",
        meta: {
          isTag: true,
        },
      }),
      columnHelper.accessor("id", {
        cell: (info) => <EditButton info={info} reloadUsers={reloadUsers} />,
        header: "Edit",
        meta: {
          isTag: true,
        },
      }),
    ],
    [reloadUsers]
  );
  const data = React.useMemo(() => {
    if (selectedOption.value === "all") {
      return users;
    } else if (selectedOption.value === "admin") {
      return users.filter((user) => user?.roles.includes("admin"));
    } else if (selectedOption.value === "doctor") {
      return users.filter((user) => user?.roles.includes("doctor"));
    } else if (selectedOption.value === "patient") {
      return users.filter((user) => user?.roles.includes("patient"));
    }
  }, [selectedOption.value, users]);
  const methods = useForm({});
  return (
    <Flex style={{ height: "100%" }}>
      <Flex
        vertical
        style={{
          width: "100%",
          background: "white",
          paddingLeft: 10,
          paddingRight: 10,
          paddingTop: 10,
          paddingBottom: 10,
          borderRadius: 6,
        }}
      >
        <Button
          style={{
            alignSelf: "flex-end",
            marginBottom: 10,
          }}
          type="primary"
          shape="default"
          icon={<UserAddOutlined />}
        />

        <UserTable data={data} columns={columns} />
      </Flex>
      {/* <CreateAdminModal
        reloadUsers={reloadUsers}
        isOpen={isOpen}
        onClose={onClose}
      /> */}
      <CreatePatientModal
        methods={methods}
        mode={"create"}
        reloadUsers={reloadUsers}
        isOpen={isOpen}
        onClose={onClose}
      />
    </Flex>
  );
};
export const getServerSideProps: GetServerSideProps<{}> = async () => {
  const prisma = new PrismaClient();
  const users = await prisma.users.findMany({
    include: {
      devices: true,
    },
  });
  console.log(users[2].devices);
  return {
    props: {
      defaultUsers: JSON.parse(
        JSON.stringify(users.map((user) => exclude(user, ["token"])))
      ),
    },
  };
};
export default Users;
