import Layout from "@/components/layout";
import "@/styles/globals.css";
import componentStyleOverrides from "@/theme/compStyleOverride";
import Palette from "@/theme/palette";
import Typography from "@/theme/typography";
import { CookiesProvider } from "react-cookie";
import chakraTheme from "@chakra-ui/theme";
import { ConfigProvider, ThemeConfig } from "antd";
import {
  createTheme,
  ThemeOptions,
  ThemeProvider,
  Theme,
  TypographyVariantsOptions,
} from "@mui/material/styles";
import type { AppProps } from "next/app";
import React, { useMemo } from "react";
import { ChakraProvider } from "@chakra-ui/react";
export default function App({ Component, pageProps }: AppProps) {
  const theme: Theme = useMemo<Theme>(() => Palette("dark", "theme5"), []);
  const themeTypography: TypographyVariantsOptions =
    useMemo<TypographyVariantsOptions>(
      () => Typography(theme, 8, "'Roboto', sans-serif"),
      [theme]
    );
  const themeOptions: ThemeOptions = useMemo(
    () => ({
      palette: theme.palette,
      mixins: {
        toolbar: {
          minHeight: "48px",
          padding: "16px",
          "@media (min-width: 600px)": {
            minHeight: "48px",
          },
        },
      },
      typography: themeTypography,
    }),
    [theme, themeTypography]
  );
  const themes: Theme = createTheme(themeOptions);
  themes.components = useMemo(
    () => componentStyleOverrides(themes, 8, true),
    [themes]
  );

  return (
    <ConfigProvider theme={antTheme}>
      <CookiesProvider>
        <ChakraProvider theme={chakraTheme}>
          <ThemeProvider theme={themes}>
            <Layout>
              <Component {...pageProps} />
            </Layout>
          </ThemeProvider>
        </ChakraProvider>
      </CookiesProvider>
    </ConfigProvider>
  );
}

const antTheme: ThemeConfig = {};
