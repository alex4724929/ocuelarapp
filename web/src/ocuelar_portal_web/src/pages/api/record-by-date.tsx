// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import moment from "moment";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const user = await prisma.users.findFirst({
    where: {
      token: req.headers.authorization?.replace("Bearer ", ""),
    },
  });

  if (!user) return res.status(401).json({ message: "Unauthorized" });
  const devices = await prisma.devices.findMany({
    where: {
      owner: user.id,
      start_date: {
        not: null,
      },
      end_date: {
        not: null,
      },
      times: {
        not: null,
      },
    },
  });
  const data = await prisma.drop_record.findMany({
    where: {
      user_id: user.id,
      timestamp: {
        gte: req.body.start,
        lte: req.body.end,
      },
    },
    include: {
      devices: true,
    },
  });
  let keysObject = {};
  keysObject.devices = devices;
  data.map((item) => {
    if (!keysObject[item.devices.label]) {
      keysObject[item.devices.label] = [];
    }
    keysObject[item.devices.label].push(item);
  });
  return res.status(200).json(keysObject);
}
