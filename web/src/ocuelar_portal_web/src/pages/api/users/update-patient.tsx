// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";
import { v4 as uuidv4 } from "uuid";
import jws from "jws";
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method !== "POST") {
    res.status(405).json({ message: "Method not allowed" });
    return;
  }
  const user = await prisma.users.update({
    where: {
      id: req.body.id,
    },
    data: {
      first_name: req.body.first_name ?? "",
      last_name: req.body.last_name ?? "",
      user_name: req.body.user_name,
      password: req.body.password,
      roles: req?.body?.roles?.map((item) => item.value) ?? [],
      email: req.body.email ?? "",
      phone: req.body.phone ?? "",
    },
  });
  const devices = await prisma.devices.findMany({
    where: {
      owner: req.body.id,
    },
  });
  const deviceIds = devices.map((item) => item.id);
  const newDevices = req?.body?.devices?.map((item) => item.value) ?? [];
  const toDelete = deviceIds.filter((item) => !newDevices.includes(item));
  const toAdd = newDevices.filter((item) => !deviceIds.includes(item));
  await prisma.devices.updateMany({
    where: {
      id: {
        in: toDelete,
      },
    },
    data: {
      owner: null,
    },
  });
  await prisma.devices.updateMany({
    where: {
      id: {
        in: toAdd,
      },
    },
    data: {
      owner: req.body.id,
    },
  });
  console.log("update devices", req?.body?.devices);
  req?.body?.devices &&
    req?.body?.devices.forEach(async (device) => {
      console.log(device);
      await prisma.devices.update({
        where: {
          id: device.value,
        },
        data: {
          start_date: device.start_date,
          end_date: device.end_date,
          times: device.times,
        },
      });
    });
  res.status(200).json({ message: "success" });
  return;
}
