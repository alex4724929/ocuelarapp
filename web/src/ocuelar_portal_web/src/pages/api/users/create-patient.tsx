// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";
import { v4 as uuidv4 } from "uuid";
import jws from "jws";
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method !== "POST") {
    res.status(405).json({ message: "Method not allowed" });
    return;
  }
  if (!req.body.user_name || !req.body.password) {
    res.status(400).json({ message: "Username and password required" });
    return;
  }
  const findUser = await prisma.users.findFirst({
    where: {
      user_name: req.body.user_name,
    },
  });
  if (findUser) {
    res.status(400).json({ message: "Username already exists" });
    return;
  }
  try {
    const user = await prisma.users.create({
      data: {
        first_name: req.body.first_name ?? "",
        last_name: req.body.last_name ?? "",
        email: req.body.email ?? "",
        phone: req.body.phone ?? "",
        user_name: req.body.user_name,
        password: req.body.password,
        id: uuidv4(),
        token: jws.sign({
          header: { alg: "HS256" },
          payload: `${req.body.username}-${req.body.password}`,
          secret: process.env.TOKEN_SECRET,
        }),
        roles: req?.body?.roles?.map((item) => item.value) ?? [],
      },
    });
    await prisma.devices.updateMany({
      where: {
        id: {
          in: req?.body?.devices?.map((item) => item.value) ?? [],
        },
      },
      data: {
        owner: user.id,
      },
    });
    res.status(200).json({ token: user.token });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Internal server error" });
  }
}
