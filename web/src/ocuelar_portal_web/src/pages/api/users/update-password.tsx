// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const user = await prisma.users.findFirst({
    where: {
      user_name: req.body.user_name,
    },
  });
  if (!user) {
    return res.status(400).json({ error: "User not found" });
  } else {
  }
  const data = await prisma.users.update({
    where: {
      id: user.id,
    },
    data: {
      password: req.body.password,
    },
  });
  res.status(200).json(data);
}
