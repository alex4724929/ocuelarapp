// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import moment from "moment-timezone";
import type { NextApiRequest, NextApiResponse } from "next";
import adminInstance from "@/lib/admin";
import { Message } from "firebase-admin/messaging";
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const today = moment().tz("America/Los_Angeles");
  const data = await prisma.schedule.findMany({
    where: {
      AND: [
        { start_date: { lte: today.toDate() } },
        { end_date: { gte: today.toDate() } },
      ],
      isActive: true,
    },
    include: {
      users: true,
      devices: true,
    },
  });
  const totalMins = 24 * 60;
  let notifications = [];
  data.forEach(async (schedule) => {
    const diff = totalMins / (schedule?.frequency ? schedule?.frequency! : 1);
    const startTime = moment()
      .tz("America/Los_Angeles")
      .startOf("day")
      .add("hours", schedule.start_hour ?? 0)
      .add("minutes", schedule.start_mins);
    if (today.isAfter(startTime)) {
      const currentDiff = today.diff(startTime, "minutes");
      if (currentDiff % diff === 0 && schedule.users?.notification_Id) {
        const message: Message = {
          token: schedule.users?.notification_Id,
          notification: {
            title: `It is time to take your medicine ${schedule.devices?.label}`,
            body: "Please take your medicine",
          },
        };

        // Send a message to the device corresponding to the provided
        // registration token.
        notifications.push(adminInstance.messaging().send(message));
      }
    }
  });
  await Promise.all(notifications);

  return res.status(200).json("done");
  // res.status(200).json(data);
}
