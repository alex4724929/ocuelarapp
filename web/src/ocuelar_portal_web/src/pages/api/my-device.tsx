// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const result = await prisma.users.findFirst({
    where: {
      token: req.headers.authorization?.replace("Bearer ", ""),
    },
    include: {
      devices: true,
    },
  });
  res.status(200).json(result?.devices);
}
