// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { exclude } from "@/lib/exclude";
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "GET") {
    try {
      const users = await prisma.users.findMany({
        include: {
          devices: true,
        },
      });
      res.status(200).json({
        users: users.map((user) => exclude(user, ["token"])),
      });
    } catch (error) {
      res.status(500).json({ message: error?.message ?? "unknow error" });
      return;
    }
  }
}
