// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const id = req.body.id;
  const display_udid = `Ocuelar-${id.slice(-6).toUpperCase()}`;
  const data = await prisma.devices.create({
    data: {
      id: id,
      label: req.body.label,
      display_udid: display_udid,
    },
  });
  res.status(200).json(data);
}
