// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";
import { v4 as uuidv4 } from "uuid";
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method !== "POST") {
    return res.status(405).json({ message: "Method not allowed" });
  }
  const id = uuidv4();
  if (!req.body.device_id)
    return res.status(400).json({ message: "device_id is required" });
  if (!req.body.timestamp)
    return res.status(400).json({ message: "timestamp is required" });
  if (!req.body.count)
    return res.status(400).json({ message: "count is required" });
  const device = await prisma.devices.findFirst({
    where: {
      id: req.body.device_id,
    },
  });
  if (device == null) {
    return res.status(400).json({ message: "Invalid device id" });
  }
  const data = await prisma.drop_record_data.create({
    data: {
      id: id,
      timestamp: req.body.timestamp,
      device_id: req.body.device_id,
      count: req.body.count,
    },
  });
  return res.status(200).json(data);
}
