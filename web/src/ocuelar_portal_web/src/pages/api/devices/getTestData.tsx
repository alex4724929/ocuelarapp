// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const data = await prisma.drop_record_data.findMany({
    where: {
      device_id: req.query.device_id ?? "",
    },
    orderBy: {
      timestamp: "desc",
    },
    take: 20,
  });
  res.status(200).json(data);
}
