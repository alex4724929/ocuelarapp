// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const token = req.headers.authorization?.replace("Bearer ", "");
  const user = await prisma.users.findFirst({
    where: {
      token: token,
    },
  });
  const deviceId = req.body.deviceId;
  try {
    await prisma.devices.update({
      where: {
        id: deviceId,
      },
      data: {
        owner: user?.id,
      },
    });
    res.status(200).json({
      message: "success",
    });
  } catch (error) {
    res.status(500).json({
      message: "error",
    });
  }
}
