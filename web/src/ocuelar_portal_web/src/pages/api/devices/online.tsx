// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import moment from "moment";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method != "POST") {
    return res.status(405).json({ message: "Method not allowed" });
  }
  if (!req.body.device_id) {
    return res.status(400).json({ message: "Bad request" });
  }
  const device_id: string = req.body.device_id;
  const data = await prisma.devices.update({
    where: {
      id: device_id.toLowerCase(),
    },
    data: {
      last_online_time: moment().unix(),
    },
  });
  res.status(200).json(data);
}
