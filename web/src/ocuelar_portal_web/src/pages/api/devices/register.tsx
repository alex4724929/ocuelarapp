// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method != "POST") {
    return res.status(405).json({ message: "Method not allowed" });
  }
  const id = req.body.deviceId;
  const display_udid = `Ocuelar-${id.slice(-6).toUpperCase()}`;
  const ownerId = req.body.owner_id || null;
  const label = req.body.label || null;
  const data = await prisma.devices.upsert({
    where: {
      id: id,
    },
    create: {
      id: id,
      display_udid: display_udid,
      owner: ownerId,
      label,
    },
    update: {
      display_udid: display_udid,
      owner: ownerId,
      label,
    },
  });
  res.status(200).json(data);
}
