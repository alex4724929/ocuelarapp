// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  await prisma.drop_record_data.deleteMany({
    where: {
      device_id: req.body.deviceId,
    },
  });
  res.status(200).json("delete success");
}
