// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const data = await prisma.devices.findFirst({
    where: {
      id: req.body.deviceId,
    },
  });
  if (data) {
    res.status(200).json(data);
  } else {
    res.status(404).json({
      message: "not found",
    });
  }
  return;
}
