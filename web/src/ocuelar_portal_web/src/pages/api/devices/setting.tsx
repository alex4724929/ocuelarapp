// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import moment from "moment";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const device_id: string = req.query.device_id as string;
  if (!device_id) {
    return res.status(400).json({ message: "Bad request" });
  }

  if (req.method == "GET") {
    const data = await prisma.devices.findFirst({
      where: {
        id: device_id.toLowerCase(),
      },
    });
    return res.status(200).json(data?.config || {});
  }
  if (req.method == "POST") {
    const data = await prisma.devices.update({
      where: {
        id: device_id.toLowerCase(),
      },
      data: {
        config: req.body.config,
      },
    });
    return res.status(200).json(data);
  }
  return res.status(405).json({ message: "Method not allowed" });
}
