// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";
import { v4 as uuidv4 } from "uuid";
import jws from "jws";
import { StreamChat } from "stream-chat";
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method !== "POST") {
    res.status(405).json({ message: "Method not allowed" });
    return;
  }
  if (!req.body.username || !req.body.password) {
    res.status(400).json({ message: "Username and password required" });
    return;
  }
  const userId = uuidv4();
  const serverClient = StreamChat.getInstance(
    "g7m74vwxmjkg",
    "vfe38rnvwnqtpkfkk625suqfuuvkbdrxkcfzk6vu3nfeeh8ere33mpx5ngbjpq9z"
  );
  const token = serverClient.createToken(userId);
  const user = await prisma.users.create({
    data: {
      user_name: req.body.username,
      password: req.body.password,
      id: userId,
      token: jws.sign({
        header: { alg: "HS256" },
        payload: `${req.body.username}-${req.body.password}`,
        secret: process.env.TOKEN_SECRET,
      }),
      chat_token: token,
      roles: req.body.roles ?? [],
    },
  });

  if (!user) {
    res.status(401).json({ message: "Invalid credentials" });
    return;
  }
  res.status(200).json({ token: user.token, chatToken: user.chat_token });
}
