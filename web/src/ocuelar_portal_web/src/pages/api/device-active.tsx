// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const data = await prisma.devices.findMany({
    where: {
      owner: null,
      label: {
        notIn: null,
      },
    },
  });
  console.log(data);
  res.status(200).json(data);
}
