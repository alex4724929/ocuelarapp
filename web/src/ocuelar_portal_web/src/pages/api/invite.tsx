// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";
import { v4 as uuidv4 } from "uuid";
import jws from "jws";
import { StreamChat } from "stream-chat";
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const inviterId = req.body.inviterId;
  console.log(inviterId);
  const userId = uuidv4();
  const serverClient = StreamChat.getInstance(
    "g7m74vwxmjkg",
    "vfe38rnvwnqtpkfkk625suqfuuvkbdrxkcfzk6vu3nfeeh8ere33mpx5ngbjpq9z"
  );
  const token = serverClient.createToken(userId);
  const inviter = await prisma.users.findUnique({
    where: {
      id: inviterId,
    },
  });
  console.log(inviter);
  const user = await prisma.users.create({
    data: {
      user_name: req.body.username,
      password: req.body.password,
      id: userId,
      token: jws.sign({
        header: { alg: "HS256" },
        payload: `${req.body.username}-${req.body.password}`,
        secret: process.env.TOKEN_SECRET,
      }),
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      chat_token: token,
      roles: req.body.roles ?? [],
    },
  });
  console.log("user created");
  const channel = serverClient.channel("messaging", undefined, {
    members: [user.id, inviterId],
    name: `${inviter?.first_name}_${user.first_name}`,
    created_by_id: user.id,
  });
  await channel.create();
  return res.status(200).json("done");
}
