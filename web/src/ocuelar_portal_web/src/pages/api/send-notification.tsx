// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";
import admin from "firebase-admin";
import adminInstance from "@/lib/admin";
import { Message } from "firebase-admin/messaging";
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const registrationToken =
    "eByrOZvz0ERJm66GZ091ef:APA91bHia8CkhAMa1Vi4lmeadWgZzKesMOl95nEe7awVfScpPloqkrCkIvJKHyK1sYF_tDl7fOSC2LvafA0aMXllvdAxvX7uo-W07iD2uXCQgk8vdIZexH8NOwBdoT-pE90adB5fn7Om";

  const message: Message = {
    token: registrationToken,
    notification: {
      title: "Portugal vs. Denmark",
      body: "great match!",
    },
  };

  // Send a message to the device corresponding to the provided
  // registration token.
  adminInstance
    .messaging()
    .send(message)
    .then((response) => {
      // Response is a message ID string.
      console.log("Successfully sent message:", response);
      res.status(200).json(response);
    })
    .catch((error) => {
      console.log("Error sending message:", error);
    });
}
