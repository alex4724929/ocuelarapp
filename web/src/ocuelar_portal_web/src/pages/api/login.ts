// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  try {
    if (req.method !== "POST") {
      res.status(405).json({ message: "Method not allowed" });
      return;
    }
    if (!req.body.account || !req.body.password) {
      res.status(400).json({ message: "Account and password required" });
      return;
    }
    const user = await prisma.users.findFirst({
      where: {
        user_name: req.body.account,
        password: req.body.password,
      },
    });
    if (!user) {
      res.status(401).json({ message: "Invalid credentials" });
      return;
    }
    res.status(200).json({ ...user });
    return;
  } catch (error) {
    res.status(500).json({ message: error?.message ?? "unknow error" });
    return;
  }
}
