// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  try {
    if (req.method !== "POST") {
      res.status(405).json({ message: "Method not allowed" });
      return;
    }
    if (req.body.notificationToken && req.headers.authorization) {
      const user = await prisma.users.findFirst({
        where: {
          token: req.headers.authorization?.replace("Bearer ", ""),
        },
      });
      await prisma.users.update({
        where: {
          id: user?.id,
        },
        data: {
          notification_Id: req.body.notificationToken,
        },
      });
      res.status(200).json({ message: "sucess" });
      return;
    }
    res.status(400).json({ message: "bad request" });
    return;
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: error?.message ?? "unknow error" });
    return;
  }
}
