// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import path from "path";
import { promises as fs } from "fs";
import prisma from "@/lib/prisma";
// const file = require("../../assets/v4_uuids.txt");
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  try {
    const jsonDirectory = path.join(process.cwd(), "assets");
    console.log(jsonDirectory);
    const fileContents = await fs.readFile(
      jsonDirectory + "/v4_uuids.txt",
      "utf8"
    );
    let updates = [];
    console.log(
      fileContents.split("\r\n").forEach((item) => {
        updates.push(
          prisma.devices.upsert({
            where: {
              id: item,
            },
            update: {
              display_udid: `Ocuelar-${item.slice(-6).toUpperCase()}`,
            },
            create: {
              id: item,
              display_udid: `Ocuelar-${item.slice(-6).toUpperCase()}`,
            },
          })
        );
      })
    );
    Promise.all(updates);
  } catch (err) {
    console.log(err);
  }

  //   const data = await prisma.doctors.findMany();
  res.status(200).json([]);
}
