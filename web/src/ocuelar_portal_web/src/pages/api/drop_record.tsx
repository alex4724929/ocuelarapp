// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";
import { v4 as uuidv4 } from "uuid";
import moment from "moment";
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method != "POST") {
    res.status(405).json({ message: "Method not allowed" });
    return;
  }
  if (!req.body.records || !req.body.device_id) {
    res.status(400).json({ message: "bad request" });
    return;
  }
  try {
    const user = await prisma.users.findFirst({
      where: {
        token: req.headers.authorization?.replace("Bearer ", ""),
      },
    });
    if (!user) {
      res.status(401).json({ message: "Unauthorized" });
      return;
    }

    const id = uuidv4();
    const data = await prisma.drop_record_row.create({
      data: {
        id: id,
        createAt: moment().unix(),
        data: req.body.records,
        device_id: req.body.device_id,
        user_id: user.id,
      },
    });
    if (Array.isArray(req.body.records)) {
      req.body.records.forEach(async (item) => {
        const id = `${item.timestamp}-${item.device_id}-${item.data}-${user.id}`;
        await prisma.drop_record.upsert({
          where: {
            id: id,
          },
          update: {
            data: item.data,
            device_id: req.body.device_id,
            user_id: user.id,
            timestamp: item.timestamp,
          },
          create: {
            id: id,
            timestamp: item.timestamp,
            data: item.data,
            device_id: req.body.device_id,
            user_id: user.id,
          },
        });
      });
      return res.status(200).json({ message: "ok" });
    }

    return res.status(200).json(data);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: error?.message ?? "unknow error" });
  }
}
