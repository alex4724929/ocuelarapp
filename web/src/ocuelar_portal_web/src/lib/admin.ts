import admin from "firebase-admin";
if (!global.adminInstance) {
  global.adminInstance = admin;
  var serviceAccount = require("../../assets/ocuelar-7695e-firebase-adminsdk-uhdwt-0c14e10d64.json");
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
  });
}

export default global.adminInstance;
