import {
  Box,
  Button,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
  useToast,
} from "@chakra-ui/react";
import {
  FormControl,
  FormHelperText,
  InputLabel,
  OutlinedInput,
  Typography,
  useTheme,
} from "@mui/material";
import React from "react";
import Select from "react-select";
import { Controller, FormProvider, useForm } from "react-hook-form";
import axios from "axios";
const options = [
  { value: "admin", label: "Admin" },
  { value: "doctor", label: "Doctor" },
  { value: "patient", label: "Patient" },
];
const CreateAdminModal = ({ isOpen, onClose, reloadUsers }) => {
  const initialRef = React.useRef(null);
  const finalRef = React.useRef(null);
  const theme = useTheme();
  const methods = useForm();
  const toast = useToast();
  React.useEffect(() => {
    if (!isOpen) {
      methods.reset();
    }
  }, [isOpen, methods]);
  const onSubmit = async (data) => {
    try {
      await axios.post("/api/create-user", {
        username: data.account,
        password: data.password,
        roles: data?.roles?.map((role) => role.value) ?? [],
        type: "admin",
        email: data.email,
        first_name: data.first_name,
        last_name: data.last_name,
        phone: data.phone,
      });
      toast({
        title: "Create admin account successfully",
        status: "success",
        duration: 3000,
        isClosable: true,
        position: "top",
      });
      reloadUsers();
      onClose();
    } catch (error) {
      toast({
        title: "failed to create admin account",
        description: error?.response?.data?.message ?? "unknown error",
        status: "error",
        duration: 3000,
        isClosable: true,
        position: "top",
      });
    }
  };
  return (
    <FormProvider {...methods}>
      <Modal
        initialFocusRef={initialRef}
        finalFocusRef={finalRef}
        isOpen={isOpen}
        onClose={onClose}
      >
        <ModalOverlay bgColor={"whiteAlpha.200"} w={"100vw"} h={"100vh"} />
        <ModalContent
          // bgColor={"white"}
          display={"flex"}
          alignSelf={"center"}
          justifySelf={"center"}
          borderRadius={5}
          alignItems={"center"}
        >
          <Box
            bgColor={"white"}
            minW={300}
            w={"30vw"}
            h={"max-content"}
            borderRadius={5}
            color={"black"}
            p={3}
          >
            <ModalHeader fontSize={24} fontWeight={"bold"} textAlign={"center"}>
              Create Admin account
            </ModalHeader>
            <ModalBody pb={6} display={"flex"} flexDirection={"column"}>
              <Typography fontWeight={"500"} mb={1}>
                Account
              </Typography>
              <Controller
                name="account"
                render={({ field, fieldState: { error, isTouched } }) => {
                  return (
                    <Input
                      h={"40px"}
                      borderWidth={1}
                      borderRadius={6}
                      placeholder="Account"
                      pl={3}
                      value={field.value}
                      onChange={field.onChange}
                      onBlur={field.onBlur}
                    />
                  );
                }}
              />
              <Typography fontWeight={"500"} mb={1} mt={2}>
                Password
              </Typography>
              <Controller
                name="password"
                render={({ field, fieldState: { error, isTouched } }) => {
                  return (
                    <Input
                      h={"40px"}
                      borderWidth={1}
                      borderRadius={6}
                      placeholder="Password"
                      pl={3}
                      value={field.value}
                      onChange={field.onChange}
                      onBlur={field.onBlur}
                    />
                  );
                }}
              />
              <Typography fontWeight={"500"} mb={1} mt={2}>
                First Name
              </Typography>
              <Controller
                name="first_name"
                render={({ field, fieldState: { error, isTouched } }) => {
                  return (
                    <Input
                      h={"40px"}
                      borderWidth={1}
                      borderRadius={6}
                      placeholder="First Name"
                      pl={3}
                      value={field.value}
                      onChange={field.onChange}
                      onBlur={field.onBlur}
                    />
                  );
                }}
              />
              <Typography fontWeight={"500"} mb={1} mt={2}>
                Last Name
              </Typography>
              <Controller
                name="last_name"
                render={({ field, fieldState: { error, isTouched } }) => {
                  return (
                    <Input
                      h={"40px"}
                      borderWidth={1}
                      borderRadius={6}
                      placeholder="Last Name"
                      pl={3}
                      value={field.value}
                      onChange={field.onChange}
                      onBlur={field.onBlur}
                    />
                  );
                }}
              />
              <Typography fontWeight={"500"} mb={1} mt={2}>
                Email
              </Typography>
              <Controller
                name="email"
                render={({ field, fieldState: { error, isTouched } }) => {
                  return (
                    <Input
                      h={"40px"}
                      borderWidth={1}
                      borderRadius={6}
                      placeholder="Email"
                      pl={3}
                      value={field.value}
                      onChange={field.onChange}
                      onBlur={field.onBlur}
                    />
                  );
                }}
              />
              <Typography fontWeight={"500"} mb={1} mt={2}>
                Phone
              </Typography>
              <Controller
                name="phone"
                render={({ field, fieldState: { error, isTouched } }) => {
                  return (
                    <Input
                      h={"40px"}
                      borderWidth={1}
                      borderRadius={6}
                      placeholder="phone"
                      pl={3}
                      value={field.value}
                      onChange={field.onChange}
                      onBlur={field.onBlur}
                    />
                  );
                }}
              />
              <Typography fontWeight={"500"} mb={1} mt={2}>
                Roles
              </Typography>
              <Controller
                name="roles"
                defaultValue={[{ label: "Admin", value: "admin" }]}
                render={({ field }) => {
                  return (
                    <Select
                      value={field.value}
                      onChange={(value) => {
                        const admin = value.findIndex(
                          (item) => item.value === "admin"
                        );
                        if (admin !== -1) {
                          field.onChange(value);
                        }
                      }}
                      isMulti
                      options={options}
                    />
                  );
                }}
              />

              <Stack direction={"row"} mt={20}>
                <Box
                  onClick={methods.handleSubmit(onSubmit)}
                  fontWeight={"bold"}
                  fontSize={20}
                  cursor={"pointer"}
                  bgColor={"teal.400"}
                  flex={1}
                  display={"flex"}
                  borderRadius={6}
                  justifyContent={"center"}
                  alignItems={"center"}
                  py={3}
                  color={"white"}
                >
                  Create
                </Box>
                <Box
                  onClick={onClose}
                  fontWeight={"bold"}
                  fontSize={20}
                  cursor={"pointer"}
                  bgColor={"red.400"}
                  flex={1}
                  display={"flex"}
                  borderRadius={6}
                  justifyContent={"center"}
                  alignItems={"center"}
                  py={3}
                  color={"white"}
                >
                  Cancel
                </Box>
              </Stack>
            </ModalBody>
          </Box>
        </ModalContent>
      </Modal>
    </FormProvider>
  );
};
export default CreateAdminModal;
