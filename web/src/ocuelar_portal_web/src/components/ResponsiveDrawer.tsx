import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import MailIcon from "@mui/icons-material/Mail";
import Person from "@mui/icons-material/Person";
import MedicalServices from "@mui/icons-material/MedicalServices";
import MedicalInformation from "@mui/icons-material/MedicalInformation";
import MenuIcon from "@mui/icons-material/Menu";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { ExpandLess, ExpandMore, StarBorder } from "@mui/icons-material";
import { Collapse, Stack } from "@mui/material";
import { useRouter } from "next/router";
import { FaUserFriends } from "react-icons/fa";
import { PiDevicesFill } from "react-icons/pi";
import { BiSolidNotification } from "react-icons/bi";
import Link from "next/link";
import { Flex } from "antd";
import {
  AppstoreOutlined,
  MailOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import type { MenuProps } from "antd";
import { Menu } from "antd";
const drawerWidth = 240;

interface Props {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window?: () => Window;
  children: React.ReactNode;
}
type MenuItem = Required<MenuProps>["items"][number];
function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: "group"
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}

const items: MenuProps["items"] = [
  getItem("Users", "sub1", <AppstoreOutlined />, [
    getItem("All Users", "all_users"),
  ]),

  getItem("Devices", "sub2", <AppstoreOutlined />, [
    getItem("All Devices", "all_devices"),
  ]),

  { type: "divider" },

  getItem("Notifications", "sub3", <SettingOutlined />, [
    getItem("All Notifications", "all_notifications"),
  ]),

  getItem(
    "Group",
    "grp",
    null,
    [getItem("Option 13", "13"), getItem("Option 14", "14")],
    "group"
  ),
];
export default function ResponsiveDrawer(props: Props) {
  const { window } = props;
  const { pathname } = useRouter();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [permissionItemCollapsed, setPermissionItemCollapsed] =
    React.useState(true);
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const handlePermissionItemToggle = React.useCallback(() => {
    setPermissionItemCollapsed(!permissionItemCollapsed);
  }, [permissionItemCollapsed]);
  const drawer = React.useMemo(() => {
    return (
      <div>
        <List>
          <ListItemButton onClick={handlePermissionItemToggle}>
            <ListItemIcon>
              <Person />
            </ListItemIcon>
            <ListItemText primary="Users" />
            {permissionItemCollapsed ? <ExpandLess /> : <ExpandMore />}
          </ListItemButton>
          <Collapse in={permissionItemCollapsed} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              <ListItemButton selected sx={{ pl: 4 }}>
                <ListItemIcon>
                  <MedicalInformation />
                </ListItemIcon>
                <ListItemText primary="Patient" />
              </ListItemButton>
              <ListItemButton sx={{ pl: 4 }}>
                <ListItemIcon>
                  <MedicalServices />
                </ListItemIcon>
                <ListItemText primary="Doctor" />
              </ListItemButton>
            </List>
          </Collapse>
        </List>
        <Divider />
        {/* <List>
          {["All mail", "Trash", "Spam"].map((text, index) => (
            <ListItem key={text} disablePadding>
              <ListItemButton>
                <ListItemIcon>
                  {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                </ListItemIcon>
                <ListItemText primary={text} />
              </ListItemButton>
            </ListItem>
          ))}
        </List> */}
      </div>
    );
  }, [handlePermissionItemToggle, permissionItemCollapsed]);

  return (
    <Flex>
      <Menu
        onClick={() => {}}
        style={{ width: 256 }}
        defaultSelectedKeys={["1"]}
        defaultOpenKeys={["sub1"]}
        mode="inline"
        items={items}
      />
    </Flex>
  );
}
