import * as React from "react";
import { Thead, Tbody, Tr, Th, Td, chakra } from "@chakra-ui/react";
import { TriangleDownIcon, TriangleUpIcon, DeleteIcon } from "@chakra-ui/icons";
import {
  useReactTable,
  flexRender,
  getCoreRowModel,
  ColumnDef,
  SortingState,
  getSortedRowModel,
  createColumnHelper,
} from "@tanstack/react-table";
import { Box, Stack } from "@mui/material";
import { Space, TableProps, Tag, Table, Button, Modal } from "antd";
import { users } from "@prisma/client";
export type UserTableProps<Data extends object> = {
  data: Data[];
  columns: ColumnDef<Data, any>[];
};
let a: users;

export function UserTable<Data extends object>({ data }: UserTableProps<Data>) {
  const [modal, contextHolder] = Modal.useModal();
  const onDeleted = React.useCallback((record: users) => {
    Modal.confirm({
      title: "Confirm",
      content: "Are you sure you want to delete this user?",
      footer: (_, { OkBtn, CancelBtn }) => (
        <>
          <CancelBtn />
          <OkBtn>Sure</OkBtn>
        </>
      ),
    });
  }, []);
  const columns = React.useMemo(() => {
    const columns: TableProps<users>["columns"] = [
      {
        title: "User Name",
        dataIndex: "user_name",
        key: "user_name",
        render: (text) => <a>{text}</a>,
      },
      {
        title: "First Name",
        dataIndex: "first_name",
        key: "first_name",
      },
      {
        title: "Last Name",
        dataIndex: "last_name",
        key: "last_name",
      },
      {
        title: "Roles",
        key: "roles",
        dataIndex: "roles",
        filters: [
          {
            text: "admin",
            value: "admin",
          },
          {
            text: "doctor",
            value: "doctor",
          },
          {
            text: "patient",
            value: "patient",
          },
        ],
        onFilter(value, record) {
          const roles = record.roles as string[];
          return roles.includes(value);
        },
        render: (_, { roles }) => (
          <>
            {roles.map((tag) => {
              let color = tag === "admin" ? "geekblue" : "green";
              if (tag === "patient") {
                color = "volcano";
              }
              return (
                <Tag color={color} key={tag}>
                  {tag.toUpperCase()}
                </Tag>
              );
            })}
          </>
        ),
      },
      {
        title: "Action",
        key: "action",
        render: (_, record) => (
          <Space size="middle">
            <a>Edit </a>
            <Button
              onClick={() => {
                onDeleted(record);
              }}
            >
              Delete
            </Button>
          </Space>
        ),
      },
    ];
    return columns;
  }, [onDeleted]);
  return <Table columns={columns} dataSource={data} />;
  return (
    <Table borderRadius={3} overflow={"hidden"}>
      <Thead bgColor={"teal.500"}>
        {table.getHeaderGroups().map((headerGroup) => (
          <Tr borderRadius={3} key={headerGroup.id}>
            {headerGroup.headers.map((header) => {
              // see https://tanstack.com/table/v8/docs/api/core/column-def#meta to type this correctly
              const meta: any = header.column.columnDef.meta;
              return (
                <Th
                  key={header.id}
                  onClick={header.column.getToggleSortingHandler()}
                  isNumeric={meta?.isNumeric}
                  align="left"
                  pl={2}
                  py={3}
                >
                  {flexRender(
                    header.column.columnDef.header,
                    header.getContext()
                  )}

                  <chakra.span pl="4">
                    {header.column.getIsSorted() ? (
                      header.column.getIsSorted() === "desc" ? (
                        <TriangleDownIcon aria-label="sorted descending" />
                      ) : (
                        <TriangleUpIcon aria-label="sorted ascending" />
                      )
                    ) : null}
                  </chakra.span>
                </Th>
              );
            })}
          </Tr>
        ))}
      </Thead>
      <Tbody bgColor={"blackAlpha.500"}>
        {table.getRowModel().rows.map((row) => (
          <Tr key={row.id} py={3}>
            {row.getVisibleCells().map((cell) => {
              // see https://tanstack.com/table/v8/docs/api/core/column-def#meta to type this correctly
              const meta: any = cell.column.columnDef.meta;
              return (
                <Td pl={2} py={3} key={cell.id} isNumeric={meta?.isNumeric}>
                  {meta?.isTag ? (
                    <Stack direction={"row"}>
                      {Array.isArray(cell?.getValue())
                        ? cell?.getValue()?.map((tag) => (
                            <Box
                              key={tag}
                              mx={1}
                              px={1}
                              borderRadius={2}
                              color={"white"}
                              fontWeight={"bold"}
                              bgcolor={getColor(tag)}
                            >
                              {tag}
                            </Box>
                          ))
                        : flexRender(
                            cell.column.columnDef.cell,
                            cell.getContext()
                          )}
                    </Stack>
                  ) : (
                    flexRender(cell.column.columnDef.cell, cell.getContext())
                  )}
                </Td>
              );
            })}
          </Tr>
        ))}
      </Tbody>
    </Table>
  );
}
const getColor = (value: string) => {
  switch (value) {
    case "admin":
      return "#ff5500";
    case "doctor":
      return "#2db7f5";
    case "patient":
      return "#87d068";
    default:
      return "blue";
  }
};
