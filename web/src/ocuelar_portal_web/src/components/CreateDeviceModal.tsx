import {
  Box,
  Button,
  HStack,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
  VStack,
  useToast,
} from "@chakra-ui/react";
import {
  FormControl,
  FormHelperText,
  InputLabel,
  OutlinedInput,
  Typography,
  useTheme,
} from "@mui/material";
import React, { useState } from "react";
import Select from "react-select";
import {
  Controller,
  FormProvider,
  useController,
  useForm,
} from "react-hook-form";
import axios from "axios";
import ControlledInput from "./ControlledInput";
import { AddIcon } from "@chakra-ui/icons";
import ControlledDeviceInput from "./ControlledDeviceInput";
import { devices, users } from "@prisma/client";
const options = [
  { value: "admin", label: "Admin" },
  { value: "doctor", label: "Doctor" },
  { value: "patient", label: "Patient" },
];
interface CreateDeviceModalProps {
  isOpen: boolean;
  onClose: () => void;
  reloadDevices: () => void;
  mode: "create" | "edit";
  defaultValue?: devices;
}
const CreateDeviceModal = ({
  isOpen,
  onClose,
  reloadDevices,
  mode,
  defaultValue,
  users,
}: CreateDeviceModalProps) => {
  const initialRef = React.useRef(null);
  const finalRef = React.useRef(null);
  const theme = useTheme();
  const methods = useForm({
    defaultValues: defaultValue ?? {},
  });
  const toast = useToast();
  const onSubmit = async (data) => {
    try {
      await axios.post(`/api/devices/${mode}`, data);
      toast({
        title: `${mode} device successfully`,
        status: "success",
        duration: 3000,
        isClosable: true,
        position: "top",
      });
      reloadDevices();
      onClose();
    } catch (error) {
      console.log(error);
      toast({
        title: `failed to ${mode} device`,
        description: error?.response?.data?.message ?? "unknown error",
        status: "error",
        duration: 3000,
        isClosable: true,
        position: "top",
      });
    }
  };
  return (
    <FormProvider {...methods}>
      <Modal
        initialFocusRef={initialRef}
        finalFocusRef={finalRef}
        isOpen={isOpen}
        onClose={onClose}
      >
        <ModalOverlay bgColor={"whiteAlpha.200"} w={"100vw"} h={"100vh"} />
        <ModalContent
          // bgColor={"white"}
          display={"flex"}
          alignSelf={"center"}
          justifySelf={"center"}
          borderRadius={5}
          alignItems={"center"}
        >
          <Box
            bgColor={"white"}
            minW={300}
            w={"60vw"}
            h={"max-content"}
            borderRadius={5}
            color={"black"}
            p={3}
          >
            <ModalHeader fontSize={24} fontWeight={"bold"} textAlign={"center"}>
              Create New Device
            </ModalHeader>
            <ModalBody pb={6} display={"flex"} flexDirection={"column"}>
              <ControlledInput
                disabled={mode === "edit"}
                title={"Device Id"}
                name={"id"}
              />
              <ControlledInput name={"label"} />
              <Stack direction={"row"} mt={20}>
                <Box
                  onClick={methods.handleSubmit(onSubmit)}
                  fontWeight={"bold"}
                  fontSize={20}
                  cursor={"pointer"}
                  bgColor={"teal.400"}
                  flex={1}
                  display={"flex"}
                  borderRadius={6}
                  justifyContent={"center"}
                  alignItems={"center"}
                  py={3}
                  color={"white"}
                >
                  {mode === "edit" ? "Edit" : "Create"}
                </Box>
                <Box
                  onClick={onClose}
                  fontWeight={"bold"}
                  fontSize={20}
                  cursor={"pointer"}
                  bgColor={"red.400"}
                  flex={1}
                  display={"flex"}
                  borderRadius={6}
                  justifyContent={"center"}
                  alignItems={"center"}
                  py={3}
                  color={"white"}
                >
                  Cancel
                </Box>
              </Stack>
            </ModalBody>
          </Box>
        </ModalContent>
      </Modal>
    </FormProvider>
  );
};
const ControlledSelect = ({ name, title, options }) => {
  const [value, setValue] = useState(null);
  const { field } = useController({
    name: name,
  });
  React.useEffect(() => {
    field.onChange(value?.value ?? null);
  }, [field, value]);
  return (
    <>
      <Typography
        fontWeight={"500"}
        mb={1}
        style={{
          textTransform: "capitalize",
        }}
      >
        {title ? title : name}
      </Typography>
      <Select
        value={value}
        onChange={(e) => {
          setValue(e);
        }}
        className="w-full"
        menuPlacement="auto"
        menuPosition="fixed"
        options={options}
      />
    </>
  );
};
export default CreateDeviceModal;
