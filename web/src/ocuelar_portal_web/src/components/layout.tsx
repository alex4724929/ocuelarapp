import { ReactNode } from "react";
import { Stack } from "@mui/material";
import { useRouter } from "next/router";
import { Layout, Menu } from "antd";
import {
  AppstoreOutlined,
  MailOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import type { MenuProps } from "antd";
const { Header, Content, Footer, Sider } = Layout;
const getDefaultSelectedKeys = (pathname: string) => {
  switch (pathname) {
    case "/users":
      return ["all_users"];
    case "/devices":
      return ["all_devices"];
    case "/notifications":
      return ["all_notifications"];
    default:
      return [];
  }
};
const getDefaultOpenKeys = (pathname: string) => {
  switch (pathname) {
    case "/users":
      return ["users"];
    case "/devices":
      return ["devices"];
    case "/notifications":
      return ["notifications"];
    default:
      return [];
  }
};
export default function DefaultLayout({ children }: { children: ReactNode }) {
  const { pathname, push } = useRouter();
  if (pathname === "/invite") {
    return <Stack>{children}</Stack>;
  }
  return (
    <Layout style={{ height: "100vh" }}>
      {pathname === "/login" ? null : (
        <Sider
          breakpoint="lg"
          collapsedWidth="0"
          onBreakpoint={(broken) => {
            console.log(broken);
          }}
          onCollapse={(collapsed, type) => {
            console.log(collapsed, type);
          }}
        >
          <div className="demo-logo-vertical" />
          <Menu
            onClick={(e) => {
              push(e.keyPath[1]);
            }}
            mode="inline"
            defaultSelectedKeys={getDefaultSelectedKeys(pathname)}
            defaultOpenKeys={getDefaultOpenKeys(pathname)}
            style={{ height: "100%" }}
            items={items}
          />
        </Sider>
      )}
      <Layout>
        <Content style={{ margin: "12px 8px 0" }}>
          <div
            style={{
              padding: 24,
              minHeight: 360,
            }}
          >
            {children}
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Ocuelar ©{new Date().getFullYear()}
        </Footer>
      </Layout>
    </Layout>
  );
}

type MenuItem = Required<MenuProps>["items"][number];
function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: "group"
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}

const items: MenuProps["items"] = [
  getItem("Users", "users", <AppstoreOutlined />, [
    getItem("All Users", "all_users"),
  ]),

  getItem("Devices", "devices", <AppstoreOutlined />, [
    getItem("All Devices", "all_devices"),
  ]),

  { type: "divider" },

  getItem("Notifications", "notifications", <SettingOutlined />, [
    getItem("All Notifications", "all_notifications"),
  ]),

  // getItem(
  //   "Group",
  //   "grp",
  //   null,
  //   [getItem("Option 13", "13"), getItem("Option 14", "14")],
  //   "group"
  // ),
];
