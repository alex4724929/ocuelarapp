import {
  Box,
  Button,
  HStack,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
  VStack,
  useToast,
} from "@chakra-ui/react";
import {
  FormControl,
  FormHelperText,
  InputLabel,
  OutlinedInput,
  Typography,
  useTheme,
} from "@mui/material";
import React from "react";
import Select from "react-select";
import { Controller, FormProvider, useForm } from "react-hook-form";
import axios from "axios";
import ControlledInput from "./ControlledInput";
import { AddIcon } from "@chakra-ui/icons";
import ControlledDeviceInput from "./ControlledDeviceInput";
import { devices } from "@prisma/client";
const options = [
  { value: "admin", label: "admin" },
  { value: "doctor", label: "doctor" },
  { value: "patient", label: "patient" },
];
interface CreatePatientModalProps {
  isOpen: boolean;
  onClose: () => void;
  reloadUsers: () => void;
  mode: "create" | "edit";
}
const CreatePatientModal = ({
  isOpen,
  onClose,
  reloadUsers,
  mode,
  defaultValue = {},
  methods,
}) => {
  const initialRef = React.useRef(null);
  const finalRef = React.useRef(null);
  const theme = useTheme();
  // const methods = useForm({
  //   defaultValues: {
  //     ...defaultValue,
  //     roles: defaultValue?.roles?.map((role) => ({ label: role, value: role })),
  //     devices: defaultValue?.devices?.map((device) => ({
  //       label: `${device.display_udid} (${device.label})`,
  //       value: device.id,
  //     })),
  //   },
  // });
  const toast = useToast();
  // React.useEffect(() => {
  //   methods.reset({
  //     ...defaultValue,
  //     roles: defaultValue?.roles?.map((role) => ({ label: role, value: role })),
  //     devices: defaultValue?.devices?.map((device) => ({
  //       label: `${device.display_udid} (${device.label})`,
  //       value: device.id,
  //     })),
  //   });
  // }, [defaultValue, methods]);
  const [availableDevices, setAvailableDevices] = React.useState<devices[]>([]);
  React.useEffect(() => {
    (async () => {
      if (!isOpen) {
        methods.reset();
      }
      if (isOpen) {
        try {
          const { data } = await axios.get("/api/device-active");
          setAvailableDevices(data);
        } catch (error) {}
      }
    })();
  }, [isOpen, methods]);
  const onSubmit = async (data) => {
    try {
      await axios.post(
        `/api/users/${mode !== "edit" ? "create-patient" : "update-patient"}`,
        {
          ...data,
        }
      );
      await reloadUsers();
      toast({
        title: `${
          mode === "edit" ? "Edit" : "Create"
        } admin account successfully`,
        status: "success",
        duration: 3000,
        isClosable: true,
        position: "top",
      });

      onClose();
    } catch (error) {
      toast({
        title: `failed to ${mode} admin account`,
        description: error?.response?.data?.message ?? "unknown error",
        status: "error",
        duration: 3000,
        isClosable: true,
        position: "top",
      });
    }
  };
  return (
    <FormProvider {...methods}>
      <Modal
        initialFocusRef={initialRef}
        finalFocusRef={finalRef}
        isOpen={isOpen}
        onClose={onClose}
      >
        <ModalOverlay bgColor={"whiteAlpha.200"} w={"100vw"} h={"100vh"} />
        <ModalContent
          // bgColor={"white"}
          display={"flex"}
          alignSelf={"center"}
          justifySelf={"center"}
          borderRadius={5}
          alignItems={"center"}
        >
          <Box
            bgColor={"white"}
            minW={300}
            w={"80vw"}
            h={"max-content"}
            borderRadius={5}
            color={"black"}
            p={3}
          >
            <ModalHeader
              fontSize={24}
              fontWeight={"bold"}
              textAlign={"center"}
              mb={5}
            >
              {mode === "edit" ? "Edit" : "Create"} Account
            </ModalHeader>
            <ModalBody pb={6} display={"flex"} flexDirection={"column"}>
              <HStack w={"full"} mb={2}>
                <ControlledInput
                  disabled={mode === "edit"}
                  name={"user_name"}
                />
                <ControlledInput name={"password"} />
              </HStack>
              <HStack w={"full"} mb={2}>
                <ControlledInput name={"first_name"} />
                <ControlledInput name={"last_name"} />
              </HStack>
              <HStack>
                <ControlledInput name={"email"} />
                <ControlledInput name={"phone"} />
              </HStack>

              <ControlledDeviceInput availableDevices={availableDevices} />

              <Typography fontWeight={"500"} mb={1} mt={2}>
                Roles
              </Typography>
              <Controller
                name="roles"
                defaultValue={[{ label: "Admin", value: "admin" }]}
                render={({ field }) => {
                  return (
                    <Select
                      value={field.value}
                      onChange={(value) => {
                        const admin = value.findIndex(
                          (item) => item.value === "admin"
                        );
                        if (admin !== -1) {
                          field.onChange(value);
                        }
                      }}
                      isMulti
                      options={options}
                    />
                  );
                }}
              />

              <Stack direction={"row"} mt={20}>
                <Box
                  onClick={methods.handleSubmit(onSubmit)}
                  fontWeight={"bold"}
                  fontSize={20}
                  cursor={"pointer"}
                  bgColor={"teal.400"}
                  flex={1}
                  display={"flex"}
                  borderRadius={6}
                  justifyContent={"center"}
                  alignItems={"center"}
                  py={3}
                  color={"white"}
                >
                  {mode === "edit" ? "Edit" : "Create"}
                </Box>
                <Box
                  onClick={onClose}
                  fontWeight={"bold"}
                  fontSize={20}
                  cursor={"pointer"}
                  bgColor={"red.400"}
                  flex={1}
                  display={"flex"}
                  borderRadius={6}
                  justifyContent={"center"}
                  alignItems={"center"}
                  py={3}
                  color={"white"}
                >
                  Cancel
                </Box>
              </Stack>
            </ModalBody>
          </Box>
        </ModalContent>
      </Modal>
    </FormProvider>
  );
};
export default CreatePatientModal;
