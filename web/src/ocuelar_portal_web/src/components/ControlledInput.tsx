import { Input, VStack } from "@chakra-ui/react";
import { Typography } from "@mui/material";
import { Controller, useController } from "react-hook-form";

const ControlledInput = ({
  name,
  title,
  disabled,
}: {
  name: string;
  title?: string;
  disabled?: boolean;
}) => {
  const { field } = useController({
    name,
  });
  return (
    <VStack alignItems={"start"} w={"100%"} mb={2}>
      <Typography
        fontWeight={"500"}
        style={{
          textTransform: "capitalize",
        }}
      >
        {title ? title : name}
      </Typography>
      <Input
        disabled={disabled}
        w={"100%"}
        h={"40px"}
        borderWidth={1}
        borderRadius={6}
        placeholder={name}
        pl={3}
        value={field.value}
        onChange={field.onChange}
        onBlur={field.onBlur}
      />
    </VStack>
  );
};
export default ControlledInput;
