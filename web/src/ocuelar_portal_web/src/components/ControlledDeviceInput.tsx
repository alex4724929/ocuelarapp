import { AddIcon, DeleteIcon } from "@chakra-ui/icons";
import { Box, HStack, Input, VStack } from "@chakra-ui/react";
import { Typography } from "@mui/material";
import { devices } from "@prisma/client";
import { useController, useFieldArray, useWatch } from "react-hook-form";
import Select from "react-select";
import ControlledInput from "./ControlledInput";
import React, { HTMLInputTypeAttribute } from "react";
const ControlledDeviceInput = ({
  availableDevices = [],
}: {
  availableDevices: devices[];
}) => {
  const { fields, append, prepend, remove, swap, move, insert } = useFieldArray(
    {
      name: "devices", // unique name for your Field Array
    }
  );
  console.log(fields);
  return (
    <>
      <HStack align={"center"} mt={3} w={"full"}>
        <Typography fontWeight={"700"} fontSize={24}>
          Devices
        </Typography>
        <Box
          onClick={() => {
            append({ id: null });
          }}
          cursor={"pointer"}
          width={5}
          bgColor={"teal.400"}
          justifyContent={"center"}
          alignItems={"center"}
          height={5}
          borderRadius={3}
          display={"flex"}
        >
          <AddIcon w={3} h={3} color={"white"} />
        </Box>
      </HStack>
      <HStack w={"full"}>
        <VStack width={"full"} align={"start"} textColor={"black"}>
          {fields.map((field, index) => {
            return (
              <VStack w={"full"} key={field.id}>
                <ControlledSelect
                  onRemove={() => remove(index)}
                  availableDevices={availableDevices}
                  index={index}
                />
              </VStack>
            );
          })}
        </VStack>
      </HStack>
    </>
  );
};
const ControlledSelect = ({ index, availableDevices, onRemove }) => {
  const { field } = useController({
    name: `devices.${index}`,
  });

  return (
    <HStack
      w={"full"}
      alignItems={"center"}
      justify={"center"}
      align={"center"}
    >
      <VStack flex={2} w={"full"} alignItems={"start"} mb={2}>
        <Typography
          fontWeight={"500"}
          style={{
            textTransform: "capitalize",
          }}
        >
          Device Name
        </Typography>
        <Select
          value={field.value}
          className="w-full "
          menuPlacement="auto"
          menuPosition="fixed"
          onChange={(e) => {
            field.onChange(e);
          }}
          options={availableDevices.map((item) => ({
            value: item.id,
            label: `${item.display_udid} (${item.label})`,
          }))}
        />
      </VStack>

      <DefaultInput
        type="date"
        name={`devices.${index}.start_date`}
        title={"Start Date"}
      />
      <DefaultInput
        type="date"
        name={`devices.${index}.end_date`}
        title={"End Date"}
      />
      <DefaultInput
        type="number"
        name={`devices.${index}.times`}
        title={"Times"}
      />
      <Box
        onClick={onRemove}
        cursor={"pointer"}
        width={10}
        bgColor={"red.400"}
        justifyContent={"center"}
        alignItems={"center"}
        height={10}
        borderRadius={3}
        display={"flex"}
        mt={4}
      >
        <DeleteIcon w={3} h={3} color={"white"} />
      </Box>
    </HStack>
  );
};
const DefaultInput = ({
  name,
  title,
  disabled,
  type,
}: {
  name: string;
  title?: string;
  disabled?: boolean;
  type: HTMLInputTypeAttribute;
}) => {
  const { field } = useController({
    name,
  });
  console.log(field.value);
  return (
    <VStack alignItems={"start"} mb={2} flex={1}>
      <Typography
        fontWeight={"500"}
        style={{
          textTransform: "capitalize",
        }}
      >
        {title ? title : name}
      </Typography>
      <Input
        type={type}
        disabled={disabled}
        w={"100%"}
        h={"40px"}
        borderWidth={1}
        borderRadius={6}
        placeholder={title ? title : name}
        pl={3}
        value={field.value}
        onChange={field.onChange}
        onBlur={field.onBlur}
      />
    </VStack>
  );
};
export default ControlledDeviceInput;
