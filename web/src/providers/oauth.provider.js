import axios from "axios"

axios.defaults.baseURL = import.meta.env.VITE_SERVER_URL

export const login = (credentials) => {
    const data = {
        grant_type: "password",
        scope: "doctor",
        client_id: import.meta.env.VITE_OAUTH_ID,
        client_secret: import.meta.env.VITE_OAUTH_SECRET,
        username: credentials.username,
        password: credentials.password,
    }
    return axios.post("/api/v1/oauth/token", data)
}

export const logout = (credentials) => {
    const data = {
        token: credentials.token,
        client_id: import.meta.env.VITE_OAUTH_ID,
        client_secret: import.meta.env.VITE_OAUTH_SECRET
    }
    return axios.post("/api/v1/oauth/revoke", data)

}

export const refresh = (credentials) => {
    const data = {
        refresh_token: credentials.refresh_token,
        grant_type: "refresh_token",
        client_id: import.meta.env.VITE_OAUTH_ID,
        client_secret: import.meta.env.VITE_OAUTH_SECRET
    }
    return axios.post("/api/v1/oauth/token", data)
}
