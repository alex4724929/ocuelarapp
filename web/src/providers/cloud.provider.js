import {axiosInstance} from "../utils/axios.util.js"

export const patient = {
    create: (data) => axiosInstance.post("/api/v1/doctor/patient", data),
    setOne: (id, data) => axiosInstance.patch(`/api/v1/doctor/patient/${id}`, data),
    getOne: (id) => axiosInstance.get(`/api/v1/doctor/patient/${id}`),
    getAll: (filter) => axiosInstance.get("/api/v1/doctor/patient", {params: filter}),
}

export const doctor = {
    create: (data) => axiosInstance.post("/api/v1/doctor/doctor", data),
    setOne: (id, data) => axiosInstance.patch(`/api/v1/doctor/doctor/${id}`, data),
    getAll: (filter) => axiosInstance.get("/api/v1/doctor/doctor", {params: filter}),
    getOne: (id) => axiosInstance.get(`/api/v1/doctor/doctor/${id}`),
}

export const admin = {
    create: (data) => axiosInstance.post("/api/v1/doctor/admin", data),
    setOne: (id, data) => axiosInstance.patch(`/api/v1/doctor/admin/${id}`, data),
    getAll: (filter) => axiosInstance.get("/api/v1/doctor/admin", {params: filter}),
    getOne: (id) => axiosInstance.get(`/api/v1/doctor/admin/${id}`),
}

export const prescription = {
    create: (data) => axiosInstance.post("/api/v1/doctor/prescription", data),
    setOne: (id, data) => axiosInstance.patch(`/api/v1/doctor/prescription/${id}`, data),
    getAll: (filter) => axiosInstance.get("/api/v1/doctor/prescription", {params: filter}),
    getOne: (id) => axiosInstance.get(`/api/v1/doctor/prescription/${id}`),
}

export const ocuelar = {
    create: (data) => axiosInstance.post("/api/v1/doctor/ocuelar", data),
    setOne: (id, data) => axiosInstance.patch(`/api/v1/doctor/ocuelar/${id}`, data),
    getAll: (filter) => axiosInstance.get("/api/v1/doctor/ocuelar", {params: filter}),
    getOneDrops: (id) => axiosInstance.get(`/api/v1/doctor/ocuelar/${id}?type=drops`),
    getOneOnline: (id) => axiosInstance.get(`/api/v1/doctor/ocuelar/${id}?type=online`),
}

export const message = {
    getPatients: () => axiosInstance.get("/api/v1/doctor/message"),
    getMessages: (id, messageBeforeId) => axiosInstance.get(`/api/v1/doctor/message/${id}?message_before_id=${messageBeforeId}`),
    sendMessage: (data) => axiosInstance.post("/api/v1/doctor/message", data),
    getLastMessage: (id) => axiosInstance.get(`/api/v1/doctor/message/${id}/last`),
}

export const drop = {
    getNames: () => axiosInstance.get("/api/v1/doctor/drop"),
    getInfo: (id) => axiosInstance.get(`/api/v1/doctor/drop/${id}/info`),
    setInfo: (id, data) => axiosInstance.post(`/api/v1/doctor/drop/${id}/info`, data),
    getPrescriptions: (id) => axiosInstance.get(`/api/v1/doctor/drop/${id}/prescription`),
    getPrescription: (id, prescriptionId) => axiosInstance.get(`/api/v1/doctor/drop/${id}/prescription/${prescriptionId}`),
    setPrescription: (id, prescriptionId, data) => axiosInstance.patch(`/api/v1/doctor/drop/${id}/prescription/${prescriptionId}`, data),
}