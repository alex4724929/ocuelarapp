import React, {memo, useEffect, useRef} from "react"
import "./index.css"

import Logo from "../../assets/images/logo.png"
import {login} from "../../providers/oauth.provider"
import useToken from "../../hooks/useToken.js"

function Login() {
    const {token, setToken} = useToken()

    const refUsername = useRef()
    const refPassword = useRef()

    const [errorMessage, setErrorMessage] = React.useState("")

    useEffect(() => {
        if (token) document.location.replace("/app/patient")
    }, [token])

    function formSubmit(event) {
        event.preventDefault()

        const username = refUsername.current["value"]
        const password = refPassword.current["value"]

        login({username, password}).then(response => {
            setToken(response.data)
            document.location.replace("/app/patient")
        }).catch(error => {
            if (error.response.data.error === "invalid_grant") setErrorMessage("Invalid username or password")
            else console.error(error)
        })
    }

    return (
        <form className={"background min-h-screen flex items-center"} onSubmit={formSubmit}>
            <div className={"card max-w-xl w-full mx-auto shadow py-12 px-10 bg-white"}>
                <div className={"flex gap-2 items-center"}>
                    <img src={Logo} alt={"Logo"} width={40}/>
                    <div className={"text-2xl font-bold text-slate-600"}>Ocuelar</div>
                </div>
                <div className={"text-3xl font-bold my-4"}>Sign in</div>
                <div className={"flex flex-col gap-4"}>
                    <input ref={refUsername} type="text" placeholder="Username" className="input input-bordered w-full"
                           required/>
                    <input ref={refPassword} type="password" placeholder="Password"
                           className="input input-bordered w-full"
                           required/>
                </div>
                <div className={"flex justify-between"}>
                    <div className={"text-center text-error"}>{errorMessage}</div>
                </div>
                <div className={"flex justify-end mt-4"}>
                    <button type={"submit"} className={"btn btn-primary w-32 text-white"}>Login</button>
                </div>
            </div>
        </form>
    )
}

export default memo(Login)
