import React, {memo, useEffect, useState} from "react"
import {MdDelete, MdSave} from "react-icons/md"
import {drop} from "../../../providers/cloud.provider.js"
import {useSearchParams} from "react-router-dom"
import {useTranslation} from "react-i18next"
import Alert from "../../../components/Alert.jsx"
import useAlert, {AlertStyle} from "../../../hooks/useAlert.js"

const Page = () => {
    let [searchParams, setSearchParams] = useSearchParams()
    const {t} = useTranslation()
    const [isOpenAlert, styleAlert, messageAlert, showAlert] = useAlert()

    const [drops, setDrops] = useState([])
    const [id, setId] = useState(null)
    const [infos, setInfos] = useState([])

    useEffect(() => {
        // load drops
        drop.getNames().then(response => {
            setDrops(response.data)
            if (searchParams.get("drops") !== null) {
                loadInfo(searchParams.get("drops"))
            } else if (response.data.length > 0) {
                loadInfo(response.data[0].id)
                setSearchParams({drops: response.data[0].id})
            }
        }).catch(error => {
            console.error(error)
        })
    }, [])

    function loadInfo(id) {
        drop.getInfo(id).then(response => {
            setId(response.data.id)
            setInfos(response.data.info)
        }).catch(error => {
            console.error(error)
        })
    }

    function updateInfo(index, key, value) {
        infos.map((info, infoIndex) => {
            if (infoIndex === index) {
                if (key === "order") info[key] = parseInt(value)
                else info[key] = value
            }
        })
        setInfos([...infos])
    }

    function deleteInfo(order) {
        let newInfos = infos.filter(info => info.id !== order)
        newInfos.map((info, index) => info.id = index)
        setInfos([...newInfos])
    }

    function addInfo() {
        setInfos([...infos, {
            id: infos.length + 1,
            order: infos.length + 1,
            info_type: "title",
            content: ""
        }])
    }

    function saveInfo(event) {
        event.preventDefault()

        drop.setInfo(id, {data: infos}).then(response => {
            showAlert(AlertStyle.Success, response.data.message)
        }).catch(error => {
            console.error(error)
        })
    }

    const dropStyle = (dropId) => {
        if (dropId === id) {
            return "w-full bg-gray-100 h-10 flex items-center px-4 cursor-pointer rounded-md"
        } else {
            return "w-full bg-white h-10 flex items-center px-4 cursor-pointer"
        }
    }

    return (
        <div>
            <div className={"flex gap-2 h-screen py-2"}>
                {/* Menu */}
                <div className={"bg-white shadow-md w-full md:w-72 overflow-y-auto rounded-xl p-2"}>
                    {drops.map((drop) => (
                        <div key={drop.id} data-id={drop.id} className={dropStyle(drop.id)}
                             onClick={() => loadInfo(drop.id)}>
                            <div className={"text-md"}>{t(drop.name)}</div>
                        </div>
                    ))}
                </div>

                <div className={"bg-white shadow-xl flex-1 flex flex-col rounded-xl"}>
                    <div className={"flex-1 w-full overflow-y-auto p-2 flex flex-col gap-2"}>
                        {infos?.map((info, index) => (
                            <div key={index} className={"flex items-end gap-2 w-full bg-gray-100 p-2 rounded-md"}>
                                <div className={"flex flex-col w-28 gap-2"}>
                                    <div className={"form-control"}>
                                        <label className={"label-text"}>Order</label>
                                        <select className={"select select-sm select-bordered bg-white w-full"} value={info?.order}
                                                onChange={(e) => updateInfo(index, "order", e.target.value)}>
                                            {Array.from({length: 9}, (_, index) => index + 1).map(i => (
                                                <option key={i} value={i}>{i}</option>
                                            ))}
                                        </select>
                                    </div>
                                    <div className={"form-control"}>
                                        <label className={"label-text"}>Type</label>
                                        <select className={"select select-sm select-bordered bg-white w-full"} value={info?.info_type}
                                                onChange={(e) => updateInfo(index, "info_type", e.target.value)}>
                                            <option value={"title"}>Title</option>
                                            <option value={"text"}>Content</option>
                                        </select>
                                    </div>
                                </div>
                                <textarea className={"textarea textarea-sm textarea-bordered flex-1 bg-white h-full text-base/5"}
                                          value={info?.content} rows={1}
                                          onChange={(e) => updateInfo(index, "content", e.target.value)}/>
                                <button className={"btn btn-sm btn-ghost text-error btn-square"}
                                        onClick={() => deleteInfo(info.id)}><MdDelete size={20}/>
                                </button>
                            </div>
                        ))}
                    </div>

                    <div className={"divider m-0 p-0"}></div>

                    <div className={"flex justify-between p-4"}>
                        <div className={"flex gap-2"}>
                            <button className={"btn btn-sm btn-primary btn-outline w-40 rounded-md"} disabled={infos?.length >= 9}
                                    onClick={addInfo}>Add Content
                            </button>
                        </div>
                        <div>
                            <div className={"btn btn-sm btn-primary w-28 gap-2 rounded-md"} onClick={saveInfo}><MdSave size={20}/>Save
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Alert isOpen={isOpenAlert} style={styleAlert} message={messageAlert}/>
        </div>
    )
}

export default memo(Page)
