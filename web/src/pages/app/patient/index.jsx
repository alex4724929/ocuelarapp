import React, {lazy, memo, useEffect} from "react"
import {patient as provider} from "../../../providers/cloud.provider.js"
import {useTranslation} from "react-i18next"
import moment from "moment"
import useModal from "../../../hooks/useModal.js"

const ModalView = lazy(() => import("./ModalView.jsx"))
const ModalEdit = lazy(() => import("./ModalEdit.jsx"))
const ModalNew = lazy(() => import("./ModalNew.jsx"))

const Page = () => {
    const {t} = useTranslation()
    const [patients, setPatients] = React.useState([])
    const [filter, setFilter] = React.useState({
        af_uid: "",
        ocuelars: [],
        control_group: "",
        post_op_phase: ""
    })

    const [isOpenNew, paramsNew, openNew, closeNew, cancelNew] = useModal(() => loadPatients())
    const [isOpenEdit, paramsEdit, openEdit, closeEdit, cancelEdit] = useModal(() => loadPatients())
    const [isOpenView, paramsView, openView, closeView, cancelView] = useModal(() => loadPatients())

    useEffect(() => {
        loadPatients()
    }, [])

    function updateFilter(key, value) {
        setFilter({...filter, [key]: value})
    }

    function submitFilter(event) {
        event.preventDefault()
        loadPatients()
    }

    function loadPatients() {
        provider.getAll(filter).then(response => {
            setPatients(response.data)
            console.log(response.data)
        }).catch(error => {
            console.error(error)
        })
    }

    return (
        <div className={"flex flex-col gap-4 py-6"}>
            <form className={"bg-white p-4 shadow-md flex flex-col gap-2"} onSubmit={submitFilter}>
                <div className={"grid grid-cols-1 md:grid-cols-4 gap-2"}>
                    <label className="input-group input-group-sm input-group-vertical">
                        <span className={"bg-slate-800 text-slate-100"}>AF UID</span>
                        <input type="text" className="input input-sm bg-slate-100 input-bordered"
                               value={filter.af_uid} onChange={e => updateFilter("af_uid", e.target.value)}/>
                    </label>
                    <label className="input-group input-group-sm input-group-vertical">
                        <span className={"bg-slate-800 text-slate-100"}>Ocuelar ID</span>
                        <input type="text" className="input input-sm bg-slate-100 input-bordered"
                        value={filter.ocuelar_id} onChange={e => updateFilter("ocuelar_id", e.target.value)}/>
                    </label>
                    <label className="input-group input-group-sm input-group-vertical">
                        <span className={"bg-slate-800 text-slate-100"}>Control Group</span>
                        <select className="select select-bordered select-sm"
                        value={filter.control_group} onChange={e => updateFilter("control_group", e.target.value)}>
                            <option value={""}>All</option>
                            <option value="control">Control</option>
                            <option value="smartcap">SmartCap</option>
                            <option value="smartcap_gamification">SmartCap + Gamification</option>
                        </select>
                    </label>
                    <label className="input-group input-group-sm input-group-vertical">
                        <span className={"bg-slate-800 text-slate-100"}>Post Op Phase</span>
                        <select className="select select-bordered select-sm"
                                value={filter.post_op_phase} onChange={e => updateFilter("post_op_phase", e.target.value)}>
                            <option value={""}>All</option>
                            <option value="week_1">Week 1</option>
                            <option value="week_2">Week 2</option>
                            <option value="week_3">Week 3</option>
                            <option value="week_4">Week 4</option>
                            <option value="week_5">Week 5</option>
                            <option value="week_6">Week 6</option>
                            <option value="week_7">Week 7</option>
                            <option value="week_8">Week 8</option>
                            <option value="week_9">Week 9</option>
                        </select>
                    </label>
                </div>
                <div className={"w-full flex justify-between"}>
                    <button type={"button"} className={"btn btn-sm btn-primary btn-outline w-32"} onClick={openNew}>Add Patient</button>
                    <button type={"submit"} className={"btn btn-sm btn-primary w-32"}>Search</button>
                </div>
            </form>
            <div className={"bg-white p-4 shadow-md"}>
                <div className={"overflow-x-auto"}>
                    <table className={"table table-compact w-full"}>
                        <thead>
                        <tr>
                            <th className={"text-left"}>AF UID</th>
                            <th className={"text-left"}>Ocuelar ID 1</th>
                            <th className={"text-left"}>Ocuelar ID 2</th>
                            <th className={"text-left"}>Ocuelar ID 3</th>
                            <th className={"text-left"}>Control Group</th>
                            <th className={"text-left"}>Post Op Phase</th>
                            <th className={"text-left"}>Start Date</th>
                            <th className={"text-left"}>Adherence</th>
                            <th className={"text-left w-20"}>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {patients.map(patient => (
                            <tr key={patient.id}>
                                <td><div className={"cursor-pointer underline text-blue-600"} id={patient.id} onClick={openView}>{patient["af_uid"]}</div></td>
                                <td>{patient?.ocuelars[0]?.ocuelar_id}</td>
                                <td>{patient?.ocuelars[1]?.ocuelar_id}</td>
                                <td>{patient?.ocuelars[2]?.ocuelar_id}</td>
                                <td>{t(patient.control_group)}</td>
                                <td>{t(patient.post_op_phase)}</td>
                                <td>{moment(patient.medication_start_at).format("DD MMM YYYY")}</td>
                                <td>{patient.adherence}</td>
                                <td>
                                    <button id={patient.id} className={"btn btn-xs btn-ghost w-20"} onClick={openEdit}>Edit</button>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
            </div>
            <ModalNew isOpen={isOpenNew} close={closeNew} cancel={cancelNew} params={paramsNew}/>
            <ModalEdit isOpen={isOpenEdit} close={closeEdit} cancel={cancelEdit} params={paramsEdit}/>
            <ModalView isOpen={isOpenView} close={closeView} cancel={cancelView} params={paramsView}/>
        </div>
    )
}

export default memo(Page)
