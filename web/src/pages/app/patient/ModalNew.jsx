import React, {memo, useEffect} from "react"
import moment from "moment"
import {patient as provider} from "../../../providers/cloud.provider"
import Alert from "../../../components/Alert"
import useAlert, {AlertStyle} from "../../../hooks/useAlert"
import patientProfile from "../../../assets/images/profile.png"

const defaultPatient = {
    af_uid: "",
    ocuelars: [
        {ocuelar_id: "", ocuelar_type: "flourometholone"},
        {ocuelar_id: "", ocuelar_type: "vigamox"},
        {ocuelar_id: "", ocuelar_type: "ketorolac"}
    ],
    control_group: "control",
    medication_start_at: moment().format("YYYY-MM-DD"),
    username: "",
    password: "",
    password_confirmation: "",
}

const Modal = (props) => {
    const {isOpen, close, cancel} = props
    const [isOpenAlert, styleAlert, messageAlert, showAlert] = useAlert()
    const [patient, setPatient] = React.useState(defaultPatient)

    useEffect(() => {
        if (isOpen) {
            setPatient(defaultPatient)
        }
    }, [isOpen])

    function submitForm(event) {
        event.preventDefault()

        console.log(patient)
        provider.create(patient).then(() => {
            showAlert(AlertStyle.Success, "Success")
            close()
        }).catch((error) => {
            console.error(error)
            showAlert(AlertStyle.Error, `Failed: ${error.response.data.error}`)
            close()
        })
    }

    function updatePatient(key, value) {
        setPatient({...patient, [key]: value})
    }

    function updateOcuelar(index, key, value) {
        setPatient({...patient, ocuelars: [...patient.ocuelars.slice(0, index), {...patient.ocuelars[index], [key]: value}, ...patient.ocuelars.slice(index + 1)]})
    }

    return (
        <div>
            <div className={isOpen ? "modal modal-open" : "modal"}>
                <form className={"modal-box bg-gray-100 max-w-xl md:ml-60"} onSubmit={submitForm} autoComplete={"off"}>
                    <h3 className="font-bold text-lg">New Patient</h3>

                    <div className={"mt-2 flex flex-col gap-2 items-center"}>
                        <img className={"w-20"} src={patientProfile} alt={"profile"}/>

                        <div className="form-control w-full">
                            <span className="label-text font-semibold">Username</span>
                            <input type="text" required className="input input-bordered input-sm w-full"
                                   value={patient.username || ""}
                                   onChange={e => updatePatient("username", e.target.value)}/>
                        </div>
                        <div className="form-control w-full">
                            <span className="label-text font-semibold">Password</span>
                            <input type="password" required className="input input-bordered input-sm w-full"
                                   value={patient.password || ""} pattern=".{6,}"
                                   onChange={e => updatePatient("password", e.target.value)}/>
                        </div>
                        <div className="form-control w-full">
                            <span className="label-text font-semibold">Password Confirmation</span>
                            <input type="password" required className="input input-bordered input-sm w-full"
                                   value={patient.password_confirmation || ""} pattern=".{6,}"
                                   onChange={e => updatePatient("password_confirmation", e.target.value)}/>
                        </div>
                        <div className={"divider m-0"}></div>
                        <div className="form-control w-full">
                            <span className="label-text font-semibold">AF UID</span>
                            <input type="text" required className="input input-bordered input-sm w-full"
                                   value={patient.af_uid || ""}
                                   onChange={e => updatePatient("af_uid", e.target.value)}/>
                        </div>
                        <div className="form-control w-full">
                            <span className="label-text font-semibold">Ocuelar ID</span>
                            <div className={"w-full flex"}>
                                <input type="text" className="input input-bordered input-sm flex-1"
                                       placeholder={"Ocuelar-xxxxxx"}
                                       value={patient.ocuelars[0].ocuelar_id || ""}
                                       onChange={e => updateOcuelar(0, "ocuelar_id", e.target.value)}/>
                                <select className={"select select-bordered select-sm flex-1"}
                                        value={patient.ocuelars[0].ocuelar_type}
                                        onChange={e => updateOcuelar(0, "ocuelar_type", e.target.value)}>
                                    <option value={"flourometholone"}>Flourometholone</option>
                                    <option value={"vigamox"}>Vigamox</option>
                                    <option value={"ketorolac"}>Ketorolac</option>
                                </select>
                            </div>
                            <div className={"w-full flex mt-2"}>
                                <input type="text" className="input input-bordered input-sm flex-1"
                                       placeholder={"Ocuelar-xxxxxx"}
                                       value={patient.ocuelars[1].ocuelar_id || ""}
                                       onChange={e => updateOcuelar(1, "ocuelar_id", e.target.value)}/>
                                <select className={"select select-bordered select-sm flex-1"}
                                        value={patient.ocuelars[1].ocuelar_type}
                                        onChange={e => updateOcuelar(1, "ocuelar_type", e.target.value)}>
                                    <option value={"flourometholone"}>Flourometholone</option>
                                    <option value={"vigamox"}>Vigamox</option>
                                    <option value={"ketorolac"}>Ketorolac</option>
                                </select>
                            </div>
                            <div className={"w-full flex mt-2"}>
                                <input type="text" className="input input-bordered input-sm flex-1"
                                       placeholder={"Ocuelar-xxxxxx"}
                                       value={patient.ocuelars[2].ocuelar_id || ""}
                                       onChange={e => updateOcuelar(2, "ocuelar_id", e.target.value)}/>
                                <select className={"select select-bordered select-sm flex-1"}
                                        value={patient.ocuelars[2].ocuelar_type}
                                        onChange={e => updateOcuelar(2, "ocuelar_type", e.target.value)}>
                                    <option value={"flourometholone"}>Flourometholone</option>
                                    <option value={"vigamox"}>Vigamox</option>
                                    <option value={"ketorolac"}>Ketorolac</option>
                                </select>
                            </div>
                        </div>
                        <div className="form-control w-full">
                            <span className="label-text font-semibold">Control Group</span>
                            <select required className="select select-bordered select-sm w-full"
                                    value={patient.control_group || ""}
                                    onChange={e => updatePatient("control_group", e.target.value)}>
                                <option value="control">Control</option>
                                <option value="smartcap">SmartCap</option>
                                <option value="smartcap_gamification">SmartCap + Gamification</option>
                            </select>
                        </div>
                        <div className="form-control w-full">
                            <span className="label-text font-semibold">Start Date</span>
                            <input type="date" required className="input input-bordered input-sm w-full"
                                   value={moment(patient.medication_start_at).format("YYYY-MM-DD")}
                                   onChange={e => updatePatient("medication_start_at", e.target.value)}/>
                        </div>
                    </div>
                    <div className={"modal-action justify-between"}>
                        <button type={"button"} className={"btn btn-sm w-20 btn-ghost text-error"} onClick={cancel}>Cancel</button>
                        <button type={"submit"} className={"btn btn-sm w-20 btn-primary"}>Add</button>
                    </div>
                </form>
            </div>
            <Alert isOpen={isOpenAlert} style={styleAlert} message={messageAlert}/>
        </div>
    )
}

export default memo(Modal)
