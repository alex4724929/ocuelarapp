import React, {memo, useEffect, useState} from "react"
import {MdCancel} from "react-icons/md"
import {patient as provider} from "../../../providers/cloud.provider.js"

import patientProfile from "../../../assets/images/profile.png"
import {useTranslation} from "react-i18next"
import moment from "moment"

const Modal = (props) => {
    const {t} = useTranslation()

    const {isOpen, params, cancel} = props

    const [patient, setPatient] = useState({
        af_uid: "",
        ocuelars: [
            {ocuelar_id: "", ocuelar_type: ""},
            {ocuelar_id: "", ocuelar_type: ""},
            {ocuelar_id: "", ocuelar_type: ""}
        ],
        control_group: "",
        post_op_phase: "",
        medication_start_at: "",
        adherence: ""
    })

    useEffect(() => {
        if (isOpen) {
            provider.getOne(params.target.id).then(response => {
                setPatient(response.data)
            }).catch(error => {
                console.error(error)
            })
        }
    }, [isOpen])

    return (
        <div className={isOpen ? "modal modal-open" : "modal"}>
            <div className={"modal-box bg-base-100 max-w-xl md:ml-60"}>
                <div className={"flex justify-between w-full"}>
                    <h3 className="font-bold text-lg">Patient Profile</h3>
                    <button className={"btn btn-sm btn-circle btn-ghost"} onClick={cancel}><MdCancel size={24}/>
                    </button>
                </div>

                <div className={"mt-2 flex flex-col gap-2 items-center"}>
                    <img className={"w-20"} src={patientProfile} alt={"profile"}/>
                    <div className="form-control w-full">
                        <span className="label-text font-semibold">AF UID</span>
                        <div className={"items-center flex text-sm px-2 h-8 w-full border-0 outline outline-slate-400"}>
                            {patient.af_uid}
                        </div>
                    </div>
                    <div className="form-control w-full gap-2">
                        <span className="label-text font-semibold">Ocuelar ID</span>
                        <div className={"w-full justify-between border-0 outline flex h-8 outline-slate-400 items-center"}>
                            <div className={"text-sm px-2"}>{patient.ocuelars[0]?.ocuelar_id}</div>
                            <div className={"badge border-0 text-slate-100 font-bold bg-slate-400 w-40 h-8"}>{t(patient.ocuelars[0]?.ocuelar_type)}</div>
                        </div>
                        <div className={"w-full justify-between border-0 outline flex h-8 outline-slate-400 items-center"}>
                            <div className={"text-sm px-2"}>{patient.ocuelars[1]?.ocuelar_id}</div>
                            <div className={"badge border-0 text-slate-100 font-bold bg-slate-400 w-40 h-8"}>{t(patient.ocuelars[1]?.ocuelar_type)}</div>
                        </div>
                        <div className={"w-full justify-between border-0 outline flex h-8 outline-slate-400 items-center"}>
                            <div className={"text-sm px-2"}>{patient.ocuelars[2]?.ocuelar_id}</div>
                            <div className={"badge border-0 text-slate-100 font-bold bg-slate-400 w-40 h-8"}>{t(patient.ocuelars[2]?.ocuelar_type)}</div>
                        </div>
                    </div>
                    <div className="form-control w-full">
                        <span className="label-text font-semibold">Control Group</span>
                        <div className={"items-center flex text-sm px-2 h-8 w-full border-0 outline outline-slate-400"}>
                            {t(patient.control_group)}
                        </div>
                    </div>
                    <div className="form-control w-full">
                        <span className="label-text font-semibold">Post Op Phase</span>
                        <div className={"items-center flex text-sm px-2 h-8 w-full border-0 outline outline-slate-400"}>
                            {t(patient.post_op_phase)}
                        </div>
                    </div>
                    <div className="form-control w-full">
                        <span className="label-text font-semibold">Start Date</span>
                        <div className={"items-center flex text-sm px-2 h-8 w-full border-0 outline outline-slate-400"}>
                            {moment(patient.medication_start_at).format("DD MMM YYYY")}
                        </div>
                    </div>
                    <div className="form-control w-full">
                        <span className="label-text font-semibold">Adherence</span>
                        <div className={"items-center flex text-sm px-2 h-8 w-full border-0 outline outline-slate-400"}>
                            {patient.adherence}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default memo(Modal)
