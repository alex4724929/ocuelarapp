import React, {memo, useEffect, useState} from "react"
import {patient as provider} from "../../../providers/cloud.provider.js"
import moment from "moment"
import Alert from "../../../components/Alert.jsx"
import useAlert, {AlertStyle} from "../../../hooks/useAlert"

import patientProfile from "../../../assets/images/profile.png"

const Modal = (props) => {
    const {isOpen, close, cancel, params} = props
    const [isOpenAlert, styleAlert, messageAlert, showAlert] = useAlert()
    const [patient, setPatient] = useState({
        af_uid: "",
        ocuelars: [
            {ocuelar_id: "", ocuelar_type: ""},
            {ocuelar_id: "", ocuelar_type: ""},
            {ocuelar_id: "", ocuelar_type: ""}
        ],
        control_group: "",
        post_op_phase: "",
        medication_start_at: "",
        adherence: ""
    })

    useEffect(() => {
        if (isOpen) {
            provider.getOne(params.target.id).then(response => {
                setPatient(response.data)
            }).catch(error => {
                console.error(error)
            })
        }
    }, [isOpen])

    function submitForm(event) {
        event.preventDefault()

        provider.setOne(params.target.id, patient).then(() => {
            showAlert(AlertStyle.Success, "Success")
            close()
        }).catch(error => {
            console.error(error)
            showAlert(AlertStyle.Error, `Failed: ${error.response.data.error}`)
            close()
        })
    }

    function handleArchive(event) {
        event.preventDefault()

        if (confirm("Are you sure to archive this patient?") === false) return

        provider.setOne(params.target.id, {active: false}).then(() => {
            showAlert(AlertStyle.Success, "Success")
            close()
        }).catch(error => {
            console.error(error)
            showAlert(AlertStyle.Error, `Failed: ${error.response.data.error}`)
            close()
        })
    }

    function updatePatient(key, value) {
        setPatient({...patient, [key]: value})
    }

    function updateOcuelar(index, key, value) {
        setPatient({...patient, ocuelars: [...patient.ocuelars.slice(0, index), {...patient.ocuelars[index], [key]: value}, ...patient.ocuelars.slice(index + 1)]})
    }

    return (
        <div>
            <div className={isOpen ? "modal modal-open" : "modal"}>
                <form className={"modal-box bg-gray-100 max-w-xl md:ml-60"} onSubmit={submitForm}>
                    <h3 className="font-bold text-lg">Edit Patient</h3>

                    <div className={"mt-2 flex flex-col gap-2 items-center"}>
                        <img className={"w-20"} src={patientProfile} alt={"profile"}/>
                        <div className="form-control w-full">
                            <span className="label-text font-semibold">AF UID</span>
                            <input type="text" required className="input input-bordered input-sm w-full"
                                   value={patient.af_uid || ""}
                                   onChange={e => updatePatient("af_uid", e.target.value)}/>
                        </div>
                        <div className="form-control w-full">
                            <span className="label-text font-semibold">Ocuelar ID</span>
                            <div className={"w-full flex"}>
                                <input type="text" className="input input-bordered input-sm flex-1"
                                       placeholder={"Ocuelar-xxxxxx"}
                                       value={patient?.ocuelars[0]?.ocuelar_id || ""}
                                       onChange={e => updateOcuelar(0, "ocuelar_id", e.target.value)}/>
                                <select className={"select select-bordered select-sm flex-1"}
                                        value={patient?.ocuelars[0]?.ocuelar_type}
                                        onChange={e => updateOcuelar(0, "ocuelar_type", e.target.value)}>
                                    <option value={"flourometholone"}>Flourometholone</option>
                                    <option value={"vigamox"}>Vigamox</option>
                                    <option value={"ketorolac"}>Ketorolac</option>
                                </select>
                            </div>
                            <div className={"w-full flex mt-2"}>
                                <input type="text" className="input input-bordered input-sm flex-1"
                                       placeholder={"Ocuelar-xxxxxx"}
                                       value={patient?.ocuelars[1]?.ocuelar_id || ""}
                                       onChange={e => updateOcuelar(1, "ocuelar_id", e.target.value)}/>
                                <select className={"select select-bordered select-sm flex-1"}
                                        value={patient?.ocuelars[1]?.ocuelar_type}
                                        onChange={e => updateOcuelar(1, "ocuelar_type", e.target.value)}>
                                    <option value={"flourometholone"}>Flourometholone</option>
                                    <option value={"vigamox"}>Vigamox</option>
                                    <option value={"ketorolac"}>Ketorolac</option>
                                </select>
                            </div>
                            <div className={"w-full flex mt-2"}>
                                <input type="text" className="input input-bordered input-sm flex-1"
                                       placeholder={"Ocuelar-xxxxxx"}
                                       value={patient?.ocuelars[2]?.ocuelar_id || ""}
                                       onChange={e => updateOcuelar(2, "ocuelar_id", e.target.value)}/>
                                <select className={"select select-bordered select-sm flex-1"}
                                        value={patient?.ocuelars[2]?.ocuelar_type}
                                        onChange={e => updateOcuelar(2, "ocuelar_type", e.target.value)}>
                                    <option value={"flourometholone"}>Flourometholone</option>
                                    <option value={"vigamox"}>Vigamox</option>
                                    <option value={"ketorolac"}>Ketorolac</option>
                                </select>
                            </div>
                        </div>
                        <div className="form-control w-full">
                            <span className="label-text font-semibold">Control Group</span>
                            <select required className="select select-bordered select-sm w-full"
                                    value={patient.control_group || ""}
                                    onChange={e => updatePatient("control_group", e.target.value)}>
                                <option value="control">Control</option>
                                <option value="smartcap">SmartCap</option>
                                <option value="smartcap_gamification">SmartCap + Gamification</option>
                            </select>
                        </div>
                        <div className="form-control w-full">
                            <span className="label-text font-semibold">Start Date</span>
                            <input type="date" required className="input input-bordered input-sm w-full"
                                   value={moment(patient.medication_start_at).format("YYYY-MM-DD")}
                                   onChange={e => updatePatient("medication_start_at", e.target.value)}/>
                        </div>
                    </div>

                    <div className={"modal-action justify-between"}>
                        <div>
                            <button type={"button"} className={"btn btn-sm w-20 btn-ghost"}
                                    onClick={cancel}>Cancel
                            </button>
                        </div>
                        <div className={"flex gap-4"}>
                            <button type={"button"} className={"btn btn-sm w-24 btn-ghost text-error"} onClick={handleArchive}>Archive</button>
                            <button type={"submit"} className={"btn btn-sm w-24 btn-primary"}>Save</button>
                        </div>
                    </div>
                </form>
            </div>
            <Alert isOpen={isOpenAlert} style={styleAlert} message={messageAlert}/>
        </div>

    )
}

export default memo(Modal)
