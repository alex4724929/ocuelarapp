import React, {memo, useEffect, useState} from "react"
import {MdCancel} from "react-icons/md"
import {ocuelar as provider} from "../../../providers/cloud.provider.js"

import patientProfile from "../../../assets/images/profile.png"
import {useTranslation} from "react-i18next"
import moment from "moment"

const Modal = (props) => {
    const {t} = useTranslation()

    const {isOpen, params, cancel} = props

    const [ocuelar, setOcuelar] = useState({
        id: "",
        ocuelar_id: "",
        ocuelar_type: "",
        onlines_20: []
    })

    useEffect(() => {
        if (isOpen) {
            provider.getOneOnline(params.target.id).then(response => {
                setOcuelar(response.data)
            }).catch(error => {
                console.error(error)
            })
        }
    }, [isOpen])

    return (
        <div className={isOpen ? "modal modal-open" : "modal"}>
            <div className={"modal-box bg-white max-w-md md:ml-60"}>
                <div className={"flex justify-between w-full"}>
                    <div className="flex gap-2 items-center">
                        <div className={"font-bold text-lg"}>{ocuelar.ocuelar_id}</div>
                        <div className={"badge bg-slate-800 text-slate-100 border-0 rounded-full px-4"}>{t(ocuelar.ocuelar_type)}</div>
                    </div>
                    <button className={"btn btn-sm btn-circle btn-ghost"} onClick={cancel}><MdCancel size={24}/>
                    </button>
                </div>

                <div className={"overflow-x-auto mt-2"}>
                    <table className={"table table-compact w-full"}>
                        <thead>
                        <tr>
                            <td>Online Time</td>
                        </tr>
                        </thead>
                        <tbody>
                        {ocuelar.onlines_20.map((online, index) => (
                            <tr key={index}>
                                <td>{moment(online?.created_at).format("DD MMM YYYY hh:mm:ss A")}</td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default memo(Modal)
