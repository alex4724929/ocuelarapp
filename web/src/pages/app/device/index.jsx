import React, {lazy, memo, useEffect} from "react"
import {ocuelar as provider} from "../../../providers/cloud.provider.js"
import {useTranslation} from "react-i18next"
import moment from "moment"
import useModal from "../../../hooks/useModal.js"

const ModalOnline = lazy(() => import("./ModalOnline.jsx"))
const ModalDrops = lazy(() => import("./ModalDrops.jsx"))

const Page = () => {
    const {t} = useTranslation()
    const [isOpenDrops, paramsDrops, openDrops, closeDrops, cancelDrops] = useModal(() => reload())
    const [isOpenOnline, paramsOnline, openOnline, closeOnline, cancelOnline] = useModal(() => reload())
    const [filter, setFilter] = React.useState({
        af_uid: "",
        ocuelar_id: "",
        ocuelar_type: ""
    })
    const [ocuelars, setOcuelars] = React.useState([])

    useEffect(() => {
        reload()
    }, [])

    function reload() {
        provider.getAll(filter).then(response => {
            setOcuelars(response.data)
        }).catch(error => {
            console.error(error)
        })
    }

    function submitFilter(event) {
        event.preventDefault()
        reload()
    }

    function update(key, value) {
        setFilter({...filter, [key]: value})
    }

    return (
        <div className={"flex flex-col gap-4 py-6"}>
            <form className={"bg-white p-4 shadow-md flex flex-col gap-2"} onSubmit={submitFilter}>
                <div className={"grid grid-cols-1 md:grid-cols-4 gap-2"}>
                    <label className="input-group input-group-sm input-group-vertical">
                        <span className={"bg-slate-800 text-slate-100"}>AF UID</span>
                        <input type="text" className="input input-sm bg-slate-100 input-bordered"
                               value={filter.af_uid} onChange={e => update("af_uid", e.target.value)}/>
                    </label>
                    <label className="input-group input-group-sm input-group-vertical">
                        <span className={"bg-slate-800 text-slate-100"}>Ocuelar ID</span>
                        <input type="text" className="input input-sm bg-slate-100 input-bordered"
                               value={filter.ocuelar_id} onChange={e => update("ocuelar_id", e.target.value)}/>
                    </label>
                    <label className="input-group input-group-sm input-group-vertical">
                        <span className={"bg-slate-800 text-slate-100"}>Ocuelar Type</span>
                        <select className="select select-sm bg-slate-100 select-bordered"
                                value={filter.ocuelar_type} onChange={e => update("ocuelar_type", e.target.value)}>
                            <option value={""}>All</option>
                            <option value={"flourometholone"}>Flourometholone</option>
                            <option value={"vigamox"}>Vigamox</option>
                            <option value={"ketorolac"}>Ketorolac</option>
                        </select>
                    </label>
                </div>
                <div className={"w-full flex justify-end"}>
                    <button type={"submit"} className={"btn btn-sm btn-primary w-32"}>Search</button>
                </div>
            </form>
            <div className={"bg-white p-4 shadow-md"}>
                <div className={"overflow-x-auto"}>
                    <table className={"table table-compact w-full"}>
                        <thead>
                        <tr>
                            <td>AF UID</td>
                            <td>Ocuelar ID</td>
                            <td>Ocuelar Type</td>
                            <td>Last Drops</td>
                            <td>Last Online</td>
                            <td className={"w-40"}>Details</td>
                        </tr>
                        </thead>
                        <tbody>
                        {ocuelars.map((ocuelar) => (
                            <tr key={ocuelar.id}>
                                <td>{ocuelar?.patient?.af_uid}</td>
                                <td>{ocuelar.ocuelar_id}</td>
                                <td>{t(ocuelar?.ocuelar_type)}</td>
                                <td>{ocuelar?.last_drops?.created_at ? moment(ocuelar?.last_drops?.created_at).format("DD MMM YYYY hh:mm:ss A") : ""}</td>
                                <td>{ocuelar?.last_online?.created_at ? moment(ocuelar?.last_online?.created_at).format("DD MMM YYYY hh:mm:ss A") : ""}</td>
                                <td>
                                    <div className={"flex gap-2"}>
                                        <button id={ocuelar.id} className={"btn btn-xs btn-ghost text-slate-900 w-20"} onClick={openDrops}>Drops</button>
                                        <button id={ocuelar.id} className={"btn btn-xs btn-ghost text-primary w-20"} onClick={openOnline}>Online</button>
                                    </div>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
            </div>
            <ModalDrops isOpen={isOpenDrops} close={closeDrops} cancel={cancelDrops} params={paramsDrops}/>
            <ModalOnline isOpen={isOpenOnline} close={closeOnline} cancel={cancelOnline} params={paramsOnline}/>
        </div>
    )
}

export default memo(Page)
