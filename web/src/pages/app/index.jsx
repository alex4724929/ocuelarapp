import React, {memo} from "react"

import LeftDrawer from "../../containers/LeftDrawer.jsx"
import PageContent from "../../containers/PageContent.jsx"

function App() {
    return (
        <div className={"min-h-screen bg-base-200"}>
            <div className={"drawer drawer-mobile"}>
                <input id="drawer-left" type="checkbox" className="drawer-toggle"/>
                <PageContent/>
                <LeftDrawer/>
            </div>
        </div>
    )
}

export default memo(App)
