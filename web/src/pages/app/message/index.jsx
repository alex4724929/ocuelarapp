import React, {memo, useEffect, useRef, useState} from "react"
import {message as provider} from "../../../providers/cloud.provider.js"
import {MdSend} from "react-icons/md"

import patientProfile from "../../../assets/images/profile.png"
import doctorProfile from "../../../assets/images/profile.png"
import {useSearchParams} from "react-router-dom"

const Page = () => {
    let [searchParams, setSearchParams] = useSearchParams();
    const [patients, setPatients] = useState([])
    const [messages, setMessages] = useState([])

    const [firstMessageId, setFirstMessageId] = useState(0)
    const [lastMessageId, setLastMessageId] = useState(0)
    const [prevMessagesHeight, setPrevMessagesHeight] = useState(0)
    const [patientId, setPatientId] = useState("")

    const refInput = useRef(null)
    const refMessages = useRef(null)

    let interval = null

    useEffect(() => {
        loadPatient()
    }, [])

    useEffect(() => {
        if (patientId !== "") {
            if (interval !== null) clearInterval(interval)
            loadMessage()

            interval = setInterval(() => {
                checkLastMessage()
            }, 2500)
        }
        return () => {
            clearInterval(interval)
        }
    }, [patientId])

    useEffect(() => {
        refMessages.current.scrollTop = refMessages.current.scrollHeight - prevMessagesHeight
        setPrevMessagesHeight(refMessages.current.scrollHeight)
    }, [messages])

    useEffect(() => {
        if (lastMessageId !== 0) {
            loadMessage()
        }
    }, [lastMessageId])

    function checkLastMessage() {
        provider.getLastMessage(patientId).then(response => {
            setLastMessageId(response.data.id)
        }).catch(error => {
            console.error(error)
        })
    }

    function loadPatient() {
        provider.getPatients().then(response => {
            setPatients(response.data)
            if (searchParams.get("patientId") !== null) {
                setPatientId(searchParams.get("patientId"))
            } else if (response.data.length > 0) {
                setPatientId(response.data[0].id)
                setSearchParams({patientId: response.data[0].id})
            }
        }).catch(error => {
            console.error(error)
        })
    }

    function loadMessage(messageBeforeId = "") {
        provider.getMessages(patientId, messageBeforeId).then(response => {
            setPrevMessagesHeight(0)
            setMessages(response.data.messages)
            setFirstMessageId(response.data.first_message_id)
            refInput.current.focus()
        }).catch(error => {
            console.error(error)
        })
    }

    const messageForList = (originalMessage) => {
        const message = originalMessage.toString().substring(0, 20)
        if (message.length === 20) {
            return message + "..."
        }
        return message
    }

    function handlePatient(event) {
        setPatientId(event.currentTarget.dataset.id)
        setSearchParams({patientId: event.currentTarget.dataset.id})
    }

    function sendMessage(event) {
        event.preventDefault()

        const data = {
            id: patientId,
            message: event.target[0].value
        }

        provider.sendMessage(data).then(() => {
            loadMessage()
            loadPatient()
        }).catch(error => {
            console.error(error)
        })
        event.target[0].value = ""
    }

    function messageOnScroll(event) {
        if (event.target.scrollTop === 0 && messages.length > 0 && messages[0].id !== firstMessageId) {
            provider.getMessages(patientId, messages[0].id).then(response => {
                setMessages([...response.data.messages, ...messages])
                setFirstMessageId(response.data.first_message_id)
            }).catch(error => {
                console.error(error)
            })
        }
    }

    const patientStyle = (id) => {
        if (id.toString() === patientId.toString()) {
            return "w-full bg-gray-100 h-14 flex items-center p-2 cursor-pointer rounded-md"
        } else {
            return "w-full bg-white h-14 flex items-center p-2 cursor-pointer rounded-md"
        }
    }

    const messageStyle = (isFromDoctor) => {
        if (isFromDoctor) return "chat chat-end"
        return "chat chat-start"
    }

    const bubbleStyle = (isFromDoctor) => {
        if (isFromDoctor) return "chat-bubble bg-slate-800 text-slate-100 rounded-xl outline-0 border-0"
        return "chat-bubble bg-primary text-white rounded-xl outline-0 border-0"
    }

    return (
        <div className={"flex gap-2 h-screen py-2"}>
            {/* Menu */}
            <div className={"bg-white shadow-xl w-full md:w-72 overflow-y-auto rounded-xl p-2"}>
                {patients.map((patient) => (
                    <div key={patient.id} data-id={patient.id} className={patientStyle(patient.id)}
                         onClick={handlePatient}>
                        <div className={"px-2"}>
                            <img src={patientProfile} alt={"profile"} className={"rounded-full w-10 h-10"}/>
                        </div>
                        <div className={"flex-1"}>
                            <div className={"font-semibold text-md"}>{patient.af_uid}</div>
                            <div className={"text-gray-400 text-sm"}>{messageForList(patient?.last_message?.content || "")}</div>
                        </div>
                    </div>
                ))}
            </div>

            {/* Message area */}
            <div className={"bg-white shadow-xl flex-1 flex flex-col rounded-xl"}>
                <div ref={refMessages} className={"flex-1 w-full overflow-y-auto p-4 flex flex-col"} onScroll={messageOnScroll}>
                    {messages.map((message, index) => (
                        <div key={index} className={messageStyle(message?.is_from_doctor)}>
                            <div className="chat-image avatar">
                                <div className="w-10 rounded-full">
                                    <img src={message.is_from_doctor ? doctorProfile : patientProfile} alt={"profile"}/>
                                </div>
                            </div>
                            <div className={bubbleStyle(message?.is_from_doctor)}>{message.content}</div>
                        </div>
                    ))}
                </div>
                <form className={"bg-slate-800 h-16 flex items-center p-4 gap-2 rounded-b-xl"} onSubmit={sendMessage}>
                    <input type="text" disabled={patientId === ""} ref={refInput}
                           className={"input input-sm input-bordered rounded-full w-full"}/>
                    <button type={"submit"} className={"btn btn-sm btn-primary rounded-full w-24 flex gap-2"}><MdSend/>Send</button>
                </form>
            </div>
        </div>
    )
}

export default memo(Page)
