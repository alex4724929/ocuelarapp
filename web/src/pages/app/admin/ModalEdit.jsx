import React, {memo, useEffect, useState} from "react"
import {admin as provider} from "../../../providers/cloud.provider.js"
import Alert from "../../../components/Alert.jsx"
import useAlert, {AlertStyle} from "../../../hooks/useAlert"

import adminProfile from "../../../assets/images/profile.png"

const Modal = (props) => {
    const {isOpen, close, cancel, params} = props
    const [isOpenAlert, styleAlert, messageAlert, showAlert] = useAlert()
    const [admin, setAdmin] = useState({
        username: "",
        password: "",
        password_confirmation: "",
    })

    useEffect(() => {
        if (isOpen) {
            provider.getOne(params.target.id).then(response => {
                setAdmin(response.data)
            }).catch(error => {
                console.error(error)
            })
        }
    }, [isOpen])

    function submitForm(event) {
        event.preventDefault()

        provider.setOne(params.target.id, admin).then(() => {
            showAlert(AlertStyle.Success, "Success")
            close()
        }).catch(error => {
            console.error(error)
            showAlert(AlertStyle.Error, `Failed: ${error.response.data.error}`)
            close()
        })
    }

    function handleArchive(event) {
        event.preventDefault()

        if (confirm("Are you sure to archive this admin?") === false) return

        provider.setOne(params.target.id, {active: false}).then(() => {
            showAlert(AlertStyle.Success, "Success")
            close()
        }).catch(error => {
            console.error(error)
            showAlert(AlertStyle.Error, `Failed: ${error.response.data.error}`)
            close()
        })
    }

    function updateAdmin(key, value) {
        setAdmin({...admin, [key]: value})
    }

    return (
        <div>
            <div className={isOpen ? "modal modal-open" : "modal"}>
                <form className={"modal-box bg-gray-100 max-w-xl md:ml-60"} onSubmit={submitForm}>
                    <h3 className="font-bold text-lg">Edit Admin</h3>

                    <div className={"mt-2 flex flex-col gap-2 items-center"}>
                        <img className={"w-20"} src={adminProfile} alt={"profile"}/>
                        <div className="form-control w-full">
                            <span className="label-text font-semibold">Username</span>
                            <input type="text" required className="input input-bordered input-sm w-full"
                                   value={admin.username || ""}
                                   onChange={e => updateAdmin("username", e.target.value)}/>
                        </div>
                        <div className="form-control w-full">
                            <span className="label-text font-semibold">Password</span>
                            <input type="password" required className="input input-bordered input-sm w-full"
                                   value={admin.password || ""}
                                   onChange={e => updateAdmin("password", e.target.value)}/>
                        </div>
                        <div className="form-control w-full">
                            <span className="label-text font-semibold">Password Confirmation</span>
                            <input type="password" required className="input input-bordered input-sm w-full"
                                   value={admin.password_confirmation || ""}
                                   onChange={e => updateAdmin("password_confirmation", e.target.value)}/>
                        </div>
                    </div>

                    <div className={"modal-action justify-between"}>
                        <div>
                            <button type={"button"} className={"btn btn-sm w-20 btn-ghost"}
                                    onClick={cancel}>Cancel
                            </button>
                        </div>
                        <div className={"flex gap-4"}>
                            <button type={"button"} className={"btn btn-sm w-24 btn-ghost text-error"} onClick={handleArchive}>Archive</button>
                            <button type={"submit"} className={"btn btn-sm w-24 btn-primary"}>Save</button>
                        </div>
                    </div>
                </form>
            </div>
            <Alert isOpen={isOpenAlert} style={styleAlert} message={messageAlert}/>
        </div>

    )
}

export default memo(Modal)
