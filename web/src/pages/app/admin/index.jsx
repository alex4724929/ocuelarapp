import React, {lazy, memo, useEffect} from "react"
import useModal from "../../../hooks/useModal.js"
import {admin as provider} from "../../../providers/cloud.provider.js"
import moment from "moment"

const ModalNew = lazy(() => import("./ModalNew.jsx"))
const ModalEdit = lazy(() => import("./ModalEdit.jsx"))

const Page = () => {
    const [admins, setAdmins] = React.useState([])
    const [filter, setFilter] = React.useState({
        username: ""
    })

    const [isOpenNew, paramsNew, openNew, closeNew, cancelNew] = useModal(() => loadAdmins())
    const [isOpenEdit, paramsEdit, openEdit, closeEdit, cancelEdit] = useModal(() => loadAdmins())

    useEffect(() => {
        loadAdmins()
    }, [])

    function updateFilter(key, value) {
        setFilter({...filter, [key]: value})
    }

    function submitFilter(event) {
        event.preventDefault()
        loadAdmins()
    }

    function loadAdmins() {
        provider.getAll(filter).then(response => {
            setAdmins(response.data)
        }).catch(error => {
            console.error(error)
        })
    }

    return (
        <div className={"flex flex-col gap-4 py-6"}>
            <form className={"bg-white p-4 shadow-md flex flex-col gap-2"} onSubmit={submitFilter}>
                <div className={"grid grid-cols-1 md:grid-cols-4 gap-2"}>
                    <label className="input-group input-group-sm input-group-vertical">
                        <span className={"bg-slate-800 text-slate-100"}>Username</span>
                        <input type="text" className="input input-sm bg-slate-100 input-bordered"
                               value={filter.username} onChange={e => updateFilter("username", e.target.value)}/>
                    </label>
                </div>
                <div className={"w-full flex justify-between"}>
                    <button type={"button"} className={"btn btn-sm btn-primary btn-outline w-32"} onClick={openNew}>Add Admin</button>
                    <button type={"submit"} className={"btn btn-sm btn-primary w-32"}>Search</button>
                </div>
            </form>
            <div className={"bg-white p-4 shadow-md"}>
                <div className={"overflow-x-auto"}>
                    <table className={"table table-compact w-full"}>
                        <thead>
                        <tr>
                            <th className={"text-left"}>Username</th>
                            <th className={"text-left w-56"}>Update Date</th>
                            <th className={"text-left w-56"}>Create Date</th>
                            <th className={"text-left w-20"}>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {admins.map((doctor, key) => (
                            <tr key={key}>
                                <td>{doctor.username}</td>
                                <td>{moment(doctor.updated_at).format("DD MMM YYYY hh:mm:ss A")}</td>
                                <td>{moment(doctor.created_at).format("DD MMM YYYY hh:mm:ss A")}</td>
                                <td>
                                    <div className={"flex gap-2"}>
                                        <button id={doctor.id} className={"btn btn-xs btn-ghost w-20"} onClick={openEdit}>Edit</button>
                                    </div>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
            </div>
            <ModalNew isOpen={isOpenNew} close={closeNew} cancel={cancelNew} params={paramsNew}/>
            <ModalEdit isOpen={isOpenEdit} close={closeEdit} cancel={cancelEdit} params={paramsEdit}/>
        </div>
    )
}

export default memo(Page)
