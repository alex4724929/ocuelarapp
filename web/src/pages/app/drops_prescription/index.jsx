import React, {lazy, memo, useEffect, useState} from "react"
import {useSearchParams} from "react-router-dom"
import {drop} from "../../../providers/cloud.provider.js"
import {HiClock} from "react-icons/hi"
import {TbDropletFilled} from "react-icons/tb"
import useModal from "../../../hooks/useModal.js"
import {useTranslation} from "react-i18next"

const ModalEdit = lazy(() => import("./ModalEdit.jsx"))

const Page = () => {
    const {t} = useTranslation()
    const [searchParams, setSearchParams] = useSearchParams()
    const [drops, setDrops] = useState([])
    const [dropId, setDropId] = useState(null)
    const [prescriptions, setPrescriptions] = useState([])

    const [isOpenEdit, paramsEdit, openEdit, closeEdit, cancelEdit] = useModal(() => reload(dropId))

    useEffect(() => {
        // load drops
        drop.getNames().then(response => {
            setDrops(response.data)
            if (searchParams.get("drops") !== null) {
                setDropId(parseInt(searchParams.get("drops")))
            } else if (response.data.length > 0) {
                setDropId(response.data[0].id)
                setSearchParams({drops: response.data[0].id})
            }
        }).catch(error => {
            console.error(error)
        })
    }, [searchParams])

    useEffect(() => {
        if (dropId) reload(dropId)
    }, [dropId])

    function reload(dropId) {
        drop.getPrescriptions(dropId).then(response => {
            setPrescriptions(response.data)
        }).catch(error => {
            console.error(error)
        })
    }

    const DropDisplay = ({count}) => {
        if (count > 0) {
            return (
                <div className={"badge border-0 bg-primary text-white w-16 h-7 flex gap-2 rounded-r-full"}>
                    <span><TbDropletFilled size={18}/></span>
                    <span>{count.toString() || "0"}</span>
                </div>
            )
        } else {
            return (
                <div className={"badge border-0 bg-gray-400 text-white w-16 h-7 flex gap-2 rounded-r-full"}>
                    <span><TbDropletFilled size={18}/></span>
                    <span>{count.toString() || "0"}</span>
                </div>
            )
        }
    }

    const TimeDisplay = ({time}) => {
        return (
            <div className={"badge border-0 bg-slate-700 text-slate-100 w-24 h-7 flex gap-2 rounded-l-full"}>
                <span><HiClock size={16}/></span>
                <span>{time.toString() || ""}:00 ~</span>
            </div>
        )
    }

    return (
        <div className={"py-6"}>
            <div className="tabs tabs-boxed bg-base-100 p-0">
                {drops.map((element, key) => {
                    if (element.id === dropId) {
                        return (
                            <div key={key} className={"tab tab-active"}
                                 onClick={() => setSearchParams({drops: element.id})}>{t(element.name)}</div>)
                    } else {
                        return (
                            <div key={key} className={"tab"}
                                 onClick={() => setSearchParams({drops: element.id})}>{t(element.name)}</div>
                        )
                    }
                })}
            </div>
            <div className={"p-4 bg-white shadow-xl"}>
                <div className={"overflow-x-auto"}>
                    <table className={"table table-compact w-full"}>
                        <thead>
                        <tr>
                            <td className={"w-12"}>Day</td>
                            <td>Schedule 1</td>
                            <td>Schedule 2</td>
                            <td>Schedule 3</td>
                            <td>Schedule 4</td>
                            <td className={"w-20"}>Actions</td>
                        </tr>
                        </thead>
                        <tbody>
                        {prescriptions.map((prescription, key) => (
                            <tr key={key}>
                                <td className={"w-12 text-center"}>{prescription.days}</td>
                                <td>
                                    <div className={"flex items-center"}>
                                        <TimeDisplay time={prescription.start_hour_1}/>
                                        <DropDisplay count={prescription.count_1}/>
                                    </div>
                                </td>
                                <td>
                                    <div className={"flex items-center"}>
                                        <TimeDisplay time={prescription.start_hour_2}/>
                                        <DropDisplay count={prescription.count_2}/>
                                    </div>
                                </td>
                                <td>
                                    <div className={"flex items-center"}>
                                        <TimeDisplay time={prescription.start_hour_3}/>
                                        <DropDisplay count={prescription.count_3}/>
                                    </div>
                                </td>
                                <td>
                                    <div className={"flex items-center"}>
                                        <TimeDisplay time={prescription.start_hour_4}/>
                                        <DropDisplay count={prescription.count_4}/>
                                    </div>
                                </td>
                                <td>
                                    <button data-drop-id={dropId} data-id={prescription.id}
                                            className={"btn btn-xs btn-ghost w-20"} onClick={openEdit}>Edit</button>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
            </div>
            <ModalEdit isOpen={isOpenEdit} close={closeEdit} cancel={cancelEdit} params={paramsEdit}/>
        </div>
    )
}

export default memo(Page)
