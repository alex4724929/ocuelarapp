import React, {memo, useEffect, useState} from "react"
import {drop} from "../../../providers/cloud.provider.js"
import Alert from "../../../components/Alert.jsx"
import useAlert, {AlertStyle} from "../../../hooks/useAlert"

import {MdCalendarMonth} from "react-icons/md"

const Modal = (props) => {
    const {isOpen, close, cancel, params} = props
    const [isOpenAlert, styleAlert, messageAlert, showAlert] = useAlert()
    const [prescription, setPrescription] = useState({
        days: "",
        start_hour_1: "",
        start_hour_2: "",
        start_hour_3: "",
        start_hour_4: "",
        count_1: "",
        count_2: "",
        count_3: "",
        count_4: ""
    })

    useEffect(() => {
        if (isOpen) {
            drop.getPrescription(params.target.dataset.dropId, params.target.dataset.id).then(response => {
                setPrescription(response.data)
            }).catch(error => {
                console.error(error)
            })
        }
    }, [isOpen])

    function submitForm(event) {
        event.preventDefault()

        drop.setPrescription(params.target.dataset.dropId, params.target.dataset.id, prescription).then(() => {
            showAlert(AlertStyle.Success, "Success")
            close()
        }).catch(error => {
            console.error(error)
            showAlert(AlertStyle.Error, `Failed: ${error.response.data.error}`)
            close()
        })
    }

    function update(key, value) {
        setPrescription({...prescription, [key]: value})
    }

    return (
        <div>
            <div className={isOpen ? "modal modal-open" : "modal"}>
                <form className={"modal-box bg-gray-100 max-w-md md:ml-60"} onSubmit={submitForm}>
                    <h3 className="font-bold text-lg">Edit Prescription</h3>

                    <div className={"mt-2 flex flex-col gap-2"}>
                        <div className={"flex gap-2"}>
                            <div className={"badge badge-primary rounded-full w-28 h-8 font-bold flex gap-2"}>
                                <MdCalendarMonth size={16}/>
                                <div>Day {prescription.days}</div>
                            </div>
                        </div>

                        <div className={"mt-4 font-semibold text-md"}>Schedule 1</div>
                        <div className={"flex items-center gap-2"}>
                            <div className={""}>Start from</div>
                            <select required className="select select-bordered select-sm w-32"
                                    value={prescription.start_hour_1 || ""}
                                    onChange={e => update("start_hour_1", e.target.value)}>
                                {Array.from(Array(24).keys()).map((value, key) => {
                                    return <option key={key}
                                                   value={value}>{value.toString().padStart(2, "0")}:00</option>
                                })}
                            </select>
                            <div className={""}>,</div>
                            <select required className="select select-bordered select-sm w-20"
                                    value={prescription.count_1 || ""}
                                    onChange={e => update("count_1", e.target.value)}>
                                {Array.from(Array(5).keys()).map((value, key) => {
                                    return <option key={key} value={value}>{value.toString()}</option>
                                })}
                            </select>
                            <div className={""}>drops</div>
                        </div>

                        <div className={"mt-4 font-semibold text-md"}>Schedule 2</div>
                        <div className={"flex items-center gap-2"}>
                            <div className={""}>Start from</div>
                            <select required className="select select-bordered select-sm w-32"
                                    value={prescription.start_hour_2 || ""}
                                    onChange={e => update("start_hour_2", e.target.value)}>
                                {Array.from(Array(24).keys()).map((value, key) => {
                                    return <option key={key}
                                                   value={value}>{value.toString().padStart(2, "0")}:00</option>
                                })}
                            </select>
                            <div className={""}>,</div>
                            <select required className="select select-bordered select-sm w-20"
                                    value={prescription.count_2 || ""}
                                    onChange={e => update("count_2", e.target.value)}>
                                {Array.from(Array(5).keys()).map((value, key) => {
                                    return <option key={key} value={value}>{value.toString()}</option>
                                })}
                            </select>
                            <div className={""}>drops</div>
                        </div>

                        <div className={"mt-4 font-semibold text-md"}>Schedule 3</div>
                        <div className={"flex items-center gap-2"}>
                            <div className={""}>Start from</div>
                            <select required className="select select-bordered select-sm w-32"
                                    value={prescription.start_hour_3 || ""}
                                    onChange={e => update("start_hour_3", e.target.value)}>
                                {Array.from(Array(24).keys()).map((value, key) => {
                                    return <option key={key}
                                                   value={value}>{value.toString().padStart(2, "0")}:00</option>
                                })}
                            </select>
                            <div className={""}>,</div>
                            <select required className="select select-bordered select-sm w-20"
                                    value={prescription.count_3 || ""}
                                    onChange={e => update("count_3", e.target.value)}>
                                {Array.from(Array(5).keys()).map((value, key) => {
                                    return <option key={key} value={value}>{value.toString()}</option>
                                })}
                            </select>
                            <div className={""}>drops</div>
                        </div>

                        <div className={"mt-4 font-semibold text-md"}>Schedule 4</div>
                        <div className={"flex items-center gap-2"}>
                            <div className={""}>Start from</div>
                            <select required className="select select-bordered select-sm w-32"
                                    value={prescription.start_hour_4 || ""}
                                    onChange={e => update("start_hour_4", e.target.value)}>
                                {Array.from(Array(24).keys()).map((value, key) => {
                                    return <option key={key}
                                                   value={value}>{value.toString().padStart(2, "0")}:00</option>
                                })}
                            </select>
                            <div className={""}>,</div>
                            <select required className="select select-bordered select-sm w-20"
                                    value={prescription.count_4 || ""}
                                    onChange={e => update("count_4", e.target.value)}>
                                {Array.from(Array(5).keys()).map((value, key) => {
                                    return <option key={key} value={value}>{value.toString()}</option>
                                })}
                            </select>
                            <div className={""}>drops</div>
                        </div>
                    </div>

                    <div className={"modal-action justify-between"}>
                        <div>
                            <button type={"button"} className={"btn btn-sm w-20 btn-ghost"}
                                    onClick={cancel}>Cancel
                            </button>
                        </div>
                        <div className={"flex gap-4"}>
                            <button type={"submit"} className={"btn btn-sm w-24 btn-primary"}>Save</button>
                        </div>
                    </div>
                </form>
            </div>
            <Alert isOpen={isOpenAlert} style={styleAlert} message={messageAlert}/>
        </div>

    )
}

export default memo(Modal)
