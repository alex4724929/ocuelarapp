import React, {memo, useEffect, useState} from "react"
import {doctor as provider} from "../../../providers/cloud.provider.js"
import Alert from "../../../components/Alert.jsx"
import useAlert, {AlertStyle} from "../../../hooks/useAlert"

import doctorProfile from "../../../assets/images/profile.png"

const Modal = (props) => {
    const {isOpen, close, cancel, params} = props
    const [isOpenAlert, styleAlert, messageAlert, showAlert] = useAlert()
    const [doctor, setDoctor] = useState({
        username: "",
        password: "",
        password_confirmation: "",
    })

    useEffect(() => {
        if (isOpen) {
            provider.getOne(params.target.id).then(response => {
                setDoctor(response.data)
            }).catch(error => {
                console.error(error)
            })
        }
    }, [isOpen])

    function submitForm(event) {
        event.preventDefault()

        provider.setOne(params.target.id, doctor).then(() => {
            showAlert(AlertStyle.Success, "Success")
            close()
        }).catch(error => {
            console.error(error)
            showAlert(AlertStyle.Error, `Failed: ${error.response.data.error}`)
            close()
        })
    }

    function handleArchive(event) {
        event.preventDefault()

        if (confirm("Are you sure to archive this doctor?") === false) return

        provider.setOne(params.target.id, {active: false}).then(() => {
            showAlert(AlertStyle.Success, "Success")
            close()
        }).catch(error => {
            console.error(error)
            showAlert(AlertStyle.Error, `Failed: ${error.response.data.error}`)
            close()
        })
    }

    function updateDoctor(key, value) {
        setDoctor({...doctor, [key]: value})
    }

    return (
        <div>
            <div className={isOpen ? "modal modal-open" : "modal"}>
                <form className={"modal-box bg-gray-100 max-w-xl md:ml-60"} onSubmit={submitForm}>
                    <h3 className="font-bold text-lg">Edit Doctor</h3>

                    <div className={"mt-2 flex flex-col gap-2 items-center"}>
                        <img className={"w-20"} src={doctorProfile} alt={"profile"}/>
                        <div className="form-control w-full">
                            <span className="label-text font-semibold">Username</span>
                            <input type="text" required className="input input-bordered input-sm w-full"
                                   value={doctor.username || ""}
                                   onChange={e => updateDoctor("username", e.target.value)}/>
                        </div>
                        <div className="form-control w-full">
                            <span className="label-text font-semibold">Password</span>
                            <input type="password" required className="input input-bordered input-sm w-full"
                                   value={doctor.password || ""}
                                   onChange={e => updateDoctor("password", e.target.value)}/>
                        </div>
                        <div className="form-control w-full">
                            <span className="label-text font-semibold">Password Confirmation</span>
                            <input type="password" required className="input input-bordered input-sm w-full"
                                   value={doctor.password_confirmation || ""}
                                   onChange={e => updateDoctor("password_confirmation", e.target.value)}/>
                        </div>
                    </div>

                    <div className={"modal-action justify-between"}>
                        <div>
                            <button type={"button"} className={"btn btn-sm w-20 btn-ghost"}
                                    onClick={cancel}>Cancel
                            </button>
                        </div>
                        <div className={"flex gap-4"}>
                            <button type={"button"} className={"btn btn-sm w-24 btn-ghost text-error"} onClick={handleArchive}>Archive</button>
                            <button type={"submit"} className={"btn btn-sm w-24 btn-primary"}>Save</button>
                        </div>
                    </div>
                </form>
            </div>
            <Alert isOpen={isOpenAlert} style={styleAlert} message={messageAlert}/>
        </div>

    )
}

export default memo(Modal)
