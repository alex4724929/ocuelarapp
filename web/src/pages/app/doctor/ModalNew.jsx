import React, {memo, useEffect} from "react"
import moment from "moment"
import {doctor as provider} from "../../../providers/cloud.provider"
import Alert from "../../../components/Alert"
import useAlert, {AlertStyle} from "../../../hooks/useAlert"
import doctorProfile from "../../../assets/images/profile.png"

const defaultDoctor = {
    username: "",
    password: "",
    password_confirmation: "",
}

const Modal = (props) => {
    const {isOpen, close, cancel} = props
    const [isOpenAlert, styleAlert, messageAlert, showAlert] = useAlert()
    const [doctor, setDoctor] = React.useState(defaultDoctor)

    useEffect(() => {
        if (isOpen) {
            setDoctor(defaultDoctor)
        }
    }, [isOpen])

    function submitForm(event) {
        event.preventDefault()

        provider.create(doctor).then(() => {
            showAlert(AlertStyle.Success, "Success")
            close()
        }).catch((error) => {
            console.error(error)
            showAlert(AlertStyle.Error, `Failed: ${error.response.data.error}`)
            close()
        })
    }

    function updateDoctor(key, value) {
        setDoctor({...doctor, [key]: value})
    }

    return (
        <div>
            <div className={isOpen ? "modal modal-open" : "modal"}>
                <form className={"modal-box bg-gray-100 max-w-xl md:ml-60"} onSubmit={submitForm} autoComplete={"off"}>
                    <h3 className="font-bold text-lg">New Doctor</h3>

                    <div className={"mt-2 flex flex-col gap-2 items-center"}>
                        <img className={"w-20"} src={doctorProfile} alt={"profile"}/>

                        <div className="form-control w-full">
                            <span className="label-text font-semibold">Username</span>
                            <input type="text" required className="input input-bordered input-sm w-full"
                                   value={doctor.username || ""}
                                   onChange={e => updateDoctor("username", e.target.value)}/>
                        </div>
                        <div className="form-control w-full">
                            <span className="label-text font-semibold">Password</span>
                            <input type="password" required className="input input-bordered input-sm w-full"
                                   value={doctor.password || ""} pattern=".{6,}"
                                   onChange={e => updateDoctor("password", e.target.value)}/>
                        </div>
                        <div className="form-control w-full">
                            <span className="label-text font-semibold">Password Confirmation</span>
                            <input type="password" required className="input input-bordered input-sm w-full"
                                   value={doctor.password_confirmation || ""} pattern=".{6,}"
                                   onChange={e => updateDoctor("password_confirmation", e.target.value)}/>
                        </div>
                    </div>
                    <div className={"modal-action justify-between"}>
                        <button type={"button"} className={"btn btn-sm w-20 btn-ghost text-error"} onClick={cancel}>Cancel</button>
                        <button type={"submit"} className={"btn btn-sm w-20 btn-primary"}>Add</button>
                    </div>
                </form>
            </div>
            <Alert isOpen={isOpenAlert} style={styleAlert} message={messageAlert}/>
        </div>
    )
}

export default memo(Modal)
