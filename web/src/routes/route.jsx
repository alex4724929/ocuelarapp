import { lazy } from 'react'

const Admin = lazy(() => import("../pages/app/admin"))
const Doctor = lazy(() => import("../pages/app/doctor"))
const DropsInfo = lazy(() => import("../pages/app/drops_info"))
const DropsPrescription = lazy(() => import("../pages/app/drops_prescription"))
const Game = lazy(() => import("../pages/app/game"))
const GameCategory = lazy(() => import("../pages/app/game_category"))
const Message = lazy(() => import("../pages/app/message"))
const Device = lazy(() => import("../pages/app/device"))
const Patient = lazy(() => import("../pages/app/patient"))
// const Prescription = lazy(() => import("../pages/app/prescription"))
const Profile = lazy(() => import("../pages/app/profile"))

const routes = [
    {path: "/admin", role: ["admin"], component: Admin},
    {path: "/doctor", role: ["admin"], component: Doctor},
    {path: "/drops/info", role: ["admin", "doctor"], component: DropsInfo},
    {path: "/drops/prescription", role: ["admin", "doctor"], component: DropsPrescription},
    {path: "/game/games", role: ["admin", "doctor"], component: Game},
    {path: "/game/category", role: ["admin", "doctor"], component: GameCategory},
    {path: "/message", role: ["admin", "doctor"], component: Message},
    {path: "/device", role: ["admin", "doctor"], component: Device},
    {path: "/patient", role: ["admin", "doctor"], component: Patient},
    // {path: "/prescription", role: ["admin", "doctor"], component: Prescription},
    {path: "/profile", role: ["admin", "doctor"], component: Profile}
]

export default routes

