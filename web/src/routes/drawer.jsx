import {IoLogoGameControllerB, IoMdInformationCircle, IoMdInformationCircleOutline} from "react-icons/io"
import {MdCategory, MdDoneAll, MdMessage, MdOutlineSchedule, MdSignalWifiStatusbarNotConnected} from "react-icons/md"
import {FaBed, FaStethoscope} from "react-icons/fa"
import {RiAdminFill} from "react-icons/ri"
import {TbDropletFilled} from "react-icons/tb"

const routes = {
    "admin": [
        {name: "Admins", path: "/app/admin", icon: <RiAdminFill size={16}/>},
        {name: "Doctors", path: "/app/doctor", icon: <FaStethoscope size={16}/>},
        {name: "Patients", path: "/app/patient", icon: <FaBed size={16}/>},
        {name: "Prescription", path: "/app/drops/prescription", icon: <MdOutlineSchedule size={16}/>},
        {name: "Messages", path: "/app/message", icon: <MdMessage size={16}/>},
        {name: "Devices", path: "/app/device", icon: <TbDropletFilled size={16}/>},
        // {name: "Ocuelar", path: "", icon: <TbDropletFilled size={16}/>, submenu: [
        //     {name: "Last Online", path: "/app/ocuelar/online", icon: <MdSignalWifiStatusbarNotConnected size={16} />},
        //     {name: "Records", path: "/app/ocuelar/record", icon: <MdDoneAll size={16}/>},
        // ]},
        {name: "Drops Information", path: "/app/drops/info", icon: <IoMdInformationCircleOutline size={16}/>},
        {name: "Game", path: "", icon: <IoLogoGameControllerB size={16}/>, submenu: [
            {name: "Category", path: "/app/game/category", icon: <MdCategory size={16}/>},
            {name: "Games", path: "/app/game/games", icon: <IoLogoGameControllerB size={16}/>},
        ]},
    ],
    "doctor": [
        {name: "Patients", path: "/app/patient", icon: <FaBed size={16}/>},
        {name: "Prescription", path: "/app/drops/prescription", icon: <MdOutlineSchedule size={16}/>},
        {name: "Messages", path: "/app/message", icon: <MdMessage size={16}/>},
        {name: "Devices", path: "/app/device", icon: <TbDropletFilled size={16}/>},
        {name: "Drops Information", path: "/app/drops/info", icon: <IoMdInformationCircleOutline size={16}/>},
        // {name: "Ocuelar", path: "", icon: <TbDropletFilled size={16}/>, submenu: [
        //     {name: "Last Online", path: "/app/ocuelar/online", icon: <MdSignalWifiStatusbarNotConnected size={16} />},
        //     {name: "Records", path: "/app/ocuelar/record", icon: <MdDoneAll size={16}/>},
        // ]},
    ]
}

export default routes
