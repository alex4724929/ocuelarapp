import axios from "axios"
import {refresh} from "../providers/oauth.provider"

export const axiosInstance = axios.create({baseURL: import.meta.env.VITE_SERVER_URL})

axiosInstance.interceptors.request.use(
    (config) => {
        config.headers["Authorization"] = `Bearer ${JSON.parse(localStorage.getItem("token"))["access_token"]}`
        config.headers["Accept"] = "application/json"
        return config
    },
    error => {
        return Promise.reject(error)
    }
)

axiosInstance.interceptors.response.use(
    (response) => {
        return response
    }, async (error) => {
        const originalRequest = error.config
        if (error.response.status === 401) {
            const refreshResult = await refresh(JSON.parse(localStorage.getItem("token")))
            if (refreshResult.status === 200) {
                localStorage.setItem("token", JSON.stringify(refreshResult.data))
            } else {
                localStorage.removeItem("token")
            }
            return axiosInstance(originalRequest)
        }
        return Promise.reject(error)
    }
)
