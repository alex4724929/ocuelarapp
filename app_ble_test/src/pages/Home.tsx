import React, {useEffect, useState} from "react"
import {IonButton, IonContent, IonHeader, IonPage, IonTitle, IonToolbar} from "@ionic/react"
import ExploreContainer from "../components/ExploreContainer"
import "./Home.css"
import {BLE} from "@awesome-cordova-plugins/ble"

const Home: React.FC = () => {
    useEffect(() => {
        scanBLE()
    }, [])

    const [devices, setDevices]=  useState<any[]>([])
    const [message, setMessage] = useState<string>("Welcome")

    function scanBLE() {
        setDevices([])
        BLE.scan([], 5).subscribe((device) => {
            setDevices(prev => [...prev, device])
        })
    }

    function refresh() {
        scanBLE()
    }

    // useIonViewDidEnter(() => {
    //     const devices: string[] = []
    //     BLE.scan([], 5).subscribe((device) => {
    //         console.log(JSON.stringify(device))
    //         if (device.name !== undefined && device.name !== "") {
    //             devices.push(JSON.stringify(device))
    //             setMessages(devices)
    //         }
    //     })
    // })

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Blank</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">Blank</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonButton onClick={refresh}>Refresh</IonButton>
                <div>{message}</div>
                {devices.map((device, index) => {
                    return (
                        <div key={index}>{device.name}</div>
                    )
                })}
            </IonContent>
        </IonPage>
    )
}

export default Home
