import {CapacitorConfig} from "@capacitor/cli"

const config: CapacitorConfig = {
    appId: "com.sheep.ocuelar",
    appName: "Ocuelar",
    bundledWebRuntime: false,
    webDir: "dist",
    plugins: {
        LocalNotifications: {},
        SplashScreen: {
            launchShowDuration: 2500,
            launchAutoHide: true,
            launchFadeOutDuration: 250,
            backgroundColor: "#FFCA67",
            androidSplashResourceName: "splash",
            androidScaleType: "CENTER_CROP",
            showSpinner: true,
            androidSpinnerStyle: "large",
            iosSpinnerStyle: "small",
            spinnerColor: "#261E0F",
            splashFullScreen: true,
            splashImmersive: true,
            layoutName: "launch_screen",
            useDialog: true
        }
    }
}

export default config
