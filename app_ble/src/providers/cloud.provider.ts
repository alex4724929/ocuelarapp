import {axiosPrivate, axiosPublic} from "../utils/axios.util"

export const friend = {
    getList: () => axiosPrivate.get("/api/v1/patient/friend/list"),
    getSentRequest: () => axiosPrivate.get("/api/v1/patient/friend/request/send"),
    getReceivedRequest: () => axiosPrivate.get("/api/v1/patient/friend/request/receive"),
    sendRequest: (data: FormData) => axiosPrivate.post("/api/v1/patient/friend/request/send", data),
    responseRequest: (requestId: number, status: string) => axiosPrivate.patch(`/api/v1/patient/friend/request/receive/${requestId}`, {status: status}),
}

export const friendMessage = {
    index: () => axiosPrivate.get(`/api/v1/patient/friend/message`),
    show: (friendshipId: number, messageBeforeId: string) => axiosPrivate.get(`/api/v1/patient/friend/message/${friendshipId}?message_before_id=${messageBeforeId}`),
    send: (data: {}) => axiosPrivate.post(`/api/v1/patient/friend/message`, data),
    last: (friendshipId: number) => axiosPrivate.get(`/api/v1/patient/friend/message/${friendshipId}/last`),
}

export const daily = {
    index: () => axiosPrivate.get("/api/v1/patient/daily"),
}

export const monthly = {
    index: (date: string) => axiosPrivate.get("/api/v1/patient/monthly?date=" + date),
}

export const doctorMessage = {
    index: (messageBeforeId: string) => axiosPrivate.get("/api/v1/patient/message/doctor?message_before_id=" + messageBeforeId),
    create: (message: string) => axiosPrivate.post("/api/v1/patient/message/doctor", {content: message}),
    last: () => axiosPrivate.get("/api/v1/patient/doctor/message/last")
}

export const drop = {
    info: {
        index: (params?: any) => axiosPrivate.get("/api/v1/patient/drop/info", {params: params}),
    },
    upload: (params?: any) => axiosPublic.post("/api/v2/device/drops", {...params, "api_key": "PoVgCkJmEk6ESOi1Fph6RcDG4ZDIR0tY"}),
}

export const notification = {
    index: () => axiosPrivate.get("/api/v1/patient/notification"),
}
