import React, {useEffect} from "react"
import {IonApp, IonRouterOutlet, IonSplitPane, setupIonicReact} from "@ionic/react"
import {IonReactRouter} from "@ionic/react-router"
import {Redirect, Route} from "react-router-dom"
import Menu from "./components/Menu"

import "@ionic/react/css/core.css"

import "@ionic/react/css/normalize.css"
import "@ionic/react/css/structure.css"
import "@ionic/react/css/typography.css"

import "@ionic/react/css/padding.css"
import "@ionic/react/css/float-elements.css"
import "@ionic/react/css/text-alignment.css"
import "@ionic/react/css/text-transformation.css"
import "@ionic/react/css/flex-utils.css"
import "@ionic/react/css/display.css"

import "./theme/variables.css"
import PrivateRoute from "./components/PrivateRoute"

import PageLogin from "./pages/login"
import PageMessageDoctor from "./pages/doctor/message"
import PageDropsDaily from "./pages/drops/daily"
import PageDropsMonthly from "./pages/drops/monthly"
import PageFriend from "./pages/friend"
import PageEntertainment from "./pages/entertainment"
import PageGame from "./pages/game"
import PageReward from "./pages/reward"
import PageRewardShop from "./pages/reward/shop"
import PageRewardWallet from "./pages/reward/wallet"
import PageTest from "./pages/test"
import PageBleSync from "./pages/ble/sync"

import GameBlackjack21 from "./games/Blackjack21"
import GameCandyMatchSaga from "./games/CandyMatchSaga"
import GameSudoku from "./games/Sudoku"
import GameTronix from "./games/Tronix"

import useToken from "./hooks/useToken"
import {LocalNotifications, ScheduleOptions} from "@capacitor/local-notifications"
import {notification} from "./providers/cloud.provider"

setupIonicReact()

const App: React.FC = () => {
    const {token, setToken} = useToken()

    useEffect(() => {
        if (token) {
            LocalNotifications.checkPermissions().then(async permission => {
                if (permission.display === "granted") return
                await LocalNotifications.requestPermissions()
            }).then(() => {
                notification.index().then(response => {
                    let notificationList = []
                    for (let i = 0; i < response.data.notifications.length; i++) {
                        const notification = response.data.notifications[i]
                        notificationList.push({
                            id: i,
                            title: notification.title,
                            body: notification.body,
                            schedule: {at: new Date(notification.time)}
                        })
                    }
                    let notificationOptions: ScheduleOptions = {notifications: notificationList}
                    LocalNotifications.schedule(notificationOptions).then()
                })
            })
        }
    }, [])

    const FirstPage = () => {
        if (token) {
            if (token.control_group === "smartcap" || token.control_group === "smartcap_gamification") {
                return <Redirect exact to={"/ble/sync"}/>
            } else {
                alert("No permission")
                setToken(null)
                return <Redirect exact from={"/"} to={"/login"}/>
                // return <Redirect exact from={"/"} to={"/drops/daily"}/>
            }
        }
        return <Redirect exact from={"/"} to={"/login"}/>
    }

    return (
        <IonApp>
            <IonReactRouter>
                <IonSplitPane contentId="main">
                    <Menu/>
                    <IonRouterOutlet id="main">
                        <Route exact path={"/login"}><PageLogin/></Route>
                        <Route exact path={"/message/doctor"}><PrivateRoute component={<PageMessageDoctor/>}/></Route>
                        <Route exact path={"/drops/daily"}><PrivateRoute component={<PageDropsDaily/>}/></Route>
                        <Route exact path={"/drops/monthly"}><PrivateRoute component={<PageDropsMonthly/>}/></Route>
                        <Route exact path={"/friend/*"}><PrivateRoute component={<PageFriend/>}/></Route>
                        <Route exact path={"/entertainment"}><PrivateRoute component={<PageEntertainment/>}/></Route>
                        <Route exact path={"/game"}><PrivateRoute component={<PageGame/>}/></Route>
                        <Route exact path={"/reward"}><PrivateRoute component={<PageReward/>}/></Route>
                        <Route exact path={"/reward/shop"}><PrivateRoute component={<PageRewardShop/>}/></Route>
                        <Route exact path={"/reward/wallet"}><PrivateRoute component={<PageRewardWallet/>}/></Route>
                        <Route exact path={"/test"}><PrivateRoute component={<PageTest/>}/></Route>
                        <Route exact path={"/game/blackjack21"}><PrivateRoute component={<GameBlackjack21/>}/></Route>
                        <Route exact path={"/game/candyMatchSaga"}><PrivateRoute component={<GameCandyMatchSaga/>}/></Route>
                        <Route exact path={"/game/sudoku"}><PrivateRoute component={<GameSudoku/>}/></Route>
                        <Route exact path={"/game/tronix"}><PrivateRoute component={<GameTronix/>}/></Route>
                        <Route exact path={"/ble/sync"}><PrivateRoute component={<PageBleSync/>}/></Route>
                        <FirstPage/>
                    </IonRouterOutlet>
                </IonSplitPane>
            </IonReactRouter>
        </IonApp>
    )
}

export default App
