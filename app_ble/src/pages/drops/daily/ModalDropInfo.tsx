import React, {useEffect, useState} from "react"
import {MdCancel} from "react-icons/md"
import {modelStyle} from "../../../theme/style"
import {drop} from "../../../providers/cloud.provider"
import {useTranslation} from "react-i18next"

type ModelProps = {
    isOpen: boolean
    close: (result: {}) => void
    cancel: () => void
    param: {}
}

type InfoProps = {
    id: number
    info_type: string
    order: number
    content: string
}

const Modal: React.FC<ModelProps> = (props: ModelProps) => {
    const {isOpen, cancel, param} = props

    const {t} = useTranslation()

    const [dropName, setDropName] = useState("")
    const [infoList, setInfoList] = useState([])

    useEffect(() => {
        if (isOpen) {
            drop.info.index(param).then((response => {
                setDropName(response.data.name)
                setInfoList(response.data.info)
            })).catch(error => {
                console.error(error)
            })
        }
    }, [isOpen])

    return (
        <div className={modelStyle(isOpen)}>
            <div className={"modal-box bg-white px-4 py-2 flex flex-col mt-14"}>
                <div className={"flex justify-between items-center"}>
                    <div className="text-2xl font-bold mb-2">{t(dropName)}</div>
                    <div onClick={cancel}><MdCancel size={24}/></div>
                </div>
                {infoList.map((info: InfoProps) => {
                    if (info.info_type === "title") {
                        return (<div key={info.id} className={"font-bold mt-2"}>{info.content}</div>)
                    } else {
                        return (<div key={info.id} className={"display-linebreak"}>{info.content}</div>)
                    }
                })}
            </div>
        </div>
    )
}

export default Modal
