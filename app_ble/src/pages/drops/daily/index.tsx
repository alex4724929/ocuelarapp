import React, {useState} from "react"
import {
    IonButton,
    IonButtons,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonRefresher,
    IonRefresherContent,
    IonTitle,
    IonToolbar,
    RefresherEventDetail,
    useIonViewWillEnter
} from "@ionic/react"
import "./index.css"
import {IoPeopleCircle, IoWater} from "react-icons/io5"
import {IoMdInformationCircle} from "react-icons/io"
import moment from "moment"
import {daily} from "../../../providers/cloud.provider"
import {StatusBar, Style} from "@capacitor/status-bar"
import useModal from "../../../hooks/useModal"
import ModalDropInfo from "./ModalDropInfo"
import WeekStatus from "../../../components/WeekStatus"
import {useHistory} from "react-router-dom"
import {NavigationBar} from "@hugotomazi/capacitor-navigation-bar"

const statusBarColor: string = "#FFFFFF"
const navigationBarColor: string = "#FFCA67"
const statusBarStyle: Style = Style.Light

const Page: React.FC = () => {
    useIonViewWillEnter(() => {
        StatusBar.setBackgroundColor({color: statusBarColor}).then()
        StatusBar.setStyle({style: statusBarStyle}).then()
        NavigationBar.setColor({color: navigationBarColor, darkButtons: true}).then()
    })

    const history = useHistory()

    const [isOpenModal, paramsModal, openModal, closeModal, cancelModal] = useModal()

    const [medInfo, setMedInfo] = useState({
        total_adherence: 0.0,
        days_from_start: 0,
        flourometholone_expected: 0,
        flourometholone_actual: 0,
        vigamox_expected: 0,
        vigamox_actual: 0,
        ketorolac_expected: 0,
        ketorolac_actual: 0,
        weekly: [ // 0 = haven't, 1 = not full, 2 = success, 3 = today empty
            {text: "SUN", status: 0},
            {text: "MON", status: 0},
            {text: "TUE", status: 0},
            {text: "WED", status: 0},
            {text: "THU", status: 0},
            {text: "FRI", status: 0},
            {text: "SAT", status: 0}
        ]
    })

    useIonViewWillEnter(() => {
        daily.index().then(response => {
            setMedInfo(response.data)
        }).catch(error => {
            console.error(error)
        })
    })

    function handleRefresh(event: CustomEvent<RefresherEventDetail>) {
        daily.index().then(response => {
            setMedInfo(response.data)
        }).catch(error => {
            console.error(error)
        }).finally(() => {
            event.detail.complete();
        })
    }

    return (
        <IonPage>
            <IonHeader className="ion-no-border">
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton/>
                    </IonButtons>
                    <IonTitle>Ocuelar</IonTitle>
                    <IonButtons slot="primary">
                        <IonButton onClick={() => history.replace("/drops/monthly")}>Monthly View</IonButton>
                    </IonButtons>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen>
                <IonRefresher slot="fixed" onIonRefresh={handleRefresh}>
                    <IonRefresherContent/>
                </IonRefresher>
                <WeekStatus weekly={medInfo?.weekly}/>

                <div className={"py-6"}>
                    <div className={"w-full text-center text-black text-3xl"}>
                        {moment().format("DD MMM YYYY, dddd")}
                    </div>
                    <div className={"w-full text-center text-gray-400 text"}>
                        POST OPERATION: DAY {medInfo.days_from_start}
                    </div>
                </div>
                <div className={"px-6 py-2 flex flex-col gap-1"}>
                    <div className={"flex items-center gap-2"}>
                        <div className={"text-2xl"}>Flourometholone</div>
                        <div onClick={() => openModal({name: "flourometholone"})}>
                            <IoMdInformationCircle size={24} color={"#AAAAAA"}/>
                        </div>
                    </div>
                    <div className={"flex items-end gap-4"}>
                        <div className={"text-red-600 w-20"}>{medInfo.flourometholone_expected}X DAILY</div>
                        <div className={"flex items-end"}>
                            {Array.from(Array(medInfo.flourometholone_expected), (x, i) => i).map(i => {
                                return (
                                    <IoWater size={24} key={i} color={medInfo.flourometholone_actual >= i + 1 ? "#DD1020" : "#AAAAAA"}/>
                                )
                            })}
                        </div>
                    </div>
                </div>

                <div className={"px-6 py-2 flex flex-col gap-1"}>
                    <div className={"flex items-center gap-2"}>
                        <div className={"text-2xl"}>Vigamox</div>
                        <div onClick={() => openModal({name: "vigamox"})}>
                            <IoMdInformationCircle size={24} color={"#AAAAAA"}/>
                        </div>
                    </div>
                    <div className={"flex items-end gap-4"}>
                        <div className={"text-red-600 w-20"}>{medInfo.vigamox_expected}X DAILY</div>
                        <div className={"flex"}>
                            {Array.from(Array(medInfo.vigamox_expected), (x, i) => i).map(i => {
                                return (
                                    <IoWater size={24} key={i} color={medInfo.vigamox_actual >= i + 1 ? "#DD1020" : "#AAAAAA"}/>
                                )
                            })}
                        </div>
                    </div>
                </div>

                <div className={"px-6 py-2 flex flex-col gap-1 pb-40"}>
                    <div className={"flex items-end gap-4"}>
                        <div className={"text-2xl"}>Ketorolac</div>
                        <div onClick={() => openModal({name: "ketorolac"})}>
                            <IoMdInformationCircle size={24} color={"#AAAAAA"}/>
                        </div>
                    </div>
                    <div className={"flex gap-4 items-center"}>
                        <div className={"text-red-600 w-20"}>{medInfo.ketorolac_expected}X DAILY</div>
                        <div className={"flex"}>
                            {Array.from(Array(medInfo.ketorolac_expected), (x, i) => i).map(i => {
                                return (
                                    <IoWater size={24} key={i} color={medInfo.ketorolac_actual >= i + 1 ? "#DD1020" : "#AAAAAA"}/>
                                )
                            })}
                        </div>
                    </div>
                </div>

                <div className={"fixed bottom-0 bg-primary h-40 w-full bottom-area"}>
                    <div className={"grid grid-cols-2"}>
                        <div className={"text-center mx-auto py-4"}>
                            <div className={"indicator text-white"} onClick={() => history.replace("/message/doctor")}>
                                <IoPeopleCircle size={96}/>
                                <div className="indicator-item pr-8 pt-8">
                                    <div className="badge h-6 rounded-xl bg-error border-error w-10 text-xl">...</div>
                                </div>
                            </div>
                            <div className={"font-bold"}>Dr.Giovannini</div>
                        </div>
                        <div className={"text-center mx-auto py-4"}>
                            <div className={"text-5xl py-4"}>
                                {parseFloat(medInfo.total_adherence.toString()).toFixed(1)}%
                            </div>
                            <div className={"text-sm"}>TOTAL ADHERENCE</div>
                            <div className={"text-sm"}>TO MEDICATION</div>
                        </div>
                    </div>
                </div>
                <ModalDropInfo isOpen={isOpenModal} close={closeModal} cancel={cancelModal} param={paramsModal} />
            </IonContent>
        </IonPage>
    )
}

export default Page
