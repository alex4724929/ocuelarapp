import React, {useEffect, useState} from "react"
import {
    DatetimeChangeEventDetail,
    IonButtons,
    IonContent,
    IonDatetime,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar, useIonViewWillEnter
} from "@ionic/react"
import "./index.css"
import moment from "moment"
import {IoWater} from "react-icons/io5"
import {IonDatetimeCustomEvent} from "@ionic/core/dist/types/components"
import {monthly} from "../../../providers/cloud.provider"
import {StatusBar, Style} from "@capacitor/status-bar"
import {NavigationBar} from "@hugotomazi/capacitor-navigation-bar"

const statusBarColor: string = "#FFFFFF"
const navigationBarColor: string = "#FFFFFF"
const statusBarStyle: Style = Style.Light

const Page: React.FC = () => {
    useIonViewWillEnter(() => {
        StatusBar.setBackgroundColor({color: statusBarColor}).then()
        StatusBar.setStyle({style: statusBarStyle}).then()
        NavigationBar.setColor({color: navigationBarColor, darkButtons: true}).then()
    })

    const [today, setToday] = useState(moment().format("YYYY-MM-DD"))
    const [medInfo, setMedInfo] = useState({
        medication_start_at: "2023-04-14",
        flourometholone_expected: 4,
        flourometholone_actual: 0,
        vigamox_expected: 4,
        vigamox_actual: 0,
        ketorolac_expected: 4,
        ketorolac_actual: 0
    })

    useEffect(() => {
        monthly.index(today).then(response => {
            setMedInfo(response.data)
        }).catch(error => {
            console.error(error)
        })
    }, [today])

    function dayOnChange(event: IonDatetimeCustomEvent<DatetimeChangeEventDetail>) {
        setToday(event.target.value?.toString() || "")
    }

    return (
        <IonPage>
            <IonHeader className="ion-no-border">
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton/>
                    </IonButtons>
                    <IonTitle>Ocuelar</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">Ocuelar</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonDatetime color="ocuelar" size={"cover"} presentation={"date"} preferWheel={false}
                             min={medInfo.medication_start_at} max={moment().format("YYYY-MM-DD")}
                             value={today} onIonChange={dayOnChange} />
                <div className={"p-10"}>
                    <div className={"font-bold"}>LEGEND:</div>
                    <div>
                        <div className={"py-2"}>
                            <div className={"text-2xl"}>Flourometholone</div>
                            <div className={"flex gap-4"}>
                                <div className={"text-red-600 w-20"}>{medInfo.flourometholone_expected}X DAILY</div>
                                <div className={"flex"}>
                                    {Array.from(Array(medInfo.flourometholone_expected), (x, i) => i).map(i => {
                                        return <IoWater size={24} key={i} color={medInfo.flourometholone_actual >= i + 1 ? "#DD1020" : "#AAAAAA"}/>
                                    })}
                                </div>
                            </div>
                        </div>
                        <div className={"py-2"}>
                            <div className={"text-2xl"}>Vigamox</div>
                            <div className={"flex gap-4"}>
                                <div className={"text-red-600 w-20"}>{medInfo.vigamox_expected}X DAILY</div>
                                <div className={"flex"}>
                                    {Array.from(Array(medInfo.vigamox_expected), (x, i) => i).map(i => {
                                        return <IoWater size={24} key={i} color={medInfo.vigamox_actual >= i + 1 ? "#DD1020" : "#AAAAAA"}/>
                                    })}
                                </div>
                            </div>
                        </div>
                        <div className={"py-2"}>
                            <div className={"text-2xl"}>Ketorolac</div>
                            <div className={"flex gap-4"}>
                                <div className={"text-red-600 w-20"}>{medInfo.ketorolac_expected}X DAILY</div>
                                <div className={"flex"}>
                                    {Array.from(Array(medInfo.ketorolac_expected), (x, i) => i).map(i => {
                                        return <IoWater size={24} key={i} color={medInfo.ketorolac_actual >= i + 1 ? "#DD1020" : "#AAAAAA"}/>
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </IonContent>
        </IonPage>
    )
}

export default Page
