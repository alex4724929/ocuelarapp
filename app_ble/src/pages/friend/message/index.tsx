import React, {useEffect, useState} from "react"
import {useIonViewWillEnter, useIonViewWillLeave} from "@ionic/react"
import {useLocation} from "react-router-dom"
import {friendMessage as provider} from "../../../providers/cloud.provider"
import PatientProfile from "../../../assets/images/profile.png"
import DoctorProfile from "../../../assets/images/profile.png"
import moment from "moment/moment"

const Page: React.FC = () => {
    const {search} = useLocation()

    const [myId, setMyId] = React.useState(0)
    const [messages, setMessages] = React.useState<MessageProps[]>([])
    const [message, setMessage] = useState("")
    const [firstMessageId, setFirstMessageId] = useState(0)
    const [lastMessageId, setLastMessageId] = useState(0)
    const [prevMessagesHeight, setPrevMessagesHeight] = useState(0)
    const refMessages = React.useRef<HTMLDivElement>(null)

    const searchParams = new URLSearchParams(search)
    const friendshipId = searchParams.get("friendship_id")
    let interval: NodeJS.Timer

    useIonViewWillEnter(() => {
        if (friendshipId !== null) {
            loadMessage()
            checkLastMessage()
            interval = setInterval(() => {
                // checkLastMessage()
            }, 2500)
        }
    })

    useIonViewWillLeave(() => {
        clearInterval(interval)
    })

    useEffect(() => {
        if (refMessages !== null && refMessages.current !== null) {
            refMessages.current.scrollTop = refMessages.current.scrollHeight - prevMessagesHeight
            setPrevMessagesHeight(refMessages.current.scrollHeight)
        }
    }, [messages])

    useEffect(() => {
        if (lastMessageId !== 0) {
            loadMessage()
        }
    }, [lastMessageId])

    function checkLastMessage() {
        if (friendshipId) {
            provider.last(parseInt(friendshipId)).then(response => {
                setLastMessageId(response.data.id)
            }).catch(error => {
                console.error(error)
            })
        }
    }

    function loadMessage() {
        if (friendshipId) {
            provider.show(parseInt(friendshipId), "").then(response => {
                setMyId(response.data.my_id)
                setPrevMessagesHeight(0)
                setMessages(response.data.messages)
                setFirstMessageId(response.data.first_message_id)
            }).catch(error => {
                console.error(error)
            })
        }
    }

    function sendMessage(event: any) {
        event.preventDefault()

        const data = {
            friendship_id: friendshipId,
            content: message
        }
        provider.send(data).then(() => {
            setMessage("")
            loadMessage()
        }).catch(error => {
            console.error(error)
        })
    }

    function messageOnScroll(event: any) {
        if (event.target.scrollTop === 0 && messages.length > 0 && messages[0].id !== firstMessageId && friendshipId) {
            provider.show(parseInt(friendshipId), messages[0].id.toString()).then(response => {
                setMessages([...response.data.messages, ...messages])
                setFirstMessageId(response.data.first_message_id)
            }).catch(error => {
                console.error(error)
            })
        }
    }

    return (
        <div className={"pb-16 h-full max-w-4xl mx-auto flex flex-col bg-base-100"}>
            <div ref={refMessages} onScroll={messageOnScroll}
                className={"w-full p-4 shadow flex flex-col gap-2 flex-1 overflow-y-auto"}>
                <div className={"hidden chat-start chat-end"}></div>
                {messages.map((message: MessageProps) => {
                    if (message.patient_id === myId) {
                        return (
                            <div className="chat chat-end" key={message.id}>
                                <div className="chat-image avatar">
                                    <div className="w-10 rounded-full">
                                        <img src={PatientProfile} alt={"patient"}/>
                                    </div>
                                </div>
                                <div className="chat-bubble bg-gray-600">{message.content}</div>
                                <div className="chat-footer opacity-50">
                                    {moment(message["created_at"]).format("DD MMM YYYY hh:mm:ss A")}
                                </div>
                            </div>
                        )
                    } else {
                        return (
                            <div className="chat chat-start" key={message.id}>
                                <div className="chat-image avatar">
                                    <div className="w-10 rounded-full">
                                        <img src={DoctorProfile} alt={"doctor"}/>
                                    </div>
                                </div>
                                <div className="chat-bubble bg-white text-black">{message.content}</div>
                                <div className="chat-footer opacity-50">
                                    {moment(message["created_at"]).format("DD MMM YYYY hh:mm:ss A")}
                                </div>
                            </div>
                        )
                    }
                })}
            </div>

            <form className={"w-full bg-gray-50 p-4 shadow flex gap-2 items-center"}
                  onSubmit={sendMessage}>
                <input type="text" className={"input input-bordered input-sm flex-1 rounded-full"}
                       placeholder={"Type message here"} value={message}
                       onChange={e => setMessage(e.target.value)}/>
                <button type={"submit"} className={"btn btn-primary btn-sm rounded-full"}>Send
                </button>
            </form>
        </div>
    )
}

export default Page

interface MessageProps {
    content: string,
    created_at: string,
    friendship_id: number,
    id: number,
    message_type: string,
    patient_id: number,
    status: string,
    updated_at: string,
}
