import React, {FormEvent} from "react"
import {friend} from "../../../providers/cloud.provider"

const Page: React.FC = () => {
    const refInput = React.useRef<HTMLInputElement>(null)
    const [errorMessage, setErrorMessage] = React.useState<string>("")

    function formSubmit(event: FormEvent<HTMLFormElement>) {
        event.preventDefault()

        const data = new FormData(event.currentTarget)
        friend.sendRequest(data).then(() => {
            alert("Friend request sent")
        }).catch(error => {
            console.error(error)
            const msg = error.response.data.errors[0]
            if (msg === "Receiver must exist") setErrorMessage("Could not found this patient")
            else setErrorMessage(msg)
        }).finally(() => {
            if (refInput.current) refInput.current.value = ""
        })
    }

    return (
        <form className={"w-full p-4"} onSubmit={formSubmit}>
            <div className="form-control w-full">
                <label className="label">
                    <span className="label-text">Friend's AF UID</span>
                </label>
                <input ref={refInput} name={"receiver_id"} type="text" required={true}
                       className="input input-sm input-bordered w-full"/>
            </div>
            <div className={"py-2 text-error"}>{errorMessage}</div>
            <div className={"flex justify-end"}>
                <button type={"submit"} className={"btn btn-sm btn-primary w-full"}>Send request</button>
            </div>
        </form>
    )
}

export default Page
