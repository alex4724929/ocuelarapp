import React from "react"
import {
    IonButtons,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar,
    useIonViewWillEnter
} from "@ionic/react"
import {StatusBar, Style} from "@capacitor/status-bar"

const Page: React.FC = () => {
    useIonViewWillEnter(() => {
        StatusBar.setBackgroundColor({color: "#FFFFFF"}).then()
        StatusBar.setStyle({style: Style.Light}).then()
    })

    return (
        <IonPage>
            <IonHeader className="ion-no-border">
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton/>
                    </IonButtons>
                    <IonTitle>Games</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen>
                <div className={"min-h-screen bg-black p-2"}>
                    <iframe allowFullScreen={true} src={"/games/blackjack21/index.html"} />
                </div>
            </IonContent>
        </IonPage>
    )
}

export default Page