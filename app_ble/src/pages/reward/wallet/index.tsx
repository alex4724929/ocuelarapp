import React from "react"
import {
    IonButtons,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonRefresher,
    IonRefresherContent,
    IonTitle,
    IonToolbar,
    RefresherEventDetail, useIonViewWillEnter
} from "@ionic/react"
import {StatusBar, Style} from "@capacitor/status-bar"

import blackjack from "../../../assets/images/games/blackjack.jpg"
import {ImArrowRight2} from "react-icons/im"

import "./index.css"


import {NavigationBar} from "@hugotomazi/capacitor-navigation-bar"

const statusBarColor: string = "#FFCA67"
const navigationBarColor: string = "#F3F4F6"
const statusBarStyle: Style = Style.Light

const Page: React.FC = () => {
    useIonViewWillEnter(() => {
        StatusBar.setBackgroundColor({color: statusBarColor}).then()
        StatusBar.setStyle({style: statusBarStyle}).then()
        NavigationBar.setColor({color: navigationBarColor, darkButtons: true}).then()
    })

    function reload(event: CustomEvent<RefresherEventDetail>) {
        event.detail.complete()
    }

    return (
        <IonPage>
            <IonHeader className="ion-no-border">
                <IonToolbar color={"warning"}>
                    <IonButtons slot="start">
                        <IonMenuButton/>
                    </IonButtons>
                    <IonTitle>Wallet</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen>
                <IonRefresher slot="fixed" onIonRefresh={reload}>
                    <IonRefresherContent/>
                </IonRefresher>

                <div className={"min-h-screen bg-gray-100 p-4"}>
                    <div className={"font-black"}>GIFT CARDS</div>
                    <div className={"mt-2 flex flex-col gap-4"}>
                        <div className={"bg-white shadow rounded"}>
                            <div className={"flex"}>
                                <div className={"p-4"}>
                                    <img className={"w-40"} src={blackjack} alt={"reward"}/>
                                </div>
                                <div className={"flex-1 flex flex-col justify-between"}>
                                    <div className={"pt-4"}>
                                        <div className={"font-bold text-2xl"}>Target</div>
                                        <div className={"font-semibold"}>
                                            <span className={"px-0.5 text-xl"}>$</span>
                                            <span className={"px-0.5 text-4xl"}>50</span>
                                        </div>
                                    </div>
                                    <div className={"flex justify-end"}>
                                        <button className={"btn btn-secondary w-14 rounded-tl-3xl rounded-br"}><ImArrowRight2 size={24}/></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={"bg-white shadow rounded"}>
                            <div className={"flex"}>
                                <div className={"p-4"}>
                                    <img className={"w-40"} src={blackjack} alt={"reward"}/>
                                </div>
                                <div className={"flex-1 flex flex-col justify-between"}>
                                    <div className={"pt-4"}>
                                        <div className={"font-bold text-2xl"}>Visa</div>
                                        <div className={"font-semibold"}>
                                            <span className={"px-0.5 text-xl"}>$</span>
                                            <span className={"px-0.5 text-4xl"}>100</span>
                                        </div>
                                    </div>
                                    <div className={"flex justify-end"}>
                                        <button className={"btn btn-secondary w-14 rounded-tl-3xl rounded-br"}><ImArrowRight2 size={24}/></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={"bg-white shadow rounded"}>
                            <div className={"flex"}>
                                <div className={"p-4"}>
                                    <img className={"w-40"} src={blackjack} alt={"reward"}/>
                                </div>
                                <div className={"flex-1 flex flex-col justify-between"}>
                                    <div className={"pt-4"}>
                                        <div className={"font-bold text-2xl"}>Amazon</div>
                                        <div className={"font-semibold"}>
                                            <span className={"px-0.5 text-xl"}>$</span>
                                            <span className={"px-0.5 text-4xl"}>50</span>
                                        </div>
                                    </div>
                                    <div className={"flex justify-end"}>
                                        <button className={"btn btn-secondary w-14 rounded-tl-3xl rounded-br"}><ImArrowRight2 size={24}/></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={"bg-white shadow rounded"}>
                            <div className={"flex"}>
                                <div className={"p-4"}>
                                    <img className={"w-40"} src={blackjack} alt={"reward"}/>
                                </div>
                                <div className={"flex-1 flex flex-col justify-between"}>
                                    <div className={"pt-4"}>
                                        <div className={"font-bold text-2xl"}>Target</div>
                                        <div className={"font-semibold"}>
                                            <span className={"px-0.5 text-xl"}>$</span>
                                            <span className={"px-0.5 text-4xl"}>50</span>
                                        </div>
                                    </div>
                                    <div className={"flex justify-end"}>
                                        <button className={"btn btn-secondary w-14 rounded-tl-3xl rounded-br"}><ImArrowRight2 size={24}/></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={"bg-white shadow rounded"}>
                            <div className={"flex"}>
                                <div className={"p-4"}>
                                    <img className={"w-40"} src={blackjack} alt={"reward"}/>
                                </div>
                                <div className={"flex-1 flex flex-col justify-between"}>
                                    <div className={"pt-4"}>
                                        <div className={"font-bold text-2xl"}>Visa</div>
                                        <div className={"font-semibold"}>
                                            <span className={"px-0.5 text-xl"}>$</span>
                                            <span className={"px-0.5 text-4xl"}>100</span>
                                        </div>
                                    </div>
                                    <div className={"flex justify-end"}>
                                        <button className={"btn btn-secondary w-14 rounded-tl-3xl rounded-br"}><ImArrowRight2 size={24}/></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </IonContent>
        </IonPage>
    )
}

export default Page
