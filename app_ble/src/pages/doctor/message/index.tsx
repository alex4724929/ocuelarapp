import React, {useEffect, useRef, useState} from "react"
import {
    IonButtons,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar,
    useIonViewWillEnter,
    useIonViewWillLeave
} from "@ionic/react"

import DoctorProfile from "../../../assets/images/profile.png"
import PatientProfile from "../../../assets/images/profile.png"
import moment from "moment"
import {doctorMessage as provider} from "../../../providers/cloud.provider"
import {StatusBar, Style} from "@capacitor/status-bar"
import {NavigationBar} from "@hugotomazi/capacitor-navigation-bar"

const statusBarColor: string = "#FFCA67"
const navigationBarColor: string = "#FFFFFF"
const statusBarStyle: Style = Style.Light

const Page: React.FC = () => {
    useIonViewWillEnter(() => {
        StatusBar.setBackgroundColor({color: statusBarColor}).then()
        StatusBar.setStyle({style: statusBarStyle}).then()
        NavigationBar.setColor({color: navigationBarColor, darkButtons: true}).then()
    })

    const [messages, setMessages] = useState<MessageProps[]>([])
    const [message, setMessage] = useState("")
    const [firstMessageId, setFirstMessageId] = useState(0)
    const [lastMessageId, setLastMessageId] = useState(0)
    const [prevMessagesHeight, setPrevMessagesHeight] = useState(0)
    const refMessages = useRef<HTMLDivElement>(null)
    let interval: NodeJS.Timer

    useIonViewWillEnter(() => {
        loadMessage()
        checkLastMessage()
        interval = setInterval(() => {
            checkLastMessage()
        }, 2500)
    })

    useIonViewWillLeave(() => {
        clearInterval(interval)
    })

    useEffect(() => {
        if (refMessages !== null && refMessages.current !== null) {
            refMessages.current.scrollTop = refMessages.current.scrollHeight - prevMessagesHeight
            setPrevMessagesHeight(refMessages.current.scrollHeight)
        }
    }, [messages])

    useEffect(() => {
        if (lastMessageId !== 0) {
            loadMessage()
        }
    }, [lastMessageId])

    function checkLastMessage() {
        provider.last().then(response => {
            setLastMessageId(response.data.id)
        }).catch(error => {
            console.error(error)
        })
    }

    function loadMessage() {
        provider.index("").then(response => {
            setPrevMessagesHeight(0)
            setMessages(response.data.messages)
            setFirstMessageId(response.data.first_message_id)
        }).catch(error => {
            console.error(error)
        })
    }

    function sendMessageToDoctor(event: any) {
        event.preventDefault()

        provider.create(message).then(() => {
            setMessage("")
            loadMessage()
        })
    }

    function messageOnScroll(event: any) {
        if (event.target.scrollTop === 0 && messages.length > 0 && messages[0].id !== firstMessageId) {
            provider.index(messages[0].id.toString()).then(response => {
                setMessages([...response.data.messages, ...messages])
                setFirstMessageId(response.data.first_message_id)
            }).catch(error => {
                console.error(error)
            })
        }
    }

    return (
        <IonPage>
            <IonHeader className="ion-no-border">
                <IonToolbar color={"warning"}>
                    <IonButtons slot="start">
                        <IonMenuButton/>
                    </IonButtons>
                    <IonTitle>Chat with Doctor</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen>
                <div className={"flex-1 h-full max-w-4xl mx-auto flex flex-col bg-gray-200"}>
                    <div ref={refMessages} onScroll={messageOnScroll}
                        className={"w-full p-4 shadow flex flex-col gap-2 flex-1 overflow-y-auto"}>
                        <div className={"hidden chat-start chat-end"}></div>
                        {messages.map((message: MessageProps) => {
                            if (!message["is_from_doctor"]) {
                                return (
                                    <div className="chat chat-end" key={message.id}>
                                        <div className="chat-image avatar">
                                            <div className="w-10 rounded-full">
                                                <img src={PatientProfile} alt={"patient"}/>
                                            </div>
                                        </div>
                                        <div className="chat-bubble bg-gray-600">{message.content}</div>
                                        <div className="chat-footer opacity-50">
                                            {moment(message["created_at"]).format("DD MMM YYYY hh:mm:ss A")}
                                        </div>
                                    </div>
                                )
                            } else {
                                return (
                                    <div className="chat chat-start" key={message.id}>
                                        <div className="chat-image avatar">
                                            <div className="w-10 rounded-full">
                                                <img src={DoctorProfile} alt={"doctor"}/>
                                            </div>
                                        </div>
                                        <div className="chat-bubble bg-white text-black">{message.content}</div>
                                        <div className="chat-footer opacity-50">
                                            {moment(message["created_at"]).format("DD MMM YYYY hh:mm:ss A")}
                                        </div>
                                    </div>
                                )
                            }
                        })}
                    </div>

                    <form className={"w-full bg-white p-4 shadow flex gap-2 items-center"}
                          onSubmit={sendMessageToDoctor}>
                        <input type="text" className={"input input-bordered input-sm flex-1"}
                               placeholder={"Type message here"} value={message}
                               onChange={e => setMessage(e.target.value)}/>
                        <button type={"submit"} className={"btn btn-primary btn-sm w-20"}>Send</button>
                    </form>
                </div>
            </IonContent>
        </IonPage>
    )
}

export default Page

interface MessageProps {
    id: number
    content: string
    is_from_doctor: boolean
    created_at: string
}