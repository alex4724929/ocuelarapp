import React from "react"
import {
    IonButtons,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonRefresher,
    IonRefresherContent,
    IonTitle,
    IonToolbar,
    RefresherEventDetail, useIonViewWillEnter
} from "@ionic/react"
import {IoExtensionPuzzle} from "react-icons/io5"
import {RiMoneyDollarCircleFill} from "react-icons/ri"

import blackjack from "../../assets/images/games/blackjack.jpg"
import {MdFavorite} from "react-icons/md"
import {StatusBar, Style} from "@capacitor/status-bar"
import {NavigationBar} from "@hugotomazi/capacitor-navigation-bar"

const statusBarColor: string = "#FFCA67"
const navigationBarColor: string = "#FFFFFF"
const statusBarStyle: Style = Style.Light

const Page: React.FC = () => {
    useIonViewWillEnter(() => {
        StatusBar.setBackgroundColor({color: statusBarColor}).then()
        StatusBar.setStyle({style: statusBarStyle}).then()
        NavigationBar.setColor({color: navigationBarColor, darkButtons: true}).then()
    })

    function reload(event: CustomEvent<RefresherEventDetail>) {
        event.detail.complete()
    }

    return (
        <IonPage>
            <IonHeader className="ion-no-border">
                <IonToolbar color={"warning"}>
                    <IonButtons slot="start">
                        <IonMenuButton/>
                    </IonButtons>
                    <IonTitle>Games</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen>
                <IonRefresher slot="fixed" onIonRefresh={reload}>
                    <IonRefresherContent/>
                </IonRefresher>
                <div className={"w-full text-center min-h-screen bg-gray-100"}>
                    <div className={"py-10"}>
                        <div className={"font-black text-xl text-gray-600"}>COIN BANK TOTAL</div>
                        <div className={"font-semibold text-5xl"}>123456</div>
                    </div>
                    <div className={"w-full py-2"}>
                        <div className={"flex gap-2 items-center justify-center"}>
                            <div className={"flex gap-2 w-28"}>
                                <div className={"font-bold text-xl"}>Games</div>
                                <IoExtensionPuzzle size={24}/>
                            </div>
                            <progress className={"progress progress-secondary rounded-full w-32 outline outline-secondary"} value={50} max={100}/>
                            <div className={"flex gap-2 w-28"}>
                                <RiMoneyDollarCircleFill size={28}/>
                                <div className={"font-bold text-xl"}>300</div>
                            </div>
                        </div>
                        <div className={"w-full text-center"}>
                            <span className={"px-0.5 font-bold text-xl"}>150</span>
                            <span className={"px-0.5 font-bold text-xl text-gray-600"}>Coins</span>
                        </div>
                    </div>

                    <div className={"p-4 flex flex-col gap-4"}>
                        <div className={"w-full bg-white shadow-xl rounded-xl"}>
                            <figure><img className={"rounded-t-xl"} src={blackjack} alt={"screenshot"}/></figure>
                            <div className={"p-2"}>
                                <div className={"flex items-center justify-between"}>
                                    <div>
                                        <div className={"text-left font-black text-2xl"}>Candy Crush</div>
                                        <div className={"text-left font-bold"}>300 Coins Total Earned</div>
                                    </div>
                                    <button className={"btn btn-sm bg-red-400 text-white h-10 w-10 rounded-full"}><MdFavorite size={24}/></button>
                                </div>
                                <button className={"btn btn-secondary w-32 rounded-full my-4"}>Play</button>
                            </div>
                        </div>
                        <div className={"w-full bg-white shadow-xl rounded-xl"}>
                            <figure><img className={"rounded-t-xl"} src={blackjack} alt={"screenshot"}/></figure>
                            <div className={"p-2"}>
                                <div className={"flex items-center justify-between"}>
                                    <div>
                                        <div className={"text-left font-black text-2xl"}>Candy Crush</div>
                                        <div className={"text-left font-bold"}>300 Coins Total Earned</div>
                                    </div>
                                    <button className={"btn btn-sm bg-red-400 text-white h-10 w-10 rounded-full"}><MdFavorite size={24}/></button>
                                </div>
                                <button className={"btn btn-secondary w-32 rounded-full my-4"}>Play</button>
                            </div>
                        </div>
                    </div>
                </div>
            </IonContent>
        </IonPage>
    )
}

export default Page
