import React, {useEffect} from "react"
import {IonContent, IonHeader, IonPage, IonRefresher, IonRefresherContent, IonTitle, IonToolbar, useIonViewWillEnter} from "@ionic/react"
import useToken from "../../../hooks/useToken"
import {BLE} from "@awesome-cordova-plugins/ble"
import {StatusBar, Style} from "@capacitor/status-bar"
import {NavigationBar} from "@hugotomazi/capacitor-navigation-bar"
import {useTranslation} from "react-i18next"
import {drop} from "../../../providers/cloud.provider"
import moment from "moment/moment"

const statusBarColor: string = "#FFFFFF"
const navigationBarColor: string = "#FFFFFF"
const statusBarStyle: Style = Style.Light

const Page: React.FC = () => {
    useIonViewWillEnter(() => {
        StatusBar.setBackgroundColor({color: statusBarColor}).then()
        StatusBar.setStyle({style: statusBarStyle}).then()
        NavigationBar.setColor({color: navigationBarColor, darkButtons: true}).then()

        setBtnDone("hidden")
        loadDevice()
    })

    const {t} = useTranslation()

    const {token, setToken} = useToken()
    const [myDevice1, setMyDevice1] = React.useState<OcuelarDevice | null>(null)
    const [myDevice2, setMyDevice2] = React.useState<OcuelarDevice | null>(null)
    const [myDevice3, setMyDevice3] = React.useState<OcuelarDevice | null>(null)
    const [btnDone, setBtnDone] = React.useState<string>("hidden")

    useEffect(() => {
        if (myDevice1?.sync_status !== "" && !myDevice1?.sync_status?.includes("Syncing") && !myDevice1?.sync_status?.includes("Searching") &&
            myDevice2?.sync_status !== "" && !myDevice2?.sync_status?.includes("Syncing") && !myDevice2?.sync_status?.includes("Searching") &&
            myDevice3?.sync_status !== "" && !myDevice3?.sync_status?.includes("Syncing") && !myDevice3?.sync_status?.includes("Searching")) {
            setBtnDone("")
        } else {
            setBtnDone("hidden")
        }
    }, [myDevice1, myDevice2, myDevice3])

    function refresh(event: CustomEvent) {
        updateStatus(myDevice1?.ocuelar_id || "", "Searching")
        updateStatus(myDevice2?.ocuelar_id || "", "Searching")
        updateStatus(myDevice3?.ocuelar_id || "", "Searching")

        setBtnDone("hidden")
        scanAndSyncBle()
        setTimeout(() => {
            event.detail.complete()
        }, 5000)
    }

    function loadDevice() {
        for (const [index, device] of token.devices.entries()) {
            device.sync_status = ""
            if (index === 0) setMyDevice1(device)
            if (index === 1) setMyDevice2(device)
            if (index === 2) setMyDevice3(device)
        }
    }

    function scanAndSyncBle() {
        const _myDevices = token.devices.map((d: OcuelarDevice) => d.ocuelar_id)
        const foundedDevices: string[] = []

        BLE.scan([], 5).subscribe(async (device) => {
            if (device.name === undefined) return

            if (device.name && device.name.startsWith("Ocuelar")) {
                if (_myDevices.includes(device.name)) {
                    syncDevice(device)
                    foundedDevices.push(device.name)
                }
            }
        })

        setTimeout(() => {
            for (const device of _myDevices) {
                if (!foundedDevices.includes(device)) {
                    updateStatus(device, "Not found")
                }
            }
            setBtnDone("")
        }, 5000)
    }

    function syncDevice(bleDevice: BleDevice) {
        BLE.connect(bleDevice.id).subscribe(async (device) => {
            updateStatus(bleDevice.name || "", "Connected")

            const services = device.services
            const uuid = services.filter((s: string) => s.length > 4)[0]

            const currentTimestamp = moment().unix().toString()
            // const currentTimestamp = "1689501211"
            // alert(currentTimestamp)
            await BLE.write(device.id, uuid, "2A08", new TextEncoder().encode(currentTimestamp).buffer)
            // await BLE.write(device.id, uuid, "2A08", new TextEncoder().encode("1689604732").buffer)
            // const deviceTimestamp = await BLE.read(device.id, uuid, "2A08")
            // const decodedTimestamp = new TextDecoder().decode(new Uint8Array(deviceTimestamp))
            // alert(decodedTimestamp)

            // await BLE.write(device.id, uuid, "3AB4", new TextEncoder().encode("-2").buffer)

            try {
                pageLoop: for (let i = 0; i < 25; i++) {
                    await BLE.write(device.id, uuid, "3AB4", new TextEncoder().encode(i.toString()).buffer)
                    updateStatus(bleDevice.name || "", `Syncing (${i + 1}/25)`)

                    const res = await BLE.read(device.id, uuid, "3AB4")
                    const decoded = new TextDecoder().decode(new Uint8Array(res))
                    const records: BleData[] = JSON.parse(decoded)

                    for (const record of records) {
                        if (record.timestamp === 0) break pageLoop

                        const data = {
                            ocuelar_id: bleDevice.name,
                            timestamp: record.timestamp,
                            data: record.data
                        }
                        await drop.upload(data)
                    }
                }

                await BLE.write(device.id, uuid, "3AB4", new TextEncoder().encode("-1").buffer)

                updateStatus(bleDevice.name || "", "Synced")
            } catch (error) {
                alert("write error (" + device.name + ") :" + error)
            } finally {
                try {
                    await BLE.disconnect(device.id)
                } catch (error) {
                    alert("error (" + device.name + ") :" + error)
                }
            }
        })
    }

    function nextAction() {
        if (token.control_group === "smartcap_gamification") {
            window.location.replace("/drops/daily")
        } else {
            setToken(null)
            window.location.replace("/login")
        }
    }

    function updateStatus(name: string, status: string) {
        if (name === myDevice1?.ocuelar_id && myDevice1 !== null) setMyDevice1({...myDevice1, sync_status: status})
        else if (name === myDevice2?.ocuelar_id && myDevice2 !== null) setMyDevice2({...myDevice2, sync_status: status})
        else if (name === myDevice3?.ocuelar_id && myDevice3 !== null) setMyDevice3({...myDevice3, sync_status: status})
    }

    return (
        <IonPage>
            <IonHeader className={"ion-no-border"}>
                <IonToolbar>
                    {/*<IonButtons slot={"start"}>*/}
                    {/*    <IonMenuButton/>*/}
                    {/*</IonButtons>*/}
                    <IonTitle>Sync My Ocuelar</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen>
                <IonRefresher slot={"fixed"} onIonRefresh={refresh}>
                    <IonRefresherContent/>
                </IonRefresher>

                <div className={"w-full p-4 flex flex-col gap-4"}>
                    <div className={"bg-primary/20 w-full p-4 rounded-3xl flex justify-between items-center"}>
                        <div className={"w-full flex justify-between items-center"}>
                            <div>
                                <div className={"w-full font-bold text-2xl"}>{myDevice1?.ocuelar_id}</div>
                                <div>{t(myDevice1?.ocuelar_type || "")}</div>
                            </div>
                            <div>
                                <div>{myDevice1?.sync_status}</div>
                            </div>
                        </div>
                    </div>
                    <div className={"bg-primary/20 w-full p-4 rounded-3xl flex justify-between items-center"}>
                        <div className={"w-full flex justify-between items-center"}>
                            <div>
                                <div className={"w-full font-bold text-2xl"}>{myDevice2?.ocuelar_id}</div>
                                <div>{t(myDevice2?.ocuelar_type || "")}</div>
                            </div>
                            <div>
                                <div>{myDevice2?.sync_status}</div>
                            </div>
                        </div>
                    </div>
                    <div className={"bg-primary/20 w-full p-4 rounded-3xl flex justify-between items-center"}>
                        <div className={"w-full flex justify-between items-center"}>
                            <div>
                                <div className={"w-full font-bold text-2xl"}>{myDevice3?.ocuelar_id}</div>
                                <div>{t(myDevice3?.ocuelar_type || "")}</div>
                            </div>
                            <div>
                                <div>{myDevice3?.sync_status}</div>
                            </div>
                        </div>
                    </div>

                    <div className={"w-full text-center text-2xl"}>
                        Pull to refresh
                    </div>
                    <div className={btnDone}>
                        <div className={"fixed bottom-10 w-full flex justify-center"}>
                            <button onClick={nextAction} className={"btn btn-primary rounded-full w-40 "}>Done</button>
                        </div>
                    </div>
                </div>
            </IonContent>
        </IonPage>
    )
}

interface BleDevice {
    name?: string
    id: string
    advertising: number[]
    rssi: number
}

interface OcuelarDevice {
    id: number,
    patient_id: number,
    ocuelar_id: string,
    ocuelar_type: string,
    created_at: string,
    updated_at: string,
    sync_status: string
}

interface BleData {
    timestamp: number,
    data: number
}

export default Page
