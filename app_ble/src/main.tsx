import React from "react"
import {createRoot} from "react-dom/client"
import moment from "moment-timezone"

import App from "./App"

import "./i18n"
import "./main.css"

moment.tz.setDefault("US/Pacific")

const container = document.getElementById("root")
const root = createRoot(container!)
root.render(<App/>)
