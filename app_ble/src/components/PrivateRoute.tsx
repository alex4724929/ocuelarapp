import useToken from "../hooks/useToken"
import {Redirect} from "react-router-dom"

export default function(props: {component: JSX.Element}) {
    const {token} = useToken()
    if (!token) {
        return (<Redirect to={"/login"}/>)
    } else {
        return props.component
    }
}
