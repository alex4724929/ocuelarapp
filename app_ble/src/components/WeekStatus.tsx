import React from "react"
import {TiTick, TiTimes} from "react-icons/ti"

const WeekStatus: React.FC<WeekStatusProps> = (props: WeekStatusProps) => {
    const WeekItem = (props: { text: String; status: Number }) => {
        return (
            <div className={"flex justify-center"}>
                <button className={"flex flex-col items-center gap-2 p-0 h-20 bg-white text-black"}>
                    <div className={"text-xl font-bold"}>{props.text}</div>
                    <WeekTick status={props.status}/>
                </button>
            </div>
        )
    }

    const WeekTick = (props: { status: Number }) => {
        if (props.status === 0) { // haven't
            return <div className={"bg-gray-200 rounded-full flex justify-center text-white w-6 h-6"}></div>
        } else if (props.status === 1) { // not full
            return <div className={"bg-gray-500 rounded-full flex justify-center text-white w-6 h-6"}><TiTimes
                size={24}/></div>
        } else if (props.status === 2) { // full
            return <div className={"bg-success rounded-full flex justify-center text-white w-6 h-6"}><TiTick size={24}/>
            </div>
        } else if (props.status === 3) { // today
            return <div className={"bg-orange-400 rounded-full flex justify-center text-white w-6 h-6"}></div>
        } else {
            return <div></div>
        }
    }

    return (
        <div className={"grid grid-cols-7 py-6"}>
            {props.weekly.map((week, index) => (
                <WeekItem key={index} text={week.text} status={week.status}/>))
            }
        </div>
    )
}
export default WeekStatus

interface WeekStatusProps {
    weekly: WeeklyProps[]
}

interface WeeklyProps {
    text: string,
    status: number
}
