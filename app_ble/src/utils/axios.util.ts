import axios from "axios"
import {refresh} from "../providers/oauth.provider"

export const axiosPrivate = axios.create({baseURL: import.meta.env.VITE_SERVER_URL})
export const axiosPublic = axios.create({baseURL: import.meta.env.VITE_SERVER_URL})

axiosPrivate.interceptors.request.use(
    (config) => {
        const token = JSON.parse(localStorage.getItem("token") || "{}")?.access_token
        config.headers["Authorization"] = `Bearer ${token}`
        config.headers["Accept"] = "application/json"
        return config
    },
    error => {
        return Promise.reject(error)
    }
)

axiosPrivate.interceptors.response.use(
    (response) => {
        return response
    }, async (error) => {
        const originalRequest = error.config
        if (error.response.status === 401) {
            const refreshToken = JSON.parse(localStorage.getItem("token") || "{}")?.refresh_token
            const refreshResult = await refresh(refreshToken)
            if (refreshResult.status === 200) {
                localStorage.setItem("token", JSON.stringify(refreshResult.data))
            } else {
                localStorage.removeItem("token")
            }
            return axiosPrivate(originalRequest)
        }
        return Promise.reject(error)
    }
)
