import React from "react"
import {IonContent, IonPage, useIonViewWillEnter} from "@ionic/react"

import "./index.css"
import {StatusBar, Style} from "@capacitor/status-bar"

const Page: React.FC = () => {

    useIonViewWillEnter(() => {
        StatusBar.setBackgroundColor({color: "#F3F3F3"}).then()
        StatusBar.setStyle({style: Style.Light}).then()
    })

    return (
        <IonPage>
            <IonContent fullscreen>
                <div className={"w-full h-full flex items-center my-bg overflow-hidden"}>
                    <iframe className={"w-[100%] h-[75%] bg-black"} allowFullScreen={true} src={"/games/sudoku/index.html"}></iframe>
                </div>
            </IonContent>
        </IonPage>
    )
}

export default Page
