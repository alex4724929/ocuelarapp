export const modelStyle = (isOpen: boolean): string => {
    if (isOpen) return "modal modal-open"
    return "modal"
}
