import {useState} from "react"

export default function useToken() {

    function getToken() {
        if (localStorage.getItem("token")) {
            return JSON.parse(localStorage.getItem("token") || "{}")
        } else {
            return null
        }
    }

    const [token, setToken] = useState(getToken())

    const saveToken = (userToken?: JSON | null) => {
        if (userToken === null) localStorage.removeItem("token")

        localStorage.setItem("token", JSON.stringify(userToken))
        setToken(userToken)
    }

    return {
        setToken: saveToken,
        token
    }
}
