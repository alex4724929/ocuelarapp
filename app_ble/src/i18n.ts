import i18n from "i18next"
import {initReactI18next} from "react-i18next"

import en from "./lang/en.json"

const resources = {
    en: {translation: en}
}

i18n.use(initReactI18next).init({
    resources,
    fallbackLng: "en",
    lng: "en",
    interpolation: {
        escapeValue: false
    }
}).then()

export default i18n
